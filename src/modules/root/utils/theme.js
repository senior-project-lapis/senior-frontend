import { extendTheme } from '@chakra-ui/react';

const theme = extendTheme({
  config: { initialColorMode: 'light', useSystemColorMode: false },
  fonts: {
    heading: 'Montserrat Bold',
    subHeading: 'Montserrat SemiBold',
    medium: 'Montserrat Medium',
    body: 'Montserrat Normal',
    logo: 'Nunito',
  },
  colors: {
    blue: {
      100: '#1D70A2',
      200: '#2892D7',
      300: '#5680E9',
    },
    orange: {
      100: '#EF8354',
      200: '#F9603F',
      300: '#F9A03F',
    },
  },
  styles: {
    global: {
      body: {
        minHeight: '100vh',
        bg: '#FFFFFF',
        color: '#000000',
        _placeholder: {
          color: '#000000',
        },
      },
      h2: {
        color: '#000000',
      },
      '.tox.tox-tinymce-aux': {
        zIndex: '4000',
        position: 'relative !important',
      },
    },
  },
  components: {
    Link: {
      baseStyle: {
        fontFamily: 'medium',
        color: 'whiteAlpha.900',
        textDecoration: 'none',
        _hover: {
          textDecoration: 'none',
        },
        _focus: {
          boxShadow: 'none',
          outline: 'none',
        },
      },
    },
    Input: {
      baseStyle: {
        color: 'black.500',
        _placeholder: {
          color: 'black.900',
        },
      },
    },
    Heading: {
      baseStyle: {
        color: 'black.500',
      },
    },
    Button: {
      baseStyle: {
        _hover: {
          bg: 'gray.400',
          boxShadow: 'none',
          outline: 'none',
        },
        _focus: {
          // bg: 'gray.300',
          outline: 'none',
          boxShadow: 'none',
        },
        _active: {
          // bg: 'gray.300',
          outline: 'none',
          boxShadow: 'none',
        },
      },
      variants: {
        orange: {
          fontFamily: 'heading',
          bg: 'orange.100',
          color: 'whiteAlpha.900',
          border: 'none',
          outline: 'none',
          boxShadow: 'none',
          // _focus: {
          //   bg: 'gray.500',
          //   outline: 'none',
          //   boxShadow: 'none',
          // },
        },
        orange2: {
          fontFamily: 'heading',
          bg: 'orange.200',
          color: 'whiteAlpha.900',
          border: 'none',
          outline: 'none',
          boxShadow: 'none',
          // _focus: {
          //   bg: 'gray.500',
          //   outline: 'none',
          //   boxShadow: 'none',
          // },
        },
        blue: {
          fontFamily: 'heading',
          bg: 'blue.200',
          color: 'whiteAlpha.900',
          border: 'none',
          outline: 'none',
          boxShadow: 'none',
          // _hover: {
          //   bg: 'gray.500',
          //   outline: 'none',
          //   boxShadow: 'none',
          // },
        },
        blue2: {
          fontFamily: 'heading',
          bg: 'blue.100',
          color: 'whiteAlpha.900',
          border: 'none',
          outline: 'none',
          boxShadow: 'none',
          // _hover: {
          //   bg: 'gray.500',
          //   outline: 'none',
          //   boxShadow: 'none',
          // },
        },
        red: {
          fontFamily: 'heading',
          bg: 'red.400',
          color: 'whiteAlpha.900',
          border: 'none',
          outline: 'none',
          boxShadow: 'none',
          // _focus: {
          //   bg: 'gray.500',
          //   outline: 'none',
          //   boxShadow: 'none',
          // },
        },
        green: {
          fontFamily: 'heading',
          bg: 'green.400',
          color: 'whiteAlpha.900',
          border: 'none',
          outline: 'none',
          boxShadow: 'none',
          // _focus: {
          //   bg: 'gray.500',
          //   outline: 'none',
          //   boxShadow: 'none',
          // },
        },
      },
    },
  },
});

export default theme;
