/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState, useEffect } from 'react';

import { ApolloProvider } from '@apollo/client';
import { ColorModeScript } from '@chakra-ui/color-mode';
import { ChakraProvider } from '@chakra-ui/react';
import type { AppProps } from 'next/app';
import { useRouter } from 'next/router';
import { Provider } from 'react-redux';

import Loader from 'common/components/Loader';
import { client } from 'common/services/client';
import { store } from 'common/stores';

import Fonts from 'modules/root/utils/fonts';
import theme from 'modules/root/utils/theme';

const App = ({ Component, pageProps }: AppProps): JSX.Element => {
  const router = useRouter();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const handleStart = (url: string, { shallow }: any) => {
      if (url || shallow) {
        // console.log('handleStart', url, shallow);
        setLoading(true);
      }
    };
    const handleStop = (url: string, { shallow }: any) => {
      if (url || shallow) {
        // console.log('handleStop', url, shallow);
        setLoading(false);
      }
    };

    router.events.on('routeChangeStart', handleStart);
    router.events.on('routeChangeComplete', handleStop);
    router.events.on('routeChangeError', handleStop);

    return () => {
      router.events.off('routeChangeStart', handleStart);
      router.events.off('routeChangeComplete', handleStop);
      router.events.off('routeChangeError', handleStop);
    };
  }, [router]);

  return (
    <Provider store={store}>
      <ChakraProvider theme={theme}>
        <ApolloProvider client={client}>
          <ColorModeScript initialColorMode={theme.config.initialColorMode} />
          <Fonts />
          {loading && <Loader />}
          <Component {...pageProps} />
        </ApolloProvider>
      </ChakraProvider>
    </Provider>
  );
};

export default App;
