import { MenuType } from 'common/constants/menu/types';
export const APP_NAVBAR_CONSTANT: MenuType[] = [
  {
    key: '1',
    label: 'Jobs',
    link: ['/jobs'],
  },
  {
    key: '2',
    label: 'Companies',
    link: ['/companies'],
  },
  {
    key: '3',
    label: 'Forums',
    link: ['/forums'],
  },
  {
    key: '4',
    label: 'Job Trends',
    link: ['/jobs/trends'],
  },
];
