import React from 'react';

import { HStack, Link } from '@chakra-ui/react';
import NextLink from 'next/link';

import pathJoin from 'common/utils/path-join';

import { APP_NAVBAR_CONSTANT } from './constants';

const NavbarItems: React.FC = () => {
  return (
    <HStack spacing="40px" fontSize="16px" mb="5px">
      {APP_NAVBAR_CONSTANT.map(({ key, label, link }) => {
        return (
          <NextLink key={key} href={pathJoin(link)} passHref>
            <Link _focus={{ outline: 'none', boxShadow: 'none', border: 'none' }}>
              {label}
            </Link>
          </NextLink>
        );
      })}
    </HStack>
  );
};

export default NavbarItems;
