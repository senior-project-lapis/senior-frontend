import React from 'react';

import { MenuList, MenuItem, Link } from '@chakra-ui/react';
import NextLink from 'next/link';

import pathJoin from 'common/utils/path-join';

import { APP_MENU_CONSTANT } from './constants';

const NavbarMenuItems: React.FC = () => {
  return (
    <MenuList
      px="20px"
      py="20px"
      color="whiteAlpha.900"
      bg="blue.100"
      boxShadow="none"
      border="none"
      fontSize="16px"
      fontFamily={'medium'}
      w="222px">
      {APP_MENU_CONSTANT.map(({ key, label, link }) => {
        return (
          <NextLink key={key} href={pathJoin(link)} passHref>
            <MenuItem
              as={Link}
              borderRadius="5px"
              borderColor="none"
              _hover={{ outline: 'none', boxShadow: 'none' }}
              _active={{ bg: 'blackAlpha.400', outline: 'none', boxShadow: 'none' }}
              _focus={{ bg: 'blackAlpha.400', boxShadow: 'none' }}
              fontSize="16px">
              {label}
            </MenuItem>
          </NextLink>
        );
      })}
      <NextLink href={'/logout'} passHref>
        <MenuItem
          mt="10px"
          mb="5px"
          as={Link}
          borderRadius="5px"
          borderColor="none"
          _active={{ bg: 'blackAlpha.400', outline: 'none', boxShadow: 'none' }}
          _hover={{ outline: 'none', boxShadow: 'none' }}
          _focus={{ bg: 'blackAlpha.400', boxShadow: 'none' }}
          fontSize="18px">
          Logout
        </MenuItem>
      </NextLink>
    </MenuList>
  );
};

export default NavbarMenuItems;
