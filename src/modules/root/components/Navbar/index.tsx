/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from 'react';

import {
  Flex,
  Heading,
  Link,
  HStack,
  Text,
  Menu,
  MenuButton,
  useDisclosure,
} from '@chakra-ui/react';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import { FiUser } from 'react-icons/fi';
import { useSelector } from 'react-redux';

import Container from 'common/components/Container';
import { StoresState } from 'common/stores';

import Login from 'modules/auth/page/Login';
import PublisherRegister from 'modules/auth/page/PublisherRegister';
import UserRegister from 'modules/auth/page/UserRegister';

import NavbarMenuItems from './MenuItems';
import NavbarItems from './NavbarItems';

const Navbar: React.FC = () => {
  const router = useRouter();
  const {
    isOpen: isOpenUserRegister,
    onClose: onCloseUserRegister,
    onOpen: onOpenUserRegister,
  } = useDisclosure();
  const {
    isOpen: isOpenLogin,
    onClose: onCloseLogin,
    onOpen: onOpenLogin,
  } = useDisclosure();
  const {
    isOpen: isOpenPublisherRegister,
    onClose: onClosePublisherRegister,
    onOpen: onOpenPublisherRegister,
  } = useDisclosure();

  const auth = useSelector((state: StoresState) => state.user.authenticated);
  const [isLogin, setIsLogin] = useState<boolean>(true);
  const [isPublisher, setIsPublisher] = useState<boolean>(false);
  const [username, setUsername] = useState('');

  const handlePublisher = () => {
    if (isPublisher) {
      router.push('/publisher', undefined, { shallow: true });
    } else {
      onOpenPublisherRegister();
    }
  };

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('user') || 'null');
    auth || user ? setIsLogin(true) : setIsLogin(false);
    const publisher = JSON.parse(localStorage.getItem('publisher') || 'null');
    publisher ? setIsPublisher(true) : setIsPublisher(false);
    const firstname = user ? user.firstname : '';
    const lastname = user ? user.lastname : '';
    const fullname = firstname + ' ' + lastname.charAt(0) + '.';
    setUsername(fullname);
  }, []);

  return (
    <>
      <Flex
        top="0"
        py="20px"
        h="75px"
        width="full"
        bg="blue.100"
        position="fixed"
        zIndex="10">
        <Container>
          <Flex alignItems="flex-end" justifyContent="space-between" width="full">
            <Flex alignItems="flex-end">
              <Heading color="whiteAlpha.900" mr="60px" fontSize={'30px'}>
                <NextLink href={'/'} passHref>
                  <Link
                    fontFamily="logo"
                    _focus={{ outline: 'none', boxShadow: 'none', border: 'none' }}>
                    Lapis
                  </Link>
                </NextLink>
              </Heading>
              <NavbarItems />
            </Flex>

            <Flex alignItems="flex-end">
              {isLogin ? (
                <Flex mb="5px" mr="11px" fontSize="16px">
                  <Menu>
                    <MenuButton
                      color={'#ffffff'}
                      fontFamily="medium"
                      _focus={{ outline: 'none', boxShadow: 'none', border: 'none' }}>
                      <Flex alignItems={'center'}>
                        <FiUser />
                        <Text ml="5px">{username}</Text>
                      </Flex>
                    </MenuButton>
                    <NavbarMenuItems />
                  </Menu>
                </Flex>
              ) : (
                <HStack spacing="12px" fontSize="14px" mr="11px" mb="5px">
                  <Link
                    onClick={onOpenUserRegister}
                    as="button"
                    _focus={{ outline: 'none', boxShadow: 'none', border: 'none' }}>
                    Sign Up
                  </Link>
                  <Link
                    onClick={onOpenLogin}
                    as="button"
                    _focus={{ outline: 'none', boxShadow: 'none', border: 'none' }}>
                    Sign In
                  </Link>
                  <Text color="whiteAlpha.900" mr="11px" fontSize="18px" mb="3px">
                    |
                  </Text>
                  <Link
                    _focus={{ outline: 'none', boxShadow: 'none', border: 'none' }}
                    onClick={handlePublisher}
                    as="button"
                    fontSize="16px"
                    mb="3px">
                    Publisher
                  </Link>
                </HStack>
              )}
            </Flex>
          </Flex>
        </Container>
      </Flex>
      <Login
        isOpenLogin={isOpenLogin}
        onCloseLogin={onCloseLogin}
        onOpenUserRegister={onOpenUserRegister}
      />
      <UserRegister
        isOpenUserRegister={isOpenUserRegister}
        onCloseUserRegister={onCloseUserRegister}
        onOpenLogin={onOpenLogin}
        onOpenUserRegister={onOpenUserRegister}
      />
      <PublisherRegister
        isOpenPublisherRegister={isOpenPublisherRegister}
        onClosePublisherRegister={onClosePublisherRegister}
      />
    </>
  );
};

export default Navbar;
