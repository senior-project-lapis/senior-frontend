import styled from '@emotion/styled';

export const ContactContainer = styled.div`
  margin: auto;
  width: 100%;
  display: flex;
  justify-content: space-between;
  margin-top: 55px;
  font-family: 'Montserrat Normal';
  color: #fff;
`;

export const ContactHeader = styled.div`
  font-size: 16px;
  font-weight: 700;
  color: #fff;
  font-family: 'Montserrat Bold';
`;

export const ContactContent = styled.div`
  font-size: 16px;
`;

export const HrLine = styled.hr`
  height: 0px;
  border: 1px solid #d0d0d0;
  width: 1024px;
  margin-top: -25px;
`;

export const CopyRight = styled.div`
  font-size: 14px;
  color: #fff;
  font-family: 'Montserrat Normal';
`;

export const CopyRightContainer = styled.div`
  display: flex;
  width: 100%;
  margin-top: -45px;
`;
