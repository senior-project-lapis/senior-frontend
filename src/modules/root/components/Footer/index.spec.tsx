import * as React from 'react';

import { shallow } from 'enzyme';

import Footer from '.';

describe('Footer component', () => {
  it('Footer component should be defined', () => {
    const wrap = shallow(<Footer />);
    expect(wrap.exists()).toBe(true);
  });
});
