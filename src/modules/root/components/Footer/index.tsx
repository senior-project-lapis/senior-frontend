import React from 'react';

import { Flex } from '@chakra-ui/react';

import Container from 'common/components/Container';

import {
  ContactContainer,
  ContactContent,
  ContactHeader,
  CopyRight,
  CopyRightContainer,
  HrLine,
} from './styled';

interface FooterProps {
  color?: string;
}

const Footer: React.FC<FooterProps> = (props) => {
  const { color = 'blue.100' } = props;
  return (
    <Flex h="210px" width="full" px="50px" bg={color}>
      <Container>
        <ContactContainer>
          <ContactHeader>
            COMPUTER ENGINEERING <br />
            DEPARTMENT
          </ContactHeader>
          <ContactContent>
            126 Pracha Uthit Rd., Bang Mod, Thung Khru,
            <br /> Bangkok 10140, Thailand
          </ContactContent>
        </ContactContainer>
        <HrLine />
        <CopyRightContainer>
          <CopyRight>
            Copyright © 2021 - 61070501003 / 61070501057 / 61070501067
          </CopyRight>
        </CopyRightContainer>
      </Container>
    </Flex>
  );
};

export default Footer;
