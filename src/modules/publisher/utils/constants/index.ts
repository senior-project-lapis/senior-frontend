import { MenuType } from 'common/constants/menu/types';
export const PUBLISHER_MENU_CONSTANT: MenuType[] = [
  {
    key: '1',
    label: 'Add job',
    link: ['/publisher/add-job'],
  },
  {
    key: '2',
    label: 'All job in company',
    link: ['/publisher/all-job'],
  },
];
