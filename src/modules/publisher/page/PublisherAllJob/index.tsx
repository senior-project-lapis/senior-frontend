import React from 'react';

import { Flex, Heading, Divider } from '@chakra-ui/react';
import Head from 'next/head';

import SidebarLayout from 'common/components/SidebarLayout';

import AllJobInCompany from 'modules/publisher/components/AllJobInCompany';
import { PUBLISHER_MENU_CONSTANT } from 'modules/publisher/utils/constants';
import Footer from 'modules/root/components/Footer';

const PublisherAllJob: React.FC = () => {
  return (
    <>
      <Head>
        <title>Lapis | Publisher all job</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <SidebarLayout
        constant={PUBLISHER_MENU_CONSTANT}
        title="PUBLISHER"
        btnColor="orange.300"
        navColor="orange.300"
        sidebarColor="blue.200">
        <Flex minHeight="100vh" w="100%" flexDirection="column">
          <Flex mt="20px" w="100%" flexDirection="column">
            <Heading fontSize="20px">All jobs in your company</Heading>
            <Divider
              mt="10px"
              border="1px solid rgba(0, 0, 0, 0.6)"
              width="100%"
              borderColor="gray.700"
            />
          </Flex>
          <AllJobInCompany />
        </Flex>
      </SidebarLayout>
      <Footer color="orange.300" />
    </>
  );
};

export default PublisherAllJob;
