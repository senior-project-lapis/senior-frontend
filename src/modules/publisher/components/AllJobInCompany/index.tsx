/* eslint-disable react/no-unescaped-entities */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useMemo, useCallback, useState, useEffect, useRef } from 'react';

import {
  Flex,
  Box,
  Button,
  useDisclosure,
  useToast,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
} from '@chakra-ui/react';
import moment from 'moment';
import { BiDetail, BiTrash, BiEdit } from 'react-icons/bi';

import TableComponent from 'common/components/Table/TableComponent';
import {
  useGetJobByCompanyIdLazyQuery,
  useRemoveJobMutation,
} from 'common/generated/generated-types';

import EditJobDetailsModal from 'modules/publisher/components/EditJobDetailsModal';
import JobDetailsModal from 'modules/publisher/components/JobDetailsModal';

interface DialogProps {
  isOpen: boolean;
  onClose: () => void;
  job: any;
  handleRemoveJob: (id: string) => void;
}

const AllJobInCompany: React.FC = () => {
  const toast = useToast();
  const [job, setJob] = useState();
  const [companyId, setCompanyId] = useState('');
  const [pagination, setPagination] = React.useState({
    page: 0,
    rowsPerPage: 10,
  });

  const {
    isOpen: isOpenJobDetails,
    onClose: onCloseJobDetails,
    onOpen: onOpenJobDetails,
  } = useDisclosure();

  const {
    isOpen: isOpenEditJobDetails,
    onClose: onCloseEditJobDetails,
    onOpen: onOpenEditJobDetails,
  } = useDisclosure();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const cancelRef = useRef();

  const [loadData, { data, loading, refetch }] = useGetJobByCompanyIdLazyQuery({
    fetchPolicy: 'no-cache',
  });

  const [
    RemoveJobMutation,
    { data: removeJobData, loading: removeJobLoading, error: removeJobError },
  ] = useRemoveJobMutation({
    errorPolicy: 'all',
  });

  useEffect(() => {
    const publisher = JSON.parse(localStorage.getItem('publisher') || 'null');
    const publisherCompanyId = publisher ? publisher.companyId : '';
    setCompanyId(publisherCompanyId);
  }, []);

  const columns = useMemo(
    () => [
      {
        Header: 'Job Name',
        accessor: 'name',
      },
      {
        Header: 'Created At',
        accessor: (row: any) => {
          return moment(parseInt(row.createAt, 10)).local().format('LLL');
        },
      },
      {
        Header: 'Updated At',
        accessor: (row: any) => {
          return moment(parseInt(row.updateAt, 10)).local().format('LLL');
        },
      },
      {
        Header: 'Action',
        accessor: 'action',
        disableSortBy: true,
        Cell: (props: any) => (
          <>
            <Flex w="130px" justifyContent="space-between">
              <Button
                fontSize="25px"
                onClick={() => handleShowDetails(props)}
                variant="orange"
                size="sm"
                mr="5px">
                <BiDetail />
              </Button>
              <Button
                fontSize="25px"
                onClick={() => handleEditJobDetails(props)}
                variant="blue"
                size="sm"
                mr="5px">
                <BiEdit />
              </Button>
              <Button
                fontSize="25px"
                onClick={() => handleDialogRemove(props)}
                variant="red"
                size="sm">
                <BiTrash />
              </Button>
            </Flex>
          </>
        ),
      },
    ],
    [],
  );

  const handleShowDetails = (cell: any) => {
    const jobDetails = cell?.row?.original;
    setJob(jobDetails);
    onOpenJobDetails();
  };

  const handleEditJobDetails = (cell: any) => {
    const jobDetails = cell?.row?.original;
    setJob(jobDetails);
    onOpenEditJobDetails();
  };

  const DialogRemove: React.FC<DialogProps> = (DialogProps) => {
    const { isOpen, onClose, job, handleRemoveJob } = DialogProps;
    return (
      <>
        <AlertDialog
          motionPreset="slideInBottom"
          leastDestructiveRef={cancelRef}
          onClose={onClose}
          isOpen={isOpen}
          isCentered>
          <AlertDialogOverlay>
            <AlertDialogContent>
              <AlertDialogHeader fontSize="lg" fontWeight="bold">
                Delete {job?.name}
              </AlertDialogHeader>

              <AlertDialogBody>
                Are you sure? You can't undo this action afterwards.
              </AlertDialogBody>

              <AlertDialogFooter>
                <Button ref={cancelRef} onClick={onClose}>
                  Cancel
                </Button>
                <Button
                  colorScheme="red"
                  onClick={() => handleRemoveJob(job?._id)}
                  ml={3}>
                  Delete
                </Button>
              </AlertDialogFooter>
            </AlertDialogContent>
          </AlertDialogOverlay>
        </AlertDialog>
      </>
    );
  };

  const handleDialogRemove = (cell: any) => {
    const jobDetails = cell?.row?.original;
    setJob(jobDetails);
    onOpen();
  };

  useEffect(() => {
    if (!removeJobError && removeJobData) {
      refetch();
      toast({
        title: 'Remove successfully.',
        description: "We've remove your job.",
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
    } else {
      if (removeJobError) {
        toast({
          title: 'Error',
          description: removeJobError.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }
    }
  }, [removeJobData, removeJobLoading, removeJobError]);

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const handleRemoveJob = (id: string) => {
    sleep(500)
      .then(() => {
        RemoveJobMutation({
          variables: {
            id: id,
          },
        });
      })
      .then(() => {
        onClose();
      });
  };

  useEffect(() => {
    loadData({
      variables: {
        skip: pagination.rowsPerPage * pagination.page,
        limit: pagination.rowsPerPage,
        companyId: companyId,
        sortBy: 'createAt',
        sortOrder: 'desc',
      },
    });
  }, [pagination]);

  const fetchData = useCallback(({ pageSize, pageIndex }) => {
    setPagination({ page: pageIndex, rowsPerPage: pageSize });
  }, []);

  const totalCount = data ? data.Jobs.totalCount : 0;
  const jobData = data ? data.Jobs.jobResult : [];
  const allData = useMemo(() => jobData, [jobData]);
  return (
    <Box
      borderRadius="5px"
      mt="10px"
      boxShadow=" rgba(0, 0, 0, 0.02) 0px 1px 3px 0px, rgba(27, 31, 35, 0.15) 0px 0px 0px 1px">
      <TableComponent
        columns={columns}
        data={allData}
        fetchData={fetchData}
        totalCount={totalCount}
        loading={loading}
        initialPageIndex={pagination.page}
        initialPageSize={pagination.rowsPerPage}
        isPagination
      />
      <JobDetailsModal
        isOpenJobDetails={isOpenJobDetails}
        onCloseJobDetails={onCloseJobDetails}
        jobData={job}
      />
      <EditJobDetailsModal
        refetch={refetch}
        isOpenEditJobDetails={isOpenEditJobDetails}
        onCloseEditJobDetails={onCloseEditJobDetails}
        jobData={job}
      />
      <DialogRemove
        isOpen={isOpen}
        onClose={onClose}
        job={job}
        handleRemoveJob={handleRemoveJob}
      />
    </Box>
  );
};

export default AllJobInCompany;
