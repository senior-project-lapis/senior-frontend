/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  Text,
  Flex,
  Divider,
  Heading,
  ModalBody,
  SimpleGrid,
  Button,
} from '@chakra-ui/react';

import { JobWithId } from 'common/generated/generated-types';

interface Props {
  isOpenJobDetails: boolean;
  onCloseJobDetails: () => void;
  jobData: JobWithId;
}

const getEducationLevel = (education: string) => {
  let letter;
  switch (true) {
    case education === 'bachelor':
      letter = "Bachelor's Degree";
      break;
    case education === 'master':
      letter = "Master's Degree";
      break;
    case education === 'doctoral':
      letter = 'Doctoral Degree';
      break;
    case education === 'associate':
      letter = "Associate's Degree";
      break;
    case education === 'highShool':
      letter = 'High School or Equivalent';
      break;
    case education === 'lessThanHighSchool':
      letter = 'Less Than High School';
      break;
  }
  return letter;
};

const CapitalizeFirstLetter = (string: string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

const JobDetailsModal: React.FC<Props> = (Props) => {
  const { isOpenJobDetails, onCloseJobDetails, jobData } = Props;
  return (
    <Modal isOpen={isOpenJobDetails} onClose={onCloseJobDetails}>
      <ModalOverlay />
      <ModalContent
        bg="#F5F5F5"
        color="black"
        px="20px"
        py="20px"
        maxW="900px"
        borderBlock="none">
        <ModalCloseButton mx="20px" my="20px" fontSize="18px" />
        <ModalBody>
          <Flex overflowX="hidden" flexDirection="column" p="15px" m="5px" w="100%">
            <Flex justifyContent="space-between">
              <Heading p="10px" fontSize="20px">
                {jobData?.name}
              </Heading>
            </Flex>
            <Flex ml="40px" flexDirection="column">
              <Flex justifyContent="flex-start" flexDirection="column">
                <Flex fontSize="16px" p="5px">
                  <Text fontSize="16px" fontFamily="medium">
                    Company:
                  </Text>
                  &nbsp;
                  {jobData?.companyName}&nbsp;&nbsp;
                </Flex>
                {jobData?.locations.length > 2 ? (
                  <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                    <Text fontSize="16px" fontFamily="medium">
                      Locations:
                    </Text>
                    <SimpleGrid
                      columns={4}
                      spacing={1}
                      m="10px"
                      justifyItems="left"
                      alignItems="center">
                      {jobData?.locations.map((item: any, index) => (
                        <Text fontSize="16px" display="flex" key={index}>
                          {CapitalizeFirstLetter(item)}
                        </Text>
                      ))}
                    </SimpleGrid>
                  </Flex>
                ) : (
                  <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                    <Text fontSize="16px" fontFamily="medium">
                      Location:
                    </Text>
                    <Text fontSize="16px" display="flex">
                      &nbsp;&nbsp;
                      {jobData?.locations.map(
                        (item: any) => CapitalizeFirstLetter(item) + ' / ',
                      )}
                    </Text>
                  </Flex>
                )}
              </Flex>
              {jobData?.categories ? (
                jobData?.categories.length > 1 ? (
                  <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                    <Text fontSize="16px" fontFamily="medium">
                      Categories:
                    </Text>
                    <SimpleGrid
                      columns={2}
                      spacing={2}
                      mt="10px"
                      justifyItems="center"
                      alignItems="center">
                      {jobData?.categories.map((item: any, index) => (
                        <Text fontSize="16px" display="flex" key={index}>
                          {CapitalizeFirstLetter(item)}
                        </Text>
                      ))}
                    </SimpleGrid>
                  </Flex>
                ) : (
                  <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                    <Text fontSize="16px" fontFamily="medium">
                      Categories:
                    </Text>
                    <Text fontSize="16px" display="flex">
                      &nbsp;&nbsp;
                      {jobData?.categories.map((item: any) =>
                        CapitalizeFirstLetter(item),
                      )}
                    </Text>
                  </Flex>
                )
              ) : null}
              {jobData?.experienceLevels ? (
                jobData?.experienceLevels.length > 1 ? (
                  <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                    <Text fontSize="16px" fontFamily="medium">
                      Experience Levels:
                    </Text>
                    <SimpleGrid
                      columns={2}
                      spacing={2}
                      mt="10px"
                      justifyItems="center"
                      alignItems="center">
                      {jobData?.experienceLevels.map((item: any, index) => (
                        <Text fontSize="16px" display="flex" key={index}>
                          {CapitalizeFirstLetter(item)}
                        </Text>
                      ))}
                    </SimpleGrid>
                  </Flex>
                ) : (
                  <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                    <Text fontSize="16px" fontFamily="medium">
                      Experience Levels:
                    </Text>
                    <Text fontSize="16px" display="flex">
                      &nbsp;&nbsp;
                      {jobData?.experienceLevels.map((item: any) =>
                        CapitalizeFirstLetter(item),
                      )}
                    </Text>
                  </Flex>
                )
              ) : null}
              {jobData?.educationLevel ? (
                <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                  <Text fontSize="16px" fontFamily="medium">
                    Education Levels:
                  </Text>
                  <Text fontSize="16px" display="flex">
                    &nbsp;&nbsp;
                    {getEducationLevel(jobData?.educationLevel)}
                  </Text>
                </Flex>
              ) : null}
              {jobData?.salary ? (
                <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                  <Text fontSize="16px" fontFamily="medium">
                    Salary:
                  </Text>
                  <Text fontSize="16px" display="flex">
                    &nbsp;&nbsp;
                    {jobData?.salary}
                  </Text>
                </Flex>
              ) : null}
              {jobData?.remote && jobData?.remote === true ? (
                <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                  <Text fontSize="16px" fontFamily="medium">
                    Remote:
                  </Text>
                  <Text fontSize="16px" display="flex">
                    &nbsp;&nbsp; Yes
                  </Text>
                </Flex>
              ) : (
                <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                  <Text fontSize="16px" fontFamily="medium">
                    Remote:
                  </Text>
                  <Text fontSize="16px" display="flex">
                    &nbsp;&nbsp; No
                  </Text>
                </Flex>
              )}
              <Flex pt="10px">
                <a
                  href={`https://${jobData?.applyUrl}`}
                  target={'_blank'}
                  rel={'noreferrer'}>
                  <Button variant="orange" mr="1" w="200px">
                    Apply Job
                  </Button>
                </a>
              </Flex>
            </Flex>

            <Flex mt="40px" flexDirection="column" mb="10px">
              <Text fontSize="18px" fontFamily="medium" ml="5px">
                Job Description
              </Text>
              <Divider
                mt="10px"
                border="1px solid rgba(0, 0, 0, 0.6)"
                width="100%"
                borderColor="gray.300"
              />
            </Flex>
            {jobData?.description ? (
              <Text fontSize="16px" display="flex" flexDir="column" p="10px">
                &nbsp;&nbsp;
                {jobData?.description}
              </Text>
            ) : null}

            {jobData?.contents ? (
              <Flex
                p="30px"
                mt="10px"
                flexDirection="column"
                dangerouslySetInnerHTML={{ __html: jobData?.contents }}
              />
            ) : null}
            {jobData?.requirements ? (
              <>
                <Flex mt="40px" flexDirection="column" mb="10px">
                  <Text fontSize="18px" fontFamily="medium" ml="5px">
                    Job Requirements
                  </Text>
                  <Divider
                    mt="10px"
                    border="1px solid rgba(0, 0, 0, 0.6)"
                    width="100%"
                    borderColor="gray.300"
                  />
                </Flex>
                <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                  <SimpleGrid
                    columns={1}
                    spacing={2}
                    mt="10px"
                    p="10px"
                    justifyItems="stretch"
                    alignItems="center">
                    {jobData?.requirements.map((item: any, index) => (
                      <>
                        <Flex p="10px" key={index}>
                          <Text fontSize="16px" noOfLines={1} w="5%">
                            {index + 1} .
                          </Text>
                          <Text
                            w="95%"
                            fontSize="16px"
                            flexDir="column"
                            noOfLines={9}
                            display="flex">
                            {CapitalizeFirstLetter(item)}
                          </Text>
                        </Flex>
                      </>
                    ))}
                  </SimpleGrid>
                </Flex>
              </>
            ) : null}
            {jobData?.benefits && jobData?.benefits.length > 0 ? (
              <>
                <Flex mt="40px" flexDirection="column" mb="10px">
                  <Text fontSize="18px" fontFamily="medium" ml="5px">
                    Job Benefits
                  </Text>
                  <Divider
                    mt="10px"
                    border="1px solid rgba(0, 0, 0, 0.6)"
                    width="100%"
                    borderColor="gray.300"
                  />
                </Flex>

                <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                  <SimpleGrid
                    columns={1}
                    spacing={2}
                    mt="10px"
                    p="10px"
                    justifyItems="stretch"
                    alignItems="center">
                    {jobData?.benefits.map((item: any, index) => (
                      <>
                        <Flex p="10px" key={index}>
                          <Text fontSize="16px" noOfLines={1} w="5%">
                            {index + 1} .
                          </Text>
                          <Text
                            w="95%"
                            fontSize="16px"
                            flexDir="column"
                            noOfLines={9}
                            display="flex">
                            {CapitalizeFirstLetter(item)}
                          </Text>
                        </Flex>
                      </>
                    ))}
                  </SimpleGrid>
                </Flex>
              </>
            ) : null}
          </Flex>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

export default JobDetailsModal;
