/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState } from 'react';

import { Box, Text, useToast, SimpleGrid, Button, Flex } from '@chakra-ui/react';
import { Formik, FieldArray } from 'formik';
import {
  InputControl,
  SubmitButton,
  SelectControl,
  TextareaControl,
} from 'formik-chakra-ui';
import { FiMinus } from 'react-icons/fi';
import * as Yup from 'yup';

import { TinyMCEFormik } from 'common/components/TinyMCEFormik';
import {
  useGetLocationsQuery,
  useGetCompanyDetailLazyQuery,
  useCreateJobMutation,
} from 'common/generated/generated-types';

const CreateJob: React.FC = () => {
  const toast = useToast();

  const { data: locationsData } = useGetLocationsQuery();
  const [getCompanyDetails, { data: companyDetails }] = useGetCompanyDetailLazyQuery();
  const [addNewJobMutation, { data, loading, error }] = useCreateJobMutation({
    errorPolicy: 'all',
  });

  const [companyId, setCompanyId] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [locationsField, setLocationsField] = useState([]);

  useEffect(() => {
    const publisher = JSON.parse(localStorage.getItem('publisher') || 'null');
    const publisherCompanyId = publisher ? publisher.companyId : '';
    setCompanyId(publisherCompanyId);
  }, []);

  useEffect(() => {
    if (companyId !== '') {
      getCompanyDetails({
        variables: {
          id: companyId,
        },
      });
    }
  }, [companyId]);

  useEffect(() => {
    setLocationsField(locationsData?.Locations);
    setCompanyName(companyDetails?.Company.name);
  }, [locationsData, companyDetails]);

  const initialValues = {
    name: '',
    shortName: '',
    educationLevel: '',
    experienceLevels: [''],
    jobType: '',
    description: '',
    remote: '',
    applyUrl: '',
    companyId: '',
    companyName: '',
    locations: [''],
    requirements: [''],
    categories: [''],
    benefits: [] as string[],
    contents: '',
    salary: '',
  };
  const validationSchema = Yup.object({
    name: Yup.string().required('Job name is required'),
    shortName: Yup.string().required('Job short name is required'),
    description: Yup.string().required('Job description is required'),
    contents: Yup.string().required('Job contents is required'),
    educationLevel: Yup.string()
      .matches(/(associate|bachelor|doctoral|highShool|lessThanHighSchool|master)/)
      .required('Education Level is required'),
    experienceLevels: Yup.array().of(
      Yup.string()
        .matches(/(internship|entry|mid|senior|management)/)
        .required('Experience level is required'),
    ),
    jobType: Yup.string()
      .matches(/(external|internal)/)
      .required('Job type is required'),
    remote: Yup.boolean(),
    applyUrl: Yup.string().required('Apply URL is required'),
    salary: Yup.number(),
    companyId: Yup.string(),
    companyName: Yup.string(),
    locations: Yup.array().of(Yup.string().required('Job Locations is a required field')),
    requirements: Yup.array().of(
      Yup.string().required('Job Requirements is a required field'),
    ),
    categories: Yup.array().of(Yup.string()),
    benefits: Yup.array().of(Yup.string()),
  });

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const onSubmit = (values: any, { resetForm }: any) => {
    sleep(500).then(() => {
      addNewJobMutation({
        variables: {
          CreateJobInputDto: {
            name: values.name,
            shortName: values.shortName,
            description: values.description,
            contents: values.contents,
            educationLevel: values.educationLevel,
            experienceLevels: values.experienceLevels,
            jobType: values.jobType,
            remote: values.remote,
            applyUrl: values.applyUrl,
            companyId: values.companyId,
            companyName: values.companyName,
            locations: values.locations,
            requirements: values.requirements,
            categories: values.categories,
            benefits: values.benefits,
            salary: values.salary,
          },
        },
      }).then(() => {
        resetForm(initialValues);
      });
    });
  };

  useEffect(() => {
    if (!error && data) {
      toast({
        title: 'Create successfully.',
        description: "We've created your job.",
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
    } else {
      if (error) {
        toast({
          title: 'Error',
          description: error.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }
    }
  }, [data, loading, error]);

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}>
      {({ handleSubmit, values, setFieldValue }) => {
        useEffect(() => {
          setFieldValue('companyId', companyId);
          setFieldValue('companyName', companyName);
        }, [companyId, companyName]);
        return (
          <>
            <Box onSubmit={handleSubmit as React.FormEventHandler} w="100%">
              <SimpleGrid columns={1} spacingX="10px" spacingY="10px" py="20px" px="10px">
                <InputControl
                  label="Job Name *"
                  name="name"
                  inputProps={{
                    _placeholder: { color: 'blackAlpha.500' },
                    placeholder: 'Enter job name',
                    bg: 'white',
                    type: 'text',
                    color: 'blackAlpha.900',
                    size: 'md',
                  }}
                />
                <InputControl
                  label="Job Short Name *"
                  name="shortName"
                  inputProps={{
                    _placeholder: { color: 'blackAlpha.500' },
                    placeholder: 'Enter job short name',
                    bg: 'white',
                    type: 'text',
                    color: 'blackAlpha.900',
                    size: 'md',
                  }}
                />
                <TextareaControl
                  name="description"
                  label="Description *"
                  textareaProps={{
                    _placeholder: { color: 'blackAlpha.500' },
                    placeholder: 'Enter job description',
                    bg: 'white',
                    color: 'blackAlpha.900',
                  }}
                />
                <SimpleGrid columns={2} spacingX="10px" spacingY="10px">
                  <SelectControl
                    label="Education Level *"
                    name="educationLevel"
                    selectProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: `Select education level `,
                      bg: 'white',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}>
                    <option value="lessThanHighSchool">Less Than High School</option>
                    <option value="highShool">High School or Equivalent</option>
                    <option value="associate">Associate's Degree</option>
                    <option value="bachelor">Bachelor's Degree</option>
                    <option value="master">Master's Degree</option>
                    <option value="doctoral">Doctoral Degree</option>
                  </SelectControl>

                  <FieldArray name="experienceLevels">
                    {({ remove, push }) => (
                      <Box>
                        <Text>Experience Level *</Text>
                        {values.experienceLevels.length > 0 &&
                          values.experienceLevels.map(
                            (experienceLevel: any, index: number) => (
                              <Flex key={index}>
                                <SelectControl
                                  mt="5px"
                                  mr="5px"
                                  name={`experienceLevels.${index}`}
                                  selectProps={{
                                    _placeholder: { color: 'blackAlpha.500' },
                                    placeholder: `Select experience level ${index + 1}`,
                                    bg: 'white',
                                    color: 'blackAlpha.900',
                                    size: 'md',
                                  }}>
                                  <option value="internship">Internship</option>
                                  <option value="entry">Entry</option>
                                  <option value="mid">Mid</option>
                                  <option value="senior">Senior</option>
                                  <option value="management">Management</option>
                                </SelectControl>
                                <Button
                                  mt="5px"
                                  variant="red"
                                  onClick={() => remove(index)}>
                                  <FiMinus />
                                </Button>
                              </Flex>
                            ),
                          )}
                        <Button
                          mt="5px"
                          size="sm"
                          width="100%"
                          variant="blue"
                          onClick={() => push('')}>
                          Add more experience level
                        </Button>
                      </Box>
                    )}
                  </FieldArray>

                  <SelectControl
                    label="Job Type *"
                    name="jobType"
                    selectProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: `Select job type `,
                      bg: 'white',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}>
                    <option value="external">External</option>
                    <option value="internal">Internal</option>
                  </SelectControl>

                  <SelectControl
                    label="Remote *"
                    name="remote"
                    selectProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: `Select remote job `,
                      bg: 'white',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}>
                    <option value="true">True</option>
                    <option value="false">False</option>
                  </SelectControl>

                  <FieldArray name="locations">
                    {({ remove, push }) => (
                      <Box>
                        <Text>Locations *</Text>
                        {values.locations.length > 0 &&
                          values.locations.map((location: any, index: number) => (
                            <Flex key={index}>
                              <SelectControl
                                mt="5px"
                                mr="5px"
                                name={`locations.${index}`}
                                selectProps={{
                                  _placeholder: { color: 'blackAlpha.500' },
                                  bg: 'white',
                                  color: 'blackAlpha.900',
                                  size: 'md',
                                  placeholder: `Select job Location ${index + 1}`,
                                }}>
                                {locationsField?.map((location, index) => (
                                  <option key={index} value={location.name}>
                                    {location.name}
                                  </option>
                                ))}
                              </SelectControl>
                              <Button
                                mt="5px"
                                variant="red"
                                onClick={() => remove(index)}>
                                <FiMinus />
                              </Button>
                            </Flex>
                          ))}
                        <Button
                          mt="5px"
                          size="sm"
                          width="100%"
                          variant="blue"
                          onClick={() => push('')}>
                          Add more locations
                        </Button>
                      </Box>
                    )}
                  </FieldArray>

                  <FieldArray name="categories">
                    {({ remove, push }) => (
                      <Box>
                        <Text>Categories</Text>
                        {values.categories.length > 0 &&
                          values.categories.map((categorie: any, index: number) => (
                            <Flex key={index}>
                              <InputControl
                                mt="5px"
                                mr="5px"
                                name={`categories.${index}`}
                                inputProps={{
                                  _placeholder: { color: 'blackAlpha.500' },
                                  placeholder: `Enter job categories ${index + 1}`,
                                  bg: 'white',
                                  type: 'text',
                                  color: 'blackAlpha.900',
                                  size: 'md',
                                }}
                              />
                              <Button
                                mt="5px"
                                variant="red"
                                onClick={() => remove(index)}>
                                <FiMinus />
                              </Button>
                            </Flex>
                          ))}
                        <Button
                          mt="5px"
                          size="sm"
                          width="100%"
                          variant="blue"
                          onClick={() => push('')}>
                          Add more categories
                        </Button>
                      </Box>
                    )}
                  </FieldArray>

                  <InputControl
                    label="Salary"
                    name="salary"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Enter job salary',
                      bg: 'white',
                      type: 'text',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                </SimpleGrid>
                <FieldArray name="requirements">
                  {({ remove, push }) => (
                    <Box>
                      <Text>Requirements *</Text>
                      {values.requirements.length > 0 &&
                        values.requirements.map((requirement: any, index: number) => (
                          <Flex key={index}>
                            <InputControl
                              mt="5px"
                              mr="5px"
                              name={`requirements.${index}`}
                              inputProps={{
                                _placeholder: { color: 'blackAlpha.500' },
                                placeholder: `Enter job requirements ${index + 1}`,
                                bg: 'white',
                                type: 'text',
                                color: 'blackAlpha.900',
                                size: 'md',
                              }}
                            />
                            <Button mt="5px" variant="red" onClick={() => remove(index)}>
                              <FiMinus />
                            </Button>
                          </Flex>
                        ))}
                      <Button
                        mt="5px"
                        size="sm"
                        width="100%"
                        variant="blue"
                        onClick={() => push('')}>
                        Add more requirements
                      </Button>
                    </Box>
                  )}
                </FieldArray>
                <FieldArray name="benefits">
                  {({ remove, push }) => (
                    <Box>
                      <Text>Benefits</Text>
                      {values.benefits.length > 0 &&
                        values.benefits.map((benefit: any, index: number) => (
                          <Flex key={index}>
                            <InputControl
                              mt="5px"
                              mr="5px"
                              name={`benefits.${index}`}
                              inputProps={{
                                _placeholder: { color: 'blackAlpha.500' },
                                placeholder: `Enter job benefits ${index + 1}`,
                                bg: 'white',
                                type: 'text',
                                color: 'blackAlpha.900',
                                size: 'md',
                              }}
                            />
                            <Button mt="5px" variant="red" onClick={() => remove(index)}>
                              <FiMinus />
                            </Button>
                          </Flex>
                        ))}
                      <Button
                        mt="5px"
                        size="sm"
                        width="100%"
                        variant="blue"
                        onClick={() => push('')}>
                        Add benefits
                      </Button>
                    </Box>
                  )}
                </FieldArray>
                <InputControl
                  label="Apply Url *"
                  name="applyUrl"
                  inputProps={{
                    _placeholder: { color: 'blackAlpha.500' },
                    placeholder: 'Enter Apply Url',
                    bg: 'white',
                    type: 'text',
                    color: 'blackAlpha.900',
                    size: 'md',
                  }}
                />

                <TinyMCEFormik
                  name="contents"
                  label="Contents (what you see is what you get) *"
                />

                <SubmitButton
                  bgColor="green.400"
                  w="full"
                  size="lg"
                  color="white"
                  fontFamily="heading"
                  onClick={handleSubmit as React.FormEventHandler}
                  _hover={{ bg: 'gray.400', color: 'white' }}>
                  Submit
                </SubmitButton>
              </SimpleGrid>
            </Box>
          </>
        );
      }}
    </Formik>
  );
};
export default CreateJob;
