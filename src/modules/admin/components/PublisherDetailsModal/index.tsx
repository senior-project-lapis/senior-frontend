/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  Text,
  Flex,
  Box,
  Divider,
  Stack,
  Heading,
  ModalBody,
  ModalFooter,
  SimpleGrid,
  Image,
  Button,
} from '@chakra-ui/react';

import { useGetCompanyDetailQuery } from 'common/generated/generated-types';

interface Props {
  isOpenPublisherDetails: boolean;
  onClosePublisherDetails: () => void;
  publisherDetails?: any;
  handleApprove: (e: any, id: string) => void;
  handleReject: (e: any, id: string) => void;
}

const CapitalizeFirstLetter = (string: string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

const PublisherDetailsModal: React.FC<Props> = (Props) => {
  const {
    isOpenPublisherDetails,
    onClosePublisherDetails,
    publisherDetails,
    handleApprove,
    handleReject,
  } = Props;
  const companyId = publisherDetails?.companyId;
  const { data: companyDetails, loading } = useGetCompanyDetailQuery({
    variables: {
      id: companyId,
    },
  });

  return (
    <Modal isOpen={isOpenPublisherDetails} onClose={onClosePublisherDetails}>
      <ModalOverlay />
      <ModalContent
        bg="#F5F5F5"
        color="black"
        px="20px"
        py="20px"
        maxW="900px"
        borderBlock="none">
        <ModalCloseButton mx="20px" my="20px" fontSize="18px" />
        <ModalBody>
          <Flex overflowX="hidden" flexDirection="column">
            <Stack spacing={4}>
              <Stack spacing={2}>
                <Flex
                  justifyContent="space-between"
                  mt="20px"
                  width="100%"
                  alignItems="center">
                  <Heading fontSize="20px">Personal Details</Heading>
                </Flex>
                <Divider
                  border="1px solid rgba(0, 0, 0, 0.6)"
                  width="100%"
                  borderColor="gray.700"
                />
                <Box px="30px">
                  <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
                    First name:
                  </Text>
                  <Text fontSize="14px">{publisherDetails?.firstname}</Text>
                  <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
                    Last name:
                  </Text>
                  <Text fontSize="14px">{publisherDetails?.lastname}</Text>

                  <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
                    Middle name:
                  </Text>
                  {publisherDetails?.middlename ? (
                    <Text fontSize="14px">{publisherDetails?.middlename}</Text>
                  ) : (
                    <Text fontSize="14px">-</Text>
                  )}

                  <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
                    Phone number:
                  </Text>
                  <Text fontSize="14px">{publisherDetails?.phonenumber}</Text>
                  <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
                    Job Position:
                  </Text>
                  <Text fontSize="14px">{publisherDetails?.job}</Text>
                  <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
                    Email Address:
                  </Text>
                  <Text fontSize="14px">{publisherDetails?.email}</Text>
                </Box>
              </Stack>
              {loading ? (
                <Text fontSize="14px">Loading...</Text>
              ) : companyDetails?.Company ? (
                <Stack spacing={2}>
                  <Flex
                    justifyContent="space-between"
                    mt="20px"
                    width="100%"
                    alignItems="center">
                    <Heading fontSize="20px">Company Details</Heading>
                  </Flex>
                  <Divider
                    border="1px solid rgba(0, 0, 0, 0.6)"
                    width="100%"
                    borderColor="gray.700"
                  />
                  <Flex
                    w="100%"
                    justifyContent="space-between"
                    p="10px"
                    alignItems="center">
                    <Image
                      objectFit="contain"
                      src={companyDetails.Company.logoImage || '/images/404.png'}
                      alt={companyDetails.Company.shortName || 'not found'}
                      boxSize="200px"
                      boxShadow="rgba(0, 0, 0, 0.02) 0px 1px 3px 0px, rgba(27, 31, 35, 0.15) 0px 0px 0px 1px"
                      p="15px"
                    />

                    <Flex
                      w="100%"
                      pl="30px"
                      py="10px"
                      flexDirection="column"
                      justifyContent="space-between">
                      <Heading fontSize="18px">{companyDetails.Company.name}</Heading>
                      <Text fontSize="16px" fontFamily="medium" mt="20px">
                        Company Description :
                      </Text>
                      <Text fontSize="16px">
                        &nbsp;&nbsp;&nbsp;&nbsp;{companyDetails.Company.description}
                      </Text>
                      <Flex mt="10px" justifyContent="flex-start" flexDirection="row">
                        <Text fontSize="16px" fontFamily="medium">
                          Company Size :
                        </Text>
                        <Text fontSize="16px" display="flex">
                          &nbsp;&nbsp;
                          {CapitalizeFirstLetter(companyDetails.Company.size)}
                        </Text>
                      </Flex>
                      {companyDetails.Company.industries.length > 2 ? (
                        <Flex
                          mt="10px"
                          justifyContent="flex-start"
                          flexDirection="column">
                          <Text fontSize="16px" fontFamily="medium">
                            Company Industries:
                          </Text>
                          <SimpleGrid
                            columns={4}
                            spacing={1}
                            mt="5px"
                            ml="10px"
                            justifyItems="left"
                            alignItems="center">
                            {companyDetails.Company.industries.map((item, index) => (
                              <Text fontSize="16px" display="flex" key={index}>
                                {CapitalizeFirstLetter(item)}
                              </Text>
                            ))}
                          </SimpleGrid>
                        </Flex>
                      ) : (
                        <Flex mt="10px" justifyContent="flex-start" flexDirection="row">
                          <Text fontSize="16px" fontFamily="medium">
                            Company Industrie:
                          </Text>
                          <Text fontSize="16px" display="flex">
                            &nbsp;&nbsp;
                            {companyDetails.Company.industries.map((item) =>
                              CapitalizeFirstLetter(item),
                            )}
                          </Text>
                        </Flex>
                      )}

                      {companyDetails.Company.locations.length > 2 ? (
                        <Flex
                          mt="10px"
                          justifyContent="flex-start"
                          flexDirection="column">
                          <Text fontSize="16px" fontFamily="medium">
                            Company Locations:
                          </Text>
                          <SimpleGrid
                            columns={4}
                            spacing={1}
                            mt="5px"
                            ml="10px"
                            justifyItems="left"
                            alignItems="center">
                            {companyDetails.Company.locations.map((item, index) => (
                              <Text fontSize="16px" display="flex" key={index}>
                                {CapitalizeFirstLetter(item)}
                              </Text>
                            ))}
                          </SimpleGrid>
                        </Flex>
                      ) : (
                        <Flex mt="10px" justifyContent="flex-start" flexDirection="row">
                          <Text fontSize="16px" fontFamily="medium">
                            Company Location:
                          </Text>
                          <Text fontSize="16px" display="flex">
                            &nbsp;&nbsp;
                            {companyDetails.Company.locations.map((item) =>
                              CapitalizeFirstLetter(item),
                            )}
                          </Text>
                        </Flex>
                      )}
                    </Flex>
                  </Flex>
                </Stack>
              ) : null}
            </Stack>
          </Flex>
        </ModalBody>
        <ModalFooter>
          <Button
            variant="green"
            size="md"
            mr="5px"
            onClick={(e) => handleApprove(e, publisherDetails?._id)}>
            Approve
          </Button>
          <Button
            variant="red"
            size="md"
            onClick={(e) => handleReject(e, publisherDetails?._id)}>
            Reject
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default PublisherDetailsModal;
