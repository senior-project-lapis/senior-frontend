/* eslint-disable @typescript-eslint/no-explicit-any */
import { Box, Stack } from '@chakra-ui/react';
interface Props {
  countData: {
    name: string;
    totalCount: number;
  };
  colors: {
    bg: string;
    number: string;
    text: string;
  };
}

const DashboardCountCard: React.FC<Props> = (props) => {
  const { countData, colors } = props;
  return (
    <>
      <Box
        boxShadow="rgba(149, 157, 165, 0.3) 0px 8px 24px"
        width="100%"
        bgColor={colors.bg}
        borderRadius={10}
        padding="10%"
        textAlign="center">
        <Stack>
          <Box color={colors.number} fontWeight="bold" fontSize="1.75em">
            {countData.totalCount}
          </Box>
          <Box color={colors.text}>{countData.name}</Box>
        </Stack>
      </Box>
    </>
  );
};
export default DashboardCountCard;
