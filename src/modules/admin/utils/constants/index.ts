import { MenuType } from 'common/constants/menu/types';
export const ADMIN_MENU_CONSTANT: MenuType[] = [
  {
    key: '1',
    label: 'Dashboard',
    link: ['/admin/dashboard'],
  },
  {
    key: '2',
    label: 'Approve Table',
    link: ['/admin/approve'],
  },
  {
    key: '3',
    label: 'Report',
    link: ['/admin/report'],
  },
];
