/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useMemo, useCallback, useState, useEffect } from 'react';

import {
  Flex,
  Heading,
  Divider,
  Box,
  Button,
  useDisclosure,
  useToast,
} from '@chakra-ui/react';
import Head from 'next/head';
import { BiDetail } from 'react-icons/bi';
import { FaCheck, FaTimes } from 'react-icons/fa';

import SidebarLayout from 'common/components/SidebarLayout';
import TableComponent from 'common/components/Table/TableComponent';
import {
  useGetPendingPublisherLazyQuery,
  useUpdatePublisherMutation,
} from 'common/generated/generated-types';

import PublisherDetailsModal from 'modules/admin/components/PublisherDetailsModal';
import { ADMIN_MENU_CONSTANT } from 'modules/admin/utils/constants';
import Footer from 'modules/root/components/Footer';

const AdminApprove: React.FC = () => {
  const [publisher, setPublisher] = useState();
  const [pagination, setPagination] = React.useState({
    page: 0,
    rowsPerPage: 10,
  });
  const toast = useToast();
  const [loadData, { data, loading, refetch }] = useGetPendingPublisherLazyQuery({
    fetchPolicy: 'no-cache',
  });
  const [
    updatePublisherMutation,
    {
      data: updatePublisher,
      loading: updatePublisherLoading,
      error: updatePublisherError,
    },
  ] = useUpdatePublisherMutation({
    errorPolicy: 'all',
  });

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const handleApprove = (e: any, id: string) => {
    e.preventDefault();
    sleep(500).then(() => {
      updatePublisherMutation({
        variables: {
          PublisherUpdateInputDto: {
            id: id,
            status: 'approved',
          },
        },
      });
    });
  };

  const handleReject = (e: any, id: string) => {
    e.preventDefault();
    sleep(500).then(() => {
      updatePublisherMutation({
        variables: {
          PublisherUpdateInputDto: {
            id: id,
            status: 'rejected',
          },
        },
      });
    });
  };

  const handleShowDetails = (cell: any) => {
    const publisherDetails = cell?.row?.original;
    setPublisher(publisherDetails);
    onOpenPublisherDetails();
    // refetch();
  };

  useEffect(() => {
    if (!updatePublisherError && updatePublisher) {
      onClosePublisherDetails();
      toast({
        title: 'Success',
        description: `We've ${updatePublisher.UpdatePublisher.status} this publisher`,
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
      refetch();
    } else if (updatePublisherError) {
      toast({
        title: 'Error',
        description: updatePublisherError.message,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }
  }, [updatePublisher, updatePublisherLoading, updatePublisherError]);

  const {
    isOpen: isOpenPublisherDetails,
    onClose: onClosePublisherDetails,
    onOpen: onOpenPublisherDetails,
  } = useDisclosure();

  const columns = useMemo(
    () => [
      {
        Header: 'Email',
        accessor: 'email',
      },
      {
        Header: 'First Name',
        accessor: 'firstname',
      },
      {
        Header: 'Last Name',
        accessor: 'lastname',
      },
      {
        Header: 'Action',
        accessor: 'action',
        disableSortBy: true,
        Cell: (props: any) => (
          <>
            <Button
              onClick={() => handleShowDetails(props)}
              variant="orange"
              size="sm"
              mr="5px"
              ml="-15px">
              <BiDetail />
            </Button>
            <Button
              onClick={(e) => handleApprove(e, props?.row?.original._id)}
              variant="green"
              size="sm"
              mr="5px">
              <FaCheck />
            </Button>
            <Button
              onClick={(e) => handleReject(e, props?.row?.original._id)}
              variant="red"
              size="sm">
              <FaTimes />
            </Button>
          </>
        ),
      },
    ],
    [],
  );

  useEffect(() => {
    loadData({
      variables: {
        skip: pagination.rowsPerPage * pagination.page,
        limit: pagination.rowsPerPage,
        status: 'pending',
        sortBy: 'createAt',
        sortOrder: 'desc',
      },
    });
  }, [pagination]);

  const fetchData = useCallback(({ pageSize, pageIndex }) => {
    setPagination({ page: pageIndex, rowsPerPage: pageSize });
  }, []);

  const totalCount = data ? data.Publishers.totalCount : 0;
  const publisherData = data ? data.Publishers.publisherResult : [];
  const allData = useMemo(() => publisherData, [publisherData]);

  return (
    <>
      <Head>
        <title>Lapis | Admin approve</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <SidebarLayout
        constant={ADMIN_MENU_CONSTANT}
        title="ADMIN"
        btnColor="orange.200"
        navColor="orange.200"
        sidebarColor="blue.300">
        <Flex minHeight="100vh" w="100%" flexDirection="column">
          <Flex mt="20px" w="100%" flexDirection="column">
            <Heading fontSize="20px">Approve lists</Heading>
            <Divider
              mt="10px"
              border="1px solid rgba(0, 0, 0, 0.6)"
              width="100%"
              borderColor="gray.700"
            />
          </Flex>
          <Box
            borderRadius="5px"
            mt="10px"
            boxShadow=" rgba(0, 0, 0, 0.02) 0px 1px 3px 0px, rgba(27, 31, 35, 0.15) 0px 0px 0px 1px">
            <TableComponent
              columns={columns}
              data={allData}
              fetchData={fetchData}
              totalCount={totalCount}
              loading={loading}
              initialPageIndex={pagination.page}
              initialPageSize={pagination.rowsPerPage}
              isPagination
            />
          </Box>
        </Flex>
      </SidebarLayout>
      <PublisherDetailsModal
        isOpenPublisherDetails={isOpenPublisherDetails}
        onClosePublisherDetails={onClosePublisherDetails}
        publisherDetails={publisher}
        handleApprove={handleApprove}
        handleReject={handleReject}
      />
      <Footer color="orange.200" />
    </>
  );
};

export default AdminApprove;
