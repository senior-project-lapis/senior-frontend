/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useRef, useState } from 'react';

import { Box, Divider, Flex, Heading, SimpleGrid, Stack, Text } from '@chakra-ui/react';
import type { LottiePlayer } from 'lottie-web';
import Head from 'next/head';
import {
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';

import SidebarLayout from 'common/components/SidebarLayout';
import {
  useDashboardStatQuery,
  useLastestJobStatQuery,
} from 'common/generated/generated-types';

import DashboardCountCard from 'modules/admin/components/DashboardCountCard';
import { ADMIN_MENU_CONSTANT } from 'modules/admin/utils/constants';
import Footer from 'modules/root/components/Footer';

const AdminDashboard: React.FC = () => {
  const dashboardStatQuery = useDashboardStatQuery();
  const lastestJobStatQuery = useLastestJobStatQuery();
  const [dashboardData, setDashboardData] = useState<any>(null);
  const [lastestJobStatData, setLastestJobStatData] = useState<any>(null);

  const ref = useRef<HTMLDivElement>(null);
  const [lottie, setLottie] = useState<LottiePlayer | null>(null);

  useEffect(() => {
    setDashboardData(dashboardStatQuery.data?.DashboardStat);
  }, [dashboardStatQuery.loading]);

  useEffect(() => {
    setLastestJobStatData(lastestJobStatQuery.data?.LastestJobStat);
  }, [lastestJobStatQuery.loading]);

  useEffect(() => {
    import('lottie-web').then((Lottie) => setLottie(Lottie.default));
  }, []);

  useEffect(() => {
    if (lottie && ref.current) {
      const animation = lottie.loadAnimation({
        container: ref.current,
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: '/assets/animation2.json',
      });

      return () => animation.destroy();
    }
  }, [lottie]);

  return (
    <>
      <Head>
        <title>Lapis | Admin dashboard</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <SidebarLayout
        constant={ADMIN_MENU_CONSTANT}
        title="ADMIN"
        btnColor="orange.200"
        navColor="orange.200"
        sidebarColor="blue.300">
        <Flex minHeight="100vh" w="100%" flexDirection="column">
          <Stack>
            <Flex mt="20px" w="100%" flexDirection="column">
              <Heading fontSize="20px">Dashboard</Heading>
              <Divider
                mt="10px"
                border="1px solid rgba(0, 0, 0, 0.6)"
                width="100%"
                borderColor="gray.700"
              />
            </Flex>
            <SimpleGrid
              paddingX={2.5}
              paddingY={1}
              columns={4}
              spacing={5}
              justifyContent="center">
              {dashboardData?.nonActiveData.map((countData: any, index: number) => (
                <DashboardCountCard
                  key={index}
                  countData={countData}
                  colors={{ bg: 'white', number: 'blue.200', text: 'black' }}
                />
              ))}
            </SimpleGrid>
            <SimpleGrid
              paddingX={2.5}
              paddingY={1}
              columns={2}
              spacing={5}
              justifyContent="center">
              {dashboardData?.activeData.map((countData: any, index: number) => (
                <DashboardCountCard
                  colors={{ bg: 'white', number: 'blue.200', text: 'black' }}
                  key={index}
                  countData={countData}
                />
              ))}
            </SimpleGrid>
            <Box paddingX={2.5} paddingY={1}>
              <Box
                padding={5}
                borderRadius={10}
                boxShadow="rgba(149, 157, 165, 0.3) 0px 8px 24px">
                <Text m="2.5%" fontSize={18} fontWeight="bold" color="blue.200">
                  Job added last 1 year
                </Text>
                {lastestJobStatQuery.loading ? (
                  <Flex w="100%" h="30%" justifyContent="center">
                    <Flex w="3rem" h="3rem">
                      <div ref={ref} />
                    </Flex>
                  </Flex>
                ) : (
                  <ResponsiveContainer width="100%" aspect={3}>
                    <LineChart data={lastestJobStatData}>
                      <CartesianGrid />
                      <XAxis dataKey="name" interval={'preserveStartEnd'} />
                      <YAxis
                        label={{ value: 'Count', angle: -90, position: 'insideLeft' }}
                      />
                      <Legend />
                      <Tooltip />
                      {/* <Line dataKey="student" stroke="black" activeDot={{ r: 8 }} /> */}
                      <Line dataKey="count" stroke="#FF5100" activeDot={{ r: 6 }} />
                    </LineChart>
                  </ResponsiveContainer>
                )}
              </Box>
            </Box>
          </Stack>
        </Flex>
      </SidebarLayout>
      <Footer color="orange.200" />
    </>
  );
};

export default AdminDashboard;
