/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useMemo, useCallback, useState, useEffect } from 'react';

import {
  Flex,
  Heading,
  Divider,
  Box,
  Button,
  useDisclosure,
  useToast,
} from '@chakra-ui/react';
import Head from 'next/head';

import SidebarLayout from 'common/components/SidebarLayout';
import TableComponent from 'common/components/Table/TableComponent';
import {
  useGetJobLazyQuery,
  useUpdateJobMutation,
} from 'common/generated/generated-types';

import { ADMIN_MENU_CONSTANT } from 'modules/admin/utils/constants';
import JobDetailsModal from 'modules/publisher/components/JobDetailsModal';
import Footer from 'modules/root/components/Footer';

const AdminReport: React.FC = () => {
  const [job, setJob] = useState();
  const [pagination, setPagination] = React.useState({
    page: 0,
    rowsPerPage: 10,
  });
  const toast = useToast();

  const {
    isOpen: isOpenJobDetails,
    onClose: onCloseJobDetails,
    onOpen: onOpenJobDetails,
  } = useDisclosure();

  const [loadData, { data, loading, refetch }] = useGetJobLazyQuery({
    fetchPolicy: 'no-cache',
  });

  const [
    EditJobMutation,
    { data: updatedJobData, loading: updatedJobLoading, error: updatedJobError },
  ] = useUpdateJobMutation({
    errorPolicy: 'all',
  });

  useEffect(() => {
    loadData({
      variables: {
        skip: pagination.rowsPerPage * pagination.page,
        limit: pagination.rowsPerPage,
        status: 'reported',
        sortBy: 'createAt',
      },
    });
  }, [pagination]);

  const fetchData = useCallback(({ pageSize, pageIndex }) => {
    setPagination({ page: pageIndex, rowsPerPage: pageSize });
  }, []);

  useEffect(() => {
    if (!updatedJobError && updatedJobData) {
      refetch();
      toast({
        title: 'Update successfully.',
        description: "We've updated status",
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
    } else {
      if (updatedJobError) {
        toast({
          title: 'Error',
          description: updatedJobError.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }
    }
  }, [updatedJobData, updatedJobLoading, updatedJobError]);

  const handleShowDetails = (cell: any) => {
    const jobDetails = cell?.row?.original;
    setJob(jobDetails);
    onOpenJobDetails();
  };

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const handleBan = (e: any, id: string) => {
    e.preventDefault();
    sleep(500).then(() => {
      EditJobMutation({
        variables: {
          UpdateJobInputDto: {
            id: id,
            status: 'banned',
          },
        },
      });
    });
  };

  const handleDiscard = (e: any, id: string) => {
    e.preventDefault();
    sleep(500).then(() => {
      EditJobMutation({
        variables: {
          UpdateJobInputDto: {
            id: id,
            status: 'normal',
          },
        },
      });
    });
  };

  const totalCount = data ? data.Jobs.totalCount : 0;
  const jobsData = data ? data.Jobs.jobResult : [];
  const allData = useMemo(() => jobsData, [jobsData]);
  const columns = useMemo(
    () => [
      {
        Header: 'Job name',
        accessor: 'name',
      },
      {
        Header: 'status',
        accessor: 'status',
      },
      {
        Header: 'Action',
        accessor: 'action',
        disableSortBy: true,
        Cell: (props: any) => (
          <Flex>
            <Button
              onClick={() => handleShowDetails(props)}
              variant="orange"
              size="sm"
              mr="5px">
              Details
            </Button>
            <Button
              onClick={(e) => handleBan(e, props?.row?.original._id)}
              variant="green"
              size="sm"
              mr="5px">
              Ban
            </Button>
            <Button
              onClick={(e) => handleDiscard(e, props?.row?.original._id)}
              variant="red"
              size="sm">
              Discard
            </Button>
          </Flex>
        ),
      },
    ],
    [],
  );

  return (
    <>
      <Head>
        <title>Lapis | Admin report</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <SidebarLayout
        constant={ADMIN_MENU_CONSTANT}
        title="ADMIN"
        btnColor="orange.200"
        navColor="orange.200"
        sidebarColor="blue.300">
        <Flex minHeight="100vh" w="100%" flexDirection="column">
          <Flex mt="20px" w="100%" flexDirection="column">
            <Heading fontSize="20px">Report lists</Heading>
            <Divider
              mt="10px"
              border="1px solid rgba(0, 0, 0, 0.6)"
              width="100%"
              borderColor="gray.700"
            />
          </Flex>
          <Box
            borderRadius="5px"
            mt="10px"
            boxShadow=" rgba(0, 0, 0, 0.02) 0px 1px 3px 0px, rgba(27, 31, 35, 0.15) 0px 0px 0px 1px">
            <TableComponent
              columns={columns}
              data={allData}
              fetchData={fetchData}
              totalCount={totalCount}
              loading={loading}
              initialPageIndex={pagination.page}
              initialPageSize={pagination.rowsPerPage}
              isPagination
            />
          </Box>
        </Flex>
      </SidebarLayout>
      <JobDetailsModal
        isOpenJobDetails={isOpenJobDetails}
        onCloseJobDetails={onCloseJobDetails}
        jobData={job}
      />
      <Footer color="orange.200" />
    </>
  );
};

export default AdminReport;
