import { JobWithId } from 'common/generated/generated-types';
export const JOB = 'JOB';

export type Options = {
  name: string;
  value?: string;
};
export type Location = {
  name: string;
};

export type JobReducer = {
  autocomplete: Options[];
  isloading: boolean;
  jobType?: string;
  remote?: boolean;
  location?: string;
  limit: number;
  skip: number;
  query: string;
  totalResult: number;
  locations: Location[];
  jobsInCompare: JobWithId[];
};
