/* eslint-disable @typescript-eslint/no-explicit-any */
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { JobReducer, JOB } from './types';
export const FIRST_N_OF_NEWS = 10;
const initialState: JobReducer = {
  location: '',
  autocomplete: [],
  limit: FIRST_N_OF_NEWS,
  skip: 0,
  query: '',
  isloading: false,
  totalResult: 0,
  jobType: '',
  remote: undefined,
  locations: [],
  jobsInCompare: [],
};

export const jobSlice = createSlice({
  name: JOB,
  initialState,
  reducers: {
    setAutocomplete: (
      state,
      action: PayloadAction<{
        autocomplete: JobReducer['autocomplete'];
        isloading: boolean;
        locations: JobReducer['locations'];
      }>,
    ) => {
      const { autocomplete, isloading, locations } = action.payload;
      if (autocomplete === []) {
        return initialState;
      } else {
        state.autocomplete = autocomplete;
        state.isloading = isloading;
        state.locations = locations;
      }
    },
    onSearch(
      state,
      action: PayloadAction<{
        query: string;
        location?: string;
        jobType?: string;
        remote?: boolean;
        skip: number;
        limit: number;
      }>,
    ) {
      const { query, location, skip, limit, jobType, remote } = action.payload;
      if (query === '' && location === '' && jobType === '' && remote === false) {
        return initialState;
      } else {
        state.query = query;
        state.location = location;
        state.jobType = jobType;
        state.remote = remote;
        state.skip = skip;
        state.limit = limit;
      }
    },
    onData(
      state,
      action: PayloadAction<{
        location: JobReducer['location'];
        isloading: JobReducer['isloading'];
        query: JobReducer['query'];
        jobType?: JobReducer['jobType'];
        remote?: JobReducer['remote'];
        skip: JobReducer['skip'];
        limit: JobReducer['limit'];
        totalResult: JobReducer['totalResult'];
      }>,
    ) {
      const { query, location, skip, limit, isloading, totalResult, jobType, remote } =
        action.payload;
      state.isloading = isloading;
      state.location = location;
      state.query = query;
      state.jobType = jobType;
      state.remote = remote;
      state.skip = skip;
      state.limit = limit;
      state.totalResult = totalResult;
    },
    addToCompare(
      state,
      action: PayloadAction<{
        newItem: any;
      }>,
    ) {
      const { newItem } = action.payload;
      state.jobsInCompare.push(newItem);
      // console.log(newItem);
    },
    removeFromCompare(
      state,
      action: PayloadAction<{
        newItem: any;
      }>,
    ) {
      const { newItem } = action.payload;
      state.jobsInCompare = state.jobsInCompare.filter(
        (item) => newItem._id !== item._id,
      );
    },
  },
});

export default jobSlice.reducer;

export const jobActions = jobSlice.actions;
