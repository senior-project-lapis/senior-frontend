/* eslint-disable react/no-unescaped-entities */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';

import { Flex, useToast } from '@chakra-ui/react';
import { Formik } from 'formik';
import { SelectControl, SubmitButton } from 'formik-chakra-ui';
import Image from 'next/image';
import * as Yup from 'yup';

import Container from 'common/components/Container';
import { useGetJobTrendsLazyQuery } from 'common/generated/generated-types';

interface Props {
  setData: (value: any) => void;
  setSearch: (value: any) => void;
}

const JobTrendsSearchBanner: React.FC<Props> = (Props) => {
  const { setData, setSearch } = Props;
  const toast = useToast();
  const [getData, { data, loading, error }] = useGetJobTrendsLazyQuery({
    variables: {
      endYear: '2020',
      startYear: '2010',
      geo: '',
    },
  });

  const initialValues = {
    startYear: '',
    endYear: '',
    geo: '',
  };

  const validationSchema = Yup.object({
    startYear: Yup.string().required('start year is required'),
    endYear: Yup.string().required('end year is required'),
    geo: Yup.string(),
  });

  useEffect(() => {
    if (!error && data) {
      setData(data.JobTrend);
    } else {
      if (error) {
        toast({
          title: 'Error',
          description: error.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }
    }
  }, [data, loading, error]);

  useEffect(() => {
    getData({
      variables: {
        endYear: '2022',
        startYear: '2004',
        geo: '',
      },
    });
  }, []);

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const onSubmit = (values: any, { resetForm }: any) => {
    sleep(500)
      .then(() => {
        getData({
          variables: {
            endYear: values.endYear,
            startYear: values.startYear,
            geo: values.geo,
          },
        });
      })
      .then(() => {
        setSearch(values);
        resetForm(initialValues);
      });
  };
  const year = new Date().getFullYear();
  const years = Array.from(new Array(20), (val, index) => year - index);

  return (
    <>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}>
        {({ handleSubmit }) => (
          <Flex h="300px" w="100%">
            <Flex zIndex="1" position="relative" w="100%" h="100%">
              <Image
                src="/background_banner.png"
                layout="fill"
                objectFit="cover"
                priority
                alt="Banner Search bar Background"
              />
              <Flex
                position="relative"
                zIndex="2"
                w="100%"
                h="100%"
                border="none"
                background="linear-gradient(to left, transparent,rgba(255, 255, 255, 0.5))">
                <Container>
                  <Flex
                    onSubmit={handleSubmit as React.FormEventHandler}
                    flexWrap="wrap"
                    flexDirection="row"
                    alignContent=" center"
                    justifyContent="space-between"
                    w="100%">
                    <Flex w="100%">
                      <SelectControl
                        color="white"
                        name="geo"
                        label="Location"
                        selectProps={{
                          boxShadow:
                            ' rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px',
                          size: 'md',
                          borderRadius: '5px',
                          border: 'none',
                          color: 'blue.100',
                          fontFamily: 'medium',
                          placeholder: 'Select location',
                          bgColor: 'white',
                          w: '320px',
                          mx: '5px',
                        }}>
                        <option value="US">US</option>
                        <option value="GB">GB</option>
                        <option value="CH">CH</option>
                        <option value="AU">AU</option>
                      </SelectControl>
                      <SelectControl
                        color="white"
                        name="startYear"
                        label="From"
                        selectProps={{
                          boxShadow:
                            ' rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px',
                          size: 'md',
                          borderRadius: '5px',
                          border: 'none',
                          color: 'blue.100',
                          fontFamily: 'medium',
                          placeholder: 'Select start year',
                          bgColor: 'white',
                          w: '320px',
                          mx: '5px',
                        }}>
                        {years.map((year, index) => {
                          return (
                            <option key={`year${index}`} value={year}>
                              {year}
                            </option>
                          );
                        })}
                      </SelectControl>
                      <SelectControl
                        color="white"
                        label="To"
                        name="endYear"
                        selectProps={{
                          boxShadow:
                            ' rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px',
                          size: 'md',
                          borderRadius: '5px',
                          border: 'none',
                          color: 'blue.100',
                          fontFamily: 'medium',
                          placeholder: 'Select end year',
                          bgColor: 'white',
                          w: '320px',
                          mx: '5px',
                        }}>
                        {years.map((year, index) => {
                          return (
                            <option key={`year${index}`} value={year}>
                              {year}
                            </option>
                          );
                        })}
                      </SelectControl>
                    </Flex>

                    <SubmitButton
                      type="submit"
                      _hover={{
                        bg: 'gray.400',
                      }}
                      boxShadow=" rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px"
                      w="1000px"
                      mx="5px"
                      mt="20px"
                      fontFamily="heading"
                      bgColor="blue.200"
                      color="white"
                      onClick={() => handleSubmit()}>
                      Search
                    </SubmitButton>
                  </Flex>
                </Container>
              </Flex>
            </Flex>
          </Flex>
        )}
      </Formik>
    </>
  );
};
export default JobTrendsSearchBanner;
