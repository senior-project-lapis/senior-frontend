/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/no-unescaped-entities */
import React, { useEffect } from 'react';

import { Flex, Heading, Button, Text, SimpleGrid, Divider } from '@chakra-ui/react';
import Head from 'next/head';

import CompareJobBtn from 'common/components/CompareJobBtn';
import Container from 'common/components/Container';
import FavoriteBtn from 'common/components/FavoriteBtn';
import WatchingListBtn from 'common/components/WatchingListBtn';
import {
  GetJobDetailQuery,
  useGetRelatedJobsLazyQuery,
} from 'common/generated/generated-types';

import DetailCardHero from 'modules/home/components/DetailCardHero';
import JobSearchBanner from 'modules/job/components/JobSearchBanner';
import Footer from 'modules/root/components/Footer';
import Navbar from 'modules/root/components/Navbar';
interface Props {
  data?: GetJobDetailQuery;
}

const getEducationLevel = (education: string) => {
  let letter;
  switch (true) {
    case education === 'bachelor':
      letter = "Bachelor's Degree";
      break;
    case education === 'master':
      letter = "Master's Degree";
      break;
    case education === 'doctoral':
      letter = 'Doctoral Degree';
      break;
    case education === 'associate':
      letter = "Associate's Degree";
      break;
    case education === 'highShool':
      letter = 'High School or Equivalent';
      break;
    case education === 'lessThanHighSchool':
      letter = 'Less Than High School';
      break;
  }
  return letter;
};

const CapitalizeFirstLetter = (string: string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

const Job: React.FC<Props> = ({ data }) => {
  const [getJobs, { data: relatedJobs, loading: relatedJobsLoading }] =
    useGetRelatedJobsLazyQuery();

  useEffect(() => {
    getJobs({
      variables: {
        jobType: data.Job.jobType,
        location: data.Job.locations[0],
        companyId: data.Job.companyId,
        category: data.Job.categories[0],
        limit: 3,
        skip: 0,
        sortBy: 'createAt',
      },
    });
  }, [data]);

  return (
    <>
      <Head>
        <title>Lapis | Job Details</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />

      <Flex bg="white" flexDirection="column" flexWrap="nowrap" mt="75px" minH="100vh">
        <JobSearchBanner />
        <Flex py="30px">
          <Container>
            {/* <Flex p="20px" mt="20px">
            <Button onClick={(e) => handleBack(e)}>
              <AiOutlineArrowLeft /> &nbsp;Go Back
            </Button>
          </Flex> */}
            {data?.Job ? (
              <Flex overflowX="hidden" flexDirection="column" p="15px" m="5px" w="100%">
                <Flex justifyContent="space-between">
                  <Heading p="10px" fontSize="20px">
                    {data.Job.name}
                  </Heading>
                  <Flex p="10px">
                    <WatchingListBtn data={data.Job} watchingType="job" />
                  </Flex>
                </Flex>
                <Flex ml="40px" flexDirection="column">
                  <Flex justifyContent="flex-start" flexDirection="column">
                    <Flex fontSize="16px" p="5px">
                      <Text fontSize="16px" fontFamily="medium">
                        Company:
                      </Text>
                      &nbsp;
                      {data.Job.companyName}&nbsp;&nbsp;
                    </Flex>
                    {data.Job.locations.length > 2 ? (
                      <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                        <Text fontSize="16px" fontFamily="medium">
                          Locations:
                        </Text>
                        <SimpleGrid
                          columns={4}
                          spacing={1}
                          m="10px"
                          justifyItems="left"
                          alignItems="center">
                          {data.Job.locations.map((item, index) => (
                            <Text fontSize="16px" display="flex" key={index}>
                              {CapitalizeFirstLetter(item)}
                            </Text>
                          ))}
                        </SimpleGrid>
                      </Flex>
                    ) : (
                      <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                        <Text fontSize="16px" fontFamily="medium">
                          Location:
                        </Text>
                        <Text fontSize="16px" display="flex">
                          &nbsp;&nbsp;
                          {data.Job.locations.map(
                            (item) => CapitalizeFirstLetter(item) + ' / ',
                          )}
                        </Text>
                      </Flex>
                    )}
                  </Flex>
                  {data.Job.categories ? (
                    data.Job.categories.length > 1 ? (
                      <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                        <Text fontSize="16px" fontFamily="medium">
                          Categories:
                        </Text>
                        <SimpleGrid
                          columns={2}
                          spacing={2}
                          mt="10px"
                          justifyItems="center"
                          alignItems="center">
                          {data.Job.categories.map((item, index) => (
                            <Text fontSize="16px" display="flex" key={index}>
                              {CapitalizeFirstLetter(item)}
                            </Text>
                          ))}
                        </SimpleGrid>
                      </Flex>
                    ) : (
                      <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                        <Text fontSize="16px" fontFamily="medium">
                          Categories:
                        </Text>
                        <Text fontSize="16px" display="flex">
                          &nbsp;&nbsp;
                          {data.Job.categories.map((item) => CapitalizeFirstLetter(item))}
                        </Text>
                      </Flex>
                    )
                  ) : null}
                  {data.Job.experienceLevels ? (
                    data.Job.experienceLevels.length > 1 ? (
                      <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                        <Text fontSize="16px" fontFamily="medium">
                          Experience Levels:
                        </Text>
                        <SimpleGrid
                          columns={2}
                          spacing={2}
                          mt="10px"
                          justifyItems="center"
                          alignItems="center">
                          {data.Job.experienceLevels.map((item, index) => (
                            <Text fontSize="16px" display="flex" key={index}>
                              {CapitalizeFirstLetter(item)}
                            </Text>
                          ))}
                        </SimpleGrid>
                      </Flex>
                    ) : (
                      <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                        <Text fontSize="16px" fontFamily="medium">
                          Experience Levels:
                        </Text>
                        <Text fontSize="16px" display="flex">
                          &nbsp;&nbsp;
                          {data.Job.experienceLevels.map((item) =>
                            CapitalizeFirstLetter(item),
                          )}
                        </Text>
                      </Flex>
                    )
                  ) : null}
                  {data.Job.educationLevel ? (
                    <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                      <Text fontSize="16px" fontFamily="medium">
                        Education Levels:
                      </Text>
                      <Text fontSize="16px" display="flex">
                        &nbsp;&nbsp;
                        {getEducationLevel(data.Job.educationLevel)}
                      </Text>
                    </Flex>
                  ) : null}
                  {data.Job.salary ? (
                    <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                      <Text fontSize="16px" fontFamily="medium">
                        Salary:
                      </Text>
                      <Text fontSize="16px" display="flex">
                        &nbsp;&nbsp;
                        {data.Job.salary}
                      </Text>
                    </Flex>
                  ) : null}
                  {data.Job.remote && data.Job.remote === true ? (
                    <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                      <Text fontSize="16px" fontFamily="medium">
                        Remote:
                      </Text>
                      <Text fontSize="16px" display="flex">
                        &nbsp;&nbsp; Yes
                      </Text>
                    </Flex>
                  ) : (
                    <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                      <Text fontSize="16px" fontFamily="medium">
                        Remote:
                      </Text>
                      <Text fontSize="16px" display="flex">
                        &nbsp;&nbsp; No
                      </Text>
                    </Flex>
                  )}
                  <Flex pt="10px">
                    <a
                      href={`https://${data.Job.applyUrl}`}
                      target={'_blank'}
                      rel={'noreferrer'}>
                      <Button variant="orange" mr="1" w="200px">
                        Apply Job
                      </Button>
                    </a>
                    <CompareJobBtn
                      data={data.Job}
                      title="Compare Job"
                      titleWhenRm="Remove Compare Job"
                      btnPadding="0 40px"
                    />

                    <FavoriteBtn jobId={data.Job._id} />
                  </Flex>
                </Flex>

                <Flex mt="40px" flexDirection="column" mb="10px">
                  <Text fontSize="18px" fontFamily="medium" ml="5px">
                    Job Description
                  </Text>
                  <Divider
                    mt="10px"
                    border="1px solid rgba(0, 0, 0, 0.6)"
                    width="100%"
                    borderColor="gray.300"
                  />
                </Flex>
                {data.Job.description ? (
                  <Text fontSize="16px" display="flex" flexDir="column" p="10px">
                    &nbsp;&nbsp;
                    {data.Job.description}
                  </Text>
                ) : null}

                {data.Job.contents ? (
                  <Flex
                    p="30px"
                    mt="10px"
                    flexDirection="column"
                    dangerouslySetInnerHTML={{ __html: data.Job.contents }}
                  />
                ) : null}
                {data.Job.requirements ? (
                  <>
                    <Flex mt="40px" flexDirection="column" mb="10px">
                      <Text fontSize="18px" fontFamily="medium" ml="5px">
                        Job Requirements
                      </Text>
                      <Divider
                        mt="10px"
                        border="1px solid rgba(0, 0, 0, 0.6)"
                        width="100%"
                        borderColor="gray.300"
                      />
                    </Flex>
                    <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                      <SimpleGrid
                        columns={1}
                        spacing={2}
                        mt="10px"
                        p="10px"
                        justifyItems="stretch"
                        alignItems="center">
                        {data.Job.requirements.map((item, index) => (
                          <>
                            <Flex p="10px" key={index}>
                              <Text fontSize="16px" noOfLines={1} w="5%">
                                {index + 1} .
                              </Text>
                              <Text
                                w="95%"
                                fontSize="16px"
                                flexDir="column"
                                noOfLines={9}
                                display="flex">
                                {CapitalizeFirstLetter(item)}
                              </Text>
                            </Flex>
                          </>
                        ))}
                      </SimpleGrid>
                    </Flex>
                  </>
                ) : null}
                {data.Job.benefits && data.Job.benefits.length > 0 ? (
                  <>
                    <Flex mt="40px" flexDirection="column" mb="10px">
                      <Text fontSize="18px" fontFamily="medium" ml="5px">
                        Job Benefits
                      </Text>
                      <Divider
                        mt="10px"
                        border="1px solid rgba(0, 0, 0, 0.6)"
                        width="100%"
                        borderColor="gray.300"
                      />
                    </Flex>

                    <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                      <SimpleGrid
                        columns={1}
                        spacing={2}
                        mt="10px"
                        p="10px"
                        justifyItems="stretch"
                        alignItems="center">
                        {data.Job.benefits.map((item, index) => (
                          <>
                            <Flex p="10px" key={index}>
                              <Text fontSize="16px" noOfLines={1} w="5%">
                                {index + 1} .
                              </Text>
                              <Text
                                w="95%"
                                fontSize="16px"
                                flexDir="column"
                                noOfLines={9}
                                display="flex">
                                {CapitalizeFirstLetter(item)}
                              </Text>
                            </Flex>
                          </>
                        ))}
                      </SimpleGrid>
                    </Flex>
                  </>
                ) : null}
              </Flex>
            ) : (
              <Flex h="50%" w="100%" justifyContent="center" alignItems="center">
                <Text>Sorry, can't find this job data :(</Text>
              </Flex>
            )}
          </Container>
        </Flex>
        {!relatedJobsLoading && relatedJobs?.Jobs.totalCount >= 3 ? (
          <DetailCardHero JobData={relatedJobs} title="Related Jobs" />
        ) : null}
      </Flex>
      <Footer />
    </>
  );
};

export default Job;
