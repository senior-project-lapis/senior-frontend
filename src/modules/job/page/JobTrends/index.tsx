import React, { useState } from 'react';

import { Flex, Heading, Divider } from '@chakra-ui/react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import {
  BarChart,
  Bar,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  LabelList,
  ResponsiveContainer,
} from 'recharts';

import Container from 'common/components/Container';
import { JobTrendResponse, QueryJobTrendArgs } from 'common/generated/generated-types';

import JobTrendsSearchBanner from 'modules/job/components/JobTrendsSearchBanner';
import Footer from 'modules/root/components/Footer';
import Navbar from 'modules/root/components/Navbar';
import { instantSearchActions } from 'modules/search/reducers/instantSearchReducer';

const JobTrends: React.FC = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [data, setData] = useState<JobTrendResponse>();
  const [search, setSearch] = useState<QueryJobTrendArgs>();

  return (
    <>
      <Head>
        <title>Lapis | Job Trends</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <Flex bg="white" flexDirection="column" flexWrap="nowrap" mt="75px" minH="100vh">
        <JobTrendsSearchBanner setData={setData} setSearch={setSearch} />
        <Flex py="30px">
          <Container>
            {data && (
              <>
                <Flex mb="10px" width="100%" flexDirection="column" mt="10px">
                  <Heading fontSize="20px">
                    Jobs Trends
                    {search != null
                      ? search.geo === ''
                        ? ' in Global' +
                          ' from ' +
                          search.startYear +
                          ' to ' +
                          search.endYear
                        : ' in ' +
                          search.geo +
                          ' from ' +
                          search.startYear +
                          ' to ' +
                          search.endYear
                      : ' in Global from 2004 to 2022'}
                  </Heading>
                  <Divider
                    mt="10px"
                    border="1px solid rgba(0, 0, 0, 0.6)"
                    width="100%"
                    borderColor="gray.700"
                  />
                </Flex>

                <Flex w="100%" h="500px" p="10px">
                  <ResponsiveContainer width={1000} height="87%">
                    <BarChart
                      width={1024}
                      height={450}
                      data={data?.averages}
                      margin={{ top: 40, bottom: 5, left: -30 }}
                      onClick={(e) => {
                        if (e && e.activePayload && e.activePayload.length > 0) {
                          router.push({
                            pathname: '/search',
                            query: {
                              query: e.activePayload[0].payload.name,
                              location: '',
                              skip: 0,
                              limit: 10,
                            },
                          });
                          dispatch(
                            instantSearchActions.onSearch({
                              query: e.activePayload[0].payload.name,
                              location: '',
                              skip: 0,
                              limit: 10,
                            }),
                          );
                        }
                      }}>
                      <CartesianGrid strokeDasharray="3 3" />
                      <YAxis dataKey="score" />
                      <Tooltip />
                      <Legend />
                      <Bar dataKey="score" fill="#8884d8">
                        <LabelList dataKey="name" position="top" />
                      </Bar>
                    </BarChart>
                  </ResponsiveContainer>
                </Flex>
              </>
            )}
          </Container>
        </Flex>
      </Flex>
      <Footer />
    </>
  );
};

export default JobTrends;
