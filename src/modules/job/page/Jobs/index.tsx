/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState } from 'react';

import { Flex, ButtonProps, Stack, Heading, Divider } from '@chakra-ui/react';
import { Paginator, PageGroup, usePaginator } from 'chakra-paginator';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';

import Container from 'common/components/Container';
import DetailBox from 'common/components/DetailBox';
import Loader from 'common/components/Loader';
import ResultBox from 'common/components/ResultBox';
import { GetJobQuery, JobWithId } from 'common/generated/generated-types';
import { StoresState } from 'common/stores';

import JobSearchBanner from 'modules/job/components/JobSearchBanner';
import Footer from 'modules/root/components/Footer';
import Navbar from 'modules/root/components/Navbar';
interface Props {
  data?: GetJobQuery;
}

const Jobs: React.FC<Props> = ({ data }) => {
  const router = useRouter();

  const [selectedJob, setSelectedJob] = useState<JobWithId>();
  const skip = useSelector((state: StoresState) => state.job.skip);
  const isLoading = useSelector((state: StoresState) => state.job.isloading);
  const query = useSelector((state: StoresState) => state.job.query);
  const totalResult = useSelector((state: StoresState) => state.job.totalResult);

  const outerLimit = 1;
  const innerLimit = 1;

  // styles
  const baseStyles: ButtonProps = {
    w: '30px',
    h: '30px',
    fontSize: 'sm',
  };

  const normalStyles: ButtonProps = {
    ...baseStyles,
    _hover: {
      bg: 'blue.500',
    },
    bg: 'white',
  };

  const activeStyles: ButtonProps = {
    ...baseStyles,
    _hover: {
      bg: 'gray.400',
      color: 'black',
    },
    color: 'white',
    bg: 'blue.100',
  };

  const { currentPage, setCurrentPage, offset, pageSize, pagesQuantity } = usePaginator({
    total: totalResult,
    initialState: {
      currentPage: 1,
      pageSize: 10,
    },
  });

  useEffect(() => {
    if (isLoading === true) {
      <Loader />;
    }
    if (skip === 0) {
      setCurrentPage(1);
    }
  }, [skip, isLoading]);

  useEffect(() => {
    router.push({
      pathname: '/jobs',
      query: {
        ...router.query,
        skip: offset,
        limit: pageSize,
      },
    });
  }, [offset, currentPage]);

  return (
    <>
      <Head>
        <title>Lapis | Jobs</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />

      <Flex bg="gray.200" flexDirection="column" flexWrap="nowrap" mt="75px" minH="100vh">
        <JobSearchBanner />
        <Flex alignItems="stretch" m="50px">
          <Container>
            <Flex mb="20px" width="100%" flexDirection="column">
              <Heading fontSize="20px">Jobs</Heading>
              <Divider
                mt="10px"
                border="1px solid rgba(0, 0, 0, 0.6)"
                width="100%"
                borderColor="gray.700"
              />
            </Flex>
            {totalResult !== 0 ? (
              <>
                <Flex
                  flexDirection="column"
                  flexWrap="nowrap"
                  w="40%"
                  justifyContent="space-between">
                  <Stack spacing={2}>
                    {data?.Jobs.jobResult.map((job, i) => (
                      <ResultBox
                        isUseSelected
                        key={i}
                        index={Number(i)}
                        job={job}
                        setSelectedJob={setSelectedJob}
                      />
                    ))}
                  </Stack>
                </Flex>
                {selectedJob ? (
                  <>
                    <Flex
                      w="59%"
                      ml="1%"
                      p="10px"
                      maxH="1165px"
                      bg="white"
                      borderRadius="5px"
                      boxShadow="rgba(149, 157, 165, 0.2) 0px 8px 24px">
                      <DetailBox job={selectedJob} />
                    </Flex>
                  </>
                ) : null}
              </>
            ) : (
              <Heading transform="(50%,50%)">Can't find {query} results 🌱 </Heading>
            )}
            {totalResult > 9 ? (
              <Paginator
                normalStyles={normalStyles}
                activeStyles={activeStyles}
                pagesQuantity={pagesQuantity}
                innerLimit={innerLimit}
                currentPage={currentPage}
                outerLimit={outerLimit}
                onPageChange={setCurrentPage}>
                <PageGroup
                  isInline
                  align="center"
                  justifyContent="flexStart"
                  mt="2%"
                  w="100%"
                />
              </Paginator>
            ) : null}
          </Container>
        </Flex>
      </Flex>
      <Footer />
    </>
  );
};

export default Jobs;
