/* eslint-disable react/no-unescaped-entities */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  ModalHeader,
  Divider,
  Heading,
  Stack,
  ButtonGroup,
  Button,
  Text,
  Center,
  useToast,
} from '@chakra-ui/react';
import { Formik } from 'formik';
import { InputControl, SubmitButton } from 'formik-chakra-ui';
import { useRouter } from 'next/router';
import { AiFillFacebook } from 'react-icons/ai';
import { FcGoogle } from 'react-icons/fc';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';

import { useLoginMutation, UserLoginInputDto } from 'common/generated/generated-types';

import { userActions } from 'modules/auth/reducers/userReducer';

interface Props {
  isOpenLogin: boolean;
  onCloseLogin: () => void;
  onOpenUserRegister: () => void;
}

const initialValues = {
  email: '',
  password: '',
};

const validationSchema = Yup.object({
  email: Yup.string().email().required(),
  password: Yup.string().required(),
});

const Login: React.FC<Props> = (props) => {
  const toast = useToast();
  const { isOpenLogin, onCloseLogin, onOpenUserRegister } = props;
  const dispatch = useDispatch();
  const router = useRouter();
  const [loginMutation, { data, loading, error }] = useLoginMutation({
    errorPolicy: 'all',
  });

  const trigerSignup = async () => {
    await Promise.all([onCloseLogin(), onOpenUserRegister()]);
  };

  useEffect(() => {
    if (!error && data) {
      const duplicated = localStorage.getItem('user');
      if (duplicated) localStorage.removeItem('user');
      localStorage.setItem('user', JSON.stringify(data.Login));
      dispatch(
        userActions.setUser({
          id: data.Login._id,
          email: data.Login.email,
          firstname: data.Login.firstname,
          middlename: data?.Login.middlename || undefined,
          lastname: data.Login.lastname,
          phonenumber: data.Login.phonenumber,
          dob: data.Login.dob,
          address: data.Login.address,
          role: data.Login.role,
        }),
      );

      onCloseLogin();
      toast({
        title: 'Login successfully.',
        description: "We've Loged your in.",
        status: 'success',
        duration: 9000,
        isClosable: true,
      });
      if (data.Login.role === 'admin') {
        router.push('/admin/dashboard', undefined, { shallow: true });
      } else {
        router.reload();
      }
    } else {
      if (error) {
        // console.log(error, 'error');
        toast({
          title: 'Error',
          description: error.message,
          status: 'error',
          duration: 9000,
          isClosable: true,
        });
      }
    }
  }, [data, error, loading, router, dispatch]);

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

  const onSubmit = (values: UserLoginInputDto, { resetForm }: any) => {
    sleep(500).then(() => {
      loginMutation({
        variables: {
          UserLoginInputDTO: {
            email: values.email,
            password: values.password,
          },
        },
      }).then(() => {
        resetForm(initialValues);
      });
    });
  };

  return (
    <>
      <Modal isOpen={isOpenLogin} onClose={onCloseLogin} isCentered>
        <ModalOverlay />
        <ModalContent
          bg="blue.200"
          color="white"
          px="50px"
          py="60px"
          maxW="500px"
          borderBlock="none">
          <ModalCloseButton mx="20px" my="20px" fontSize="18px" />
          <ModalHeader ml="-25px">
            <Heading fontSize="26px" color="white">
              Sign in
            </Heading>
          </ModalHeader>
          <Divider border="2px solid #FFFFFF" />
          <ModalBody>
            <Formik
              initialValues={initialValues}
              onSubmit={onSubmit}
              validationSchema={validationSchema}>
              {({ handleSubmit }) => (
                <Stack
                  spacing="3"
                  as="form"
                  onSubmit={handleSubmit as React.FormEventHandler}
                  mt="20px"
                  mx="-25px">
                  <InputControl
                    name="email"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Email',
                      bg: 'white',
                      type: 'text',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                  <InputControl
                    name="password"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Password',
                      bg: 'white',
                      type: 'password',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                  <SubmitButton
                    size="lg"
                    bg="white"
                    color="blackAlpha.900"
                    fontFamily="heading"
                    _active={{ bg: 'gray.400', color: 'white', borderColor: '#bec3c9' }}
                    _focus={{ bg: 'gray.400', color: 'white', borderColor: '#bec3c9' }}
                    _hover={{ bg: 'gray.400', color: 'white', borderColor: '#bec3c9' }}>
                    Sign In
                  </SubmitButton>

                  <Center>
                    <Text>Or Sign In with</Text>
                  </Center>

                  <ButtonGroup justifyContent="space-between">
                    <Button
                      bg="#3b5998"
                      w="full"
                      size="md"
                      color="white"
                      _hover={{ bg: 'gray.400' }}
                      leftIcon={<AiFillFacebook />}>
                      Facebook
                    </Button>
                    <Button
                      bg="white"
                      w="full"
                      size="md"
                      color="black"
                      _hover={{ bg: 'gray.400', color: 'white' }}
                      leftIcon={<FcGoogle />}>
                      Google
                    </Button>
                  </ButtonGroup>
                  <Center mt="20px">
                    <Text>
                      If you don't have an account,
                      <Button
                        mb="4px"
                        as="a"
                        color="white"
                        bg="none"
                        cursor="pointer"
                        _active={{ bg: 'none' }}
                        _hover={{ bg: 'none' }}
                        onClick={trigerSignup}>
                        Sign up
                      </Button>
                    </Text>
                  </Center>
                </Stack>
              )}
            </Formik>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default Login;
