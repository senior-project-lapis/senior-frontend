import React, { useEffect } from 'react';

import { useToast } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';

import { useLogoutMutation } from 'common/generated/generated-types';

import { publisherActions } from 'modules/auth/reducers/publisherReducer';
import { userActions } from 'modules/auth/reducers/userReducer';
const Logout: React.FC = () => {
  const toast = useToast();
  const router = useRouter();
  const dispatch = useDispatch();
  const [logoutMutation, { error }] = useLogoutMutation();

  useEffect(() => {
    logoutMutation();
    if (!error) {
      dispatch(userActions.deleteUser());
      dispatch(publisherActions.deletePublisher());
      localStorage.removeItem('user');
      localStorage.removeItem('publisher');
      toast({
        title: 'Success',
        description: 'Logout successfully.',
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
      router.push('/', undefined, { shallow: true });
    }
  }, []);

  return <div> </div>;
};

export default Logout;
