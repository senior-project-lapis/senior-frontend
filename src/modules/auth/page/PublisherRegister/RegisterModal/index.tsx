/* eslint-disable @typescript-eslint/no-explicit-any */

import React, { useState, useEffect } from 'react';

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  ModalHeader,
  Divider,
  Heading,
  Stack,
  Box,
  Flex,
  Center,
  Text,
  Button,
  useDisclosure,
} from '@chakra-ui/react';
import { InputControl, SubmitButton, SelectControl } from 'formik-chakra-ui';

import { useGetCompanyForPublisherRegisterQuery } from 'common/generated/generated-types';

import CreateNewCompany from 'modules/company/page/CreateNewCompany';
export interface Item {
  label: string;
  value: string;
}

interface Props {
  isOpenNext: boolean;
  onCloseNext: () => void;
  trigerPublisherSignin: () => void;
  handleSubmit: (values: any) => void;
  setFieldValue: (field: string, value: any) => void;
}

const RegisterModal: React.FC<Props> = (props) => {
  const { isOpenNext, onCloseNext, trigerPublisherSignin, handleSubmit, setFieldValue } =
    props;

  const [company, setCompany] = useState<any>([]);
  const { data, loading, refetch } = useGetCompanyForPublisherRegisterQuery({
    variables: {
      sortBy: 'name',
    },
  });
  const [newCompany, setNewCompany] = useState<any>(null);
  const {
    isOpen: isOpenCreateCompany,
    onClose: onCloseCreateCompany,
    onOpen: onOpenCreateCompany,
  } = useDisclosure();

  useEffect(() => {
    const companies =
      data?.Companies.companyResult.map((el: any) => ({
        value: el._id,
        label: el.name,
      })) || [];
    setCompany(companies);
  }, [loading, data]);

  useEffect(() => {
    if (newCompany) {
      setTimeout(() => setFieldValue('companyId', newCompany._id), 500);
      refetch();
    }
  }, [newCompany]);

  return (
    <>
      <Modal isOpen={isOpenNext} onClose={onCloseNext} isCentered>
        <ModalOverlay />
        <ModalContent
          bg="blue.200"
          color="white"
          px="60px"
          py="50px"
          maxW="900px"
          borderBlock="none">
          <ModalCloseButton mx="20px" my="20px" fontSize="18px" />
          <ModalHeader ml="-25px">
            <Heading fontSize="26px" color="white">
              Publisher Sign up
            </Heading>
          </ModalHeader>
          <Divider border="2px solid #FFFFFF" w="775px" />
          <ModalBody>
            <Flex alignItems="flex-end" justifyContent="space-between" width="full">
              <Box w="45%">
                <Stack spacing="5" as="form" mt="20px" mx="-25px">
                  <InputControl
                    name="email"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Email',
                      bg: 'white',
                      type: 'text',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                  <InputControl
                    name="password"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Password',
                      bg: 'white',
                      type: 'password',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                  <InputControl
                    name="passwordConfirmation"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Confirm Password',
                      bg: 'white',
                      type: 'password',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                  <InputControl
                    name="firstname"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'First Name',
                      bg: 'white',
                      type: 'text',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                  <InputControl
                    name="lastname"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Last Name',
                      bg: 'white',
                      type: 'text',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                </Stack>
              </Box>
              <Box w="45%">
                <Stack spacing="5" as="form" mt="20px" mx="-25px">
                  <InputControl
                    name="middlename"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Middle Name',
                      bg: 'white',
                      type: 'text',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                  <InputControl
                    name="phonenumber"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Phone Number',
                      bg: 'white',
                      type: 'text',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                  <InputControl
                    name="job"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Job Title',
                      bg: 'white',
                      type: 'text',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />

                  <SelectControl
                    name="companyId"
                    selectProps={{
                      placeholder: 'Company Name',
                      _placeholder: { color: 'blackAlpha.500' },
                      color: 'blackAlpha.900',
                      size: 'md',
                      bg: 'white',
                    }}>
                    {company?.map(({ value, label }: any) => (
                      <option key={value} value={value}>
                        {label}
                      </option>
                    ))}
                  </SelectControl>

                  <Button
                    w="full"
                    as="a"
                    color="white"
                    bg="orange.300"
                    cursor="pointer"
                    flexDirection="column"
                    fontSize="16px"
                    onClick={onOpenCreateCompany}>
                    Add new company
                  </Button>
                </Stack>
              </Box>
            </Flex>
            <SubmitButton
              onClick={handleSubmit}
              type="submit"
              ml="-25px"
              mt="20px"
              bg="white"
              w="782px"
              size="lg"
              color="black"
              fontFamily="heading"
              _active={{ bg: 'gray.400', color: 'white', borderColor: '#bec3c9' }}
              _focus={{ bg: 'gray.400', color: 'white', borderColor: '#bec3c9' }}
              _hover={{ bg: 'gray.400', color: 'white', borderColor: '#bec3c9' }}>
              Become a Publisher
            </SubmitButton>
            <Center mt="20px">
              <Text>
                If you already have an account,
                <Button
                  mb="4px"
                  as="a"
                  color="white"
                  bg="none"
                  cursor="pointer"
                  _active={{ bg: 'none' }}
                  _hover={{ bg: 'none' }}
                  onClick={trigerPublisherSignin}>
                  Sign in
                </Button>
              </Text>
            </Center>
          </ModalBody>
        </ModalContent>
      </Modal>
      <CreateNewCompany
        isOpenCreateCompany={isOpenCreateCompany}
        onCloseCreateCompany={onCloseCreateCompany}
        setNewCompany={setNewCompany}
      />
    </>
  );
};

export default RegisterModal;
