/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  Divider,
  Flex,
  Box,
  Button,
  Image,
} from '@chakra-ui/react';

interface Props {
  isOpenPublisherRegister: boolean;
  onClosePublisherRegister: () => void;
  trigerNext: () => void;
  trigerPublisherSignin: () => void;
}

const SelectionModal: React.FC<Props> = (props) => {
  const {
    isOpenPublisherRegister,
    onClosePublisherRegister,
    trigerNext,
    trigerPublisherSignin,
  } = props;

  return (
    <Modal isOpen={isOpenPublisherRegister} onClose={onClosePublisherRegister} isCentered>
      <ModalOverlay />
      <ModalContent
        bg="blue.200"
        color="white"
        px="60px"
        py="50px"
        maxW="900px"
        borderBlock="none">
        <ModalCloseButton mx="20px" my="20px" fontSize="18px" />
        <ModalBody>
          <Flex alignItems="flex-end" justifyContent="space-between" width="full">
            <Box w="50%">
              <Button
                w="full"
                mb="110px"
                as="a"
                color="white"
                bg="none"
                cursor="pointer"
                _active={{ bg: 'none' }}
                _hover={{ bg: 'none' }}
                onClick={trigerPublisherSignin}
                flexDirection="column"
                fontSize="22px">
                <Image src="/images/login.png" width="200px" mb="20px" />
                Sign In
              </Button>
            </Box>
            <Divider
              orientation="vertical"
              mr="35px"
              ml="35px"
              color="white"
              border="2px"
              height="250px"
            />
            <Box w="50%">
              <Button
                w="full"
                mb="110px"
                as="a"
                color="white"
                bg="none"
                cursor="pointer"
                _active={{ bg: 'none' }}
                _hover={{ bg: 'none' }}
                onClick={trigerNext}
                flexDirection="column"
                fontSize="22px">
                <Image src="/images/register.png" width="200px" mb="20px" />
                Publisher Register
              </Button>
            </Box>
          </Flex>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

export default SelectionModal;
