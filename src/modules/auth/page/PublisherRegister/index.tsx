/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable @typescript-eslint/no-explicit-any */

import React, { useEffect } from 'react';

import { useToast, useDisclosure } from '@chakra-ui/react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import {
  useCreatePublisherMutation,
  PublisherRegisterInputDto,
} from 'common/generated/generated-types';

import PublisherLogin from 'modules/auth/page/PublisherLogin';
import RegisterModal from 'modules/auth/page/PublisherRegister/RegisterModal';
import SelectionModal from 'modules/auth/page/PublisherRegister/SelectionModal';
interface Props {
  isOpenPublisherRegister: boolean;
  onClosePublisherRegister: () => void;
}

const initialValues = {
  email: '',
  password: '',
  passwordConfirmation: '',
  firstname: '',
  lastname: '',
  middlename: '',
  phonenumber: '',
  job: '',
  companyId: '',
};

const nameRegex = /^[a-z][a-z0-9]*$/i;

const validationSchema = Yup.object({
  email: Yup.string().email().required(),
  password: Yup.string().required().min(6),
  passwordConfirmation: Yup.string().oneOf(
    [Yup.ref('password'), null],
    'Passwords must match',
  ),
  firstname: Yup.string().matches(nameRegex, 'Only English letters').required(),
  lastname: Yup.string().matches(nameRegex, 'Only English letters').required(),
  middlename: Yup.string().matches(nameRegex, 'Only English letters'),
  phonenumber: Yup.number().required(),
  job: Yup.string().matches(nameRegex, 'Only English letters').required(),
  companyId: Yup.string(),
});

const PublisherRegister: React.FC<Props> = (props) => {
  const toast = useToast();
  const { isOpenPublisherRegister, onClosePublisherRegister } = props;

  const [createAccountMutation, { data, loading, error }] = useCreatePublisherMutation({
    errorPolicy: 'all',
  });

  const {
    isOpen: isOpenNext,
    onClose: onCloseNext,
    onOpen: onOpenNext,
  } = useDisclosure();

  const {
    isOpen: isOpenPublisherLogin,
    onClose: onClosePublisherLogin,
    onOpen: onOpenPublisherLogin,
  } = useDisclosure();

  const trigerNext = async () => {
    await Promise.all([onClosePublisherRegister(), onOpenNext()]);
  };

  const trigerPublisherSignin = async () => {
    await Promise.all([
      onClosePublisherRegister(),
      onCloseNext(),
      onOpenPublisherLogin(),
    ]);
  };

  const trigerPublisherRegister = async () => {
    await Promise.all([onClosePublisherLogin(), trigerNext()]);
  };

  useEffect(() => {
    if (!error && data) {
      trigerPublisherSignin();
      toast({
        title: 'Account created.',
        description: "We've created your account.",
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
    } else {
      if (error) {
        toast({
          title: 'Error',
          description: error.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }
    }
  }, [data, loading, error]);

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const onSubmit = (values: PublisherRegisterInputDto, { resetForm }: any) => {
    sleep(500).then(() => {
      createAccountMutation({
        variables: {
          PublisherRegisterInputDto: {
            email: values.email,
            password: values.password,
            firstname: values.firstname,
            lastname: values.lastname,
            middlename: values.middlename,
            phonenumber: values.phonenumber,
            job: values.job,
            companyId: values.companyId,
          },
        },
      }).then(() => {
        resetForm(initialValues);
      });
    });
  };

  return (
    <>
      <SelectionModal
        isOpenPublisherRegister={isOpenPublisherRegister}
        onClosePublisherRegister={onClosePublisherRegister}
        trigerNext={trigerNext}
        trigerPublisherSignin={trigerPublisherSignin}
      />
      <PublisherLogin
        isOpenPublisherLogin={isOpenPublisherLogin}
        onClosePublisherLogin={onClosePublisherLogin}
        trigerPublisherRegister={trigerPublisherRegister}
      />
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}>
        {({ handleSubmit, setFieldValue }) => (
          <div onSubmit={handleSubmit as React.FormEventHandler}>
            <RegisterModal
              setFieldValue={setFieldValue}
              handleSubmit={handleSubmit}
              isOpenNext={isOpenNext}
              onCloseNext={onCloseNext}
              trigerPublisherSignin={trigerPublisherSignin}
            />
          </div>
        )}
      </Formik>
    </>
  );
};

export default PublisherRegister;
