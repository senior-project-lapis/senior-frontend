/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  ModalHeader,
  Divider,
  Heading,
  Stack,
  Text,
  Center,
  Flex,
  Box,
  Button,
  Image,
} from '@chakra-ui/react';
import { InputControl } from 'formik-chakra-ui';

interface Props {
  isOpenUserRegister: boolean;
  onCloseUserRegister: () => void;
  trigerNext: () => void;
  trigerSignin: () => void;
  values: Record<string, any>;
}

const FirstSubModal: React.FC<Props> = (props) => {
  const { isOpenUserRegister, onCloseUserRegister, trigerNext, trigerSignin, values } =
    props;

  return (
    <>
      <Modal isOpen={isOpenUserRegister} onClose={onCloseUserRegister} isCentered>
        <ModalOverlay />
        <ModalContent
          bg="blue.200"
          color="white"
          px="60px"
          py="50px"
          maxW="900px"
          borderBlock="none">
          <ModalCloseButton mx="20px" my="20px" fontSize="18px" />
          <ModalHeader ml="-25px">
            <Heading fontSize="26px" color="white">
              User Sign up
            </Heading>
          </ModalHeader>
          <Divider border="2px solid #FFFFFF" w="350px" />
          <ModalBody>
            <Flex alignItems="flex-end" justifyContent="space-between" width="full">
              <Box w="50%">
                <Image mx="-20px" src="/images/5237-removebg-preview.png" />
              </Box>
              <Box w="40%">
                <Stack spacing="4" as="form" mt="20px" mx="-25px">
                  <InputControl
                    name="email"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Email',
                      bg: 'white',
                      type: 'text',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                  <InputControl
                    name="password"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Password',
                      bg: 'white',
                      type: 'password',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                  <InputControl
                    name="passwordConfirmation"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Confirm Password',
                      bg: 'white',
                      type: 'password',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                  <Button
                    disabled={
                      values.email === '' ||
                      values.password === '' ||
                      values.passwordConfirmation === ''
                    }
                    bg="white"
                    w="full"
                    size="lg"
                    color="black"
                    fontFamily="heading"
                    _hover={{ bg: 'gray.400', color: 'white' }}
                    onClick={trigerNext}>
                    Next
                  </Button>

                  <Center>
                    <Text>
                      If you already have an account,
                      <Button
                        mb="4px"
                        as="a"
                        color="white"
                        bg="none"
                        cursor="pointer"
                        _active={{ bg: 'none' }}
                        _hover={{ bg: 'none' }}
                        onClick={trigerSignin}>
                        Sign In
                      </Button>
                    </Text>
                  </Center>
                </Stack>
              </Box>
            </Flex>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default FirstSubModal;
