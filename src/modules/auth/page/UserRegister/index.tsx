/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';

import { useToast, useDisclosure } from '@chakra-ui/react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import {
  useCreateUserMutation,
  UserRegisterInputDto,
} from 'common/generated/generated-types';

import FirstSubModal from 'modules/auth/page/UserRegister/FirstSubModal';
import SecondSubModal from 'modules/auth/page/UserRegister/SecondSubModal';

interface Props {
  isOpenUserRegister: boolean;
  onCloseUserRegister: () => void;
  onOpenUserRegister: () => void;
  onOpenLogin: () => void;
}

const initialValues = {
  email: '',
  password: '',
  passwordConfirmation: '',
  firstname: '',
  lastname: '',
  middlename: '',
  dob: '',
  phonenumber: '',
  address: '',
};

const nameRegex = /^[a-z][a-z0-9]*$/i;
const addressRegex =
  /[0-9]*[ |[a-zà-ú.,-]* ((highway)|(autoroute)|(north)|(nord)|(south)|(sud)|(east)|(est)|(west)|(ouest)|(avenue)|(lane)|(voie)|(ruelle)|(road)|(rue)|(route)|(drive)|(boulevard)|(circle)|(cercle)|(street)|(cer\.?)|(cir\.?)|(blvd\.?)|(hway\.?)|(st\.?)|(aut\.?)|(ave\.?)|(ln\.?)|(rd\.?)|(hw\.?)|(dr\.?)|(a\.))([ .,-]*[a-zà-ú0-9]*)*/i;

const validationSchema = Yup.object({
  email: Yup.string().email().required(),
  password: Yup.string().required().min(6),
  passwordConfirmation: Yup.string().oneOf(
    [Yup.ref('password'), null],
    'Passwords must match',
  ),
  firstname: Yup.string().matches(nameRegex, 'Only English letters').required(),
  lastname: Yup.string().matches(nameRegex, 'Only English letters').required(),
  middlename: Yup.string().matches(nameRegex, 'Only English letters'),
  phonenumber: Yup.number().required(),
  address: Yup.string().matches(addressRegex, 'Not match on address format').required(),
  dob: Yup.date().required(),
});

const UserRegister: React.FC<Props> = (props) => {
  const toast = useToast();
  const { onOpenUserRegister, isOpenUserRegister, onCloseUserRegister, onOpenLogin } =
    props;

  const [createAccountMutation, { data, loading, error }] = useCreateUserMutation({
    errorPolicy: 'all',
  });

  const {
    isOpen: isOpenNext,
    onClose: onCloseNext,
    onOpen: onOpenNext,
  } = useDisclosure();

  const trigerNext = async () => {
    await Promise.all([onCloseUserRegister(), onOpenNext()]);
  };
  const trigerBack = async () => {
    await Promise.all([onCloseNext(), onOpenUserRegister()]);
  };

  const trigerSignin = async () => {
    await Promise.all([onCloseUserRegister(), onCloseNext(), onOpenLogin()]);
  };

  useEffect(() => {
    if (!error && data) {
      trigerSignin();
      toast({
        title: 'Account created.',
        description: "We've created your account.",
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
    } else {
      if (error) {
        trigerBack();
        toast({
          title: 'Error',
          description: error.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }
    }
  }, [data, loading, error]);

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const onSubmit = (values: UserRegisterInputDto, { resetForm }: any) => {
    sleep(500).then(() => {
      createAccountMutation({
        variables: {
          UserRegisterInputDto: {
            email: values.email,
            password: values.password,
            address: values.address,
            dob: values.dob,
            firstname: values.firstname,
            lastname: values.lastname,
            middlename: values.middlename,
            phonenumber: values.phonenumber,
          },
        },
      }).then(() => {
        resetForm(initialValues);
      });
    });
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}>
        {({ handleSubmit, values }) => (
          <>
            <div onSubmit={handleSubmit as React.FormEventHandler}>
              <FirstSubModal
                values={values}
                isOpenUserRegister={isOpenUserRegister}
                onCloseUserRegister={onCloseUserRegister}
                trigerNext={trigerNext}
                trigerSignin={trigerSignin}
              />
              <SecondSubModal
                isOpenNext={isOpenNext}
                onCloseNext={onCloseNext}
                trigerBack={trigerBack}
              />
            </div>
          </>
        )}
      </Formik>
    </>
  );
};

export default UserRegister;
