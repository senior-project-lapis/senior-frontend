/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  ModalHeader,
  Divider,
  Heading,
  Stack,
  Box,
  Flex,
  Button,
} from '@chakra-ui/react';
import { InputControl, SubmitButton } from 'formik-chakra-ui';

import { DatePickerControl } from 'common/components/DatePickerControl';
interface Props {
  isOpenNext: boolean;
  onCloseNext: () => void;
  trigerBack: () => void;
}

const SecondSubModal: React.FC<Props> = (props) => {
  const { isOpenNext, onCloseNext, trigerBack } = props;

  return (
    <Modal isOpen={isOpenNext} onClose={onCloseNext} isCentered>
      <ModalOverlay />
      <ModalContent
        bg="blue.200"
        color="white"
        px="60px"
        py="50px"
        maxW="900px"
        pb="70px"
        borderBlock="none">
        <ModalCloseButton mx="20px" my="20px" fontSize="18px" />
        <ModalHeader ml="-25px">
          <Heading fontSize="26px" color="white">
            User Sign up
          </Heading>
        </ModalHeader>
        <Divider border="2px solid #FFFFFF" w="350px" />
        <ModalBody>
          <Flex alignItems="flex-end" justifyContent="space-between" width="full">
            <Box w="45%">
              <Stack spacing="5" as="form" mt="20px" mx="-25px">
                <InputControl
                  name="firstname"
                  inputProps={{
                    _placeholder: { color: 'blackAlpha.500' },
                    placeholder: 'First Name',
                    bg: 'white',
                    type: 'text',
                    color: 'blackAlpha.900',
                    size: 'md',
                  }}
                />
                <InputControl
                  name="middlename"
                  inputProps={{
                    _placeholder: { color: 'blackAlpha.500' },
                    placeholder: 'Middle Name',
                    bg: 'white',
                    type: 'text',
                    color: 'blackAlpha.900',
                    size: 'md',
                  }}
                />
                <InputControl
                  name="phonenumber"
                  inputProps={{
                    _placeholder: { color: 'blackAlpha.500' },
                    placeholder: 'Phone Number',
                    bg: 'white',
                    type: 'text',
                    color: 'blackAlpha.900',
                    size: 'md',
                  }}
                />
                <Button
                  bg="white"
                  w="full"
                  size="lg"
                  color="black"
                  fontFamily="heading"
                  _hover={{ bg: 'gray.400', color: 'white' }}
                  onClick={trigerBack}>
                  Back
                </Button>
              </Stack>
            </Box>
            <Box w="45%">
              <Stack spacing="5" as="form" mt="20px" mx="-25px">
                <InputControl
                  name="lastname"
                  inputProps={{
                    _placeholder: { color: 'blackAlpha.500' },
                    placeholder: 'Last Name',
                    bg: 'white',
                    type: 'text',
                    color: 'blackAlpha.900',
                    size: 'md',
                  }}
                />

                {/* TODO:need to change this cuz toast can't display */}
                <DatePickerControl
                  name="dob"
                  propsConfigs={{
                    dateNavBtnProps: {
                      color: 'blackAlpha.900',
                      bg: 'white',
                    },
                    dayOfMonthBtnProps: {
                      color: 'blackAlpha.900',
                      borderColor: 'red.300',
                      selectedBg: 'blue.200',
                      _hover: {
                        bg: 'green.400',
                      },
                    },
                    inputProps: {
                      cursor: 'pointer',
                      type: 'text',
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Date of birth',
                      bg: 'white',
                      color: 'blackAlpha.900',
                      size: 'md',
                    },
                  }}
                />
                <InputControl
                  name="address"
                  inputProps={{
                    _placeholder: { color: 'blackAlpha.500' },
                    placeholder: 'Address',
                    bg: 'white',
                    type: 'text',
                    color: 'blackAlpha.900',
                    size: 'md',
                  }}
                />
                <SubmitButton
                  bg="white"
                  w="full"
                  size="lg"
                  color="black"
                  fontFamily="heading"
                  _active={{ bg: 'gray.400', color: 'white', borderColor: '#bec3c9' }}
                  _focus={{ bg: 'gray.400', color: 'white', borderColor: '#bec3c9' }}
                  _hover={{ bg: 'gray.400', color: 'white', borderColor: '#bec3c9' }}>
                  Sign Up
                </SubmitButton>
              </Stack>
            </Box>
          </Flex>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

export default SecondSubModal;
