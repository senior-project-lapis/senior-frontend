/* eslint-disable react/no-unescaped-entities */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  ModalHeader,
  Divider,
  Heading,
  Stack,
  Button,
  Text,
  Center,
  useToast,
} from '@chakra-ui/react';
import { Formik } from 'formik';
import { InputControl, SubmitButton } from 'formik-chakra-ui';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';

import {
  useLoginPublisherMutation,
  UserLoginInputDto,
} from 'common/generated/generated-types';

import { publisherActions } from 'modules/auth/reducers/publisherReducer';

interface Props {
  isOpenPublisherLogin: boolean;
  onClosePublisherLogin: () => void;
  trigerPublisherRegister: () => void;
}

const initialValues = {
  email: '',
  password: '',
};

const validationSchema = Yup.object({
  email: Yup.string().email().required(),
  password: Yup.string().required(),
});

const PublisherLogin: React.FC<Props> = (props) => {
  const toast = useToast();
  const { isOpenPublisherLogin, onClosePublisherLogin, trigerPublisherRegister } = props;
  const dispatch = useDispatch();
  const router = useRouter();
  const [loginMutation, { data, loading, error }] = useLoginPublisherMutation({
    errorPolicy: 'all',
  });

  useEffect(() => {
    if (!error && data) {
      const duplicated = localStorage.getItem('publisher');
      if (duplicated) localStorage.removeItem('publisher');
      localStorage.setItem('publisher', JSON.stringify(data.LoginPublisher));
      dispatch(
        publisherActions.setPublisher({
          id: data.LoginPublisher._id,
          email: data.LoginPublisher.email,
          firstname: data.LoginPublisher.firstname,
          middlename: data?.LoginPublisher.middlename || undefined,
          lastname: data.LoginPublisher.lastname,
          phonenumber: data.LoginPublisher.phonenumber,
          job: data.LoginPublisher.job,
          companyId: data.LoginPublisher.companyId,
          role: data.LoginPublisher.role,
        }),
      );

      onClosePublisherLogin();
      toast({
        title: 'Login successfully.',
        description: "We've Loged your in.",
        status: 'success',
        duration: 9000,
        isClosable: true,
      });
      router.push('/publisher', undefined, { shallow: true });
    } else {
      if (error) {
        // console.log(error, 'error');

        toast({
          title: 'Error',
          description: error.message,
          status: 'error',
          duration: 9000,
          isClosable: true,
        });
      }
    }
  }, [data, error, loading, router, dispatch]);

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

  const onSubmit = (values: UserLoginInputDto, { resetForm }: any) => {
    sleep(500).then(() => {
      loginMutation({
        variables: {
          UserLoginInputDTO: {
            email: values.email,
            password: values.password,
          },
        },
      }).then(() => {
        resetForm(initialValues);
      });
    });
  };

  return (
    <>
      <Modal isOpen={isOpenPublisherLogin} onClose={onClosePublisherLogin} isCentered>
        <ModalOverlay />
        <ModalContent
          bg="blue.200"
          color="white"
          px="50px"
          py="60px"
          maxW="500px"
          borderBlock="none">
          <ModalCloseButton mx="20px" my="20px" fontSize="18px" />
          <ModalHeader ml="-25px">
            <Heading fontSize="26px" color="white">
              Publisher Sign in
            </Heading>
          </ModalHeader>
          <Divider border="2px solid #FFFFFF" />
          <ModalBody>
            <Formik
              initialValues={initialValues}
              onSubmit={onSubmit}
              validationSchema={validationSchema}>
              {({ handleSubmit }) => (
                <Stack
                  spacing="3"
                  as="form"
                  onSubmit={handleSubmit as React.FormEventHandler}
                  mt="20px"
                  mx="-25px">
                  <InputControl
                    name="email"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Email',
                      bg: 'white',
                      type: 'text',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                  <InputControl
                    name="password"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Password',
                      bg: 'white',
                      type: 'password',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                  <SubmitButton
                    size="lg"
                    bg="white"
                    color="blackAlpha.900"
                    fontFamily="heading"
                    _active={{ bg: 'gray.400', color: 'white', borderColor: '#bec3c9' }}
                    _focus={{ bg: 'gray.400', color: 'white', borderColor: '#bec3c9' }}
                    _hover={{ bg: 'gray.400', color: 'white', borderColor: '#bec3c9' }}>
                    Sign In
                  </SubmitButton>
                  <Center>
                    <Text>
                      If you don't have an account,
                      <Button
                        mb="4px"
                        as="a"
                        color="white"
                        bg="none"
                        cursor="pointer"
                        _active={{ bg: 'none' }}
                        _hover={{ bg: 'none' }}
                        onClick={trigerPublisherRegister}>
                        Sign Up
                      </Button>
                    </Text>
                  </Center>
                </Stack>
              )}
            </Formik>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default PublisherLogin;
