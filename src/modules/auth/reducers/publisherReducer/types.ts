import { UserRoleEnum } from 'common/generated/generated-types';

export const PUBLISHER = 'PUBLISHER';

export type Publisher = {
  id: string;
  email: string;
  firstname: string;
  middlename?: string;
  lastname: string;
  phonenumber: string;
  job: string;
  companyId: string;
  role: UserRoleEnum;
};

export type PublisherState = {
  publisher: Publisher;
  authenticated: boolean;
};
