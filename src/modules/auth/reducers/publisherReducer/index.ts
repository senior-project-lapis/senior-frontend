import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { initPublisherState } from './init';
import { Publisher, PUBLISHER } from './types';

const publisherSlice = createSlice({
  name: PUBLISHER,
  initialState: initPublisherState,
  reducers: {
    init(state) {
      return { ...state, ...initPublisherState };
    },
    setPublisher(state, action: PayloadAction<Publisher>) {
      state.publisher = action.payload;
      state.authenticated = true;
      // console.log(action.payload, 'action');
    },
    deletePublisher(state) {
      return { ...state, ...initPublisherState };
    },
  },
});

export const publisherActions = publisherSlice.actions;
export default publisherSlice.reducer;
