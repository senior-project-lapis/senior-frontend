import { UserRoleEnum } from 'common/generated/generated-types';

import { PublisherState } from './types';
export const initPublisherState: PublisherState = {
  publisher: {
    id: '',
    email: '',
    firstname: '',
    middlename: '',
    lastname: '',
    phonenumber: '',
    job: '',
    companyId: '',
    role: UserRoleEnum.Publisher,
  },
  authenticated: false,
};
