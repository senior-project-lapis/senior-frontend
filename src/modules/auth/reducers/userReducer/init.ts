import { UserRoleEnum } from 'common/generated/generated-types';

import { UserState } from './types';
export const initUserState: UserState = {
  user: {
    id: '',
    email: '',
    firstname: '',
    middlename: '',
    lastname: '',
    phonenumber: '',
    dob: '',
    address: '',
    role: UserRoleEnum.User,
  },
  authenticated: false,
};
