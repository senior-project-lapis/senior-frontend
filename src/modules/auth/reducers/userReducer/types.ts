import { UserRoleEnum } from 'common/generated/generated-types';

export const USER = 'USER';

export type User = {
  id: string;
  email: string;
  firstname: string;
  middlename?: string;
  lastname: string;
  phonenumber: string;
  dob: string;
  address: string;
  role: UserRoleEnum;
};

export type UserState = {
  user: User;
  authenticated: boolean;
};
