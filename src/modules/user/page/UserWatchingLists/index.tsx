/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState } from 'react';

import { Flex, Heading, SimpleGrid, Divider, Text } from '@chakra-ui/react';
import Head from 'next/head';

import SidebarLayout from 'common/components/SidebarLayout';
import { useGetWatchingListByUserIdQuery } from 'common/generated/generated-types';
import { useGetUserFromSession } from 'common/generated/page';

import Footer from 'modules/root/components/Footer';
import WatchingListCard from 'modules/user/components/WatchingListCard';
import { USER_MENU_CONSTANT } from 'modules/user/utils/constants';

const UserWatchingLists: React.FC = () => {
  const { data: userSession } = useGetUserFromSession();
  const userId: string = userSession?.GetUserBySession._id;
  const [click, setClick] = useState(false);
  const { data: existWatchingList, refetch } = useGetWatchingListByUserIdQuery({
    variables: {
      userId: userId,
    },
  });
  const handleClick = (e: React.MouseEvent, bool: boolean) => {
    setClick(bool);
  };
  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  useEffect(() => {
    sleep(500).then(() => {
      refetch();
    });
  }, [click]);

  return (
    <>
      <Head>
        <title>Lapis | User Watching Lists</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <SidebarLayout constant={USER_MENU_CONSTANT}>
        <Flex minHeight="100vh" w="100%" flexDirection="column">
          <Flex mt="20px" width="100%" flexDirection="column">
            <Heading fontSize="20px">Watching Lists</Heading>
            <Divider
              mt="10px"
              border="1px solid rgba(0, 0, 0, 0.6)"
              width="100%"
              borderColor="gray.700"
            />
          </Flex>
          {existWatchingList?.WatchingLists[0].list.length > 0 ? (
            <SimpleGrid columns={2} spacing="10px" mt="20px">
              {existWatchingList?.WatchingLists[0].list?.map((watching, i) => (
                <WatchingListCard
                  key={i}
                  data={watching}
                  onCardClick={(e) => handleClick(e, !click)}
                />
              ))}
            </SimpleGrid>
          ) : (
            <Flex h="50%" justifyContent="center" alignItems="center">
              <Text>Sorry, can't find your watching list :(</Text>
            </Flex>
          )}
        </Flex>
      </SidebarLayout>
      <Footer />
    </>
  );
};

export default UserWatchingLists;
