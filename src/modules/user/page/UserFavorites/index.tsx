/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState } from 'react';

import { Flex, Heading, Divider, SimpleGrid, Text } from '@chakra-ui/react';
import Head from 'next/head';

import SidebarLayout from 'common/components/SidebarLayout';
import { useGetFavoritesByUserIdQuery } from 'common/generated/generated-types';
import { useGetUserFromSession } from 'common/generated/page';

import Footer from 'modules/root/components/Footer';
import FavCard from 'modules/user/components/FavCard';
import { USER_MENU_CONSTANT } from 'modules/user/utils/constants';
const UserFavorites: React.FC = () => {
  const { data: userSession } = useGetUserFromSession();
  const [click, setClick] = useState(false);
  const userId: string = userSession?.GetUserBySession._id;
  const { data: existingFav, refetch } = useGetFavoritesByUserIdQuery({
    variables: {
      userId: userId,
    },
  });
  const favList = existingFav?.Favorites || undefined;
  const handleClick = (e: React.MouseEvent, bool: boolean) => {
    setClick(bool);
  };
  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  useEffect(() => {
    sleep(500).then(() => {
      refetch();
    });
  }, [click]);

  return (
    <>
      <Head>
        <title>Lapis | User Favorites</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <SidebarLayout constant={USER_MENU_CONSTANT}>
        <Flex minHeight="100vh" w="100%" flexDirection="column">
          <Flex mt="20px" width="100%" flexDirection="column">
            <Heading fontSize="20px">Favorite Lists</Heading>
            <Divider
              mt="10px"
              border="1px solid rgba(0, 0, 0, 0.6)"
              width="100%"
              borderColor="gray.700"
            />
          </Flex>
          {favList?.length > 0 ? (
            <SimpleGrid columns={3} spacing="10px" mt="20px">
              {favList?.map((fav, i) => (
                <FavCard key={i} data={fav} onCardClick={(e) => handleClick(e, !click)} />
              ))}
            </SimpleGrid>
          ) : (
            <Flex h="50%" justifyContent="center" alignItems="center">
              <Text>Sorry, can't find your favorites data :(</Text>
            </Flex>
          )}
        </Flex>
      </SidebarLayout>
      <Footer />
    </>
  );
};

export default UserFavorites;
