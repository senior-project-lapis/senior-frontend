import React from 'react';

import { Flex } from '@chakra-ui/react';
import Head from 'next/head';

import SidebarLayout from 'common/components/SidebarLayout';

import Footer from 'modules/root/components/Footer';
import ProfilePage from 'modules/user/components/ProfilePage';
import { USER_MENU_CONSTANT } from 'modules/user/utils/constants';

const UserProfile: React.FC = () => {
  return (
    <>
      <Head>
        <title>Lapis | User Profile</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <SidebarLayout constant={USER_MENU_CONSTANT}>
        <Flex w="100%">
          <ProfilePage />
        </Flex>
      </SidebarLayout>
      <Footer />
    </>
  );
};

export default UserProfile;
