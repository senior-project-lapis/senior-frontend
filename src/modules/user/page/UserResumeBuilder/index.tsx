import React from 'react';

import { Flex, Heading } from '@chakra-ui/react';
import Head from 'next/head';

import SidebarLayout from 'common/components/SidebarLayout';

import Footer from 'modules/root/components/Footer';
import ResumePage from 'modules/user/components/ResumePage';
import { USER_MENU_CONSTANT } from 'modules/user/utils/constants';

const UserResumeBuilder: React.FC = () => {
  return (
    <>
      <Head>
        <title>Lapis | User Resume Builder</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <SidebarLayout constant={USER_MENU_CONSTANT}>
        <Flex minHeight={'100vh'}>
          <ResumePage />
        </Flex>
      </SidebarLayout>
      <Footer />
    </>
  );
};

export default UserResumeBuilder;
