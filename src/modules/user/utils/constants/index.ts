import { MenuType } from 'common/constants/menu/types';
export const USER_MENU_CONSTANT: MenuType[] = [
  {
    key: '1',
    label: 'Profile',
    link: ['/user/profile'],
  },
  {
    key: '2',
    label: 'Favorites',
    link: ['/user/favorites'],
  },
  {
    key: '3',
    label: 'Resume Builder',
    link: ['/user/resume-builder'],
  },
  {
    key: '4',
    label: 'Watching Lists',
    link: ['/user/watching-lists'],
  },
];
