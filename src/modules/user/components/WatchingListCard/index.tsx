/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import { Flex, Text, Box } from '@chakra-ui/react';

import WatchingListBtn from 'common/components/WatchingListBtn';

interface Props {
  data?: any;
  onCardClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
}

const WatchingListCard: React.FC<Props> = (Props) => {
  const { data, onCardClick } = Props;
  return (
    <>
      <Flex
        bg="gray.200"
        w="330px"
        h="220px"
        justifyContent="space-evenly"
        borderRadius="5px"
        flexDirection="column">
        <Flex p="30px" flexDirection="column">
          <Flex width="100%" flexDirection="column">
            <Text fontSize="16px" fontFamily="medium" noOfLines={2}>
              {data.name}
            </Text>
          </Flex>
          <Flex
            mt="10px"
            justifyContent="flex-start"
            flexDirection="row"
            alignItems="center">
            <Text fontSize="16px" fontFamily="medium">
              Type :
            </Text>
            <Text fontSize="16px" display="flex">
              &nbsp;&nbsp; {data.listType}
            </Text>
          </Flex>
          <Flex
            mt="10px"
            justifyContent="flex-start"
            flexDirection="row"
            alignItems="center">
            <Text fontSize="16px" fontFamily="medium">
              Location :
            </Text>
            <Text fontSize="16px" display="flex">
              &nbsp;&nbsp; {data.location}
            </Text>
          </Flex>
          <Flex mt="10px" justifyContent="space-evenly">
            <Box onClick={onCardClick}>
              <WatchingListBtn data={data} watchingType={data.listType} isCard />
            </Box>
          </Flex>
        </Flex>
      </Flex>
    </>
  );
};

export default WatchingListCard;
