/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useEffect } from 'react';

import { PhoneIcon, EmailIcon } from '@chakra-ui/icons';
import {
  Heading,
  Divider,
  Text,
  Stack,
  Flex,
  Box,
  GridItem,
  Grid,
  Center,
  SimpleGrid,
} from '@chakra-ui/react';
import dynamic from 'next/dynamic';

import { useGetUserFromSession } from 'common/generated/page';

import ColorPicker from 'modules/user/components/ColorPicker';

const ResumePage: React.FC = () => {
  const ref = React.useRef();
  const GeneratePDF = dynamic(() => import('modules/user/components/GeneratePdf'), {
    ssr: false,
  });
  const [user, setUser] = useState<any>(null);
  const [color, setColor] = useState<string>('blue.500');
  const [secondaryColor, setSecondaryColor] = useState<string>('black');
  const [bgColor, setBgColor] = useState<string>('gray.100');
  const { data, loading } = useGetUserFromSession();

  const sizing = {
    pageSize: {
      width: '21cm',
      height: '29.7cm',
    },
    fontSize: {
      nameSize: '30',
      paragraphSize: '14',
      topicSize: '16',
      contactSize: '14',
    },
    logoSize: '1.5em',
  };

  // const sizing = {
  //   pageSize: {
  //     width: '42cm',
  //     height: '59.4cm',
  //   },
  //   fontSize: {
  //     nameSize: '60',
  //     paragraphSize: '28',
  //     topicSize: '32',
  //     contactSize: '28',
  //   },
  //   logoSize: '1.5em',
  // };

  useEffect(() => {
    setUser(data?.GetUserBySession);
  }, [loading]);

  const CapitalizeFirstLetter = (string: string | null) => {
    return string ? string.charAt(0).toUpperCase() + string.slice(1) : string;
  };
  const getPrimaryColor = (newColor: string) => setColor(newColor);
  const getBgColor = (newColor: string) => setBgColor(newColor);
  const getSecondaryColor = (newColor: string) => setSecondaryColor(newColor);

  const userProfileValidation = (userProfile: any) => {
    const isExperienceCorrect = userProfile?.experience.length > 0;
    const isEducationCorrect = userProfile?.education.length > 0;
    const isSkillCorrect = userProfile?.skill.length > 0;
    return isExperienceCorrect && isEducationCorrect && isSkillCorrect;
  };

  const YearHandler = (year: number) => (year == 1 ? 'year' : 'years');
  const getEducationLevel = (education: string) => {
    let letter;
    switch (true) {
      case education === 'bachelor':
        letter = "Bachelor's Degree";
        break;
      case education === 'master':
        letter = "Master's Degree";
        break;
      case education === 'doctoral':
        letter = 'Doctoral Degree';
        break;
      case education === 'associate':
        letter = "Associate's Degree";
        break;
      case education === 'highShool':
        letter = 'High School or Equivalent';
        break;
      case education === 'lessThanHighSchool':
        letter = 'Less Than High School';
        break;
    }
    return letter;
  };

  return (
    <>
      <Stack spacing={4} width="100%">
        <Stack spacing={2}>
          <Flex justifyContent="space-between" mt="20px" width="100%" alignItems="center">
            <Heading fontSize="20px">Resume Builder</Heading>
          </Flex>
          <Divider
            border="1px solid rgba(0, 0, 0, 0.6)"
            width="100%"
            borderColor="gray.700"
          />
          {userProfileValidation(user) ? (
            <>
              <Box>
                <SimpleGrid columns={3} spacing={2} padding="5">
                  <Box>
                    <ColorPicker initColor={color} changeColor={getPrimaryColor} />{' '}
                    <Text marginLeft="2" as="span">
                      Select Primary color
                    </Text>
                  </Box>
                  <Box>
                    <ColorPicker
                      initColor={secondaryColor}
                      changeColor={getSecondaryColor}
                    />
                    <Text marginLeft="2" as="span">
                      Select Text color
                    </Text>
                  </Box>
                  <Box>
                    <ColorPicker initColor={bgColor} changeColor={getBgColor} />
                    <Text marginLeft="2" as="span">
                      Select Background color
                    </Text>
                  </Box>
                </SimpleGrid>
                <Box>
                  <Box
                    id="text"
                    className="content"
                    ref={ref}
                    width={sizing.pageSize.width}
                    height={sizing.pageSize.height}
                    overflowY="hidden"
                    bg={bgColor}
                    padding="1cm">
                    <Grid
                      templateRows="repeat(1, 1fr)"
                      templateColumns="repeat(5, 1fr)"
                      gap={4}>
                      <GridItem colSpan={3} padding="3">
                        {/* Name */}
                        <Text
                          mt="2"
                          fontSize={sizing.fontSize.nameSize}
                          fontWeight="bold"
                          color={secondaryColor}>
                          {CapitalizeFirstLetter(user?.firstname)}{' '}
                          {CapitalizeFirstLetter(user?.middlename)}{' '}
                          {CapitalizeFirstLetter(user?.lastname)}
                        </Text>
                      </GridItem>
                      <GridItem colSpan={2} padding="2">
                        <Center>
                          <Stack fontSize={sizing.fontSize.contactSize}>
                            <Box>
                              <EmailIcon boxSize={sizing.logoSize} color={color} />
                              <Text
                                as="span"
                                padding="2"
                                color={secondaryColor}>{`   ${user?.email}`}</Text>
                            </Box>
                            <Box>
                              <PhoneIcon boxSize={sizing.logoSize} color={color} />
                              <Text
                                as="span"
                                padding="2"
                                color={secondaryColor}>{`   ${user?.phonenumber}`}</Text>
                            </Box>
                          </Stack>
                        </Center>
                      </GridItem>
                    </Grid>

                    <Divider
                      marginTop="5"
                      border={`2px solid ${color}`}
                      width="100%"
                      borderColor={color}
                    />

                    {/* About me */}
                    <Box marginTop="5">
                      <Text
                        color={color}
                        mt="1"
                        fontWeight="extrabold"
                        fontSize={sizing.fontSize.topicSize}
                        lineHeight="tight">
                        ABOUT ME
                      </Text>
                      <Text
                        mt="1"
                        fontSize={sizing.fontSize.paragraphSize}
                        lineHeight="tight"
                        marginLeft="5"
                        marginRight="5"
                        textAlign="justify"
                        color={secondaryColor}>
                        {user?.description}
                      </Text>
                    </Box>

                    <Divider
                      marginTop="5"
                      border="0.3px solid #D3D3D3"
                      width="100%"
                      borderColor="#D3D3D3"
                    />

                    {/* Experience */}
                    <Box marginTop="5">
                      <Text
                        mt="1"
                        color={color}
                        fontWeight="extrabold"
                        fontSize={sizing.fontSize.topicSize}
                        lineHeight="tight">
                        EXPERIENCES
                      </Text>
                      <Stack display="flex" mt="2" marginLeft="5" marginRight="5">
                        {user?.experience.map((item: any, index: number) => (
                          <Text
                            key={index}
                            color={secondaryColor}
                            mt="1"
                            fontSize={sizing.fontSize.paragraphSize}
                            lineHeight="tight"
                            fontWeight="bold">
                            {item.name}
                            <Text as="span" fontWeight="normal">{` - [${
                              item.year
                            } ${YearHandler(item.year)}]`}</Text>
                            <Text textAlign="justify" fontWeight="normal">
                              {item.description}
                            </Text>
                          </Text>
                        ))}
                      </Stack>
                    </Box>

                    <Divider
                      marginTop="5"
                      border="0.3px solid #D3D3D3"
                      width="100%"
                      borderColor="#D3D3D3"
                    />

                    {/* Skills */}
                    <Box marginTop="5">
                      <Text
                        mt="1"
                        color={color}
                        fontWeight="extrabold"
                        fontSize={sizing.fontSize.topicSize}
                        lineHeight="tight">
                        SKILLS
                      </Text>
                      <Grid
                        marginLeft="5"
                        templateRows={`repeat(${Math.ceil(user?.skill.length / 2)}, 1fr)`}
                        templateColumns="repeat(2, 1fr)"
                        gap={4}>
                        {user?.skill.map((item: any, index: number) => (
                          <>
                            <GridItem colSpan={1}>
                              {/* Name */}
                              <Text
                                key={index}
                                mt="1"
                                color={secondaryColor}
                                fontWeight="bold"
                                fontSize={sizing.fontSize.paragraphSize}
                                lineHeight="tight">
                                {CapitalizeFirstLetter(item.name)}
                              </Text>
                            </GridItem>
                          </>
                        ))}
                      </Grid>
                    </Box>

                    <Divider
                      marginTop="5"
                      border="0.3px solid #D3D3D3"
                      width="100%"
                      borderColor="#D3D3D3"
                    />

                    {/* Educations */}
                    <Box marginTop="5">
                      <Text
                        color={color}
                        mt="1"
                        fontWeight="bold"
                        fontSize={sizing.fontSize.topicSize}
                        lineHeight="tight">
                        Educations
                      </Text>
                      <Stack display="flex" mt="2" marginLeft="5">
                        {user?.education.map((item: any, index: number) => (
                          <Text
                            key={index}
                            mt="1"
                            color={secondaryColor}
                            fontSize={sizing.fontSize.paragraphSize}
                            lineHeight="tight"
                            fontWeight="bold">
                            <Text fontWeight="bold">{item.school}</Text>
                            <Text fontWeight="normal">
                              {getEducationLevel(item.educationLevel)}
                            </Text>
                            <Text fontWeight="normal">{item.graduationYear}</Text>
                          </Text>
                        ))}
                      </Stack>
                    </Box>
                  </Box>
                </Box>
                <GeneratePDF
                  html={ref}
                  filename={`${user?.firstname}_${user?.lastname}_resume`}
                />
              </Box>
            </>
          ) : (
            <Box width="21cm" padding="1cm">
              <Center>
                <Text>
                  Not enought experience, skill or education to generate resume.
                </Text>
              </Center>
            </Box>
          )}
        </Stack>
      </Stack>
    </>
  );
};

export default ResumePage;
