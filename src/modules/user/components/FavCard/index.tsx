/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import { Flex, Button, Text, Box, useDisclosure } from '@chakra-ui/react';
import { BiBuildings } from 'react-icons/bi';
import { GrMapLocation } from 'react-icons/gr';

import FavoriteBtn from 'common/components/FavoriteBtn';

import FavCardDetails from 'modules/user/components/FavCardDetails';
interface Props {
  data?: any;
  onCardClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
}

const FavCard: React.FC<Props> = (Props) => {
  const { data, onCardClick } = Props;
  const {
    isOpen: isOpenFavDetails,
    onClose: onCloseFavDetails,
    onOpen: onOpenFavDetails,
  } = useDisclosure();

  return (
    <>
      <Flex
        bg="gray.200"
        w="220px"
        h="170px"
        p="10px"
        justifyContent="center"
        borderRadius="5px"
        flexDirection="column">
        <Flex width="100%" flexDirection="column">
          <Text fontSize="16px" fontFamily="medium" noOfLines={1}>
            {data.name}
          </Text>
        </Flex>
        <Flex
          mt="10px"
          just
          ifyContent="flex-start"
          flexDirection="row"
          alignItems="center">
          <Text fontSize="16px" fontFamily="medium">
            <BiBuildings />
          </Text>
          <Text fontSize="16px" display="flex">
            &nbsp;&nbsp; {data.companyName}
          </Text>
        </Flex>
        <Flex
          mt="10px"
          justifyContent="flex-start"
          flexDirection="row"
          alignItems="center">
          <Text fontSize="16px" fontFamily="medium">
            <GrMapLocation />
          </Text>
          <Text fontSize="16px" display="flex">
            &nbsp;&nbsp; {data.locations[0]}
          </Text>
        </Flex>
        <Flex mt="10px" justifyContent="space-evenly">
          {/* <NextLink href={`/`} passHref>÷ */}
          <Button
            variant="orange"
            fontSize="14px"
            size="sm"
            w="130px"
            onClick={onOpenFavDetails}>
            See details
          </Button>
          {/* </NextLink> */}
          <Box onClick={onCardClick}>
            <FavoriteBtn jobId={data._id} size="sm" />
          </Box>
        </Flex>
      </Flex>
      <FavCardDetails
        isOpenFavDetails={isOpenFavDetails}
        onCloseFavDetails={onCloseFavDetails}
        jobDetails={data}
      />
    </>
  );
};

export default FavCard;
