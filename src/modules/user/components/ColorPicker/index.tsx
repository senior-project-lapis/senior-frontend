/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState } from 'react';

import {
  Input,
  Center,
  Button,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverArrow,
  PopoverBody,
  PopoverCloseButton,
  PopoverHeader,
  SimpleGrid,
} from '@chakra-ui/react';

interface Props {
  changeColor: any;
  initColor: string;
}

const ColorPicker: React.FC<Props> = (props) => {
  const [color, setColor] = useState(props.initColor);

  const handleColor = (newColor: string) => {
    setColor(newColor);
    props.changeColor(newColor);
  };
  const colors = [
    'white',
    'black',
    'gray.500',
    'red.500',
    'gray.700',
    'green.500',
    'blue.400',
    'teal.500',
    'yellow.500',
    'orange.500',
    'purple.500',
    'pink.500',
  ];
  return (
    <>
      <Popover variant="picker">
        <PopoverTrigger>
          <Button
            aria-label={color}
            background={color}
            height="22px"
            width="22px"
            padding={0}
            minWidth="unset"
            borderRadius={3}></Button>
        </PopoverTrigger>
        <PopoverContent width="170px">
          <PopoverArrow bg={color} />
          <PopoverCloseButton color="white" />
          <PopoverHeader
            height="100px"
            backgroundColor={color}
            borderTopLeftRadius={5}
            borderTopRightRadius={5}
            color="white">
            <Center height="100%">{color}</Center>
          </PopoverHeader>
          <PopoverBody height="120px">
            <SimpleGrid columns={6} spacing={2}>
              {colors.map((c) => (
                <Button
                  key={c}
                  aria-label={c}
                  background={c}
                  height="22px"
                  width="22px"
                  padding={0}
                  minWidth="unset"
                  borderRadius={3}
                  _hover={{ background: c }}
                  onClick={() => {
                    handleColor(c);
                  }}></Button>
              ))}
            </SimpleGrid>
            <Input
              borderRadius={3}
              marginTop={3}
              placeholder="red.100"
              size="sm"
              value={color}
              onChange={(e) => {
                handleColor(e.target.value);
              }}
            />
          </PopoverBody>
        </PopoverContent>
      </Popover>
    </>
  );
};
export default ColorPicker;
