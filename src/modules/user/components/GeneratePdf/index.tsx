import React from 'react';

import { Button } from '@chakra-ui/react';
import html2canvas from 'html2canvas';
import { jsPDF } from 'jspdf';
type props = {
  html?: React.MutableRefObject<HTMLDivElement>;
  filename?: string;
};

const GeneratePdf: React.FC<props> = ({ html, filename }) => {
  const generateAsImage = async () => {
    html2canvas(html.current, {
      scale: 4,
      allowTaint: true,
      useCORS: true,
    }).then((canvas) => {
      const imgWidth = 210;
      const imgHeight = 299;
      const imgData = canvas.toDataURL('img/png');
      const pdf = new jsPDF('p', 'mm', 'a4');
      pdf.addImage(imgData, 'PNG', 0, 0, imgWidth, imgHeight);
      pdf.save(`${filename}.pdf`);
    });
  };

  return (
    <div className="button-container">
      <Button width="100%" onClick={generateAsImage} variant="blue">
        Download
      </Button>
    </div>
  );
};

export default GeneratePdf;
