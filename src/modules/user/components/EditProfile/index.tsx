/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  ModalHeader,
  Divider,
  Heading,
  useToast,
  SimpleGrid,
} from '@chakra-ui/react';
import { Formik } from 'formik';
import { InputControl, SubmitButton, TextareaControl } from 'formik-chakra-ui';
import * as Yup from 'yup';

import { DatePickerControl } from 'common/components/DatePickerControl';
import {
  UserUpdateInputDto,
  useUpdateUserMutation,
} from 'common/generated/generated-types';

interface Props {
  isOpenEditProfile: boolean;
  onCloseEditProfile: () => void;
  user: any;
  setNewUserInfo: any;
}

const EditProfile: React.FC<Props> = (props) => {
  const toast = useToast();
  const { isOpenEditProfile, onCloseEditProfile, user, setNewUserInfo } = props;

  const [updateAccountMutation, { data, loading, error }] = useUpdateUserMutation({
    errorPolicy: 'all',
  });

  const initialValues = {
    id: '',
    email: '',
    password: '',
    confirmPassword: '',
    firstname: '',
    lastname: '',
    middlename: '',
    phonenumber: '',
    address: '',
    description: '',
    dob: '',
  };
  const nameRegex = /^[a-z][a-z0-9]*$/i;
  const validationSchema = Yup.object({
    id: Yup.string(),
    email: Yup.string().email(),
    password: Yup.string().min(6),
    confirmPassword: Yup.string().oneOf(
      [Yup.ref('password'), null],
      'Passwords must match',
    ),
    firstname: Yup.string().matches(nameRegex, 'Only English letters'),
    lastname: Yup.string().matches(nameRegex, 'Only English letters'),
    middlename: Yup.string().matches(nameRegex, 'Only English letters').nullable(true),
    phonenumber: Yup.number(),
    address: Yup.string(),
    description: Yup.string().nullable(true),
    dob: Yup.date(),
  });

  useEffect(() => {
    if (!error && data) {
      onCloseEditProfile();
      toast({
        title: 'Updated successfully.',
        description: "We've updated your information.",
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
      setNewUserInfo(data.UpdateUser);
      // console.log(data.UpdateUser);
      // router.reload('/user/profile', undefined, { shallow: true });
    } else {
      if (error) {
        toast({
          title: 'Error',
          description: error.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }
    }
  }, [data, loading, error]);

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const onSubmit = (values: UserUpdateInputDto, { resetForm }: any) => {
    sleep(500).then(() => {
      updateAccountMutation({
        variables: {
          UserUpdateInputDto: {
            id: values.id,
            email: values.email,
            password: values.password,
            address: values.address,
            description: values.description,
            dob: values.dob,
            firstname: values.firstname,
            lastname: values.lastname,
            middlename: values.middlename,
            phonenumber: values.phonenumber,
          },
        },
      }).then(() => {
        resetForm(initialValues);
      });
    });
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}>
      {({ handleSubmit, setFieldValue }) => {
        useEffect(() => {
          setFieldValue('id', user?._id);
          setFieldValue('email', user?.email);
          setFieldValue('firstname', user?.firstname);
          setFieldValue('lastname', user?.lastname);
          setFieldValue('middlename', user?.middlename);
          setFieldValue('phonenumber', user?.phonenumber);
          setFieldValue('address', user?.address);
          setFieldValue('description', user?.description);
          setFieldValue('dob', user?.dob);
        }, [user, isOpenEditProfile]);
        return (
          <>
            <div onSubmit={handleSubmit as React.FormEventHandler}>
              <Modal isOpen={isOpenEditProfile} onClose={onCloseEditProfile} isCentered>
                <ModalOverlay />
                <ModalContent
                  bg="#F5F5F5"
                  color="black"
                  px="60px"
                  py="50px"
                  maxW="900px"
                  borderBlock="none">
                  <ModalCloseButton mx="20px" my="20px" fontSize="18px" />
                  <ModalHeader ml="-25px">
                    <Heading fontSize="26px" color="black">
                      Edit Profile
                    </Heading>
                  </ModalHeader>
                  <Divider border="1px solid #FFFFFF" w="100%" />
                  <ModalBody>
                    <SimpleGrid columns={2} spacingX="20px" spacingY="10px" py="20px">
                      <InputControl
                        label="First Name"
                        name="firstname"
                        inputProps={{
                          _placeholder: { color: 'blackAlpha.500' },
                          placeholder: 'Enter first name',
                          bg: 'white',
                          type: 'text',
                          color: 'blackAlpha.900',
                          size: 'md',
                        }}
                      />
                      <InputControl
                        label="Last Name"
                        name="lastname"
                        inputProps={{
                          _placeholder: { color: 'blackAlpha.500' },
                          placeholder: 'Enter last name',
                          bg: 'white',
                          type: 'text',
                          color: 'blackAlpha.900',
                          size: 'md',
                        }}
                      />
                      <InputControl
                        label="Middle Name"
                        name="middlename"
                        inputProps={{
                          _placeholder: { color: 'blackAlpha.500' },
                          placeholder: 'Enter middle name',
                          bg: 'white',
                          type: 'text',
                          color: 'blackAlpha.900',
                          size: 'md',
                        }}
                      />
                      <InputControl
                        label="Email"
                        name="email"
                        inputProps={{
                          _placeholder: { color: 'blackAlpha.500' },
                          placeholder: 'Enter email',
                          bg: 'white',
                          type: 'text',
                          color: 'blackAlpha.900',
                          size: 'md',
                        }}
                      />
                      <InputControl
                        label="Phone Number"
                        name="phonenumber"
                        inputProps={{
                          _placeholder: { color: 'blackAlpha.500' },
                          placeholder: 'Enter phone number',
                          bg: 'white',
                          type: 'text',
                          color: 'blackAlpha.900',
                          size: 'md',
                        }}
                      />
                      <DatePickerControl
                        label="Date of Birth"
                        name="dob"
                        propsConfigs={{
                          dateNavBtnProps: {
                            color: 'blackAlpha.900',
                            bg: 'white',
                          },
                          dayOfMonthBtnProps: {
                            color: 'blackAlpha.900',
                            borderColor: 'red.300',
                            selectedBg: 'blue.200',
                            _hover: {
                              bg: 'green.400',
                            },
                          },
                          inputProps: {
                            cursor: 'pointer',
                            type: 'text',
                            _placeholder: { color: 'blackAlpha.500' },
                            placeholder: 'Select date of birth',
                            bg: 'white',
                            color: 'blackAlpha.900',
                            size: 'md',
                          },
                        }}
                      />
                    </SimpleGrid>
                    <InputControl
                      label="Address"
                      name="address"
                      inputProps={{
                        _placeholder: { color: 'blackAlpha.500' },
                        placeholder: 'Enter address',
                        bg: 'white',
                        type: 'text',
                        color: 'blackAlpha.900',
                        size: 'md',
                      }}
                    />
                    <TextareaControl
                      label="About me"
                      name="description"
                      textareaProps={{
                        _placeholder: { color: 'blackAlpha.500' },
                        placeholder: 'Tell about your self',
                        bg: 'white',
                        color: 'blackAlpha.900',
                        size: 'md',
                      }}
                    />
                    <Heading
                      fontSize="18px"
                      color="black"
                      fontFamily="medium"
                      pt="20px"
                      my="10px">
                      Change Password
                    </Heading>
                    <Divider
                      border="1px solid #FFFFFF"
                      width="100%"
                      borderColor="black"
                    />
                    <SimpleGrid columns={2} spacingX="20px" spacingY="10px" py="20px">
                      <InputControl
                        label="Password"
                        name="password"
                        inputProps={{
                          _placeholder: { color: 'blackAlpha.500' },
                          placeholder: 'Enter new password',
                          bg: 'white',
                          type: 'password',
                          color: 'blackAlpha.900',
                          size: 'md',
                        }}
                      />
                      <InputControl
                        label="Confirm Password"
                        name="confirmPassword"
                        inputProps={{
                          _placeholder: { color: 'blackAlpha.500' },
                          placeholder: 'Enter new confirm password',
                          bg: 'white',
                          type: 'password',
                          color: 'blackAlpha.900',
                          size: 'md',
                        }}
                      />
                    </SimpleGrid>
                    <SubmitButton
                      bg="orange.300"
                      mt="10px"
                      w="full"
                      size="lg"
                      color="white"
                      fontFamily="heading"
                      onClick={handleSubmit as React.FormEventHandler}
                      _hover={{ bg: 'gray.400', color: 'white' }}>
                      Submit
                    </SubmitButton>
                  </ModalBody>
                </ModalContent>
              </Modal>
            </div>
          </>
        );
      }}
    </Formik>
  );
};

export default EditProfile;
