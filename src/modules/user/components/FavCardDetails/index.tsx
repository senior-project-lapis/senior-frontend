/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  Text,
  Flex,
  Button,
  Divider,
  SimpleGrid,
  Heading,
  ModalBody,
} from '@chakra-ui/react';

interface Props {
  isOpenFavDetails: boolean;
  onCloseFavDetails: () => void;
  jobDetails?: any;
}

const FavCardDetails: React.FC<Props> = (Props) => {
  const { isOpenFavDetails, onCloseFavDetails, jobDetails } = Props;
  const CapitalizeFirstLetter = (string: string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };
  return (
    <Modal isOpen={isOpenFavDetails} onClose={onCloseFavDetails}>
      <ModalOverlay />
      <ModalContent
        bg="#F5F5F5"
        color="black"
        px="20px"
        py="20px"
        maxW="900px"
        borderBlock="none">
        <ModalCloseButton mx="20px" my="20px" fontSize="18px" />
        <ModalBody>
          <Flex overflowX="hidden" flexDirection="column">
            <Flex justifyContent="space-between">
              <Heading p="10px" fontSize="20px">
                {jobDetails.name}
              </Heading>
            </Flex>
            <Flex ml="40px" flexDirection="column">
              <Flex justifyContent="flex-start" flexDirection="column">
                <Flex fontSize="16px" p="5px">
                  <Text fontSize="16px" fontFamily="medium">
                    Company:
                  </Text>
                  &nbsp;
                  {jobDetails.companyName}&nbsp;&nbsp;
                </Flex>
                {jobDetails.locations.length > 2 ? (
                  <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                    <Text fontSize="16px" fontFamily="medium">
                      Locations:
                    </Text>
                    <SimpleGrid
                      columns={4}
                      spacing={1}
                      m="10px"
                      justifyItems="left"
                      alignItems="center">
                      {jobDetails.locations.map((item: any, index: any) => (
                        <Text fontSize="16px" display="flex" key={index}>
                          {CapitalizeFirstLetter(item)}
                        </Text>
                      ))}
                    </SimpleGrid>
                  </Flex>
                ) : (
                  <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                    <Text fontSize="16px" fontFamily="medium">
                      Location:
                    </Text>
                    <Text fontSize="16px" display="flex">
                      &nbsp;&nbsp;
                      {jobDetails.locations.map(
                        (item: any) => CapitalizeFirstLetter(item) + ' / ',
                      )}
                    </Text>
                  </Flex>
                )}
              </Flex>
              {jobDetails.categories ? (
                jobDetails.categories.length > 1 ? (
                  <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                    <Text fontSize="16px" fontFamily="medium">
                      Categories:
                    </Text>
                    <SimpleGrid
                      columns={2}
                      spacing={2}
                      mt="10px"
                      justifyItems="center"
                      alignItems="center">
                      {jobDetails.categories.map((item: any, index: any) => (
                        <Text fontSize="16px" display="flex" key={index}>
                          {CapitalizeFirstLetter(item)}
                        </Text>
                      ))}
                    </SimpleGrid>
                  </Flex>
                ) : (
                  <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                    <Text fontSize="16px" fontFamily="medium">
                      Categories:
                    </Text>
                    <Text fontSize="16px" display="flex">
                      &nbsp;&nbsp;
                      {jobDetails.categories.map((item: any) =>
                        CapitalizeFirstLetter(item),
                      )}
                    </Text>
                  </Flex>
                )
              ) : null}
              {jobDetails.experienceLevels ? (
                jobDetails.experienceLevels.length > 1 ? (
                  <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                    <Text fontSize="16px" fontFamily="medium">
                      Experience Levels:
                    </Text>
                    <SimpleGrid
                      columns={2}
                      spacing={2}
                      mt="10px"
                      justifyItems="center"
                      alignItems="center">
                      {jobDetails.experienceLevels.map((item: any, index: any) => (
                        <Text fontSize="16px" display="flex" key={index}>
                          {CapitalizeFirstLetter(item)}
                        </Text>
                      ))}
                    </SimpleGrid>
                  </Flex>
                ) : (
                  <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                    <Text fontSize="16px" fontFamily="medium">
                      Experience Levels:
                    </Text>
                    <Text fontSize="16px" display="flex">
                      &nbsp;&nbsp;
                      {jobDetails.experienceLevels.map((item: any) =>
                        CapitalizeFirstLetter(item),
                      )}
                    </Text>
                  </Flex>
                )
              ) : null}
              {jobDetails.remote && jobDetails.remote === true ? (
                <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                  <Text fontSize="16px" fontFamily="medium">
                    Remote:
                  </Text>
                  <Text fontSize="16px" display="flex">
                    &nbsp;&nbsp; Yes
                  </Text>
                </Flex>
              ) : (
                <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                  <Text fontSize="16px" fontFamily="medium">
                    Remote:
                  </Text>
                  <Text fontSize="16px" display="flex">
                    &nbsp;&nbsp; No
                  </Text>
                </Flex>
              )}
              <Flex pt="10px">
                <a
                  href={`https://${jobDetails.applyUrl}`}
                  target={'_blank'}
                  rel={'noreferrer'}>
                  <Button variant="orange" mr="1" w="200px">
                    Apply Job
                  </Button>
                </a>
                {/* <FavoriteBtn jobId={jobDetails._id} /> */}
              </Flex>
            </Flex>

            <Flex mt="40px" flexDirection="column" mb="10px">
              <Text fontSize="18px" fontFamily="medium" ml="5px">
                Job Description
              </Text>
              <Divider
                mt="10px"
                border="1px solid rgba(0, 0, 0, 0.6)"
                width="100%"
                borderColor="gray.300"
              />
            </Flex>

            {jobDetails.contents ? (
              <Flex
                p="20px"
                mt="10px"
                flexDirection="column"
                dangerouslySetInnerHTML={{ __html: jobDetails.contents }}
              />
            ) : null}
          </Flex>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

export default FavCardDetails;
