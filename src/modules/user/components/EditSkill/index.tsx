/* eslint-disable react/no-unescaped-entities */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  ModalHeader,
  Divider,
  Heading,
  useToast,
  SimpleGrid,
  Button,
  Box,
  Text,
} from '@chakra-ui/react';
import { Formik, FieldArray } from 'formik';
import { InputControl, SubmitButton } from 'formik-chakra-ui';
import { BsTrash } from 'react-icons/bs';
import * as Yup from 'yup';

import {
  UserUpdateInputDto,
  useUpdateUserMutation,
  SkillInput,
} from 'common/generated/generated-types';

interface Props {
  isOpen: boolean;
  onClose: () => void;
  user: any;
  setNewUserInfo: any;
}

const EditSkill: React.FC<Props> = (props) => {
  const toast = useToast();
  const { isOpen, onClose, user, setNewUserInfo } = props;
  const [updateAccountMutation, { data, loading, error }] = useUpdateUserMutation({
    errorPolicy: 'all',
  });

  const initialValues = {
    id: '',
    skill: [{ name: '' }] as any[],
  };

  const validationSchema = Yup.object({
    id: Yup.string(),
    skill: Yup.array().of(
      Yup.object().shape({
        name: Yup.string().required('Skill name is required'),
      }),
    ),
  });

  useEffect(() => {
    if (!error && data) {
      onClose();
      toast({
        title: 'Updated successfully.',
        description: "We've updated your information.",
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
      setNewUserInfo(data.UpdateUser);
      console.log(data.UpdateUser);
    } else {
      if (error) {
        onClose();
        toast({
          title: 'Error',
          description: error.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }
    }
  }, [data, loading, error]);

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const onSubmit = (values: UserUpdateInputDto, { resetForm }: any) => {
    sleep(500).then(() => {
      updateAccountMutation({
        variables: {
          UserUpdateInputDto: {
            id: values.id,
            skill: values.skill,
          },
        },
      }).then(() => {
        resetForm(initialValues);
      });
      // console.log(values);
    });
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}>
      {({ handleSubmit, setFieldValue, values }) => {
        useEffect(() => {
          setFieldValue('id', user?._id);
          user?.skill.map((skill: SkillInput, index: number) => {
            setFieldValue(`skill.${index}.name`, skill.name);
          });
        }, [user, isOpen]);
        return (
          <>
            <Box onSubmit={handleSubmit as React.FormEventHandler}>
              <Modal isOpen={isOpen} onClose={onClose} isCentered scrollBehavior="inside">
                <ModalOverlay />
                <ModalContent
                  bg="#F5F5F5"
                  color="black"
                  px="60px"
                  py="50px"
                  maxW="1000px"
                  borderBlock="none">
                  <ModalCloseButton mx="20px" my="20px" fontSize="18px" />
                  <ModalHeader ml="-25px">
                    {user?.skill.length > 0 ? (
                      <Heading fontSize="26px" color="black">
                        Edit Skill
                      </Heading>
                    ) : (
                      <Heading fontSize="26px" color="black">
                        Add New Skill
                      </Heading>
                    )}
                  </ModalHeader>
                  <Divider border="1px solid #FFFFFF" w="100%" />
                  <ModalBody>
                    <FieldArray
                      name="skill"
                      render={(arrayHelpers) => (
                        <>
                          {values.skill && values.skill.length > 0 ? (
                            <>
                              <SimpleGrid columns={2} spacing="20px" py="20px">
                                {values.skill.map((skill, index) => (
                                  <Box key={index}>
                                    <Text
                                      fontSize="16px"
                                      fontFamily="medium"
                                      mt="10px"
                                      mb="5px">
                                      Skill {index + 1}:
                                    </Text>
                                    <Divider
                                      border="1px solid rgba(0, 0, 0, 0.6)"
                                      width="100%"
                                      borderColor="gray.700"
                                      mb="10px"
                                    />
                                    <InputControl
                                      label="Name"
                                      name={`skill.${index}.name`}
                                      inputProps={{
                                        _placeholder: { color: 'blackAlpha.500' },
                                        placeholder: `Enter skill name `,
                                        bg: 'white',
                                        type: 'text',
                                        color: 'blackAlpha.900',
                                        size: 'md',
                                      }}
                                    />
                                    <Button
                                      variant="red"
                                      mt="5px"
                                      mr="5px"
                                      onClick={() => arrayHelpers.remove(index)}>
                                      <BsTrash />
                                    </Button>
                                  </Box>
                                ))}
                              </SimpleGrid>
                              <Button
                                variant="green"
                                w="100%"
                                mt="5px"
                                onClick={() =>
                                  arrayHelpers.push({
                                    name: '',
                                    level: '',
                                  })
                                }>
                                Add more skill
                              </Button>
                              <SubmitButton
                                bg="orange.300"
                                mt="10px"
                                w="full"
                                size="lg"
                                color="white"
                                fontFamily="heading"
                                onClick={handleSubmit as React.FormEventHandler}
                                _hover={{ bg: 'gray.400', color: 'white' }}>
                                Submit
                              </SubmitButton>
                            </>
                          ) : (
                            <>
                              <Button
                                mt="20px"
                                w="100%"
                                colorScheme="blue"
                                onClick={() =>
                                  arrayHelpers.push({
                                    name: '',
                                    year: '',
                                  })
                                }>
                                Add New Experience
                              </Button>
                              <SubmitButton
                                bg="orange.300"
                                mt="10px"
                                w="full"
                                size="lg"
                                color="white"
                                fontFamily="heading"
                                onClick={handleSubmit as React.FormEventHandler}
                                _hover={{ bg: 'gray.400', color: 'white' }}>
                                Submit
                              </SubmitButton>
                            </>
                          )}
                        </>
                      )}
                    />
                  </ModalBody>
                </ModalContent>
              </Modal>
            </Box>
          </>
        );
      }}
    </Formik>
  );
};

export default EditSkill;
