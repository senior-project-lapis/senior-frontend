/* eslint-disable react/no-unescaped-entities */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  ModalHeader,
  Divider,
  Heading,
  useToast,
  SimpleGrid,
  Button,
  Box,
  Text,
} from '@chakra-ui/react';
import { Formik, FieldArray } from 'formik';
import { InputControl, SubmitButton, SelectControl } from 'formik-chakra-ui';
import { BsTrash } from 'react-icons/bs';
import * as Yup from 'yup';

import {
  UserUpdateInputDto,
  useUpdateUserMutation,
  EducationDetailType,
} from 'common/generated/generated-types';

interface Props {
  isOpen: boolean;
  onClose: () => void;
  user: any;
  setNewUserInfo: any;
}

const EditEducation: React.FC<Props> = (props) => {
  const toast = useToast();
  const { isOpen, onClose, user, setNewUserInfo } = props;
  const [updateAccountMutation, { data, loading, error }] = useUpdateUserMutation({
    errorPolicy: 'all',
  });

  const initialValues = {
    id: '',
    education: [{ school: '', graduationYear: '', educationLevel: null }] as any[],
  };

  const validationSchema = Yup.object({
    id: Yup.string(),
    education: Yup.array().of(
      Yup.object().shape({
        school: Yup.string().required('Name is required'),
        graduationYear: Yup.number()
          .required('Graduated year is required')
          .min(1900, 'Graduated year must be greater than 1900')
          .max(new Date().getFullYear(), 'Graduated year must be less than current year'),
        educationLevel: Yup.string()
          .matches(/(associate|bachelor|doctoral|highShool|lessThanHighSchool|master)/)
          .required('Education Level is required'),
      }),
    ),
  });

  useEffect(() => {
    if (!error && data) {
      onClose();
      toast({
        title: 'Updated successfully.',
        description: "We've updated your information.",
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
      setNewUserInfo(data.UpdateUser);
      // console.log(data.UpdateUser);
    } else {
      if (error) {
        onClose();
        toast({
          title: 'Error',
          description: error.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }
    }
  }, [data, loading, error]);

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const onSubmit = (values: UserUpdateInputDto, { resetForm }: any) => {
    sleep(500).then(() => {
      updateAccountMutation({
        variables: {
          UserUpdateInputDto: {
            id: values.id,
            education: values.education,
          },
        },
      }).then(() => {
        resetForm(initialValues);
      });
      // console.log(values);
    });
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}>
      {({ handleSubmit, setFieldValue, values }) => {
        useEffect(() => {
          setFieldValue('id', user?._id);
          user?.education.map((edu: EducationDetailType, index: number) => {
            setFieldValue(`education.${index}.school`, edu.school);
            setFieldValue(`education.${index}.graduationYear`, edu.graduationYear);
            setFieldValue(`education.${index}.educationLevel`, edu.educationLevel);
          });
        }, [user, isOpen]);
        return (
          <>
            <Box onSubmit={handleSubmit as React.FormEventHandler}>
              <Modal isOpen={isOpen} onClose={onClose} isCentered scrollBehavior="inside">
                <ModalOverlay />
                <ModalContent
                  bg="#F5F5F5"
                  color="black"
                  px="60px"
                  py="50px"
                  maxW="1000px"
                  borderBlock="none">
                  <ModalCloseButton mx="20px" my="20px" fontSize="18px" />
                  <ModalHeader ml="-25px">
                    {user?.education.length > 0 ? (
                      <Heading fontSize="26px" color="black">
                        Edit Education
                      </Heading>
                    ) : (
                      <Heading fontSize="26px" color="black">
                        Add New Education
                      </Heading>
                    )}
                  </ModalHeader>
                  <Divider border="1px solid #FFFFFF" w="100%" />
                  <ModalBody>
                    <FieldArray
                      name="education"
                      render={(arrayHelpers) => (
                        <>
                          {values.education && values.education.length > 0 ? (
                            <>
                              <SimpleGrid columns={2} spacing="20px" py="20px">
                                {values.education.map((edu, index) => (
                                  <Box key={index}>
                                    <Text
                                      fontSize="16px"
                                      fontFamily="medium"
                                      mt="10px"
                                      mb="5px">
                                      Education {index + 1}:
                                    </Text>
                                    <Divider
                                      border="1px solid rgba(0, 0, 0, 0.6)"
                                      width="100%"
                                      borderColor="gray.700"
                                      mb="10px"
                                    />
                                    <InputControl
                                      label="Name"
                                      name={`education.${index}.school`}
                                      inputProps={{
                                        _placeholder: { color: 'blackAlpha.500' },
                                        placeholder: `Enter education name `,
                                        bg: 'white',
                                        type: 'text',
                                        color: 'blackAlpha.900',
                                        size: 'md',
                                      }}
                                    />
                                    <InputControl
                                      label="Graduated year"
                                      name={`education.${index}.graduationYear`}
                                      inputProps={{
                                        _placeholder: { color: 'blackAlpha.500' },
                                        placeholder: `Enter graduated year `,
                                        bg: 'white',
                                        type: 'text',
                                        color: 'blackAlpha.900',
                                        size: 'md',
                                      }}
                                    />
                                    <SelectControl
                                      label="Education Level"
                                      name={`education.${index}.educationLevel`}
                                      selectProps={{
                                        _placeholder: { color: 'blackAlpha.500' },
                                        placeholder: `Select education level `,
                                        bg: 'white',
                                        color: 'blackAlpha.900',
                                        size: 'md',
                                      }}>
                                      <option value="lessThanHighSchool">
                                        Less Than High School
                                      </option>
                                      <option value="highShool">
                                        High School or Equivalent
                                      </option>
                                      <option value="associate">
                                        Associate's Degree
                                      </option>
                                      <option value="bachelor">Bachelor's Degree</option>
                                      <option value="master">Master's Degree</option>
                                      <option value="doctoral">Doctoral Degree</option>
                                    </SelectControl>
                                    <Button
                                      variant="red"
                                      mt="5px"
                                      mr="5px"
                                      onClick={() => arrayHelpers.remove(index)}>
                                      <BsTrash />
                                    </Button>
                                  </Box>
                                ))}
                              </SimpleGrid>
                              <Button
                                variant="green"
                                w="100%"
                                mt="5px"
                                onClick={() =>
                                  arrayHelpers.push({
                                    school: '',
                                    graduationYear: '',
                                    educationLevel: '',
                                  })
                                }>
                                Add more education
                              </Button>
                              <SubmitButton
                                bg="orange.300"
                                mt="10px"
                                w="full"
                                size="lg"
                                color="white"
                                fontFamily="heading"
                                onClick={handleSubmit as React.FormEventHandler}
                                _hover={{ bg: 'gray.400', color: 'white' }}>
                                Submit
                              </SubmitButton>
                            </>
                          ) : (
                            <>
                              <Button
                                mt="20px"
                                w="100%"
                                colorScheme="blue"
                                onClick={() =>
                                  arrayHelpers.push({
                                    school: '',
                                    graduationYear: '',
                                    educationLevel: '',
                                  })
                                }>
                                Add New Education
                              </Button>
                              <SubmitButton
                                bg="orange.300"
                                mt="10px"
                                w="full"
                                size="lg"
                                color="white"
                                fontFamily="heading"
                                onClick={handleSubmit as React.FormEventHandler}
                                _hover={{ bg: 'gray.400', color: 'white' }}>
                                Submit
                              </SubmitButton>
                            </>
                          )}
                        </>
                      )}
                    />
                  </ModalBody>
                </ModalContent>
              </Modal>
            </Box>
          </>
        );
      }}
    </Formik>
  );
};

export default EditEducation;
