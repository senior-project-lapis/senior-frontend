/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useEffect } from 'react';

import {
  Heading,
  Divider,
  Text,
  Stack,
  Flex,
  Box,
  useDisclosure,
  Button,
  SimpleGrid,
} from '@chakra-ui/react';
import moment from 'moment';

import { useGetUserFromSession } from 'common/generated/page';

import EditEducation from 'modules/user/components/EditEducation';
import EditExperience from 'modules/user/components/EditExperience';
import EditProfile from 'modules/user/components/EditProfile';
import EditSkill from 'modules/user/components/EditSkill';

const ProfilePage: React.FC = () => {
  const [newUserInfo, setNewUserInfo] = useState<any>(null);
  const [user, setUser] = useState<any>(null);
  const { data, loading, refetch } = useGetUserFromSession();

  const {
    isOpen: isOpenEditProfile,
    onClose: onCloseEditProfile,
    onOpen: onOpenEditProfile,
  } = useDisclosure();

  const {
    isOpen: isOpenEditEducation,
    onClose: onCloseEditEducation,
    onOpen: onOpenEditEducation,
  } = useDisclosure();

  const {
    isOpen: isOpenEditExp,
    onClose: onCloseEditExp,
    onOpen: onOpenEditExp,
  } = useDisclosure();

  const {
    isOpen: isOpenEditSkill,
    onClose: onCloseEditSkill,
    onOpen: onOpenEditSkill,
  } = useDisclosure();

  useEffect(() => {
    refetch();
    setUser(newUserInfo);
  }, [newUserInfo]);

  useEffect(() => {
    setUser(data?.GetUserBySession);
  }, [loading]);

  const getEducationLevel = (education: string) => {
    let letter;
    switch (true) {
      case education === 'bachelor':
        letter = "Bachelor's Degree";
        break;
      case education === 'master':
        letter = "Master's Degree";
        break;
      case education === 'doctoral':
        letter = 'Doctoral Degree';
        break;
      case education === 'associate':
        letter = "Associate's Degree";
        break;
      case education === 'highShool':
        letter = 'High School or Equivalent';
        break;
      case education === 'lessThanHighSchool':
        letter = 'Less Than High School';
        break;
    }
    return letter;
  };

  return (
    <>
      <Stack spacing={4} width="100%">
        <Stack spacing={2}>
          <Flex justifyContent="space-between" mt="20px" width="100%" alignItems="center">
            <Heading fontSize="20px">Personal Details</Heading>
            <Text onClick={onOpenEditProfile} fontSize="16px" bg="none" cursor="pointer">
              edit
            </Text>
          </Flex>
          <Divider
            border="1px solid rgba(0, 0, 0, 0.6)"
            width="100%"
            borderColor="gray.700"
          />
          <Box px="30px">
            <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
              First name:
            </Text>
            <Text fontSize="14px">{user?.firstname}</Text>
            <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
              Last name:
            </Text>
            <Text fontSize="14px">{user?.lastname}</Text>

            <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
              Middle name:
            </Text>
            {user?.middlename ? (
              <Text fontSize="14px">{user?.middlename}</Text>
            ) : (
              <Text fontSize="14px">-</Text>
            )}
            <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
              Date of birth:
            </Text>
            <Text fontSize="14px">{moment(user?.dob).format('YYYY-MM-DD')}</Text>
            <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
              Phone number:
            </Text>
            <Text fontSize="14px">{user?.phonenumber}</Text>
            <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
              Email Address:
            </Text>
            <Text fontSize="14px">{user?.email}</Text>
            <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
              Address:
            </Text>
            <Text fontSize="14px">{user?.address}</Text>
            <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
              About me:
            </Text>
            <Text textAlign="justify" fontSize="14px">
              {user?.description}
            </Text>
          </Box>
        </Stack>
        {/* TODO: add education */}
        {user?.education.length > 0 ? (
          <Stack spacing={2}>
            <Flex
              justifyContent="space-between"
              mt="20px"
              width="100%"
              alignItems="center">
              <Heading fontSize="20px">Education</Heading>
              <Text
                onClick={onOpenEditEducation}
                fontSize="16px"
                bg="none"
                cursor="pointer">
                edit
              </Text>
            </Flex>
            <Divider
              border="1px solid rgba(0, 0, 0, 0.6)"
              width="100%"
              borderColor="gray.700"
            />
            <SimpleGrid columns={2} spacing={5}>
              {user?.education.map((a: any, index: number) => (
                <Box p="10px" key={index} borderRadius="5px">
                  <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
                    Education {index + 1}:
                  </Text>
                  <Divider
                    border="1px solid rgba(0, 0, 0, 0.6)"
                    width="100%"
                    borderColor="gray.700"
                  />
                  <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
                    {a.school}
                  </Text>
                  <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
                    {getEducationLevel(a?.educationLevel)}
                  </Text>
                  <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
                    {a.graduationYear}
                  </Text>
                </Box>
              ))}
            </SimpleGrid>
          </Stack>
        ) : (
          <Button onClick={onOpenEditEducation} variant="blue">
            Add new Education
          </Button>
        )}
        {user?.experience.length > 0 ? (
          <Stack spacing={2}>
            <Flex
              justifyContent="space-between"
              mt="20px"
              width="100%"
              alignItems="center">
              <Heading fontSize="20px">Experiences</Heading>
              <Text onClick={onOpenEditExp} fontSize="16px" bg="none" cursor="pointer">
                edit
              </Text>
            </Flex>
            <Divider
              border="1px solid rgba(0, 0, 0, 0.6)"
              width="100%"
              borderColor="gray.700"
            />
            <SimpleGrid columns={1} spacing={5}>
              {user?.experience.map((a: any, index: number) => (
                <Box p="10px" key={index} borderRadius="5px">
                  <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
                    Experience {index + 1}:
                  </Text>
                  <Divider
                    border="1px solid rgba(0, 0, 0, 0.6)"
                    width="100%"
                    borderColor="gray.700"
                    mb="10px"
                  />
                  <Text fontSize="16px" fontFamily="medium">
                    {a.name}
                  </Text>

                  <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
                    {a.year} {'year(s)'}
                  </Text>

                  <Text
                    textAlign="justify"
                    fontSize="14px"
                    color="gray.500"
                    fontFamily="medium"
                    mt="10px"
                    mb="5px">
                    {a.description}
                  </Text>
                </Box>
              ))}
            </SimpleGrid>
          </Stack>
        ) : (
          <Button onClick={onOpenEditExp} variant="blue">
            Add new Experience
          </Button>
        )}
        {user?.skill.length > 0 ? (
          <Stack spacing={2}>
            <Flex
              justifyContent="space-between"
              mt="20px"
              width="100%"
              alignItems="center">
              <Heading fontSize="20px">Skills</Heading>
              <Text onClick={onOpenEditSkill} fontSize="16px" bg="none" cursor="pointer">
                edit
              </Text>
            </Flex>
            <Divider
              border="1px solid rgba(0, 0, 0, 0.6)"
              width="100%"
              borderColor="gray.700"
            />
            <SimpleGrid columns={2} spacing={5}>
              {user?.skill.map((a: any, index: number) => (
                <Box p="10px" key={index} borderRadius="5px">
                  <Text fontSize="16px" fontFamily="medium" mt="10px" mb="5px">
                    Skill {index + 1}:
                  </Text>
                  <Divider
                    border="1px solid rgba(0, 0, 0, 0.6)"
                    width="100%"
                    borderColor="gray.700"
                    mb="10px"
                  />
                  <Flex justifyContent="space-between" alignItems="center">
                    <Text fontSize="16px" fontFamily="medium">
                      {a.name}
                    </Text>
                  </Flex>
                </Box>
              ))}
            </SimpleGrid>
          </Stack>
        ) : (
          <Button onClick={onOpenEditSkill} variant="blue">
            Add new Skill
          </Button>
        )}
      </Stack>
      <EditProfile
        user={user}
        setNewUserInfo={setNewUserInfo}
        isOpenEditProfile={isOpenEditProfile}
        onCloseEditProfile={onCloseEditProfile}
      />
      <EditEducation
        user={user}
        setNewUserInfo={setNewUserInfo}
        isOpen={isOpenEditEducation}
        onClose={onCloseEditEducation}
      />
      <EditExperience
        user={user}
        setNewUserInfo={setNewUserInfo}
        isOpen={isOpenEditExp}
        onClose={onCloseEditExp}
      />
      <EditSkill
        user={user}
        setNewUserInfo={setNewUserInfo}
        isOpen={isOpenEditSkill}
        onClose={onCloseEditSkill}
      />
    </>
  );
};

export default ProfilePage;
