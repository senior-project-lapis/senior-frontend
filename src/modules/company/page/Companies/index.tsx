/* eslint-disable react/no-unescaped-entities */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useEffect } from 'react';

import { Flex, SimpleGrid, Heading, Divider, Button } from '@chakra-ui/react';
import Head from 'next/head';
import { useRouter } from 'next/router';

import Card from 'common/components/Card';
import Container from 'common/components/Container';
import { GetCompaniesQuery } from 'common/generated/generated-types';

import CompanySearchBanner from 'modules/company/components/CompanySearchBanner';
import Footer from 'modules/root/components/Footer';
import Navbar from 'modules/root/components/Navbar';
interface Props {
  data: GetCompaniesQuery;
  totalResult: number;
}
const Companies: React.FC<Props> = (Props) => {
  const { data, totalResult } = Props;
  const router = useRouter();
  const oldCompanies = data.Companies.companyResult;
  const [companies, setCompanies] = useState(oldCompanies);
  const [start, setStart] = useState(0);
  // console.log(companies);

  const loadMore = (e: any) => {
    e.preventDefault();
    setStart(start + 12);
    router.push(
      {
        pathname: '/companies',
        query: {
          ...router.query,
          skip: 12 + start,
          limit: 12,
        },
      },
      undefined,
      { scroll: false },
    );
  };
  const query = router.query.query;
  const location = router.query.location;
  const size = router.query.size;
  const industry = router.query.industry;

  useEffect(() => {
    if (start > 12 || start === 12) {
      const newCompanies = data.Companies.companyResult;
      setCompanies([...companies, ...newCompanies]);
    }
  }, [data]);

  useEffect(() => {
    const newCompanies = data.Companies.companyResult;
    setCompanies(newCompanies);
  }, [query, location, size, industry]);

  return (
    <>
      <Head>
        <title>Lapis | Companies</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />

      <Flex bg="gray.100" mt="75px" minH="100vh" flexDirection="column" flexWrap="nowrap">
        <CompanySearchBanner />
        <Flex py="50px">
          <Container>
            <Flex mb="20px" width="100%" flexDirection="column">
              <Heading fontSize="20px">Companies</Heading>
              <Divider
                mt="10px"
                border="1px solid rgba(0, 0, 0, 0.6)"
                width="100%"
                borderColor="gray.700"
              />
            </Flex>
            {totalResult > 0 ? (
              <Flex flexDir="column">
                <SimpleGrid columns={[2, null, 3]} spacing="20px">
                  {companies?.map((item, index) => (
                    <Card key={index} isShowImage data={item} />
                  ))}
                </SimpleGrid>
                {totalResult > 12 ? (
                  <Flex justifyContent="center" mt="20px">
                    <Button w="200px" variant="orange" onClick={(e) => loadMore(e)}>
                      Load More
                    </Button>
                  </Flex>
                ) : null}
              </Flex>
            ) : (
              <Heading transform="(50%,50%)">Can't find {query} results 🌱 </Heading>
            )}
          </Container>
        </Flex>
      </Flex>
      <Footer />
    </>
  );
};

export default Companies;
