/* eslint-disable @typescript-eslint/no-explicit-any */

import React, { useEffect } from 'react';

import {
  Stack,
  DrawerHeader,
  Drawer,
  DrawerContent,
  DrawerFooter,
  DrawerBody,
  DrawerCloseButton,
  Button,
  useToast,
} from '@chakra-ui/react';
import { Formik, FieldArray } from 'formik';
import { InputControl, SubmitButton, SelectControl } from 'formik-chakra-ui';
import * as Yup from 'yup';

import {
  useCreateCompanyMutation,
  CreateCompanyInputDto,
} from 'common/generated/generated-types';

interface Props {
  isOpenCreateCompany: boolean;
  onCloseCreateCompany: () => void;
  setNewCompany: any;
}

const initialValues = {
  description: '',
  industries: [] as string[],
  locations: [] as string[],
  name: '',
  size: '',
  logoImage: '',
};

const validationSchema = Yup.object({
  description: Yup.string(),
  industries: Yup.array().of(
    Yup.string().required('Company Industries is a required field'),
  ),
  locations: Yup.array().of(
    Yup.string().required('Company Locations is a required field'),
  ),
  name: Yup.string().required('Company name is a required field'),
  size: Yup.string().required('Company size is a required field'),
  logoImage: Yup.string(),
});

const CreateNewCompany: React.FC<Props> = (props) => {
  const toast = useToast();
  const { isOpenCreateCompany, onCloseCreateCompany, setNewCompany } = props;

  const [createCompanyMutation, { data, loading, error }] = useCreateCompanyMutation({
    errorPolicy: 'all',
  });

  useEffect(() => {
    if (!error && data) {
      onCloseCreateCompany();
      toast({
        title: 'Company created.',
        description: "We've created your company.",
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
      setNewCompany(data.CreateCompany);
    } else {
      if (error) {
        toast({
          title: 'Error',
          description: error.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }
    }
  }, [data, loading, error]);

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const onSubmit = (values: CreateCompanyInputDto, { resetForm }: any) => {
    sleep(500).then(() => {
      createCompanyMutation({
        variables: {
          CreateCompanyInputDto: {
            name: values.name,
            size: values.size,
            description: values.description,
            industries: values.industries,
            locations: values.locations,
            logoImage: values.logoImage,
          },
        },
      }).then(() => {
        resetForm(initialValues);
      });
    });
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}>
        {({ handleSubmit, values }) => (
          <div onSubmit={handleSubmit as React.FormEventHandler}>
            <Drawer
              isOpen={isOpenCreateCompany}
              placement="right"
              onClose={onCloseCreateCompany}>
              <DrawerContent>
                <DrawerCloseButton />
                <DrawerHeader borderBottomWidth="1px">Create a new company</DrawerHeader>
                <DrawerBody mt="20px">
                  <Stack spacing="24px">
                    <InputControl
                      name="name"
                      label="Company Name"
                      inputProps={{
                        _placeholder: { color: 'blackAlpha.500' },
                        placeholder: 'Enter company name',
                        bg: 'white',
                        type: 'text',
                        color: 'blackAlpha.900',
                        size: 'md',
                      }}
                    />
                    <InputControl
                      name="description"
                      label="Company Description"
                      inputProps={{
                        _placeholder: { color: 'blackAlpha.500' },
                        placeholder: 'Enter company description',
                        bg: 'white',
                        type: 'text',
                        color: 'blackAlpha.900',
                        size: 'md',
                      }}
                    />
                    <FieldArray
                      name="locations"
                      render={(arrayHelpers) => (
                        <div>
                          {values.locations && values.locations.length > 0 ? (
                            values.locations.map((location, index) => (
                              <div key={index}>
                                <InputControl
                                  label="Company Location "
                                  name={`locations.${index}`}
                                  inputProps={{
                                    _placeholder: { color: 'blackAlpha.500' },
                                    placeholder: `Enter company Location ${index + 1}`,
                                    bg: 'white',
                                    type: 'text',
                                    color: 'blackAlpha.900',
                                    size: 'md',
                                  }}
                                />
                                <Button
                                  mt="5px"
                                  mr="5px"
                                  onClick={() => arrayHelpers.remove(index)}>
                                  -
                                </Button>
                                <Button
                                  mt="5px"
                                  onClick={() => arrayHelpers.insert(index, '')}>
                                  +
                                </Button>
                              </div>
                            ))
                          ) : (
                            <Button
                              colorScheme="blue"
                              onClick={() => arrayHelpers.push('')}>
                              Add locations
                            </Button>
                          )}
                        </div>
                      )}
                    />
                    <FieldArray
                      name="industries"
                      render={(arrayHelpers) => (
                        <div>
                          {values.industries && values.industries.length > 0 ? (
                            values.industries.map((industry, index) => (
                              <div key={index}>
                                <InputControl
                                  label="Company Industry"
                                  name={`industries.${index}`}
                                  inputProps={{
                                    _placeholder: { color: 'blackAlpha.500' },
                                    placeholder: `Enter company Industry ${index + 1}`,
                                    bg: 'white',
                                    type: 'text',
                                    color: 'blackAlpha.900',
                                    size: 'md',
                                  }}
                                />
                                <Button
                                  mt="5px"
                                  mr="5px"
                                  onClick={() => arrayHelpers.remove(index)}>
                                  -
                                </Button>
                                <Button
                                  mt="5px"
                                  onClick={() => arrayHelpers.insert(index, '')}>
                                  +
                                </Button>
                              </div>
                            ))
                          ) : (
                            <Button
                              colorScheme="blue"
                              onClick={() => arrayHelpers.push('')}>
                              Add industries
                            </Button>
                          )}
                        </div>
                      )}
                    />

                    <SelectControl
                      label="Company Size"
                      name="size"
                      selectProps={{
                        placeholder: 'Select company size',
                        _placeholder: { color: 'blackAlpha.500' },
                        color: 'blackAlpha.900',
                        size: 'md',
                        bg: 'white',
                      }}>
                      <option value="small">small</option>
                      <option value="medium">medium</option>
                      <option value="large">large</option>
                    </SelectControl>
                    <InputControl
                      label="Company Logo"
                      name="logoImage"
                      inputProps={{
                        _placeholder: { color: 'blackAlpha.500' },
                        placeholder: 'Enter company logo URL',
                        bg: 'white',
                        type: 'text',
                        color: 'blackAlpha.900',
                        size: 'md',
                      }}
                    />
                  </Stack>
                </DrawerBody>

                <DrawerFooter borderTopWidth="1px">
                  <SubmitButton
                    width="100%"
                    mr="5px"
                    colorScheme="green"
                    fontFamily="heading"
                    onClick={() => {
                      handleSubmit();
                    }}>
                    Submit
                  </SubmitButton>
                  <Button variant="outline" onClick={onCloseCreateCompany}>
                    Cancel
                  </Button>
                </DrawerFooter>
              </DrawerContent>
            </Drawer>
          </div>
        )}
      </Formik>
    </>
  );
};

export default CreateNewCompany;
