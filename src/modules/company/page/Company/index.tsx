/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect } from 'react';

import {
  Flex,
  Heading,
  Button,
  Text,
  Divider,
  Image,
  SimpleGrid,
  useDisclosure,
} from '@chakra-ui/react';
import Head from 'next/head';
import { useSelector } from 'react-redux';

import Container from 'common/components/Container';
import WatchingListBtn from 'common/components/WatchingListBtn';
import {
  GetCompanyDetailQuery,
  useGetJobByCompanyIdQuery,
  useGetReviewsByCompanyIdQuery,
} from 'common/generated/generated-types';
import { StoresState } from 'common/stores';

import AddNewReview from 'modules/company/components/AddNewReview';
import CompanySearchBanner from 'modules/company/components/CompanySearchBanner';
import JobsInCompany from 'modules/company/components/JobsInCompany';
import ReviewCards from 'modules/company/components/ReviewCards';
import Footer from 'modules/root/components/Footer';
import Navbar from 'modules/root/components/Navbar';
interface Props {
  data: GetCompanyDetailQuery;
}
const CapitalizeFirstLetter = (string: string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};
const Company: React.FC<Props> = ({ data }) => {
  const auth = useSelector((state: StoresState) => state.user.authenticated);
  const [isLogin, setIsLogin] = useState<boolean>(true);
  const [start, setStart] = useState(0);
  const [skip, setSkip] = useState(0);
  const {
    isOpen: isOpenAddReview,
    onClose: onCloseAddReview,
    onOpen: onOpenAddReview,
  } = useDisclosure();

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('user') || 'null');
    auth || user ? setIsLogin(true) : setIsLogin(false);
  }, []);

  useEffect(() => {
    refetch();
  }, [isOpenAddReview]);

  const { data: jobsInThisCompany, fetchMore } = useGetJobByCompanyIdQuery({
    variables: {
      companyId: data.Company._id,
      sortBy: 'createAt',
      limit: 4,
      skip: 0,
    },
  });

  const {
    data: reviewInThisCompany,
    fetchMore: fetchMoreReviews,
    refetch,
  } = useGetReviewsByCompanyIdQuery({
    variables: {
      companyId: data.Company._id,
      sortBy: 'createAt',
      sortOrder: 'desc',
      limit: 4,
      skip: 0,
    },
  });

  const jobsCompany = jobsInThisCompany?.Jobs.jobResult;
  const loadMore = (e: any) => {
    e.preventDefault();
    setStart(start + 4);
    fetchMore({
      variables: {
        skip: 4 + start,
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        if (!fetchMoreResult) return previousResult;
        return {
          ...previousResult,
          Jobs: {
            jobResult: [
              ...previousResult.Jobs.jobResult,
              ...fetchMoreResult.Jobs.jobResult,
            ],
            totalCount: previousResult.Jobs.totalCount,
          },
        };
      },
    });
  };

  const loadMoreReview = (e: any) => {
    e.preventDefault();
    setSkip(skip + 4);
    fetchMoreReviews({
      variables: {
        skip: 4 + skip,
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        if (!fetchMoreResult) return previousResult;
        return {
          ...previousResult,
          Reviews: {
            reviewResult: [
              ...previousResult.Reviews.reviewResult,
              ...fetchMoreResult.Reviews.reviewResult,
            ],
            totalCount: previousResult.Reviews.totalCount,
          },
        };
      },
    });
  };

  return (
    <>
      <Head>
        <title>Lapis | Company Details</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />

      <Flex bg="white" flexDirection="column" flexWrap="nowrap" mt="75px" minH="100vh">
        <CompanySearchBanner />
        <Flex py="30px">
          <Container>
            {data?.Company ? (
              <Flex flexDirection="column" p="15px" m="5px" w="100%">
                <Flex w="100%" justifyContent="space-between" p="10px">
                  <Image
                    objectFit="contain"
                    src={data.Company.logoImage || '/images/404.png'}
                    alt={data.Company.shortName || 'not found'}
                    boxSize="250px"
                    boxShadow="rgba(0, 0, 0, 0.02) 0px 1px 3px 0px, rgba(27, 31, 35, 0.15) 0px 0px 0px 1px"
                    p="15px"
                  />

                  <Flex
                    w="100%"
                    pl="30px"
                    flexDirection="column"
                    justifyContent="space-between">
                    <Heading fontSize="28px">{data.Company.name}</Heading>
                    <Text fontSize="18px" fontFamily="medium" mt="20px">
                      Company Description :
                    </Text>
                    <Text fontSize="16px">
                      &nbsp;&nbsp;&nbsp;&nbsp;{data.Company.description}
                    </Text>
                    <Flex mt="10px" justifyContent="flex-start" flexDirection="row">
                      <Text fontSize="18px" fontFamily="medium">
                        Company Size :
                      </Text>
                      <Text fontSize="16px" display="flex">
                        &nbsp;&nbsp;
                        {CapitalizeFirstLetter(data.Company.size)}
                      </Text>
                    </Flex>
                    {data.Company.industries.length > 2 ? (
                      <Flex mt="10px" justifyContent="flex-start" flexDirection="column">
                        <Text fontSize="18px" fontFamily="medium">
                          Company Industries:
                        </Text>
                        <SimpleGrid
                          columns={4}
                          spacing={1}
                          mt="5px"
                          ml="10px"
                          justifyItems="left"
                          alignItems="center">
                          {data.Company.industries.map((item, index) => (
                            <Text fontSize="16px" display="flex" key={index}>
                              {CapitalizeFirstLetter(item)}
                            </Text>
                          ))}
                        </SimpleGrid>
                      </Flex>
                    ) : (
                      <Flex mt="10px" justifyContent="flex-start" flexDirection="row">
                        <Text fontSize="18px" fontFamily="medium">
                          Company Industrie:
                        </Text>
                        <Text fontSize="16px" display="flex">
                          &nbsp;&nbsp;
                          {data.Company.industries.map((item) =>
                            CapitalizeFirstLetter(item),
                          )}
                        </Text>
                      </Flex>
                    )}

                    {data.Company.locations.length > 2 ? (
                      <Flex mt="10px" justifyContent="flex-start" flexDirection="column">
                        <Text fontSize="18px" fontFamily="medium">
                          Company Locations:
                        </Text>
                        <SimpleGrid
                          columns={4}
                          spacing={1}
                          mt="5px"
                          ml="10px"
                          justifyItems="left"
                          alignItems="center">
                          {data.Company.locations.map((item, index) => (
                            <Text fontSize="16px" display="flex" key={index}>
                              {CapitalizeFirstLetter(item)}
                            </Text>
                          ))}
                        </SimpleGrid>
                      </Flex>
                    ) : (
                      <Flex mt="10px" justifyContent="flex-start" flexDirection="row">
                        <Text fontSize="18px" fontFamily="medium">
                          Company Location:
                        </Text>
                        <Text fontSize="16px" display="flex">
                          &nbsp;&nbsp;
                          {data.Company.locations.map((item) =>
                            CapitalizeFirstLetter(item),
                          )}
                        </Text>
                      </Flex>
                    )}

                    <Flex mt="20px">
                      {/* <Button mr="20px">❤️</Button> */}
                      <WatchingListBtn data={data.Company} watchingType="company" />
                    </Flex>
                  </Flex>
                </Flex>

                {/* <Flex mt="40px" flexDirection="column" mb="10px">
                  <Text fontSize="18px" fontFamily="heading" ml="5px">
                    Properties
                  </Text>
                  <Divider
                    mt="10px"
                    border="1px solid rgba(0, 0, 0, 0.6)"
                    width="100%"
                    borderColor="gray.300"
                  />
                </Flex> */}

                {jobsInThisCompany ? (
                  <Flex mt="40px" flexDirection="column">
                    <Text fontSize="18px" fontFamily="heading" ml="5px">
                      Job in this company
                    </Text>
                    <Divider
                      mt="10px"
                      border="1px solid rgba(0, 0, 0, 0.6)"
                      width="100%"
                      borderColor="gray.300"
                    />
                    {jobsInThisCompany.Jobs.totalCount > 0 ? (
                      <>
                        <Flex p="10px" w="100%" justifyContent="center">
                          <JobsInCompany jobs={jobsCompany} />
                        </Flex>
                        {jobsInThisCompany.Jobs.totalCount > 4 &&
                        start + 4 < jobsInThisCompany.Jobs.totalCount ? (
                          <Flex justifyContent="center" mt="20px">
                            <Button
                              w="200px"
                              variant="orange"
                              onClick={(e) => loadMore(e)}>
                              Load More
                            </Button>
                          </Flex>
                        ) : null}
                      </>
                    ) : (
                      <Flex p="10px" w="100%" justifyContent="center" mt="20px">
                        <Text fontSize="18px" fontFamily="medium">
                          Don't have Jobs in this Company
                        </Text>
                      </Flex>
                    )}
                  </Flex>
                ) : null}

                {reviewInThisCompany ? (
                  <Flex mt="40px" flexDirection="column" mb="10px">
                    <Flex
                      flexDir="row"
                      justifyContent="space-between"
                      alignItems="center">
                      <Text fontSize="18px" fontFamily="heading" ml="5px">
                        Company Reviews
                      </Text>
                      {isLogin ? (
                        <Button variant="blue" w="200px" onClick={onOpenAddReview}>
                          Create new review
                        </Button>
                      ) : null}
                    </Flex>

                    <Divider
                      mt="10px"
                      border="1px solid rgba(0, 0, 0, 0.6)"
                      width="100%"
                      borderColor="gray.300"
                    />
                    {reviewInThisCompany.Reviews.totalCount > 0 ? (
                      <>
                        <ReviewCards data={reviewInThisCompany} />
                        {reviewInThisCompany.Reviews.totalCount > 4 &&
                        skip + 4 < reviewInThisCompany.Reviews.totalCount ? (
                          <Flex justifyContent="center" mt="20px">
                            <Button
                              w="200px"
                              variant="orange"
                              onClick={(e) => loadMoreReview(e)}>
                              See older reviews
                            </Button>
                          </Flex>
                        ) : null}
                      </>
                    ) : (
                      <Flex p="10px" w="100%" justifyContent="center" mt="20px">
                        <Text fontSize="18px" fontFamily="medium">
                          Don't have Review in this Company
                        </Text>
                      </Flex>
                    )}
                  </Flex>
                ) : null}
              </Flex>
            ) : null}
          </Container>
        </Flex>
      </Flex>
      <AddNewReview
        isOpen={isOpenAddReview}
        onClose={onCloseAddReview}
        companyId={data.Company._id}
      />
      <Footer />
    </>
  );
};

export default Company;
