import { companyForPublisher } from 'modules/company/reducers/companyReducer/types';

export const initState: companyForPublisher = {
  companies: [],
};
