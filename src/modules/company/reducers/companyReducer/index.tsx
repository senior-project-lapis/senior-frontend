import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { CompanyReducer, COMPANY } from './types';
const initialState: CompanyReducer = {
  autocomplete: [],
  isloading: false,
  locations: [],
};

export const companySlice = createSlice({
  name: COMPANY,
  initialState,
  reducers: {
    setAutocomplete: (
      state,
      action: PayloadAction<{
        autocomplete: CompanyReducer['autocomplete'];
        isloading: boolean;
        locations: CompanyReducer['locations'];
      }>,
    ) => {
      const { autocomplete, isloading, locations } = action.payload;
      if (autocomplete === []) {
        return initialState;
      } else {
        state.autocomplete = autocomplete;
        state.isloading = isloading;
        state.locations = locations;
      }
    },
  },
});

export default companySlice.reducer;

export const companyActions = companySlice.actions;
