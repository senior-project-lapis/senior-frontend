export const COMPANY = 'COMPANY';

export type company = {
  _id: string;
  name: string;
};

export type companyForPublisher = {
  companies: company[];
};

export type Location = {
  name: string;
};

export type CompanyReducer = {
  autocomplete: company[];
  isloading: boolean;
  locations: Location[];
};
