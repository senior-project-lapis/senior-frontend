/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import { SimpleGrid } from '@chakra-ui/react';

import ResultBox from 'common/components/ResultBox';

interface Props {
  jobs?: any;
}
const JobsInCompany: React.FC<Props> = (Props) => {
  const { jobs } = Props;
  // console.log(jobs);
  return (
    <SimpleGrid columns={2} spacing="20px" mt="10px">
      {jobs?.map((job: any, index: number) => (
        <ResultBox key={index} index={Number(index)} job={job} />
      ))}
    </SimpleGrid>
  );
};

export default JobsInCompany;
