/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';

import {
  Heading,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  ModalHeader,
  Divider,
  SimpleGrid,
  Box,
  useToast,
} from '@chakra-ui/react';
import { Formik } from 'formik';
import { InputControl, SubmitButton, TextareaControl } from 'formik-chakra-ui';
import * as Yup from 'yup';

import {
  useAddNewCompanyReviewMutation,
  CreateReviewInputDto,
} from 'common/generated/generated-types';

interface Props {
  isOpen: boolean;
  onClose: () => void;
  companyId: string;
}

const AddNewReview: React.FC<Props> = (Props) => {
  const toast = useToast();
  const { isOpen, onClose, companyId } = Props;
  const [addNewReviewMutation, { data, loading, error }] = useAddNewCompanyReviewMutation(
    {
      errorPolicy: 'all',
    },
  );

  const initialValues = {
    companyId: companyId,
    title: '',
    authorPosition: '',
    content: '',
  };
  const validationSchema = Yup.object({
    companyId: Yup.string().required(),
    title: Yup.string().required('Review title is required'),
    authorPosition: Yup.string().required('Position is required'),
    content: Yup.string().required('Review content is required'),
  });

  useEffect(() => {
    if (!error && data) {
      onClose();
      toast({
        title: 'Create successfully.',
        description: "We've created your review.",
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
    } else {
      if (error) {
        toast({
          title: 'Error',
          description: error.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }
    }
  }, [data, loading, error]);

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const onSubmit = (values: CreateReviewInputDto, { resetForm }: any) => {
    sleep(500).then(() => {
      addNewReviewMutation({
        variables: {
          CreateReviewInputDto: {
            companyId: values.companyId,
            title: values.title,
            authorPosition: values.authorPosition,
            content: values.content,
          },
        },
      }).then(() => {
        resetForm(initialValues);
      });
    });
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}>
      {({ handleSubmit }) => (
        <Box onSubmit={handleSubmit as React.FormEventHandler}>
          <Modal isOpen={isOpen} onClose={onClose} isCentered>
            <ModalOverlay />
            <ModalContent
              p="50px"
              maxW="900px"
              borderBlock="none"
              bg="#F5F5F5"
              color="black">
              <ModalCloseButton m="20px" fontSize="18px" />
              <ModalHeader ml="-25px">
                <Heading fontSize="26px" color="black">
                  Add new review
                </Heading>
              </ModalHeader>
              <Divider border="1px solid #FFFFFF" w="100%" />
              <ModalBody>
                <SimpleGrid spacingY="10px" py="20px">
                  <InputControl
                    label="Review Title"
                    name="title"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Enter review title',
                      bg: 'white',
                      type: 'text',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />

                  <InputControl
                    label="Your position in this company"
                    name="authorPosition"
                    inputProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Enter your position',
                      bg: 'white',
                      type: 'text',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                  <TextareaControl
                    label="Review content"
                    name="content"
                    textareaProps={{
                      _placeholder: { color: 'blackAlpha.500' },
                      placeholder: 'Enter review content',
                      bg: 'white',
                      color: 'blackAlpha.900',
                      size: 'md',
                    }}
                  />
                </SimpleGrid>
                <SubmitButton
                  bg="orange.300"
                  mt="10px"
                  w="full"
                  size="lg"
                  color="white"
                  fontFamily="heading"
                  onClick={handleSubmit as React.FormEventHandler}
                  _hover={{ bg: 'gray.400', color: 'white' }}>
                  Submit
                </SubmitButton>
              </ModalBody>
            </ModalContent>
          </Modal>
        </Box>
      )}
    </Formik>
  );
};

export default AddNewReview;
