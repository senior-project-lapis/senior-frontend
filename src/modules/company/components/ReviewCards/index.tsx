/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import { Flex, Divider, Text, VStack } from '@chakra-ui/react';
import moment from 'moment';

import {
  GetReviewsByCompanyIdQuery,
  ReviewWithId,
} from 'common/generated/generated-types';

interface Props {
  data: GetReviewsByCompanyIdQuery;
}

interface CardProps {
  review: ReviewWithId;
}

const Card: React.FC<CardProps> = ({ review }) => (
  <Flex
    w="1000px"
    color="black"
    minH="150px"
    bg="gray.100"
    m="15px"
    p="30px"
    borderRadius="5px">
    <Flex w="100%" justifyContent="space-between" flexDir="column">
      <Flex flexDir="column">
        <Flex flexDir="column">
          <Text fontFamily="medium" fontSize="18px">
            {review.title}
          </Text>
          <Flex justifyContent="flex-start" flexDirection="row" mt="5px">
            <Text fontSize="16px">{review.authorPosition}</Text>
          </Flex>
        </Flex>
        <Divider
          mt="5px"
          border="1px solid rgba(0, 0, 0, 0.6)"
          width="100%"
          borderColor="gray.300"
        />
        <Flex flexDir="column" mt="10px">
          <Text fontSize="16px">{review.content}</Text>
          <Flex mt="15px" justifyContent="flex-start" flexDirection="column">
            <Text fontSize="15px" fontFamily="medium">
              Pros
            </Text>
            <Text fontSize="14px" display="flex">
              -
            </Text>
          </Flex>
          <Flex justifyContent="flex-start" flexDirection="column">
            <Text fontSize="15px" fontFamily="medium">
              Cons
            </Text>
            <Text fontSize="14px" display="flex">
              -
            </Text>
          </Flex>
        </Flex>
      </Flex>
      <Flex alignItems="flex-end" justifyContent="flex-end">
        <Text mt="5px" fontSize="14px">
          {moment(parseInt(review.createAt, 10)).local().format('LLL')}
        </Text>
      </Flex>
    </Flex>
  </Flex>
);

const ReviewCards: React.FC<Props> = (Props) => {
  const { data } = Props;
  return (
    <>
      <Flex w="100%" flexDir="column" justifyContent="center" alignItems="center">
        <VStack spacing="20px">
          {data?.Reviews.reviewResult.map((review: any, index: number) => (
            <Card key={index} review={review} />
          ))}
        </VStack>
      </Flex>
    </>
  );
};

export default ReviewCards;
