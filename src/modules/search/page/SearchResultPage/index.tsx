/* eslint-disable react/jsx-key */
/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState } from 'react';

import { Flex, ButtonProps, Stack, Heading, Divider } from '@chakra-ui/react';
import { Paginator, PageGroup, usePaginator } from 'chakra-paginator';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';

import Container from 'common/components/Container';
import DetailBox from 'common/components/DetailBox';
import Loader from 'common/components/Loader';
import ResultBox from 'common/components/ResultBox';
import { GetSearchResultQuery, JobWithId } from 'common/generated/generated-types';
import { StoresState } from 'common/stores';

import Footer from 'modules/root/components/Footer';
import Navbar from 'modules/root/components/Navbar';
import SearchBanner from 'modules/search/components/SearchBanner';
interface Props {
  data?: GetSearchResultQuery;
}

const SearchResultPage: React.FC<Props> = ({ data }) => {
  const router = useRouter();

  const [selectedJob, setSelectedJob] = useState<JobWithId>();
  const skip = useSelector((state: StoresState) => state.instantSearch.skip);
  const query = useSelector((state: StoresState) => state.instantSearch.query);
  const location = useSelector((state: StoresState) => state.instantSearch.location);
  const isLoading = useSelector((state: StoresState) => state.job.isloading);
  const jobType = useSelector((state: StoresState) => state.instantSearch.jobType);
  const remote = useSelector((state: StoresState) => state.instantSearch.remote);

  const totalResult = useSelector(
    (state: StoresState) => state.instantSearch.totalResult,
  );

  const outerLimit = 1;
  const innerLimit = 1;

  // styles
  const baseStyles: ButtonProps = {
    w: '30px',
    h: '30px',
    fontSize: 'sm',
  };

  const normalStyles: ButtonProps = {
    ...baseStyles,
    _hover: {
      bg: 'blue.500',
    },
    bg: 'white',
  };

  const activeStyles: ButtonProps = {
    ...baseStyles,
    _hover: {
      bg: 'gray.400',
      color: 'black',
    },
    color: 'white',
    bg: 'blue.100',
  };

  const { currentPage, setCurrentPage, offset, pageSize, pagesQuantity } = usePaginator({
    total: totalResult,
    initialState: {
      currentPage: 1,
      pageSize: 10,
    },
  });

  useEffect(() => {
    router.push(
      `/search?query=${query}&location=${location}&jobType=${jobType}&remote=${remote}&skip=${offset}&limit=${pageSize}`,
    );
  }, [offset]);

  useEffect(() => {
    router.push(
      `/search?query=${query}&location=${location}&jobType=${jobType || ''}&remote=${
        remote || ''
      }&skip=0&limit=10`,
    );
  }, [query]);

  useEffect(() => {
    if (skip === 0) {
      setCurrentPage(1);
    }
  }, [skip]);

  useEffect(() => {
    if (isLoading === true) {
      <Loader />;
    }
  }, [isLoading]);

  return (
    <>
      <Head>
        <title>Lapis | Search result</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />

      <Flex bg="gray.200" flexDirection="column" flexWrap="nowrap" mt="75px" minH="100vh">
        <SearchBanner />
        <Flex alignItems="stretch" m="50px">
          <Container>
            <Flex mb="20px" width="100%" flexDirection="column">
              <Heading fontSize="20px">Search Result</Heading>
              <Divider
                mt="10px"
                border="1px solid rgba(0, 0, 0, 0.6)"
                width="100%"
                borderColor="gray.700"
              />
            </Flex>
            {totalResult !== 0 ? (
              <>
                <Flex
                  flexDirection="column"
                  flexWrap="nowrap"
                  w="40%"
                  justifyContent="space-between">
                  <Stack spacing={2}>
                    {data?.Jobs.jobResult.map((job, i) => (
                      <ResultBox
                        isUseSelected
                        key={i}
                        index={Number(i)}
                        job={job}
                        setSelectedJob={setSelectedJob}
                      />
                    ))}
                  </Stack>
                </Flex>
                {selectedJob ? (
                  <>
                    <Flex
                      w="59%"
                      ml="1%"
                      p="10px"
                      maxH="1165px"
                      bg="white"
                      borderRadius="5px"
                      boxShadow="rgba(149, 157, 165, 0.2) 0px 8px 24px">
                      <DetailBox job={selectedJob} />
                    </Flex>
                  </>
                ) : null}
              </>
            ) : (
              <Heading transform="(50%,50%)">Can't find {query} results 🌱 </Heading>
            )}
            {totalResult > 9 ? (
              <Paginator
                normalStyles={normalStyles}
                activeStyles={activeStyles}
                pagesQuantity={pagesQuantity}
                innerLimit={innerLimit}
                currentPage={currentPage}
                outerLimit={outerLimit}
                onPageChange={setCurrentPage}>
                <PageGroup
                  isInline
                  align="center"
                  justifyContent="flexStart"
                  mt="2%"
                  w="100%"
                />
              </Paginator>
            ) : null}
          </Container>
        </Flex>
      </Flex>
      <Footer />
    </>
  );
};

export default SearchResultPage;
