/* eslint-disable react/no-unescaped-entities */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Flex, useToast } from '@chakra-ui/react';
import { Formik } from 'formik';
import { SelectControl, SubmitButton, SwitchControl } from 'formik-chakra-ui';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';

import AutoCompleteFormik from 'common/components/AutoCompleteFormik';
import Container from 'common/components/Container';
import { StoresState } from 'common/stores';

import { instantSearchActions } from 'modules/search/reducers/instantSearchReducer';

const SearchBanner: React.FC = () => {
  const toast = useToast();
  const dispatch = useDispatch();
  const router = useRouter();
  const locations = useSelector((s: StoresState) => s.instantSearch.locations);
  const autoCompleteValue = useSelector((s: StoresState) => s.instantSearch.autocomplete);
  const isloading = useSelector((state: StoresState) => state.instantSearch.isloading);

  const initialValues = {
    autoComplete: '',
    location: '',
    jobType: '',
    properties: '',
    educationLevel: '',
    remote: false,
  };

  const validationSchema = Yup.object({
    autoComplete: Yup.string(),
    location: Yup.string(),
    remote: Yup.boolean(),
    jobType: Yup.string().matches(/(internal|external)/),
    properties: Yup.string(),
    educationLevel: Yup.string().matches(
      /(associate|bachelor|doctoral|highShool|lessThanHighSchool|master)/,
    ),
  });

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

  const onSubmit = (values: any, { resetForm }: any) => {
    sleep(500).then(() => {
      sleep(500).then(() => {
        if (values != initialValues) {
          router.push(
            `/search?query=${values.autoComplete}&location=${values.location}&jobType=${values.jobType}&remote=${values.remote}&skip=0&limit=10`,
          );
          dispatch(
            instantSearchActions.onSearch({
              query: values.autoComplete,
              location: values.location,
              jobType: values.jobType,
              remote: values.remote,
              skip: 0,
              limit: 10,
            }),
          );

          resetForm(initialValues);
        } else {
          toast({
            title: 'Error',
            description: "Don't have value to search",
            status: 'error',
            duration: 9000,
            isClosable: true,
          });
        }
      });
    });
  };
  return (
    <>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}>
        {({ handleSubmit }) => (
          <Flex h="300px" w="100%">
            <Flex zIndex="1" position="relative" w="100%" h="100%">
              <Image
                src="/background_banner.png"
                layout="fill"
                objectFit="cover"
                priority
                alt="Banner Search bar Background"
              />
              <Flex
                position="relative"
                zIndex="2"
                w="100%"
                h="100%"
                border="none"
                background="linear-gradient(to left, transparent,rgba(255, 255, 255, 0.5))">
                <Container>
                  <Flex
                    onSubmit={handleSubmit as React.FormEventHandler}
                    flexWrap="wrap"
                    flexDirection="row"
                    alignContent=" center"
                    justifyContent="space-between">
                    <AutoCompleteFormik
                      _placeholder={{ color: 'blackAlpha.900' }}
                      name="autoComplete"
                      options={autoCompleteValue}
                      isloading={isloading.toString()}
                      boxShadow=" rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px"
                      bg="white"
                      color="black"
                      border="none"
                      size="lg"
                      placeholder="type something...."
                      w="700px"
                      mx="5px"
                    />

                    <SubmitButton
                      type="submit"
                      _hover={{
                        bg: 'gray.400',
                      }}
                      boxShadow=" rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px"
                      size="lg"
                      w="300px"
                      mx="5px"
                      fontFamily="heading"
                      bgColor="blue.200"
                      color="white"
                      onClick={() => handleSubmit()}>
                      Search
                    </SubmitButton>

                    <Flex mt="15px" mx="5px">
                      <SelectControl
                        name="jobType"
                        selectProps={{
                          boxShadow:
                            ' rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px',
                          size: 'md',
                          borderRadius: '5px',
                          border: 'none',
                          color: 'white',
                          fontFamily: 'medium',
                          placeholder: 'Select Job types',
                          bgColor: 'orange.100',
                          w: '250px',
                          mx: '2px',
                        }}>
                        <option value="external">External</option>
                        <option value="internal">Internal</option>
                      </SelectControl>
                      <SelectControl
                        name="properties"
                        selectProps={{
                          boxShadow:
                            ' rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px',
                          size: 'md',
                          borderRadius: '5px',
                          border: 'none',
                          color: 'white',
                          fontFamily: 'medium',
                          placeholder: 'Select Properties',
                          bgColor: 'orange.100',
                          w: '250px',
                          mx: '2px',
                        }}>
                        <option value="true">Yes</option>
                        <option value="false">No</option>
                      </SelectControl>
                      <SelectControl
                        name="educationLevel"
                        selectProps={{
                          boxShadow:
                            ' rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px',
                          size: 'md',
                          borderRadius: '5px',
                          border: 'none',
                          color: 'white',
                          fontFamily: 'medium',
                          placeholder: 'Select Education level',
                          bgColor: 'orange.100',
                          w: '250px',
                          mx: '2px',
                        }}>
                        <option value="lessThanHighSchool">Less Than High School</option>
                        <option value="highShool">High School or Equivalent</option>
                        <option value="associate">Associate's Degree</option>
                        <option value="bachelor">Bachelor's Degree</option>
                        <option value="master">Master's Degree</option>
                        <option value="doctoral">Doctoral Degree</option>
                      </SelectControl>
                      <SelectControl
                        name="location"
                        selectProps={{
                          boxShadow:
                            ' rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px',
                          size: 'md',
                          borderRadius: '5px',
                          border: 'none',
                          color: 'white',
                          fontFamily: 'medium',
                          placeholder: 'Select Location',
                          bgColor: 'orange.100',
                          w: '250px',
                          mx: '2px',
                        }}>
                        {locations.map((location, index) => (
                          <option key={index} value={location.name}>
                            {location.name}
                          </option>
                        ))}
                      </SelectControl>
                    </Flex>
                    <SwitchControl
                      name="remote"
                      label="Remote"
                      mt="10px"
                      mx="10px"
                      color="white"
                      fontFamily="heading"
                    />
                  </Flex>
                </Container>
              </Flex>
            </Flex>
          </Flex>
        )}
      </Formik>
    </>
  );
};
export default SearchBanner;
