import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { InstantSearchReducer, INSTANT_SEARCH } from './types';
export const FIRST_N_OF_NEWS = 10;
const initialState: InstantSearchReducer = {
  location: '',
  autocomplete: [],
  limit: FIRST_N_OF_NEWS,
  skip: 0,
  query: '',
  isloading: false,
  totalResult: 0,
  jobType: '',
  remote: false,
  locations: [],
};

export const instantSearchSlice = createSlice({
  name: INSTANT_SEARCH,
  initialState,
  reducers: {
    setAutocomplete: (
      state,
      action: PayloadAction<{
        autocomplete: InstantSearchReducer['autocomplete'];
        isloading: boolean;
        locations: InstantSearchReducer['locations'];
      }>,
    ) => {
      const { autocomplete, isloading, locations } = action.payload;
      if (autocomplete === []) {
        return initialState;
      } else {
        state.autocomplete = autocomplete;
        state.isloading = isloading;
        state.locations = locations;
      }
    },
    onSearch(
      state,
      action: PayloadAction<{
        query: string;
        location?: string;
        jobType?: string;
        remote?: boolean;
        skip: number;
        limit: number;
      }>,
    ) {
      const { query, location, skip, limit, jobType, remote } = action.payload;
      if (query === '' && location === '' && jobType === '' && remote === false) {
        return initialState;
      } else {
        state.query = query;
        state.location = location;
        state.jobType = jobType;
        state.remote = remote;
        state.skip = skip;
        state.limit = limit;
      }
    },
    onData(
      state,
      action: PayloadAction<{
        location: InstantSearchReducer['location'];
        isloading: InstantSearchReducer['isloading'];
        query: InstantSearchReducer['query'];
        jobType?: InstantSearchReducer['jobType'];
        remote?: InstantSearchReducer['remote'];
        skip: InstantSearchReducer['skip'];
        limit: InstantSearchReducer['limit'];
        totalResult: InstantSearchReducer['totalResult'];
      }>,
    ) {
      const { query, location, skip, limit, isloading, totalResult, jobType, remote } =
        action.payload;
      state.isloading = isloading;
      state.location = location;
      state.query = query;
      state.jobType = jobType;
      state.remote = remote;
      state.skip = skip;
      state.limit = limit;
      state.totalResult = totalResult;
    },
  },
});

export default instantSearchSlice.reducer;

export const instantSearchActions = instantSearchSlice.actions;
