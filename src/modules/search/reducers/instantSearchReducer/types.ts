export const INSTANT_SEARCH = 'INSTANT_SEARCH';

export type Options = {
  name: string;
  value?: string;
};

export type Location = {
  name: string;
};

export type InstantSearchReducer = {
  autocomplete: Options[];
  isloading: boolean;
  jobType?: string;
  remote?: boolean;
  location?: string;
  limit: number;
  skip: number;
  query: string;
  totalResult: number;
  locations: Location[];
};
