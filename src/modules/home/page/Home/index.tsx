import React, { useEffect, useState } from 'react';

import { Flex } from '@chakra-ui/react';
import Head from 'next/head';
import { useRouter } from 'next/router';

import {
  GetAutoCompleteQuery,
  GetLocationsQuery,
  GetLatestCompaniesQuery,
  GetLatestJobsQuery,
} from 'common/generated/generated-types';

import DetailCardHero from 'modules/home/components/DetailCardHero';
import SearchHeroBar from 'modules/home/components/SearchHeroBar';
import Footer from 'modules/root/components/Footer';
import Navbar from 'modules/root/components/Navbar';

interface Props {
  autoCompleteArray?: GetAutoCompleteQuery;
  autoCompleteLoading?: boolean;
  locations?: GetLocationsQuery;
  latestCompanies?: GetLatestCompaniesQuery;
  latestJobs?: GetLatestJobsQuery;
}

const Home: React.FC<Props> = (Props) => {
  const router = useRouter();
  const {
    autoCompleteArray,
    autoCompleteLoading,
    locations,
    latestCompanies,
    latestJobs,
  } = Props;

  // const [publisher, setPublisher] = useState<boolean>(true);
  // useEffect(() => {
  //   const publisher = localStorage.getItem('publisher');
  //   if (publisher) {
  //     setPublisher(true);
  //   } else {
  //     setPublisher(false);
  //   }
  // }, []);

  // if (publisher) {
  //   router.push('/publisher', undefined, { shallow: true });
  // }

  return (
    <>
      <Head>
        <title>Lapis </title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />

      <Flex
        bg="white"
        minHeight="100vh"
        flexDirection="column"
        flexWrap="nowrap"
        mt="75px">
        <SearchHeroBar
          data={autoCompleteArray}
          loading={autoCompleteLoading}
          locations={locations}
        />
        <DetailCardHero bgColor="white" JobData={latestJobs} />
        <DetailCardHero CompanyData={latestCompanies} title="Latest Companies" />
      </Flex>

      <Footer />
    </>
  );
};

export default Home;
