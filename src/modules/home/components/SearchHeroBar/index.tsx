/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useEffect } from 'react';

import { Flex, Button, useToast } from '@chakra-ui/react';
import { Formik } from 'formik';
import { SelectControl, SubmitButton } from 'formik-chakra-ui';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';

import AutoCompleteFormik from 'common/components/AutoCompleteFormik';
import Container from 'common/components/Container';
import {
  GetAutoCompleteQuery,
  GetLocationsQuery,
} from 'common/generated/generated-types';
import { StoresState } from 'common/stores';

import { instantSearchActions } from 'modules/search/reducers/instantSearchReducer';

interface Props {
  data?: GetAutoCompleteQuery;
  loading?: boolean;
  locations: GetLocationsQuery;
}

const SearchHeroBar: React.FC<Props> = (Props) => {
  const { data, loading, locations } = Props;
  const toast = useToast();
  const dispatch = useDispatch();
  const router = useRouter();
  const auth = useSelector((state: StoresState) => state.user.authenticated);
  const isloading = useSelector((state: StoresState) => state.instantSearch.isloading);
  const autoCompleteValue = useSelector((s: StoresState) => s.instantSearch.autocomplete);
  const [isLogin, setIsLogin] = useState<boolean>(true);

  const getUniqueListBy = (arr: any, key: string) => {
    return [...new Map(arr.map((item: any) => [item[key], item])).values()];
  };

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('user') || 'null');
    auth || user ? setIsLogin(true) : setIsLogin(false);
  }, []);

  useEffect(() => {
    const Jobs =
      data?.Jobs.jobResult.map((el: any) => ({
        name: el.name,
      })) || [];
    const CompaniesName =
      data?.Jobs.jobResult.map((el: any) => ({
        name: el.companyName,
      })) || [];

    const UniqeJobs: any = getUniqueListBy(Jobs, 'name');
    const UniqeCompaniesName: any = getUniqueListBy(CompaniesName, 'name');
    const AutoCompleteVariable: any = UniqeJobs.concat(UniqeCompaniesName);

    dispatch(
      instantSearchActions.setAutocomplete({
        autocomplete: AutoCompleteVariable || [],
        isloading: loading || false,
        locations: locations.Locations || [],
      }),
    );
  }, [data]);

  const initialValues = {
    autoComplete: '',
    location: '',
  };

  const validationSchema = Yup.object({
    autoComplete: Yup.string().required('Please enter a job name'),
    location: Yup.string(),
  });

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

  const onSubmit = (values: any, { resetForm }: any) => {
    sleep(500).then(() => {
      if (values != initialValues) {
        router.push(
          `/search?query=${values.autoComplete}&location=${values.location}&jobType=&remote=false&skip=0&limit=10`,
        );
        dispatch(
          instantSearchActions.onSearch({
            query: values.autoComplete,
            location: values.location,
            skip: 0,
            limit: 10,
          }),
        );
        resetForm(initialValues);
      } else {
        toast({
          title: 'Error',
          description: "Don't have value to search",
          status: 'error',
          duration: 9000,
          isClosable: true,
        });
      }
    });
  };
  return (
    <>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}>
        {({ handleSubmit }) => (
          <Flex
            h="600px"
            w="100%"
            backgroundColor="rgba(255, 255, 255, 0.8)"
            background="linear-gradient(330deg, rgba(255, 255, 255, 0.3), rgba(255, 255, 255, 0.75))"
            backdropFilter="blur(10px) saturate(120%)">
            <Flex zIndex="1" position="relative" w="100%" h="100%">
              <Image
                src="/background_hero.jpg"
                layout="fill"
                objectFit="none"
                alt="Hero Search bar Background"
                priority
              />
              <Flex
                position="relative"
                zIndex="2"
                w="100%"
                h="100%"
                border="none"
                background="linear-gradient(to left, transparent, rgba(255, 255, 255, 0.5))">
                <Container>
                  <Flex
                    onSubmit={handleSubmit as React.FormEventHandler}
                    flexWrap="wrap"
                    flexDirection="row"
                    alignContent=" center"
                    justifyContent="space-between">
                    <AutoCompleteFormik
                      _placeholder={{ color: 'blackAlpha.900' }}
                      name="autoComplete"
                      options={autoCompleteValue}
                      isloading={isloading.toString()}
                      boxShadow=" rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px"
                      bg="white"
                      color="black"
                      border="none"
                      size="lg"
                      placeholder="type something...."
                      w="700px"
                      mx="5px"
                    />
                    <Flex>
                      <SelectControl
                        name="location"
                        selectProps={{
                          boxShadow:
                            ' rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px',
                          size: 'lg',
                          border: 'none',
                          color: 'white',
                          bgColor: 'orange.100',
                          placeholder: 'Select Location',
                          w: '300px',
                          mx: '5px',
                        }}>
                        {locations?.Locations.map((location, index) => (
                          <option key={index} value={location.name}>
                            {location.name}
                          </option>
                        ))}
                        {/* <option value="option1">Option 1</option>
                        <option value="option2">Option 2</option>
                        <option value="option3">Option 3</option> */}
                      </SelectControl>
                    </Flex>

                    <SubmitButton
                      type="submit"
                      _hover={{
                        bg: 'gray.400',
                      }}
                      boxShadow=" rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px"
                      size="lg"
                      w="400px"
                      mx="5px"
                      my="10px"
                      bgColor="blue.200"
                      color="white"
                      onClick={() => handleSubmit()}>
                      Search
                    </SubmitButton>
                    {isLogin ? (
                      <Button
                        _hover={{
                          bg: 'gray.400',
                        }}
                        boxShadow=" rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px"
                        size="lg"
                        w="600px"
                        mx="5px"
                        my="10px"
                        bgColor="blue.200"
                        color="white">
                        Search with user profile
                      </Button>
                    ) : null}
                  </Flex>
                </Container>
              </Flex>
            </Flex>
          </Flex>
        )}
      </Formik>
    </>
  );
};

export default SearchHeroBar;
