/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import { Flex, HStack, Heading, Divider } from '@chakra-ui/react';

import Card from 'common/components/Card';
import Container from 'common/components/Container';

interface Props {
  title?: string;
  bgColor?: string;
  CompanyData?: any;
  JobData?: any;
}

const DetailCardHero: React.FC<Props> = (Props) => {
  const { title = 'Latest Job', bgColor = 'gray.50', CompanyData, JobData } = Props;
  return (
    <Flex h="450" bg={bgColor} justifyContent="center" p="10px" alignItems="center">
      <Container>
        <Flex
          width="100%"
          flexDirection="column"
          justifyContent="center"
          alignItems="center">
          <Flex mb="20px" width="100%" flexDirection="column">
            <Heading fontSize="22px">{title}</Heading>
            <Divider
              mt="10px"
              border="1px solid rgba(0, 0, 0, 0.6)"
              width="100%"
              borderColor="gray.700"
            />
          </Flex>
          <HStack spacing="20px">
            {CompanyData
              ? CompanyData?.Companies.companyResult.map((item: any, index: number) => (
                  <Card key={index} isCompanyDatail data={item} />
                ))
              : JobData?.Jobs.jobResult.map((item: any, index: number) => (
                  <Card key={index} data={item} />
                ))}
          </HStack>
        </Flex>
      </Container>
    </Flex>
  );
};

export default DetailCardHero;
