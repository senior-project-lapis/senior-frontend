/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';

import { useDispatch } from 'react-redux';

import { GetServerSideProps } from 'next';

import {
  GetJobDetailQuery,
  GetJobDetailDocument,
  GetJobAutoCompleteQuery,
  GetJobAutoCompleteDocument,
  GetLocationsQuery,
  GetLocationsDocument,
} from 'common/generated/generated-types';
import { client } from 'common/services/client';

import Job from 'modules/job/page/Job';
import { jobActions } from 'modules/job/reducers/jobReducer';

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  const { data } = await client.query<GetJobDetailQuery>({
    query: GetJobDetailDocument,
    variables: {
      id: params?.id,
    },
  });
  const { data: autoCompleteData, loading: autoCompleteLoading } =
    await client.query<GetJobAutoCompleteQuery>({
      query: GetJobAutoCompleteDocument,
      variables: {
        sortBy: 'name',
      },
    });

  const { data: locations } = await client.query<GetLocationsQuery>({
    query: GetLocationsDocument,
  });
  return {
    props: { data, autoCompleteData, autoCompleteLoading, locations },
  };
};

interface Props {
  data: GetJobDetailQuery;
  autoCompleteData: GetJobAutoCompleteQuery;
  autoCompleteLoading: boolean;
  locations: GetLocationsQuery;
}
const JobDetailPage: React.FC<Props> = (Props) => {
  const { data, autoCompleteData, autoCompleteLoading, locations } = Props;
  const dispatch = useDispatch();
  const getUniqueListBy = (arr: any, key: string) => {
    return [...new Map(arr.map((item: any) => [item[key], item])).values()];
  };
  useEffect(() => {
    const Jobs =
      autoCompleteData?.Jobs.jobResult.map((el: any) => ({
        name: el.name,
      })) || [];
    const UniqeJobs: any = getUniqueListBy(Jobs, 'name');

    dispatch(
      jobActions.setAutocomplete({
        autocomplete: UniqeJobs || [],
        isloading: autoCompleteLoading || false,
        locations: locations.Locations || [],
      }),
    );
  }, [autoCompleteData, locations]);
  return <Job data={data} />;
};

export default JobDetailPage;
