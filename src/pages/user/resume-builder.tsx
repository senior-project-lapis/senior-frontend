import UserGuard from 'common/components/UserGuard';

import UserResumeBuilder from 'modules/user/page/UserResumeBuilder';

const ResumebuilderPages: React.FC = () => {
  return (
    <>
      <UserGuard>
        <UserResumeBuilder />
      </UserGuard>
    </>
  );
};

export default ResumebuilderPages;
