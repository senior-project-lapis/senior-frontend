import UserGuard from 'common/components/UserGuard';

import UserProfile from 'modules/user/page/UserProfile';

const ProfilePages: React.FC = () => {
  return (
    <>
      <UserGuard>
        <UserProfile />
      </UserGuard>
    </>
  );
};

export default ProfilePages;
