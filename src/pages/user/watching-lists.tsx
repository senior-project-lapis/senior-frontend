import UserGuard from 'common/components/UserGuard';

import UserWatchingLists from 'modules/user/page/UserWatchingLists';

const WatchingListsPages: React.FC = () => {
  return (
    <>
      <UserGuard>
        <UserWatchingLists />
      </UserGuard>
    </>
  );
};

export default WatchingListsPages;
