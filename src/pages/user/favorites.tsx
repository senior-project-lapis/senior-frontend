import UserGuard from 'common/components/UserGuard';

import UserFavorites from 'modules/user/page/UserFavorites';

const FavoritePages: React.FC = () => {
  return (
    <>
      <UserGuard>
        <UserFavorites />
      </UserGuard>
    </>
  );
};

export default FavoritePages;
