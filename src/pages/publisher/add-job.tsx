import PublisherGuard from 'common/components/PublisherGuard';

import PublisherAddJob from 'modules/publisher/page/PublisherAddJob';

const AddJobPages: React.FC = () => {
  return (
    <>
      <PublisherGuard>
        <PublisherAddJob />
      </PublisherGuard>
    </>
  );
};

export default AddJobPages;
