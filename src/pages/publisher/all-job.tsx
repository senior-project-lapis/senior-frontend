import PublisherGuard from 'common/components/PublisherGuard';

import PublisherAllJob from 'modules/publisher/page/PublisherAllJob';

const AllJobPages: React.FC = () => {
  return (
    <>
      <PublisherGuard>
        <PublisherAllJob />
      </PublisherGuard>
    </>
  );
};

export default AllJobPages;
