import React from 'react';

import { GetServerSideProps } from 'next';

import {
  GetAutoCompleteQuery,
  GetAutoCompleteDocument,
  GetLocationsQuery,
  GetLocationsDocument,
  GetLatestCompaniesQuery,
  GetLatestCompaniesDocument,
  GetLatestJobsQuery,
  GetLatestJobsDocument,
} from 'common/generated/generated-types';
import { client } from 'common/services/client';

import Home from 'modules/home/page/Home';

export const getServerSideProps: GetServerSideProps = async () => {
  const { data: autoCompleteData, loading: autoCompleteLoading } =
    await client.query<GetAutoCompleteQuery>({
      query: GetAutoCompleteDocument,
      variables: {
        sortBy: 'name',
      },
    });
  const { data: locations } = await client.query<GetLocationsQuery>({
    query: GetLocationsDocument,
  });
  const { data: latestCompanies } = await client.query<GetLatestCompaniesQuery>({
    query: GetLatestCompaniesDocument,
    variables: {
      skip: 0,
      limit: 3,
      sortBy: 'createAt',
      sortOrder: 'desc',
    },
  });
  const { data: latestJobs } = await client.query<GetLatestJobsQuery>({
    query: GetLatestJobsDocument,
    variables: {
      skip: 0,
      limit: 3,
      sortBy: 'createAt',
      sortOrder: 'desc',
    },
  });

  return {
    props: {
      autoCompleteData,
      autoCompleteLoading,
      locations,
      latestCompanies,
      latestJobs,
    },
  };
};

interface Props {
  autoCompleteData: GetAutoCompleteQuery;
  autoCompleteLoading: boolean;
  locations: GetLocationsQuery;
  latestCompanies: GetLatestCompaniesQuery;
  latestJobs: GetLatestJobsQuery;
}

const HomePage: React.FC<Props> = (Props) => {
  const {
    autoCompleteData,
    autoCompleteLoading,
    locations,
    latestCompanies,
    latestJobs,
  } = Props;

  return (
    <Home
      autoCompleteArray={autoCompleteData}
      autoCompleteLoading={autoCompleteLoading}
      locations={locations}
      latestCompanies={latestCompanies}
      latestJobs={latestJobs}
    />
  );
};

export default HomePage;
