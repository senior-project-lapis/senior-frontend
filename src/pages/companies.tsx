/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';

import { useDispatch } from 'react-redux';

import { GetServerSideProps } from 'next';

import {
  GetCompaniesDocument,
  GetCompaniesQuery,
  GetCompaniesAutocompleteQuery,
  GetCompaniesAutocompleteDocument,
  GetLocationsQuery,
  GetLocationsDocument,
} from 'common/generated/generated-types';
import { client } from 'common/services/client';

import Companies from 'modules/company/page/Companies';
import { companyActions } from 'modules/company/reducers/companyReducer';

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const { data } = await client.query<GetCompaniesQuery>({
    query: GetCompaniesDocument,
    variables: {
      name: query.query || '',
      location: query.location || '',
      size: query.size || '',
      industry: query.industry || '',
      skip: Number(query.skip) || 0,
      limit: Number(query.limit) || 12,
      sortBy: 'name',
    },
  });

  const { data: autoCompleteCompanyData, loading: autoCompleteCompanyLoading } =
    await client.query<GetCompaniesAutocompleteQuery>({
      query: GetCompaniesAutocompleteDocument,
      variables: {
        sortBy: 'name',
      },
    });
  const { data: locations } = await client.query<GetLocationsQuery>({
    query: GetLocationsDocument,
  });

  const totalResult = data.Companies.totalCount;

  return {
    props: {
      data,
      totalResult,
      autoCompleteCompanyData,
      autoCompleteCompanyLoading,
      locations,
    },
  };
};

interface Props {
  data: GetCompaniesQuery;
  totalResult: number;
  autoCompleteCompanyData: GetCompaniesAutocompleteQuery;
  autoCompleteCompanyLoading: boolean;
  locations: GetLocationsQuery;
}
const CompaniesPage: React.FC<Props> = (Props) => {
  const {
    data,
    totalResult,
    autoCompleteCompanyData,
    autoCompleteCompanyLoading,
    locations,
  } = Props;
  const dispatch = useDispatch();
  const getUniqueListBy = (arr: any, key: string) => {
    return [...new Map(arr.map((item: any) => [item[key], item])).values()];
  };
  useEffect(() => {
    const Companies =
      autoCompleteCompanyData?.Companies.companyResult.map((el: any) => ({
        name: el.name,
      })) || [];
    const UniqeJobs: any = getUniqueListBy(Companies, 'name');

    dispatch(
      companyActions.setAutocomplete({
        autocomplete: UniqeJobs || [],
        isloading: autoCompleteCompanyLoading || false,
        locations: locations.Locations || [],
      }),
    );
  }, [autoCompleteCompanyData, locations]);
  return (
    <>
      <Companies data={data} totalResult={totalResult} />
    </>
  );
};

export default CompaniesPage;
