import React, { useEffect } from 'react';

import { useDispatch } from 'react-redux';

import { GetServerSideProps } from 'next';

import {
  GetSearchResultQuery,
  GetSearchResultDocument,
} from 'common/generated/generated-types';
import { client } from 'common/services/client';

import SearchResultPage from 'modules/search/page/SearchResultPage';
import { instantSearchActions } from 'modules/search/reducers/instantSearchReducer';

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const { data, loading } = await client.query<GetSearchResultQuery>({
    query: GetSearchResultDocument,
    variables: {
      name: query.query,
      location: query.location,
      jobType: query.jobType,
      remote: Boolean(query.remote) || undefined,
      skip: Number(query.skip) || 0,
      limit: Number(query.limit) || 10,
      sortBy: 'name',
      status: 'normal',
    },
  });

  const totalResult = data.Jobs.totalCount;
  return {
    props: { data, query, totalResult, loading },
  };
};

export type InstantSearchReducer = {
  isloading: boolean;
  jobType: string;
  remote: boolean;
  result: string[];
  location: string;
  limit: number;
  skip: number;
  query: string;
};

interface Props {
  data: GetSearchResultQuery;
  query: InstantSearchReducer;
  totalResult: number;
  loading: boolean;
}
const SearchPage: React.FC<Props> = (Props) => {
  const { data, query, totalResult, loading } = Props;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      instantSearchActions.onData({
        query: query.query,
        location: query.location,
        jobType: query.jobType,
        remote: query.remote,
        limit: query.limit,
        skip: query.skip,
        isloading: loading,
        totalResult: totalResult,
      }),
    );
  }, [query]);
  return <SearchResultPage data={data} />;
};

export default SearchPage;
