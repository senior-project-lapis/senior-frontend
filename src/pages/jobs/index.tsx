/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';

import { useDispatch } from 'react-redux';

import { GetServerSideProps } from 'next';

import {
  GetJobAutoCompleteDocument,
  GetJobQuery,
  GetJobDocument,
  GetJobAutoCompleteQuery,
  GetLocationsQuery,
  GetLocationsDocument,
} from 'common/generated/generated-types';
import { client } from 'common/services/client';

import Jobs from 'modules/job/page/Jobs';
import { jobActions } from 'modules/job/reducers/jobReducer';

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const { data, loading } = await client.query<GetJobQuery>({
    query: GetJobDocument,
    variables: {
      name: query.query || '',
      location: query.location || '',
      jobType: query.jobType || '',
      remote: Boolean(query.remote) || undefined,
      skip: Number(query.skip) || 0,
      limit: Number(query.limit) || 10,
      sortBy: 'name',
      status: 'normal',
    },
  });
  const { data: autoCompleteData, loading: autoCompleteLoading } =
    await client.query<GetJobAutoCompleteQuery>({
      query: GetJobAutoCompleteDocument,
      variables: {
        sortBy: 'name',
      },
    });

  const { data: locations } = await client.query<GetLocationsQuery>({
    query: GetLocationsDocument,
  });

  const totalResult = data.Jobs.totalCount;
  return {
    props: {
      data,
      query,
      totalResult,
      loading,
      autoCompleteData,
      autoCompleteLoading,
      locations,
    },
  };
};

export type jobReducer = {
  isloading: boolean;
  jobType: string;
  remote: boolean;
  result: string[];
  location: string;
  limit: number;
  skip: number;
  query: string;
};

interface Props {
  data: GetJobQuery;
  query: jobReducer;
  totalResult: number;
  loading: boolean;
  autoCompleteData: GetJobAutoCompleteQuery;
  autoCompleteLoading: boolean;
  locations: GetLocationsQuery;
}
const JobsPage: React.FC<Props> = (Props) => {
  const {
    data,
    query,
    totalResult,
    loading,
    autoCompleteData,
    autoCompleteLoading,
    locations,
  } = Props;
  const dispatch = useDispatch();
  const getUniqueListBy = (arr: any, key: string) => {
    return [...new Map(arr.map((item: any) => [item[key], item])).values()];
  };
  useEffect(() => {
    const Jobs =
      autoCompleteData?.Jobs.jobResult.map((el: any) => ({
        name: el.name,
      })) || [];
    const UniqeJobs: any = getUniqueListBy(Jobs, 'name');

    dispatch(
      jobActions.setAutocomplete({
        autocomplete: UniqeJobs || [],
        isloading: autoCompleteLoading || false,
        locations: locations.Locations || [],
      }),
    );
  }, [autoCompleteData, locations]);

  useEffect(() => {
    dispatch(
      jobActions.onData({
        query: query.query,
        location: query.location,
        jobType: query.jobType,
        remote: query.remote,
        limit: query.limit,
        skip: query.skip,
        isloading: loading,
        totalResult: totalResult,
      }),
    );
  }, [query]);

  return <Jobs data={data} />;
};

export default JobsPage;
