/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';

import { useDispatch } from 'react-redux';

import { GetServerSideProps } from 'next';

import {
  GetCompanyDetailQuery,
  GetCompanyDetailDocument,
  GetCompaniesAutocompleteQuery,
  GetCompaniesAutocompleteDocument,
  GetLocationsQuery,
  GetLocationsDocument,
} from 'common/generated/generated-types';
import { client } from 'common/services/client';

import Company from 'modules/company/page/Company';
import { companyActions } from 'modules/company/reducers/companyReducer';

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  const { data } = await client.query<GetCompanyDetailQuery>({
    query: GetCompanyDetailDocument,
    variables: {
      id: params?.id,
    },
  });
  const { data: autoCompleteCompanyData, loading: autoCompleteCompanyLoading } =
    await client.query<GetCompaniesAutocompleteQuery>({
      query: GetCompaniesAutocompleteDocument,
      variables: {
        sortBy: 'name',
      },
    });
  const { data: locations } = await client.query<GetLocationsQuery>({
    query: GetLocationsDocument,
  });

  return {
    props: { data, autoCompleteCompanyData, autoCompleteCompanyLoading, locations },
  };
};

interface Props {
  data: GetCompanyDetailQuery;
  autoCompleteCompanyData: GetCompaniesAutocompleteQuery;
  autoCompleteCompanyLoading: boolean;
  locations: GetLocationsQuery;
}
const CompanyDetailPage: React.FC<Props> = (Props) => {
  const { data, autoCompleteCompanyData, autoCompleteCompanyLoading, locations } = Props;
  const dispatch = useDispatch();
  const getUniqueListBy = (arr: any, key: string) => {
    return [...new Map(arr.map((item: any) => [item[key], item])).values()];
  };
  useEffect(() => {
    const Companies =
      autoCompleteCompanyData?.Companies.companyResult.map((el: any) => ({
        name: el.name,
      })) || [];
    const UniqeJobs: any = getUniqueListBy(Companies, 'name');

    dispatch(
      companyActions.setAutocomplete({
        autocomplete: UniqeJobs || [],
        isloading: autoCompleteCompanyLoading || false,
        locations: locations.Locations || [],
      }),
    );
  }, [autoCompleteCompanyData, locations]);
  return <Company data={data} />;
};

export default CompanyDetailPage;
