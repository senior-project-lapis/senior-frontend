import AdminGuard from 'common/components/AdminGuard';

import AdminApprove from 'modules/admin/page/AdminApprove';

const ApprovePage: React.FC = () => {
  return (
    <>
      <AdminGuard>
        <AdminApprove />
      </AdminGuard>
    </>
  );
};

export default ApprovePage;
