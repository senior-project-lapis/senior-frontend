import AdminGuard from 'common/components/AdminGuard';

import AdminReport from 'modules/admin/page/AdminReport';

const ReportPage: React.FC = () => {
  return (
    <>
      <AdminGuard>
        <AdminReport />
      </AdminGuard>
    </>
  );
};

export default ReportPage;
