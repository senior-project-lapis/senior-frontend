import AdminGuard from 'common/components/AdminGuard';

import AdminDashboard from 'modules/admin/page/AdminDashboard';

const DashboardPage: React.FC = () => {
  return (
    <>
      <AdminGuard>
        <AdminDashboard />
      </AdminGuard>
    </>
  );
};

export default DashboardPage;
