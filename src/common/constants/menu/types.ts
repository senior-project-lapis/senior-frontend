export type MenuType = {
  key: string;
  icon?: string;
  label: string;
  link: string[];
};
