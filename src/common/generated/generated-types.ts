import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A date-time string at UTC, such as 2019-12-03T09:54:33Z, compliant with the date-time format. */
  DateTime: any;
};

export type ActiveDataType = {
  __typename?: 'ActiveDataType';
  name: Scalars['String'];
  totalCount: Scalars['Float'];
};

export type AppModel = {
  __typename?: 'AppModel';
  status: Scalars['Int'];
};

export enum ApproveStatusEnum {
  Approved = 'approved',
  Pending = 'pending',
  Rejected = 'rejected',
}

export type AverageType = {
  __typename?: 'AverageType';
  name: Scalars['String'];
  score: Scalars['Float'];
};

export type CompanyDocumentWithCount = {
  __typename?: 'CompanyDocumentWithCount';
  companyResult: Array<CompanyWithId>;
  totalCount: Scalars['Float'];
};

export type CompanyProperty = {
  __typename?: 'CompanyProperty';
  name: Scalars['String'];
  negative: Scalars['Float'];
  positive: Scalars['Float'];
};

export enum CompanySizeEnum {
  Large = 'large',
  Medium = 'medium',
  Small = 'small',
}

export type CompanyWithId = {
  __typename?: 'CompanyWithId';
  _id: Scalars['String'];
  createAt: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  industries: Array<Scalars['String']>;
  locations: Array<Scalars['String']>;
  logoImage?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  properties?: Maybe<Array<CompanyProperty>>;
  shortName: Scalars['String'];
  size: CompanySizeEnum;
  sourceId?: Maybe<Scalars['String']>;
  sourceType: SourceTypeEnum;
  status: Scalars['String'];
  updateAt: Scalars['String'];
};

export type CountryType = {
  __typename?: 'CountryType';
  code: Scalars['String'];
  name: Scalars['String'];
};

export type CreateCompanyInputDto = {
  description?: InputMaybe<Scalars['String']>;
  industries: Array<Scalars['String']>;
  locations: Array<Scalars['String']>;
  logoImage?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  size: Scalars['String'];
};

export type CreateJobInputDto = {
  applyUrl: Scalars['String'];
  benefits?: InputMaybe<Array<Scalars['String']>>;
  categories?: InputMaybe<Array<Scalars['String']>>;
  companyId: Scalars['String'];
  companyName: Scalars['String'];
  contents?: InputMaybe<Scalars['String']>;
  description: Scalars['String'];
  educationLevel: Scalars['String'];
  experienceLevels: Array<Scalars['String']>;
  jobType: Scalars['String'];
  locations: Array<Scalars['String']>;
  name: Scalars['String'];
  remote: Scalars['String'];
  requirements: Array<Scalars['String']>;
  salary?: InputMaybe<Scalars['String']>;
  shortName: Scalars['String'];
};

export type CreateReviewInputDto = {
  authorPosition: Scalars['String'];
  companyId: Scalars['String'];
  con?: InputMaybe<Scalars['String']>;
  content: Scalars['String'];
  pro?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
};

export type DashboardStatDto = {
  __typename?: 'DashboardStatDto';
  activeData: Array<ActiveDataType>;
  nonActiveData: Array<NonActiveDataType>;
};

export type EducationDetailInput = {
  educationLevel: EducationLevelEnum;
  graduationYear: Scalars['String'];
  school: Scalars['String'];
};

export type EducationDetailType = {
  __typename?: 'EducationDetailType';
  educationLevel: EducationLevelEnum;
  graduationYear: Scalars['String'];
  school: Scalars['String'];
};

export enum EducationLevelEnum {
  Associate = 'associate',
  Bachelor = 'bachelor',
  Doctoral = 'doctoral',
  HighShool = 'highShool',
  LessThanHighSchool = 'lessThanHighSchool',
  Master = 'master',
}

export type EducationWithId = {
  __typename?: 'EducationWithId';
  _id: Scalars['String'];
  createAt: Scalars['String'];
  level: Scalars['Float'];
  name: Scalars['String'];
  updateAt: Scalars['String'];
};

export type ExperienceInput = {
  description?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  year: Scalars['String'];
};

export enum ExperienceLevelEnum {
  Entry = 'entry',
  Internship = 'internship',
  Management = 'management',
  Mid = 'mid',
  Senior = 'senior',
}

export type ExperienceType = {
  __typename?: 'ExperienceType';
  description?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  year: Scalars['String'];
};

export type FavoriteJobInputDto = {
  jobId: Scalars['String'];
  userId: Scalars['String'];
};

export type FavoriteWithId = {
  __typename?: 'FavoriteWithId';
  _id: Scalars['String'];
  createAt: Scalars['String'];
  jobsId: Array<Scalars['String']>;
  updateAt: Scalars['String'];
  userId: Scalars['String'];
};

export type JobDocumentWithCount = {
  __typename?: 'JobDocumentWithCount';
  jobResult: Array<JobWithId>;
  totalCount: Scalars['Float'];
};

export type JobTrendResponse = {
  __typename?: 'JobTrendResponse';
  averages: Array<AverageType>;
  endDate: Scalars['String'];
  keyword: Array<Scalars['String']>;
  startDate: Scalars['String'];
  timelineData: Array<TimelineTrendType>;
};

export enum JobTypeEnum {
  External = 'external',
  Internal = 'internal',
}

export type JobWithId = {
  __typename?: 'JobWithId';
  _id: Scalars['String'];
  applyUrl: Scalars['String'];
  benefits?: Maybe<Array<Scalars['String']>>;
  categories?: Maybe<Array<Scalars['String']>>;
  companyId: Scalars['String'];
  companyName: Scalars['String'];
  contents?: Maybe<Scalars['String']>;
  createAt: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  educationLevel?: Maybe<Scalars['String']>;
  experienceLevels: Array<ExperienceLevelEnum>;
  jobType: JobTypeEnum;
  locations: Array<Scalars['String']>;
  name: Scalars['String'];
  remote: Scalars['Boolean'];
  requirements?: Maybe<Array<Scalars['String']>>;
  salary?: Maybe<Scalars['String']>;
  shortName: Scalars['String'];
  sourceId?: Maybe<Scalars['String']>;
  sourceType: SourceTypeEnum;
  status: Scalars['String'];
  updateAt: Scalars['String'];
};

export type LastestJobDto = {
  __typename?: 'LastestJobDto';
  count: Scalars['Float'];
  name: Scalars['String'];
};

export type LocaitionWithId = {
  __typename?: 'LocaitionWithId';
  _id: Scalars['String'];
  name: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  CreateCompany: CompanyWithId;
  CreateFavorite: FavoriteWithId;
  CreateJob: JobWithId;
  CreatePublisher: PublisherWithId;
  CreateReview: ReviewWithId;
  CreateUser: UserWithId;
  CreateWatchingList: WatchingListWithId;
  Login: UserSessionDto;
  LoginPublisher: PublisherSessionDto;
  Logout?: Maybe<Scalars['String']>;
  RefreshToken?: Maybe<Scalars['String']>;
  RemoveCompany: Scalars['Float'];
  RemoveFavorite: FavoriteWithId;
  RemoveJob: Scalars['Float'];
  RemoveReview: Scalars['Float'];
  RemoveWatchingList: WatchingListWithId;
  UpdateCompany: CompanyWithId;
  UpdateJob: JobWithId;
  UpdatePublisher: PublisherWithId;
  UpdateUser: UserWithId;
};

export type MutationCreateCompanyArgs = {
  CreateCompanyInputDto: CreateCompanyInputDto;
};

export type MutationCreateFavoriteArgs = {
  FavoriteJobInputDto: FavoriteJobInputDto;
};

export type MutationCreateJobArgs = {
  CreateJobInputDto: CreateJobInputDto;
};

export type MutationCreatePublisherArgs = {
  PublisherRegisterInputDto: PublisherRegisterInputDto;
};

export type MutationCreateReviewArgs = {
  CreateReviewInputDto: CreateReviewInputDto;
};

export type MutationCreateUserArgs = {
  UserRegisterInputDto: UserRegisterInputDto;
};

export type MutationCreateWatchingListArgs = {
  WatchingListInputDto: WatchingListInputDto;
};

export type MutationLoginArgs = {
  UserLoginInputDTO: UserLoginInputDto;
};

export type MutationLoginPublisherArgs = {
  UserLoginInputDTO: UserLoginInputDto;
};

export type MutationRemoveCompanyArgs = {
  id: Scalars['String'];
};

export type MutationRemoveFavoriteArgs = {
  FavoriteJobInputDto: FavoriteJobInputDto;
};

export type MutationRemoveJobArgs = {
  id: Scalars['String'];
};

export type MutationRemoveReviewArgs = {
  id: Scalars['String'];
};

export type MutationRemoveWatchingListArgs = {
  WatchingListInputDto: WatchingListInputDto;
};

export type MutationUpdateCompanyArgs = {
  UpdateCompanyInputDto: UpdateCompanyInputDto;
};

export type MutationUpdateJobArgs = {
  UpdateJobInputDto: UpdateJobInputDto;
};

export type MutationUpdatePublisherArgs = {
  PublisherUpdateInputDto: PublisherUpdateInputDto;
};

export type MutationUpdateUserArgs = {
  UserUpdateInputDto: UserUpdateInputDto;
};

export type NonActiveDataType = {
  __typename?: 'NonActiveDataType';
  name: Scalars['String'];
  totalCount: Scalars['Float'];
};

export type PublisherDocumentWithCount = {
  __typename?: 'PublisherDocumentWithCount';
  publisherResult: Array<PublisherWithId>;
  totalCount: Scalars['Float'];
};

export type PublisherRegisterInputDto = {
  companyId?: InputMaybe<Scalars['String']>;
  email: Scalars['String'];
  firstname: Scalars['String'];
  job: Scalars['String'];
  lastname: Scalars['String'];
  middlename?: InputMaybe<Scalars['String']>;
  password: Scalars['String'];
  phonenumber: Scalars['String'];
};

export type PublisherSessionDto = {
  __typename?: 'PublisherSessionDTO';
  _id: Scalars['String'];
  companyId: Scalars['String'];
  email: Scalars['String'];
  firstname: Scalars['String'];
  job: Scalars['String'];
  lastname: Scalars['String'];
  middlename: Scalars['String'];
  phonenumber: Scalars['String'];
  role: UserRoleEnum;
  status: ApproveStatusEnum;
};

export type PublisherUpdateInputDto = {
  companyId?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  firstname?: InputMaybe<Scalars['String']>;
  id: Scalars['String'];
  job?: InputMaybe<Scalars['String']>;
  lastname?: InputMaybe<Scalars['String']>;
  middlename?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
  phonenumber?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
};

export type PublisherWithId = {
  __typename?: 'PublisherWithId';
  _id: Scalars['String'];
  companyId: Scalars['String'];
  createAt: Scalars['String'];
  email: Scalars['String'];
  firstname: Scalars['String'];
  job: Scalars['String'];
  lastname: Scalars['String'];
  middlename?: Maybe<Scalars['String']>;
  password: Scalars['String'];
  phonenumber: Scalars['String'];
  role: UserRoleEnum;
  status: ApproveStatusEnum;
  updateAt: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  Companies: CompanyDocumentWithCount;
  Company?: Maybe<CompanyWithId>;
  Countries: Array<CountryType>;
  DashboardStat: DashboardStatDto;
  Education?: Maybe<EducationWithId>;
  EducationByLevel?: Maybe<EducationWithId>;
  Educations: Array<EducationWithId>;
  Favorite?: Maybe<FavoriteWithId>;
  Favorites: Array<JobWithId>;
  GetPublisherBySession?: Maybe<PublisherWithId>;
  GetUserBySession?: Maybe<UserWithId>;
  Job?: Maybe<JobWithId>;
  JobTrend: JobTrendResponse;
  Jobs: JobDocumentWithCount;
  LastestJobStat: Array<LastestJobDto>;
  Location?: Maybe<LocaitionWithId>;
  Locations: Array<LocaitionWithId>;
  Publisher?: Maybe<PublisherWithId>;
  Publishers: PublisherDocumentWithCount;
  Review?: Maybe<ReviewWithId>;
  Reviews: ReviewDocumentWithCount;
  ServerStatus: AppModel;
  User?: Maybe<UserWithId>;
  Users: Array<UserWithId>;
  WatchingList?: Maybe<WatchingListWithId>;
  WatchingLists: Array<WatchingListWithId>;
};

export type QueryCompaniesArgs = {
  industry?: InputMaybe<Scalars['String']>;
  limit?: InputMaybe<Scalars['Int']>;
  location?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['String']>;
  skip?: InputMaybe<Scalars['Int']>;
  sortBy?: InputMaybe<Scalars['String']>;
  sortOrder?: InputMaybe<Scalars['String']>;
};

export type QueryCompanyArgs = {
  id: Scalars['String'];
};

export type QueryEducationArgs = {
  id: Scalars['String'];
};

export type QueryEducationByLevelArgs = {
  level: Scalars['String'];
};

export type QueryFavoriteArgs = {
  id: Scalars['String'];
};

export type QueryFavoritesArgs = {
  userId: Scalars['String'];
};

export type QueryJobArgs = {
  id: Scalars['String'];
};

export type QueryJobTrendArgs = {
  endYear: Scalars['String'];
  geo?: InputMaybe<Scalars['String']>;
  startYear: Scalars['String'];
};

export type QueryJobsArgs = {
  category?: InputMaybe<Scalars['String']>;
  companyId?: InputMaybe<Scalars['String']>;
  jobType?: InputMaybe<Scalars['String']>;
  limit?: InputMaybe<Scalars['Int']>;
  location?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  remote?: InputMaybe<Scalars['Boolean']>;
  salary?: InputMaybe<Scalars['Int']>;
  since?: InputMaybe<Scalars['DateTime']>;
  skip?: InputMaybe<Scalars['Int']>;
  sortBy?: InputMaybe<Scalars['String']>;
  sortOrder?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
};

export type QueryLocationArgs = {
  id: Scalars['String'];
};

export type QueryPublisherArgs = {
  id: Scalars['String'];
};

export type QueryPublishersArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  sortBy?: InputMaybe<Scalars['String']>;
  sortOrder?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
};

export type QueryReviewArgs = {
  id: Scalars['String'];
};

export type QueryReviewsArgs = {
  companyId?: InputMaybe<Scalars['String']>;
  limit?: InputMaybe<Scalars['Int']>;
  sentiment?: InputMaybe<Scalars['String']>;
  skip?: InputMaybe<Scalars['Int']>;
  sortBy?: InputMaybe<Scalars['String']>;
  sortOrder?: InputMaybe<Scalars['String']>;
  topic?: InputMaybe<Scalars['String']>;
};

export type QueryUserArgs = {
  id: Scalars['String'];
};

export type QueryUsersArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};

export type QueryWatchingListArgs = {
  id: Scalars['String'];
};

export type QueryWatchingListsArgs = {
  companyId?: InputMaybe<Scalars['String']>;
  jobName?: InputMaybe<Scalars['String']>;
  limit?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  sortBy?: InputMaybe<Scalars['String']>;
  sortOrder?: InputMaybe<Scalars['String']>;
  userId?: InputMaybe<Scalars['String']>;
};

export type ReviewDocumentWithCount = {
  __typename?: 'ReviewDocumentWithCount';
  reviewResult: Array<ReviewWithId>;
  totalCount: Scalars['Float'];
};

export type ReviewWithId = {
  __typename?: 'ReviewWithId';
  _id: Scalars['String'];
  authorPosition?: Maybe<Scalars['String']>;
  companyId: Scalars['String'];
  con?: Maybe<Scalars['String']>;
  content: Scalars['String'];
  createAt: Scalars['String'];
  pro?: Maybe<Scalars['String']>;
  score?: Maybe<Scalars['Float']>;
  sentiment?: Maybe<SentimentEnum>;
  title?: Maybe<Scalars['String']>;
  topic?: Maybe<Scalars['String']>;
  updateAt: Scalars['String'];
};

export enum SentimentEnum {
  Negative = 'negative',
  Positive = 'positive',
}

export type SkillInput = {
  name: Scalars['String'];
};

export type SkillType = {
  __typename?: 'SkillType';
  name: Scalars['String'];
};

export enum SourceTypeEnum {
  External = 'external',
  Internal = 'internal',
}

export type TimelineTrendType = {
  __typename?: 'TimelineTrendType';
  formattedAxisTime: Scalars['String'];
  formattedTime: Scalars['String'];
  formattedValue: Array<Scalars['String']>;
  hasData: Array<Scalars['Boolean']>;
  time: Scalars['String'];
  value: Array<Scalars['Float']>;
};

export type UpdateCompanyInputDto = {
  description?: InputMaybe<Scalars['String']>;
  id: Scalars['String'];
  industries?: InputMaybe<Array<Scalars['String']>>;
  locations?: InputMaybe<Array<Scalars['String']>>;
  logoImage?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
};

export type UpdateJobInputDto = {
  applyUrl?: InputMaybe<Scalars['String']>;
  benefits?: InputMaybe<Array<Scalars['String']>>;
  categories?: InputMaybe<Array<Scalars['String']>>;
  companyId?: InputMaybe<Scalars['String']>;
  companyName?: InputMaybe<Scalars['String']>;
  contents?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  educationLevel?: InputMaybe<Scalars['String']>;
  experienceLevels?: InputMaybe<Array<Scalars['String']>>;
  id: Scalars['String'];
  jobType?: InputMaybe<Scalars['String']>;
  locations?: InputMaybe<Array<Scalars['String']>>;
  name?: InputMaybe<Scalars['String']>;
  remote?: InputMaybe<Scalars['String']>;
  requirements?: InputMaybe<Array<Scalars['String']>>;
  salary?: InputMaybe<Scalars['String']>;
  shortName?: InputMaybe<Scalars['String']>;
  sourceType?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
};

export type UserLoginInputDto = {
  email: Scalars['String'];
  password: Scalars['String'];
};

export type UserRegisterInputDto = {
  address: Scalars['String'];
  description?: InputMaybe<Scalars['String']>;
  dob: Scalars['DateTime'];
  email: Scalars['String'];
  firstname: Scalars['String'];
  lastname: Scalars['String'];
  middlename?: InputMaybe<Scalars['String']>;
  password: Scalars['String'];
  phonenumber: Scalars['String'];
};

export enum UserRoleEnum {
  Admin = 'admin',
  Publisher = 'publisher',
  User = 'user',
}

export type UserSessionDto = {
  __typename?: 'UserSessionDTO';
  _id: Scalars['String'];
  address: Scalars['String'];
  dob: Scalars['String'];
  email: Scalars['String'];
  firstname: Scalars['String'];
  lastname: Scalars['String'];
  middlename?: Maybe<Scalars['String']>;
  phonenumber: Scalars['String'];
  role: UserRoleEnum;
};

export type UserUpdateInputDto = {
  address?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  dob?: InputMaybe<Scalars['DateTime']>;
  education?: InputMaybe<Array<EducationDetailInput>>;
  email?: InputMaybe<Scalars['String']>;
  experience?: InputMaybe<Array<ExperienceInput>>;
  firstname?: InputMaybe<Scalars['String']>;
  id: Scalars['String'];
  lastname?: InputMaybe<Scalars['String']>;
  middlename?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
  phonenumber?: InputMaybe<Scalars['String']>;
  skill?: InputMaybe<Array<SkillInput>>;
};

export type UserWithId = {
  __typename?: 'UserWithId';
  _id: Scalars['String'];
  address: Scalars['String'];
  createAt: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  dob: Scalars['DateTime'];
  education?: Maybe<Array<EducationDetailType>>;
  email: Scalars['String'];
  experience?: Maybe<Array<ExperienceType>>;
  firstname: Scalars['String'];
  lastname: Scalars['String'];
  middlename?: Maybe<Scalars['String']>;
  password: Scalars['String'];
  phonenumber: Scalars['String'];
  role: UserRoleEnum;
  skill?: Maybe<Array<SkillType>>;
  updateAt: Scalars['String'];
};

export type WatchingListInputDto = {
  companyId?: InputMaybe<Scalars['String']>;
  listType: Scalars['String'];
  location: Scalars['String'];
  name: Scalars['String'];
  userId: Scalars['String'];
};

export type WatchingListType = {
  __typename?: 'WatchingListType';
  companyId?: Maybe<Scalars['String']>;
  createAt: Scalars['String'];
  listType: Scalars['String'];
  location: Scalars['String'];
  name: Scalars['String'];
};

export type WatchingListWithId = {
  __typename?: 'WatchingListWithId';
  _id: Scalars['String'];
  createAt: Scalars['String'];
  list: Array<WatchingListType>;
  updateAt: Scalars['String'];
  userId: Scalars['String'];
};

export type AddWatchingListMutationVariables = Exact<{
  WatchingListInputDto: WatchingListInputDto;
}>;

export type AddWatchingListMutation = {
  __typename?: 'Mutation';
  CreateWatchingList: { __typename?: 'WatchingListWithId'; createAt: string };
};

export type RemoveWatchingListMutationVariables = Exact<{
  WatchingListInputDto: WatchingListInputDto;
}>;

export type RemoveWatchingListMutation = {
  __typename?: 'Mutation';
  RemoveWatchingList: { __typename?: 'WatchingListWithId'; createAt: string };
};

export type GetWatchingListByUserIdQueryVariables = Exact<{
  userId: Scalars['String'];
}>;

export type GetWatchingListByUserIdQuery = {
  __typename?: 'Query';
  WatchingLists: Array<{
    __typename?: 'WatchingListWithId';
    _id: string;
    userId: string;
    list: Array<{
      __typename?: 'WatchingListType';
      companyId?: string | null;
      listType: string;
      location: string;
      name: string;
      createAt: string;
    }>;
  }>;
};

export type GetPendingPublisherQueryVariables = Exact<{
  limit: Scalars['Int'];
  skip: Scalars['Int'];
  status?: InputMaybe<Scalars['String']>;
  sortBy?: InputMaybe<Scalars['String']>;
  sortOrder?: InputMaybe<Scalars['String']>;
}>;

export type GetPendingPublisherQuery = {
  __typename?: 'Query';
  Publishers: {
    __typename?: 'PublisherDocumentWithCount';
    totalCount: number;
    publisherResult: Array<{
      __typename?: 'PublisherWithId';
      _id: string;
      email: string;
      firstname: string;
      lastname: string;
      middlename?: string | null;
      job: string;
      companyId: string;
      status: ApproveStatusEnum;
      phonenumber: string;
      role: UserRoleEnum;
    }>;
  };
};

export type UpdatePublisherMutationVariables = Exact<{
  PublisherUpdateInputDto: PublisherUpdateInputDto;
}>;

export type UpdatePublisherMutation = {
  __typename?: 'Mutation';
  UpdatePublisher: { __typename?: 'PublisherWithId'; status: ApproveStatusEnum };
};

export type DashboardStatQueryVariables = Exact<{ [key: string]: never }>;

export type DashboardStatQuery = {
  __typename?: 'Query';
  DashboardStat: {
    __typename?: 'DashboardStatDto';
    activeData: Array<{
      __typename?: 'ActiveDataType';
      name: string;
      totalCount: number;
    }>;
    nonActiveData: Array<{
      __typename?: 'NonActiveDataType';
      name: string;
      totalCount: number;
    }>;
  };
};

export type LastestJobStatQueryVariables = Exact<{ [key: string]: never }>;

export type LastestJobStatQuery = {
  __typename?: 'Query';
  LastestJobStat: Array<{ __typename?: 'LastestJobDto'; name: string; count: number }>;
};

export type LoginMutationVariables = Exact<{
  UserLoginInputDTO: UserLoginInputDto;
}>;

export type LoginMutation = {
  __typename?: 'Mutation';
  Login: {
    __typename?: 'UserSessionDTO';
    _id: string;
    email: string;
    firstname: string;
    middlename?: string | null;
    lastname: string;
    phonenumber: string;
    dob: string;
    address: string;
    role: UserRoleEnum;
  };
};

export type LoginPublisherMutationVariables = Exact<{
  UserLoginInputDTO: UserLoginInputDto;
}>;

export type LoginPublisherMutation = {
  __typename?: 'Mutation';
  LoginPublisher: {
    __typename?: 'PublisherSessionDTO';
    _id: string;
    email: string;
    firstname: string;
    middlename: string;
    lastname: string;
    phonenumber: string;
    job: string;
    companyId: string;
    status: ApproveStatusEnum;
    role: UserRoleEnum;
  };
};

export type LogoutMutationVariables = Exact<{ [key: string]: never }>;

export type LogoutMutation = { __typename?: 'Mutation'; Logout?: string | null };

export type RefreshTokenMutationVariables = Exact<{ [key: string]: never }>;

export type RefreshTokenMutation = {
  __typename?: 'Mutation';
  RefreshToken?: string | null;
};

export type GetServerStatusQueryVariables = Exact<{ [key: string]: never }>;

export type GetServerStatusQuery = {
  __typename?: 'Query';
  ServerStatus: { __typename?: 'AppModel'; status: number };
};

export type CreateUserMutationVariables = Exact<{
  UserRegisterInputDto: UserRegisterInputDto;
}>;

export type CreateUserMutation = {
  __typename?: 'Mutation';
  CreateUser: {
    __typename?: 'UserWithId';
    _id: string;
    email: string;
    firstname: string;
    middlename?: string | null;
    password: string;
    lastname: string;
    phonenumber: string;
    dob: any;
    address: string;
    role: UserRoleEnum;
  };
};

export type CreatePublisherMutationVariables = Exact<{
  PublisherRegisterInputDto: PublisherRegisterInputDto;
}>;

export type CreatePublisherMutation = {
  __typename?: 'Mutation';
  CreatePublisher: {
    __typename?: 'PublisherWithId';
    _id: string;
    email: string;
    firstname: string;
    lastname: string;
    middlename?: string | null;
    phonenumber: string;
    job: string;
    companyId: string;
    status: ApproveStatusEnum;
    role: UserRoleEnum;
  };
};

export type GetCompanyForPublisherRegisterQueryVariables = Exact<{
  sortBy: Scalars['String'];
}>;

export type GetCompanyForPublisherRegisterQuery = {
  __typename?: 'Query';
  Companies: {
    __typename?: 'CompanyDocumentWithCount';
    companyResult: Array<{ __typename?: 'CompanyWithId'; _id: string; name: string }>;
  };
};

export type CreateCompanyMutationVariables = Exact<{
  CreateCompanyInputDto: CreateCompanyInputDto;
}>;

export type CreateCompanyMutation = {
  __typename?: 'Mutation';
  CreateCompany: { __typename?: 'CompanyWithId'; _id: string; name: string };
};

export type GetCompaniesAutocompleteQueryVariables = Exact<{
  sortBy: Scalars['String'];
}>;

export type GetCompaniesAutocompleteQuery = {
  __typename?: 'Query';
  Companies: {
    __typename?: 'CompanyDocumentWithCount';
    companyResult: Array<{ __typename?: 'CompanyWithId'; name: string }>;
  };
};

export type GetCompanyDetailQueryVariables = Exact<{
  id: Scalars['String'];
}>;

export type GetCompanyDetailQuery = {
  __typename?: 'Query';
  Company?: {
    __typename?: 'CompanyWithId';
    _id: string;
    sourceType: SourceTypeEnum;
    sourceId?: string | null;
    description?: string | null;
    locations: Array<string>;
    industries: Array<string>;
    shortName: string;
    name: string;
    size: CompanySizeEnum;
    logoImage?: string | null;
    createAt: string;
  } | null;
};

export type GetCompaniesQueryVariables = Exact<{
  sortBy: Scalars['String'];
  name: Scalars['String'];
  size: Scalars['String'];
  location: Scalars['String'];
  limit: Scalars['Int'];
  skip: Scalars['Int'];
  industry: Scalars['String'];
}>;

export type GetCompaniesQuery = {
  __typename?: 'Query';
  Companies: {
    __typename?: 'CompanyDocumentWithCount';
    totalCount: number;
    companyResult: Array<{
      __typename?: 'CompanyWithId';
      _id: string;
      sourceType: SourceTypeEnum;
      sourceId?: string | null;
      description?: string | null;
      locations: Array<string>;
      industries: Array<string>;
      shortName: string;
      name: string;
      size: CompanySizeEnum;
      logoImage?: string | null;
      createAt: string;
    }>;
  };
};

export type GetJobByCompanyIdQueryVariables = Exact<{
  sortBy: Scalars['String'];
  companyId: Scalars['String'];
  limit: Scalars['Int'];
  skip: Scalars['Int'];
  sortOrder?: InputMaybe<Scalars['String']>;
}>;

export type GetJobByCompanyIdQuery = {
  __typename?: 'Query';
  Jobs: {
    __typename?: 'JobDocumentWithCount';
    totalCount: number;
    jobResult: Array<{
      __typename?: 'JobWithId';
      _id: string;
      name: string;
      shortName: string;
      sourceType: SourceTypeEnum;
      contents?: string | null;
      jobType: JobTypeEnum;
      locations: Array<string>;
      categories?: Array<string> | null;
      experienceLevels: Array<ExperienceLevelEnum>;
      educationLevel?: string | null;
      benefits?: Array<string> | null;
      requirements?: Array<string> | null;
      description?: string | null;
      salary?: string | null;
      companyId: string;
      companyName: string;
      applyUrl: string;
      remote: boolean;
      status: string;
      createAt: string;
      updateAt: string;
    }>;
  };
};

export type GetReviewsByCompanyIdQueryVariables = Exact<{
  sortBy: Scalars['String'];
  companyId: Scalars['String'];
  limit: Scalars['Int'];
  skip: Scalars['Int'];
  sortOrder: Scalars['String'];
}>;

export type GetReviewsByCompanyIdQuery = {
  __typename?: 'Query';
  Reviews: {
    __typename?: 'ReviewDocumentWithCount';
    totalCount: number;
    reviewResult: Array<{
      __typename?: 'ReviewWithId';
      _id: string;
      title?: string | null;
      score?: number | null;
      authorPosition?: string | null;
      content: string;
      pro?: string | null;
      con?: string | null;
      createAt: string;
    }>;
  };
};

export type AddNewCompanyReviewMutationVariables = Exact<{
  CreateReviewInputDto: CreateReviewInputDto;
}>;

export type AddNewCompanyReviewMutation = {
  __typename?: 'Mutation';
  CreateReview: {
    __typename?: 'ReviewWithId';
    _id: string;
    title?: string | null;
    score?: number | null;
    authorPosition?: string | null;
    content: string;
    pro?: string | null;
    con?: string | null;
    createAt: string;
  };
};

export type GetAutoCompleteQueryVariables = Exact<{
  sortBy?: InputMaybe<Scalars['String']>;
}>;

export type GetAutoCompleteQuery = {
  __typename?: 'Query';
  Jobs: {
    __typename?: 'JobDocumentWithCount';
    jobResult: Array<{ __typename?: 'JobWithId'; name: string; companyName: string }>;
  };
};

export type GetLocationsQueryVariables = Exact<{ [key: string]: never }>;

export type GetLocationsQuery = {
  __typename?: 'Query';
  Locations: Array<{ __typename?: 'LocaitionWithId'; name: string }>;
};

export type GetLatestCompaniesQueryVariables = Exact<{
  sortBy: Scalars['String'];
  limit: Scalars['Int'];
  skip: Scalars['Int'];
  sortOrder?: InputMaybe<Scalars['String']>;
}>;

export type GetLatestCompaniesQuery = {
  __typename?: 'Query';
  Companies: {
    __typename?: 'CompanyDocumentWithCount';
    totalCount: number;
    companyResult: Array<{
      __typename?: 'CompanyWithId';
      _id: string;
      sourceType: SourceTypeEnum;
      sourceId?: string | null;
      description?: string | null;
      locations: Array<string>;
      industries: Array<string>;
      shortName: string;
      name: string;
      size: CompanySizeEnum;
      logoImage?: string | null;
      createAt: string;
    }>;
  };
};

export type GetLatestJobsQueryVariables = Exact<{
  limit?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  sortBy?: InputMaybe<Scalars['String']>;
  sortOrder?: InputMaybe<Scalars['String']>;
}>;

export type GetLatestJobsQuery = {
  __typename?: 'Query';
  Jobs: {
    __typename?: 'JobDocumentWithCount';
    totalCount: number;
    jobResult: Array<{
      __typename?: 'JobWithId';
      _id: string;
      name: string;
      shortName: string;
      sourceType: SourceTypeEnum;
      contents?: string | null;
      jobType: JobTypeEnum;
      locations: Array<string>;
      categories?: Array<string> | null;
      experienceLevels: Array<ExperienceLevelEnum>;
      companyId: string;
      companyName: string;
      applyUrl: string;
      salary?: string | null;
      remote: boolean;
      benefits?: Array<string> | null;
      status: string;
      createAt: string;
    }>;
  };
};

export type GetJobQueryVariables = Exact<{
  limit?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  sortBy?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  location?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
  jobType?: InputMaybe<Scalars['String']>;
  remote?: InputMaybe<Scalars['Boolean']>;
}>;

export type GetJobQuery = {
  __typename?: 'Query';
  Jobs: {
    __typename?: 'JobDocumentWithCount';
    totalCount: number;
    jobResult: Array<{
      __typename?: 'JobWithId';
      _id: string;
      name: string;
      shortName: string;
      sourceType: SourceTypeEnum;
      contents?: string | null;
      jobType: JobTypeEnum;
      locations: Array<string>;
      categories?: Array<string> | null;
      experienceLevels: Array<ExperienceLevelEnum>;
      educationLevel?: string | null;
      benefits?: Array<string> | null;
      requirements?: Array<string> | null;
      description?: string | null;
      salary?: string | null;
      companyId: string;
      companyName: string;
      applyUrl: string;
      remote: boolean;
      status: string;
      createAt: string;
    }>;
  };
};

export type GetJobAutoCompleteQueryVariables = Exact<{
  sortBy?: InputMaybe<Scalars['String']>;
}>;

export type GetJobAutoCompleteQuery = {
  __typename?: 'Query';
  Jobs: {
    __typename?: 'JobDocumentWithCount';
    jobResult: Array<{ __typename?: 'JobWithId'; name: string }>;
  };
};

export type GetJobDetailQueryVariables = Exact<{
  id: Scalars['String'];
}>;

export type GetJobDetailQuery = {
  __typename?: 'Query';
  Job?: {
    __typename?: 'JobWithId';
    _id: string;
    name: string;
    shortName: string;
    sourceType: SourceTypeEnum;
    contents?: string | null;
    jobType: JobTypeEnum;
    locations: Array<string>;
    categories?: Array<string> | null;
    experienceLevels: Array<ExperienceLevelEnum>;
    educationLevel?: string | null;
    benefits?: Array<string> | null;
    requirements?: Array<string> | null;
    description?: string | null;
    salary?: string | null;
    companyId: string;
    companyName: string;
    applyUrl: string;
    remote: boolean;
    status: string;
    createAt: string;
  } | null;
};

export type GetFavoritesByUserIdQueryVariables = Exact<{
  userId: Scalars['String'];
}>;

export type GetFavoritesByUserIdQuery = {
  __typename?: 'Query';
  Favorites: Array<{
    __typename?: 'JobWithId';
    _id: string;
    name: string;
    shortName: string;
    sourceType: SourceTypeEnum;
    contents?: string | null;
    jobType: JobTypeEnum;
    locations: Array<string>;
    categories?: Array<string> | null;
    experienceLevels: Array<ExperienceLevelEnum>;
    companyId: string;
    companyName: string;
    applyUrl: string;
    salary?: string | null;
    remote: boolean;
    benefits?: Array<string> | null;
    status: string;
    createAt: string;
  }>;
};

export type AddFavoriteMutationVariables = Exact<{
  FavoriteJobInputDto: FavoriteJobInputDto;
}>;

export type AddFavoriteMutation = {
  __typename?: 'Mutation';
  CreateFavorite: { __typename?: 'FavoriteWithId'; createAt: string };
};

export type RemoveFavoriteMutationVariables = Exact<{
  FavoriteJobInputDto: FavoriteJobInputDto;
}>;

export type RemoveFavoriteMutation = {
  __typename?: 'Mutation';
  RemoveFavorite: { __typename?: 'FavoriteWithId'; createAt: string };
};

export type GetRelatedJobsQueryVariables = Exact<{
  limit?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  sortBy?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  location?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
  jobType?: InputMaybe<Scalars['String']>;
  remote?: InputMaybe<Scalars['Boolean']>;
  companyId?: InputMaybe<Scalars['String']>;
  category?: InputMaybe<Scalars['String']>;
}>;

export type GetRelatedJobsQuery = {
  __typename?: 'Query';
  Jobs: {
    __typename?: 'JobDocumentWithCount';
    totalCount: number;
    jobResult: Array<{
      __typename?: 'JobWithId';
      _id: string;
      name: string;
      shortName: string;
      sourceType: SourceTypeEnum;
      contents?: string | null;
      jobType: JobTypeEnum;
      locations: Array<string>;
      categories?: Array<string> | null;
      experienceLevels: Array<ExperienceLevelEnum>;
      educationLevel?: string | null;
      benefits?: Array<string> | null;
      requirements?: Array<string> | null;
      description?: string | null;
      salary?: string | null;
      companyId: string;
      companyName: string;
      applyUrl: string;
      remote: boolean;
      status: string;
      createAt: string;
    }>;
  };
};

export type GetJobTrendsQueryVariables = Exact<{
  endYear: Scalars['String'];
  startYear: Scalars['String'];
  geo?: InputMaybe<Scalars['String']>;
}>;

export type GetJobTrendsQuery = {
  __typename?: 'Query';
  JobTrend: {
    __typename?: 'JobTrendResponse';
    startDate: string;
    keyword: Array<string>;
    averages: Array<{ __typename?: 'AverageType'; name: string; score: number }>;
    timelineData: Array<{
      __typename?: 'TimelineTrendType';
      formattedTime: string;
      formattedValue: Array<string>;
      formattedAxisTime: string;
      value: Array<number>;
    }>;
  };
};

export type CreateJobMutationVariables = Exact<{
  CreateJobInputDto: CreateJobInputDto;
}>;

export type CreateJobMutation = {
  __typename?: 'Mutation';
  CreateJob: { __typename?: 'JobWithId'; _id: string; name: string };
};

export type UpdateJobMutationVariables = Exact<{
  UpdateJobInputDto: UpdateJobInputDto;
}>;

export type UpdateJobMutation = {
  __typename?: 'Mutation';
  UpdateJob: { __typename?: 'JobWithId'; _id: string; name: string };
};

export type RemoveJobMutationVariables = Exact<{
  id: Scalars['String'];
}>;

export type RemoveJobMutation = { __typename?: 'Mutation'; RemoveJob: number };

export type GetSearchResultQueryVariables = Exact<{
  limit?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  sortBy?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  location?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
  jobType?: InputMaybe<Scalars['String']>;
  remote?: InputMaybe<Scalars['Boolean']>;
}>;

export type GetSearchResultQuery = {
  __typename?: 'Query';
  Jobs: {
    __typename?: 'JobDocumentWithCount';
    totalCount: number;
    jobResult: Array<{
      __typename?: 'JobWithId';
      _id: string;
      name: string;
      shortName: string;
      sourceType: SourceTypeEnum;
      contents?: string | null;
      jobType: JobTypeEnum;
      locations: Array<string>;
      categories?: Array<string> | null;
      experienceLevels: Array<ExperienceLevelEnum>;
      companyId: string;
      companyName: string;
      applyUrl: string;
      salary?: string | null;
      remote: boolean;
      benefits?: Array<string> | null;
      status: string;
      createAt: string;
    }>;
  };
};

export type GetUserFromSessionQueryVariables = Exact<{ [key: string]: never }>;

export type GetUserFromSessionQuery = {
  __typename?: 'Query';
  GetUserBySession?: {
    __typename?: 'UserWithId';
    _id: string;
    email: string;
    firstname: string;
    lastname: string;
    middlename?: string | null;
    dob: any;
    phonenumber: string;
    address: string;
    role: UserRoleEnum;
    description?: string | null;
    education?: Array<{
      __typename?: 'EducationDetailType';
      school: string;
      graduationYear: string;
      educationLevel: EducationLevelEnum;
    }> | null;
    experience?: Array<{
      __typename?: 'ExperienceType';
      name: string;
      year: string;
      description?: string | null;
    }> | null;
    skill?: Array<{ __typename?: 'SkillType'; name: string }> | null;
  } | null;
};

export type UpdateUserMutationVariables = Exact<{
  UserUpdateInputDto: UserUpdateInputDto;
}>;

export type UpdateUserMutation = {
  __typename?: 'Mutation';
  UpdateUser: {
    __typename?: 'UserWithId';
    _id: string;
    email: string;
    firstname: string;
    lastname: string;
    middlename?: string | null;
    dob: any;
    phonenumber: string;
    address: string;
    role: UserRoleEnum;
    description?: string | null;
    education?: Array<{
      __typename?: 'EducationDetailType';
      school: string;
      graduationYear: string;
      educationLevel: EducationLevelEnum;
    }> | null;
    experience?: Array<{
      __typename?: 'ExperienceType';
      name: string;
      year: string;
      description?: string | null;
    }> | null;
    skill?: Array<{ __typename?: 'SkillType'; name: string }> | null;
  };
};

export const AddWatchingListDocument = gql`
  mutation AddWatchingList($WatchingListInputDto: WatchingListInputDto!) {
    CreateWatchingList(WatchingListInputDto: $WatchingListInputDto) {
      createAt
    }
  }
`;
export type AddWatchingListMutationFn = Apollo.MutationFunction<
  AddWatchingListMutation,
  AddWatchingListMutationVariables
>;

/**
 * __useAddWatchingListMutation__
 *
 * To run a mutation, you first call `useAddWatchingListMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddWatchingListMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addWatchingListMutation, { data, loading, error }] = useAddWatchingListMutation({
 *   variables: {
 *      WatchingListInputDto: // value for 'WatchingListInputDto'
 *   },
 * });
 */
export function useAddWatchingListMutation(
  baseOptions?: Apollo.MutationHookOptions<
    AddWatchingListMutation,
    AddWatchingListMutationVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<AddWatchingListMutation, AddWatchingListMutationVariables>(
    AddWatchingListDocument,
    options,
  );
}
export type AddWatchingListMutationHookResult = ReturnType<
  typeof useAddWatchingListMutation
>;
export type AddWatchingListMutationResult =
  Apollo.MutationResult<AddWatchingListMutation>;
export type AddWatchingListMutationOptions = Apollo.BaseMutationOptions<
  AddWatchingListMutation,
  AddWatchingListMutationVariables
>;
export const RemoveWatchingListDocument = gql`
  mutation RemoveWatchingList($WatchingListInputDto: WatchingListInputDto!) {
    RemoveWatchingList(WatchingListInputDto: $WatchingListInputDto) {
      createAt
    }
  }
`;
export type RemoveWatchingListMutationFn = Apollo.MutationFunction<
  RemoveWatchingListMutation,
  RemoveWatchingListMutationVariables
>;

/**
 * __useRemoveWatchingListMutation__
 *
 * To run a mutation, you first call `useRemoveWatchingListMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRemoveWatchingListMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [removeWatchingListMutation, { data, loading, error }] = useRemoveWatchingListMutation({
 *   variables: {
 *      WatchingListInputDto: // value for 'WatchingListInputDto'
 *   },
 * });
 */
export function useRemoveWatchingListMutation(
  baseOptions?: Apollo.MutationHookOptions<
    RemoveWatchingListMutation,
    RemoveWatchingListMutationVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    RemoveWatchingListMutation,
    RemoveWatchingListMutationVariables
  >(RemoveWatchingListDocument, options);
}
export type RemoveWatchingListMutationHookResult = ReturnType<
  typeof useRemoveWatchingListMutation
>;
export type RemoveWatchingListMutationResult =
  Apollo.MutationResult<RemoveWatchingListMutation>;
export type RemoveWatchingListMutationOptions = Apollo.BaseMutationOptions<
  RemoveWatchingListMutation,
  RemoveWatchingListMutationVariables
>;
export const GetWatchingListByUserIdDocument = gql`
  query GetWatchingListByUserId($userId: String!) {
    WatchingLists(userId: $userId) {
      _id
      userId
      list {
        companyId
        listType
        location
        name
        createAt
      }
    }
  }
`;

/**
 * __useGetWatchingListByUserIdQuery__
 *
 * To run a query within a React component, call `useGetWatchingListByUserIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetWatchingListByUserIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetWatchingListByUserIdQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useGetWatchingListByUserIdQuery(
  baseOptions: Apollo.QueryHookOptions<
    GetWatchingListByUserIdQuery,
    GetWatchingListByUserIdQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<
    GetWatchingListByUserIdQuery,
    GetWatchingListByUserIdQueryVariables
  >(GetWatchingListByUserIdDocument, options);
}
export function useGetWatchingListByUserIdLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetWatchingListByUserIdQuery,
    GetWatchingListByUserIdQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<
    GetWatchingListByUserIdQuery,
    GetWatchingListByUserIdQueryVariables
  >(GetWatchingListByUserIdDocument, options);
}
export type GetWatchingListByUserIdQueryHookResult = ReturnType<
  typeof useGetWatchingListByUserIdQuery
>;
export type GetWatchingListByUserIdLazyQueryHookResult = ReturnType<
  typeof useGetWatchingListByUserIdLazyQuery
>;
export type GetWatchingListByUserIdQueryResult = Apollo.QueryResult<
  GetWatchingListByUserIdQuery,
  GetWatchingListByUserIdQueryVariables
>;
export const GetPendingPublisherDocument = gql`
  query GetPendingPublisher(
    $limit: Int!
    $skip: Int!
    $status: String
    $sortBy: String
    $sortOrder: String
  ) {
    Publishers(
      limit: $limit
      skip: $skip
      status: $status
      sortBy: $sortBy
      sortOrder: $sortOrder
    ) {
      publisherResult {
        _id
        email
        firstname
        lastname
        middlename
        job
        companyId
        status
        phonenumber
        role
      }
      totalCount
    }
  }
`;

/**
 * __useGetPendingPublisherQuery__
 *
 * To run a query within a React component, call `useGetPendingPublisherQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPendingPublisherQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPendingPublisherQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      skip: // value for 'skip'
 *      status: // value for 'status'
 *      sortBy: // value for 'sortBy'
 *      sortOrder: // value for 'sortOrder'
 *   },
 * });
 */
export function useGetPendingPublisherQuery(
  baseOptions: Apollo.QueryHookOptions<
    GetPendingPublisherQuery,
    GetPendingPublisherQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetPendingPublisherQuery, GetPendingPublisherQueryVariables>(
    GetPendingPublisherDocument,
    options,
  );
}
export function useGetPendingPublisherLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetPendingPublisherQuery,
    GetPendingPublisherQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetPendingPublisherQuery, GetPendingPublisherQueryVariables>(
    GetPendingPublisherDocument,
    options,
  );
}
export type GetPendingPublisherQueryHookResult = ReturnType<
  typeof useGetPendingPublisherQuery
>;
export type GetPendingPublisherLazyQueryHookResult = ReturnType<
  typeof useGetPendingPublisherLazyQuery
>;
export type GetPendingPublisherQueryResult = Apollo.QueryResult<
  GetPendingPublisherQuery,
  GetPendingPublisherQueryVariables
>;
export const UpdatePublisherDocument = gql`
  mutation UpdatePublisher($PublisherUpdateInputDto: PublisherUpdateInputDto!) {
    UpdatePublisher(PublisherUpdateInputDto: $PublisherUpdateInputDto) {
      status
    }
  }
`;
export type UpdatePublisherMutationFn = Apollo.MutationFunction<
  UpdatePublisherMutation,
  UpdatePublisherMutationVariables
>;

/**
 * __useUpdatePublisherMutation__
 *
 * To run a mutation, you first call `useUpdatePublisherMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdatePublisherMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updatePublisherMutation, { data, loading, error }] = useUpdatePublisherMutation({
 *   variables: {
 *      PublisherUpdateInputDto: // value for 'PublisherUpdateInputDto'
 *   },
 * });
 */
export function useUpdatePublisherMutation(
  baseOptions?: Apollo.MutationHookOptions<
    UpdatePublisherMutation,
    UpdatePublisherMutationVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<UpdatePublisherMutation, UpdatePublisherMutationVariables>(
    UpdatePublisherDocument,
    options,
  );
}
export type UpdatePublisherMutationHookResult = ReturnType<
  typeof useUpdatePublisherMutation
>;
export type UpdatePublisherMutationResult =
  Apollo.MutationResult<UpdatePublisherMutation>;
export type UpdatePublisherMutationOptions = Apollo.BaseMutationOptions<
  UpdatePublisherMutation,
  UpdatePublisherMutationVariables
>;
export const DashboardStatDocument = gql`
  query DashboardStat {
    DashboardStat {
      activeData {
        name
        totalCount
      }
      nonActiveData {
        name
        totalCount
      }
    }
  }
`;

/**
 * __useDashboardStatQuery__
 *
 * To run a query within a React component, call `useDashboardStatQuery` and pass it any options that fit your needs.
 * When your component renders, `useDashboardStatQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useDashboardStatQuery({
 *   variables: {
 *   },
 * });
 */
export function useDashboardStatQuery(
  baseOptions?: Apollo.QueryHookOptions<DashboardStatQuery, DashboardStatQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<DashboardStatQuery, DashboardStatQueryVariables>(
    DashboardStatDocument,
    options,
  );
}
export function useDashboardStatLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    DashboardStatQuery,
    DashboardStatQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<DashboardStatQuery, DashboardStatQueryVariables>(
    DashboardStatDocument,
    options,
  );
}
export type DashboardStatQueryHookResult = ReturnType<typeof useDashboardStatQuery>;
export type DashboardStatLazyQueryHookResult = ReturnType<
  typeof useDashboardStatLazyQuery
>;
export type DashboardStatQueryResult = Apollo.QueryResult<
  DashboardStatQuery,
  DashboardStatQueryVariables
>;
export const LastestJobStatDocument = gql`
  query LastestJobStat {
    LastestJobStat {
      name
      count
    }
  }
`;

/**
 * __useLastestJobStatQuery__
 *
 * To run a query within a React component, call `useLastestJobStatQuery` and pass it any options that fit your needs.
 * When your component renders, `useLastestJobStatQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useLastestJobStatQuery({
 *   variables: {
 *   },
 * });
 */
export function useLastestJobStatQuery(
  baseOptions?: Apollo.QueryHookOptions<
    LastestJobStatQuery,
    LastestJobStatQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<LastestJobStatQuery, LastestJobStatQueryVariables>(
    LastestJobStatDocument,
    options,
  );
}
export function useLastestJobStatLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    LastestJobStatQuery,
    LastestJobStatQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<LastestJobStatQuery, LastestJobStatQueryVariables>(
    LastestJobStatDocument,
    options,
  );
}
export type LastestJobStatQueryHookResult = ReturnType<typeof useLastestJobStatQuery>;
export type LastestJobStatLazyQueryHookResult = ReturnType<
  typeof useLastestJobStatLazyQuery
>;
export type LastestJobStatQueryResult = Apollo.QueryResult<
  LastestJobStatQuery,
  LastestJobStatQueryVariables
>;
export const LoginDocument = gql`
  mutation Login($UserLoginInputDTO: UserLoginInputDTO!) {
    Login(UserLoginInputDTO: $UserLoginInputDTO) {
      _id
      email
      firstname
      middlename
      lastname
      phonenumber
      dob
      address
      role
    }
  }
`;
export type LoginMutationFn = Apollo.MutationFunction<
  LoginMutation,
  LoginMutationVariables
>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      UserLoginInputDTO: // value for 'UserLoginInputDTO'
 *   },
 * });
 */
export function useLoginMutation(
  baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<LoginMutation, LoginMutationVariables>(
    LoginDocument,
    options,
  );
}
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<
  LoginMutation,
  LoginMutationVariables
>;
export const LoginPublisherDocument = gql`
  mutation LoginPublisher($UserLoginInputDTO: UserLoginInputDTO!) {
    LoginPublisher(UserLoginInputDTO: $UserLoginInputDTO) {
      _id
      email
      firstname
      middlename
      lastname
      phonenumber
      job
      companyId
      status
      role
    }
  }
`;
export type LoginPublisherMutationFn = Apollo.MutationFunction<
  LoginPublisherMutation,
  LoginPublisherMutationVariables
>;

/**
 * __useLoginPublisherMutation__
 *
 * To run a mutation, you first call `useLoginPublisherMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginPublisherMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginPublisherMutation, { data, loading, error }] = useLoginPublisherMutation({
 *   variables: {
 *      UserLoginInputDTO: // value for 'UserLoginInputDTO'
 *   },
 * });
 */
export function useLoginPublisherMutation(
  baseOptions?: Apollo.MutationHookOptions<
    LoginPublisherMutation,
    LoginPublisherMutationVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<LoginPublisherMutation, LoginPublisherMutationVariables>(
    LoginPublisherDocument,
    options,
  );
}
export type LoginPublisherMutationHookResult = ReturnType<
  typeof useLoginPublisherMutation
>;
export type LoginPublisherMutationResult = Apollo.MutationResult<LoginPublisherMutation>;
export type LoginPublisherMutationOptions = Apollo.BaseMutationOptions<
  LoginPublisherMutation,
  LoginPublisherMutationVariables
>;
export const LogoutDocument = gql`
  mutation Logout {
    Logout
  }
`;
export type LogoutMutationFn = Apollo.MutationFunction<
  LogoutMutation,
  LogoutMutationVariables
>;

/**
 * __useLogoutMutation__
 *
 * To run a mutation, you first call `useLogoutMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLogoutMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [logoutMutation, { data, loading, error }] = useLogoutMutation({
 *   variables: {
 *   },
 * });
 */
export function useLogoutMutation(
  baseOptions?: Apollo.MutationHookOptions<LogoutMutation, LogoutMutationVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<LogoutMutation, LogoutMutationVariables>(
    LogoutDocument,
    options,
  );
}
export type LogoutMutationHookResult = ReturnType<typeof useLogoutMutation>;
export type LogoutMutationResult = Apollo.MutationResult<LogoutMutation>;
export type LogoutMutationOptions = Apollo.BaseMutationOptions<
  LogoutMutation,
  LogoutMutationVariables
>;
export const RefreshTokenDocument = gql`
  mutation RefreshToken {
    RefreshToken
  }
`;
export type RefreshTokenMutationFn = Apollo.MutationFunction<
  RefreshTokenMutation,
  RefreshTokenMutationVariables
>;

/**
 * __useRefreshTokenMutation__
 *
 * To run a mutation, you first call `useRefreshTokenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRefreshTokenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [refreshTokenMutation, { data, loading, error }] = useRefreshTokenMutation({
 *   variables: {
 *   },
 * });
 */
export function useRefreshTokenMutation(
  baseOptions?: Apollo.MutationHookOptions<
    RefreshTokenMutation,
    RefreshTokenMutationVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<RefreshTokenMutation, RefreshTokenMutationVariables>(
    RefreshTokenDocument,
    options,
  );
}
export type RefreshTokenMutationHookResult = ReturnType<typeof useRefreshTokenMutation>;
export type RefreshTokenMutationResult = Apollo.MutationResult<RefreshTokenMutation>;
export type RefreshTokenMutationOptions = Apollo.BaseMutationOptions<
  RefreshTokenMutation,
  RefreshTokenMutationVariables
>;
export const GetServerStatusDocument = gql`
  query GetServerStatus {
    ServerStatus {
      status
    }
  }
`;

/**
 * __useGetServerStatusQuery__
 *
 * To run a query within a React component, call `useGetServerStatusQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetServerStatusQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetServerStatusQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetServerStatusQuery(
  baseOptions?: Apollo.QueryHookOptions<
    GetServerStatusQuery,
    GetServerStatusQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetServerStatusQuery, GetServerStatusQueryVariables>(
    GetServerStatusDocument,
    options,
  );
}
export function useGetServerStatusLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetServerStatusQuery,
    GetServerStatusQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetServerStatusQuery, GetServerStatusQueryVariables>(
    GetServerStatusDocument,
    options,
  );
}
export type GetServerStatusQueryHookResult = ReturnType<typeof useGetServerStatusQuery>;
export type GetServerStatusLazyQueryHookResult = ReturnType<
  typeof useGetServerStatusLazyQuery
>;
export type GetServerStatusQueryResult = Apollo.QueryResult<
  GetServerStatusQuery,
  GetServerStatusQueryVariables
>;
export const CreateUserDocument = gql`
  mutation CreateUser($UserRegisterInputDto: UserRegisterInputDto!) {
    CreateUser(UserRegisterInputDto: $UserRegisterInputDto) {
      _id
      email
      firstname
      middlename
      password
      lastname
      phonenumber
      dob
      address
      role
    }
  }
`;
export type CreateUserMutationFn = Apollo.MutationFunction<
  CreateUserMutation,
  CreateUserMutationVariables
>;

/**
 * __useCreateUserMutation__
 *
 * To run a mutation, you first call `useCreateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createUserMutation, { data, loading, error }] = useCreateUserMutation({
 *   variables: {
 *      UserRegisterInputDto: // value for 'UserRegisterInputDto'
 *   },
 * });
 */
export function useCreateUserMutation(
  baseOptions?: Apollo.MutationHookOptions<
    CreateUserMutation,
    CreateUserMutationVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<CreateUserMutation, CreateUserMutationVariables>(
    CreateUserDocument,
    options,
  );
}
export type CreateUserMutationHookResult = ReturnType<typeof useCreateUserMutation>;
export type CreateUserMutationResult = Apollo.MutationResult<CreateUserMutation>;
export type CreateUserMutationOptions = Apollo.BaseMutationOptions<
  CreateUserMutation,
  CreateUserMutationVariables
>;
export const CreatePublisherDocument = gql`
  mutation CreatePublisher($PublisherRegisterInputDto: PublisherRegisterInputDto!) {
    CreatePublisher(PublisherRegisterInputDto: $PublisherRegisterInputDto) {
      _id
      email
      firstname
      lastname
      middlename
      phonenumber
      job
      companyId
      status
      role
    }
  }
`;
export type CreatePublisherMutationFn = Apollo.MutationFunction<
  CreatePublisherMutation,
  CreatePublisherMutationVariables
>;

/**
 * __useCreatePublisherMutation__
 *
 * To run a mutation, you first call `useCreatePublisherMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreatePublisherMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createPublisherMutation, { data, loading, error }] = useCreatePublisherMutation({
 *   variables: {
 *      PublisherRegisterInputDto: // value for 'PublisherRegisterInputDto'
 *   },
 * });
 */
export function useCreatePublisherMutation(
  baseOptions?: Apollo.MutationHookOptions<
    CreatePublisherMutation,
    CreatePublisherMutationVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<CreatePublisherMutation, CreatePublisherMutationVariables>(
    CreatePublisherDocument,
    options,
  );
}
export type CreatePublisherMutationHookResult = ReturnType<
  typeof useCreatePublisherMutation
>;
export type CreatePublisherMutationResult =
  Apollo.MutationResult<CreatePublisherMutation>;
export type CreatePublisherMutationOptions = Apollo.BaseMutationOptions<
  CreatePublisherMutation,
  CreatePublisherMutationVariables
>;
export const GetCompanyForPublisherRegisterDocument = gql`
  query GetCompanyForPublisherRegister($sortBy: String!) {
    Companies(sortBy: $sortBy) {
      companyResult {
        _id
        name
      }
    }
  }
`;

/**
 * __useGetCompanyForPublisherRegisterQuery__
 *
 * To run a query within a React component, call `useGetCompanyForPublisherRegisterQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCompanyForPublisherRegisterQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCompanyForPublisherRegisterQuery({
 *   variables: {
 *      sortBy: // value for 'sortBy'
 *   },
 * });
 */
export function useGetCompanyForPublisherRegisterQuery(
  baseOptions: Apollo.QueryHookOptions<
    GetCompanyForPublisherRegisterQuery,
    GetCompanyForPublisherRegisterQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<
    GetCompanyForPublisherRegisterQuery,
    GetCompanyForPublisherRegisterQueryVariables
  >(GetCompanyForPublisherRegisterDocument, options);
}
export function useGetCompanyForPublisherRegisterLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetCompanyForPublisherRegisterQuery,
    GetCompanyForPublisherRegisterQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<
    GetCompanyForPublisherRegisterQuery,
    GetCompanyForPublisherRegisterQueryVariables
  >(GetCompanyForPublisherRegisterDocument, options);
}
export type GetCompanyForPublisherRegisterQueryHookResult = ReturnType<
  typeof useGetCompanyForPublisherRegisterQuery
>;
export type GetCompanyForPublisherRegisterLazyQueryHookResult = ReturnType<
  typeof useGetCompanyForPublisherRegisterLazyQuery
>;
export type GetCompanyForPublisherRegisterQueryResult = Apollo.QueryResult<
  GetCompanyForPublisherRegisterQuery,
  GetCompanyForPublisherRegisterQueryVariables
>;
export const CreateCompanyDocument = gql`
  mutation CreateCompany($CreateCompanyInputDto: CreateCompanyInputDto!) {
    CreateCompany(CreateCompanyInputDto: $CreateCompanyInputDto) {
      _id
      name
    }
  }
`;
export type CreateCompanyMutationFn = Apollo.MutationFunction<
  CreateCompanyMutation,
  CreateCompanyMutationVariables
>;

/**
 * __useCreateCompanyMutation__
 *
 * To run a mutation, you first call `useCreateCompanyMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCompanyMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCompanyMutation, { data, loading, error }] = useCreateCompanyMutation({
 *   variables: {
 *      CreateCompanyInputDto: // value for 'CreateCompanyInputDto'
 *   },
 * });
 */
export function useCreateCompanyMutation(
  baseOptions?: Apollo.MutationHookOptions<
    CreateCompanyMutation,
    CreateCompanyMutationVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<CreateCompanyMutation, CreateCompanyMutationVariables>(
    CreateCompanyDocument,
    options,
  );
}
export type CreateCompanyMutationHookResult = ReturnType<typeof useCreateCompanyMutation>;
export type CreateCompanyMutationResult = Apollo.MutationResult<CreateCompanyMutation>;
export type CreateCompanyMutationOptions = Apollo.BaseMutationOptions<
  CreateCompanyMutation,
  CreateCompanyMutationVariables
>;
export const GetCompaniesAutocompleteDocument = gql`
  query GetCompaniesAutocomplete($sortBy: String!) {
    Companies(sortBy: $sortBy) {
      companyResult {
        name
      }
    }
  }
`;

/**
 * __useGetCompaniesAutocompleteQuery__
 *
 * To run a query within a React component, call `useGetCompaniesAutocompleteQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCompaniesAutocompleteQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCompaniesAutocompleteQuery({
 *   variables: {
 *      sortBy: // value for 'sortBy'
 *   },
 * });
 */
export function useGetCompaniesAutocompleteQuery(
  baseOptions: Apollo.QueryHookOptions<
    GetCompaniesAutocompleteQuery,
    GetCompaniesAutocompleteQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<
    GetCompaniesAutocompleteQuery,
    GetCompaniesAutocompleteQueryVariables
  >(GetCompaniesAutocompleteDocument, options);
}
export function useGetCompaniesAutocompleteLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetCompaniesAutocompleteQuery,
    GetCompaniesAutocompleteQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<
    GetCompaniesAutocompleteQuery,
    GetCompaniesAutocompleteQueryVariables
  >(GetCompaniesAutocompleteDocument, options);
}
export type GetCompaniesAutocompleteQueryHookResult = ReturnType<
  typeof useGetCompaniesAutocompleteQuery
>;
export type GetCompaniesAutocompleteLazyQueryHookResult = ReturnType<
  typeof useGetCompaniesAutocompleteLazyQuery
>;
export type GetCompaniesAutocompleteQueryResult = Apollo.QueryResult<
  GetCompaniesAutocompleteQuery,
  GetCompaniesAutocompleteQueryVariables
>;
export const GetCompanyDetailDocument = gql`
  query GetCompanyDetail($id: String!) {
    Company(id: $id) {
      _id
      sourceType
      sourceId
      description
      locations
      industries
      shortName
      name
      size
      logoImage
      createAt
    }
  }
`;

/**
 * __useGetCompanyDetailQuery__
 *
 * To run a query within a React component, call `useGetCompanyDetailQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCompanyDetailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCompanyDetailQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetCompanyDetailQuery(
  baseOptions: Apollo.QueryHookOptions<
    GetCompanyDetailQuery,
    GetCompanyDetailQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetCompanyDetailQuery, GetCompanyDetailQueryVariables>(
    GetCompanyDetailDocument,
    options,
  );
}
export function useGetCompanyDetailLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetCompanyDetailQuery,
    GetCompanyDetailQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetCompanyDetailQuery, GetCompanyDetailQueryVariables>(
    GetCompanyDetailDocument,
    options,
  );
}
export type GetCompanyDetailQueryHookResult = ReturnType<typeof useGetCompanyDetailQuery>;
export type GetCompanyDetailLazyQueryHookResult = ReturnType<
  typeof useGetCompanyDetailLazyQuery
>;
export type GetCompanyDetailQueryResult = Apollo.QueryResult<
  GetCompanyDetailQuery,
  GetCompanyDetailQueryVariables
>;
export const GetCompaniesDocument = gql`
  query GetCompanies(
    $sortBy: String!
    $name: String!
    $size: String!
    $location: String!
    $limit: Int!
    $skip: Int!
    $industry: String!
  ) {
    Companies(
      sortBy: $sortBy
      name: $name
      size: $size
      location: $location
      limit: $limit
      skip: $skip
      industry: $industry
    ) {
      companyResult {
        _id
        sourceType
        sourceId
        description
        locations
        industries
        shortName
        name
        size
        logoImage
        createAt
      }
      totalCount
    }
  }
`;

/**
 * __useGetCompaniesQuery__
 *
 * To run a query within a React component, call `useGetCompaniesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCompaniesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCompaniesQuery({
 *   variables: {
 *      sortBy: // value for 'sortBy'
 *      name: // value for 'name'
 *      size: // value for 'size'
 *      location: // value for 'location'
 *      limit: // value for 'limit'
 *      skip: // value for 'skip'
 *      industry: // value for 'industry'
 *   },
 * });
 */
export function useGetCompaniesQuery(
  baseOptions: Apollo.QueryHookOptions<GetCompaniesQuery, GetCompaniesQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetCompaniesQuery, GetCompaniesQueryVariables>(
    GetCompaniesDocument,
    options,
  );
}
export function useGetCompaniesLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetCompaniesQuery,
    GetCompaniesQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetCompaniesQuery, GetCompaniesQueryVariables>(
    GetCompaniesDocument,
    options,
  );
}
export type GetCompaniesQueryHookResult = ReturnType<typeof useGetCompaniesQuery>;
export type GetCompaniesLazyQueryHookResult = ReturnType<typeof useGetCompaniesLazyQuery>;
export type GetCompaniesQueryResult = Apollo.QueryResult<
  GetCompaniesQuery,
  GetCompaniesQueryVariables
>;
export const GetJobByCompanyIdDocument = gql`
  query GetJobByCompanyId(
    $sortBy: String!
    $companyId: String!
    $limit: Int!
    $skip: Int!
    $sortOrder: String
  ) {
    Jobs(
      sortBy: $sortBy
      companyId: $companyId
      limit: $limit
      skip: $skip
      sortOrder: $sortOrder
    ) {
      jobResult {
        _id
        name
        shortName
        sourceType
        contents
        jobType
        locations
        categories
        experienceLevels
        educationLevel
        benefits
        requirements
        description
        salary
        companyId
        companyName
        applyUrl
        salary
        remote
        benefits
        status
        createAt
        updateAt
      }
      totalCount
    }
  }
`;

/**
 * __useGetJobByCompanyIdQuery__
 *
 * To run a query within a React component, call `useGetJobByCompanyIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetJobByCompanyIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetJobByCompanyIdQuery({
 *   variables: {
 *      sortBy: // value for 'sortBy'
 *      companyId: // value for 'companyId'
 *      limit: // value for 'limit'
 *      skip: // value for 'skip'
 *      sortOrder: // value for 'sortOrder'
 *   },
 * });
 */
export function useGetJobByCompanyIdQuery(
  baseOptions: Apollo.QueryHookOptions<
    GetJobByCompanyIdQuery,
    GetJobByCompanyIdQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetJobByCompanyIdQuery, GetJobByCompanyIdQueryVariables>(
    GetJobByCompanyIdDocument,
    options,
  );
}
export function useGetJobByCompanyIdLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetJobByCompanyIdQuery,
    GetJobByCompanyIdQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetJobByCompanyIdQuery, GetJobByCompanyIdQueryVariables>(
    GetJobByCompanyIdDocument,
    options,
  );
}
export type GetJobByCompanyIdQueryHookResult = ReturnType<
  typeof useGetJobByCompanyIdQuery
>;
export type GetJobByCompanyIdLazyQueryHookResult = ReturnType<
  typeof useGetJobByCompanyIdLazyQuery
>;
export type GetJobByCompanyIdQueryResult = Apollo.QueryResult<
  GetJobByCompanyIdQuery,
  GetJobByCompanyIdQueryVariables
>;
export const GetReviewsByCompanyIdDocument = gql`
  query GetReviewsByCompanyId(
    $sortBy: String!
    $companyId: String!
    $limit: Int!
    $skip: Int!
    $sortOrder: String!
  ) {
    Reviews(
      sortBy: $sortBy
      companyId: $companyId
      limit: $limit
      skip: $skip
      sortOrder: $sortOrder
    ) {
      reviewResult {
        _id
        title
        score
        authorPosition
        content
        pro
        con
        createAt
      }
      totalCount
    }
  }
`;

/**
 * __useGetReviewsByCompanyIdQuery__
 *
 * To run a query within a React component, call `useGetReviewsByCompanyIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetReviewsByCompanyIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetReviewsByCompanyIdQuery({
 *   variables: {
 *      sortBy: // value for 'sortBy'
 *      companyId: // value for 'companyId'
 *      limit: // value for 'limit'
 *      skip: // value for 'skip'
 *      sortOrder: // value for 'sortOrder'
 *   },
 * });
 */
export function useGetReviewsByCompanyIdQuery(
  baseOptions: Apollo.QueryHookOptions<
    GetReviewsByCompanyIdQuery,
    GetReviewsByCompanyIdQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetReviewsByCompanyIdQuery, GetReviewsByCompanyIdQueryVariables>(
    GetReviewsByCompanyIdDocument,
    options,
  );
}
export function useGetReviewsByCompanyIdLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetReviewsByCompanyIdQuery,
    GetReviewsByCompanyIdQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<
    GetReviewsByCompanyIdQuery,
    GetReviewsByCompanyIdQueryVariables
  >(GetReviewsByCompanyIdDocument, options);
}
export type GetReviewsByCompanyIdQueryHookResult = ReturnType<
  typeof useGetReviewsByCompanyIdQuery
>;
export type GetReviewsByCompanyIdLazyQueryHookResult = ReturnType<
  typeof useGetReviewsByCompanyIdLazyQuery
>;
export type GetReviewsByCompanyIdQueryResult = Apollo.QueryResult<
  GetReviewsByCompanyIdQuery,
  GetReviewsByCompanyIdQueryVariables
>;
export const AddNewCompanyReviewDocument = gql`
  mutation AddNewCompanyReview($CreateReviewInputDto: CreateReviewInputDto!) {
    CreateReview(CreateReviewInputDto: $CreateReviewInputDto) {
      _id
      title
      score
      authorPosition
      content
      pro
      con
      createAt
    }
  }
`;
export type AddNewCompanyReviewMutationFn = Apollo.MutationFunction<
  AddNewCompanyReviewMutation,
  AddNewCompanyReviewMutationVariables
>;

/**
 * __useAddNewCompanyReviewMutation__
 *
 * To run a mutation, you first call `useAddNewCompanyReviewMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddNewCompanyReviewMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addNewCompanyReviewMutation, { data, loading, error }] = useAddNewCompanyReviewMutation({
 *   variables: {
 *      CreateReviewInputDto: // value for 'CreateReviewInputDto'
 *   },
 * });
 */
export function useAddNewCompanyReviewMutation(
  baseOptions?: Apollo.MutationHookOptions<
    AddNewCompanyReviewMutation,
    AddNewCompanyReviewMutationVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    AddNewCompanyReviewMutation,
    AddNewCompanyReviewMutationVariables
  >(AddNewCompanyReviewDocument, options);
}
export type AddNewCompanyReviewMutationHookResult = ReturnType<
  typeof useAddNewCompanyReviewMutation
>;
export type AddNewCompanyReviewMutationResult =
  Apollo.MutationResult<AddNewCompanyReviewMutation>;
export type AddNewCompanyReviewMutationOptions = Apollo.BaseMutationOptions<
  AddNewCompanyReviewMutation,
  AddNewCompanyReviewMutationVariables
>;
export const GetAutoCompleteDocument = gql`
  query GetAutoComplete($sortBy: String) {
    Jobs(sortBy: $sortBy) {
      jobResult {
        name
        companyName
      }
    }
  }
`;

/**
 * __useGetAutoCompleteQuery__
 *
 * To run a query within a React component, call `useGetAutoCompleteQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAutoCompleteQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAutoCompleteQuery({
 *   variables: {
 *      sortBy: // value for 'sortBy'
 *   },
 * });
 */
export function useGetAutoCompleteQuery(
  baseOptions?: Apollo.QueryHookOptions<
    GetAutoCompleteQuery,
    GetAutoCompleteQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetAutoCompleteQuery, GetAutoCompleteQueryVariables>(
    GetAutoCompleteDocument,
    options,
  );
}
export function useGetAutoCompleteLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetAutoCompleteQuery,
    GetAutoCompleteQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetAutoCompleteQuery, GetAutoCompleteQueryVariables>(
    GetAutoCompleteDocument,
    options,
  );
}
export type GetAutoCompleteQueryHookResult = ReturnType<typeof useGetAutoCompleteQuery>;
export type GetAutoCompleteLazyQueryHookResult = ReturnType<
  typeof useGetAutoCompleteLazyQuery
>;
export type GetAutoCompleteQueryResult = Apollo.QueryResult<
  GetAutoCompleteQuery,
  GetAutoCompleteQueryVariables
>;
export const GetLocationsDocument = gql`
  query GetLocations {
    Locations {
      name
    }
  }
`;

/**
 * __useGetLocationsQuery__
 *
 * To run a query within a React component, call `useGetLocationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetLocationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetLocationsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetLocationsQuery(
  baseOptions?: Apollo.QueryHookOptions<GetLocationsQuery, GetLocationsQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetLocationsQuery, GetLocationsQueryVariables>(
    GetLocationsDocument,
    options,
  );
}
export function useGetLocationsLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetLocationsQuery,
    GetLocationsQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetLocationsQuery, GetLocationsQueryVariables>(
    GetLocationsDocument,
    options,
  );
}
export type GetLocationsQueryHookResult = ReturnType<typeof useGetLocationsQuery>;
export type GetLocationsLazyQueryHookResult = ReturnType<typeof useGetLocationsLazyQuery>;
export type GetLocationsQueryResult = Apollo.QueryResult<
  GetLocationsQuery,
  GetLocationsQueryVariables
>;
export const GetLatestCompaniesDocument = gql`
  query GetLatestCompanies(
    $sortBy: String!
    $limit: Int!
    $skip: Int!
    $sortOrder: String
  ) {
    Companies(sortBy: $sortBy, limit: $limit, skip: $skip, sortOrder: $sortOrder) {
      companyResult {
        _id
        sourceType
        sourceId
        description
        locations
        industries
        shortName
        name
        size
        logoImage
        createAt
      }
      totalCount
    }
  }
`;

/**
 * __useGetLatestCompaniesQuery__
 *
 * To run a query within a React component, call `useGetLatestCompaniesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetLatestCompaniesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetLatestCompaniesQuery({
 *   variables: {
 *      sortBy: // value for 'sortBy'
 *      limit: // value for 'limit'
 *      skip: // value for 'skip'
 *      sortOrder: // value for 'sortOrder'
 *   },
 * });
 */
export function useGetLatestCompaniesQuery(
  baseOptions: Apollo.QueryHookOptions<
    GetLatestCompaniesQuery,
    GetLatestCompaniesQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetLatestCompaniesQuery, GetLatestCompaniesQueryVariables>(
    GetLatestCompaniesDocument,
    options,
  );
}
export function useGetLatestCompaniesLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetLatestCompaniesQuery,
    GetLatestCompaniesQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetLatestCompaniesQuery, GetLatestCompaniesQueryVariables>(
    GetLatestCompaniesDocument,
    options,
  );
}
export type GetLatestCompaniesQueryHookResult = ReturnType<
  typeof useGetLatestCompaniesQuery
>;
export type GetLatestCompaniesLazyQueryHookResult = ReturnType<
  typeof useGetLatestCompaniesLazyQuery
>;
export type GetLatestCompaniesQueryResult = Apollo.QueryResult<
  GetLatestCompaniesQuery,
  GetLatestCompaniesQueryVariables
>;
export const GetLatestJobsDocument = gql`
  query GetLatestJobs($limit: Int, $skip: Int, $sortBy: String, $sortOrder: String) {
    Jobs(limit: $limit, skip: $skip, sortBy: $sortBy, sortOrder: $sortOrder) {
      jobResult {
        _id
        name
        shortName
        sourceType
        contents
        jobType
        locations
        categories
        experienceLevels
        companyId
        companyName
        applyUrl
        salary
        remote
        benefits
        status
        createAt
      }
      totalCount
    }
  }
`;

/**
 * __useGetLatestJobsQuery__
 *
 * To run a query within a React component, call `useGetLatestJobsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetLatestJobsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetLatestJobsQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      skip: // value for 'skip'
 *      sortBy: // value for 'sortBy'
 *      sortOrder: // value for 'sortOrder'
 *   },
 * });
 */
export function useGetLatestJobsQuery(
  baseOptions?: Apollo.QueryHookOptions<GetLatestJobsQuery, GetLatestJobsQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetLatestJobsQuery, GetLatestJobsQueryVariables>(
    GetLatestJobsDocument,
    options,
  );
}
export function useGetLatestJobsLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetLatestJobsQuery,
    GetLatestJobsQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetLatestJobsQuery, GetLatestJobsQueryVariables>(
    GetLatestJobsDocument,
    options,
  );
}
export type GetLatestJobsQueryHookResult = ReturnType<typeof useGetLatestJobsQuery>;
export type GetLatestJobsLazyQueryHookResult = ReturnType<
  typeof useGetLatestJobsLazyQuery
>;
export type GetLatestJobsQueryResult = Apollo.QueryResult<
  GetLatestJobsQuery,
  GetLatestJobsQueryVariables
>;
export const GetJobDocument = gql`
  query GetJob(
    $limit: Int
    $skip: Int
    $sortBy: String
    $name: String
    $location: String
    $status: String
    $jobType: String
    $remote: Boolean
  ) {
    Jobs(
      limit: $limit
      skip: $skip
      sortBy: $sortBy
      name: $name
      location: $location
      status: $status
      jobType: $jobType
      remote: $remote
    ) {
      jobResult {
        _id
        name
        shortName
        sourceType
        contents
        jobType
        locations
        categories
        experienceLevels
        educationLevel
        benefits
        requirements
        description
        salary
        companyId
        companyName
        applyUrl
        salary
        remote
        benefits
        status
        createAt
      }
      totalCount
    }
  }
`;

/**
 * __useGetJobQuery__
 *
 * To run a query within a React component, call `useGetJobQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetJobQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetJobQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      skip: // value for 'skip'
 *      sortBy: // value for 'sortBy'
 *      name: // value for 'name'
 *      location: // value for 'location'
 *      status: // value for 'status'
 *      jobType: // value for 'jobType'
 *      remote: // value for 'remote'
 *   },
 * });
 */
export function useGetJobQuery(
  baseOptions?: Apollo.QueryHookOptions<GetJobQuery, GetJobQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetJobQuery, GetJobQueryVariables>(GetJobDocument, options);
}
export function useGetJobLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<GetJobQuery, GetJobQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetJobQuery, GetJobQueryVariables>(GetJobDocument, options);
}
export type GetJobQueryHookResult = ReturnType<typeof useGetJobQuery>;
export type GetJobLazyQueryHookResult = ReturnType<typeof useGetJobLazyQuery>;
export type GetJobQueryResult = Apollo.QueryResult<GetJobQuery, GetJobQueryVariables>;
export const GetJobAutoCompleteDocument = gql`
  query GetJobAutoComplete($sortBy: String) {
    Jobs(sortBy: $sortBy) {
      jobResult {
        name
      }
    }
  }
`;

/**
 * __useGetJobAutoCompleteQuery__
 *
 * To run a query within a React component, call `useGetJobAutoCompleteQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetJobAutoCompleteQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetJobAutoCompleteQuery({
 *   variables: {
 *      sortBy: // value for 'sortBy'
 *   },
 * });
 */
export function useGetJobAutoCompleteQuery(
  baseOptions?: Apollo.QueryHookOptions<
    GetJobAutoCompleteQuery,
    GetJobAutoCompleteQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetJobAutoCompleteQuery, GetJobAutoCompleteQueryVariables>(
    GetJobAutoCompleteDocument,
    options,
  );
}
export function useGetJobAutoCompleteLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetJobAutoCompleteQuery,
    GetJobAutoCompleteQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetJobAutoCompleteQuery, GetJobAutoCompleteQueryVariables>(
    GetJobAutoCompleteDocument,
    options,
  );
}
export type GetJobAutoCompleteQueryHookResult = ReturnType<
  typeof useGetJobAutoCompleteQuery
>;
export type GetJobAutoCompleteLazyQueryHookResult = ReturnType<
  typeof useGetJobAutoCompleteLazyQuery
>;
export type GetJobAutoCompleteQueryResult = Apollo.QueryResult<
  GetJobAutoCompleteQuery,
  GetJobAutoCompleteQueryVariables
>;
export const GetJobDetailDocument = gql`
  query GetJobDetail($id: String!) {
    Job(id: $id) {
      _id
      name
      shortName
      sourceType
      contents
      jobType
      locations
      categories
      experienceLevels
      educationLevel
      benefits
      requirements
      description
      salary
      companyId
      companyName
      applyUrl
      salary
      remote
      benefits
      status
      createAt
    }
  }
`;

/**
 * __useGetJobDetailQuery__
 *
 * To run a query within a React component, call `useGetJobDetailQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetJobDetailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetJobDetailQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetJobDetailQuery(
  baseOptions: Apollo.QueryHookOptions<GetJobDetailQuery, GetJobDetailQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetJobDetailQuery, GetJobDetailQueryVariables>(
    GetJobDetailDocument,
    options,
  );
}
export function useGetJobDetailLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetJobDetailQuery,
    GetJobDetailQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetJobDetailQuery, GetJobDetailQueryVariables>(
    GetJobDetailDocument,
    options,
  );
}
export type GetJobDetailQueryHookResult = ReturnType<typeof useGetJobDetailQuery>;
export type GetJobDetailLazyQueryHookResult = ReturnType<typeof useGetJobDetailLazyQuery>;
export type GetJobDetailQueryResult = Apollo.QueryResult<
  GetJobDetailQuery,
  GetJobDetailQueryVariables
>;
export const GetFavoritesByUserIdDocument = gql`
  query GetFavoritesByUserId($userId: String!) {
    Favorites(userId: $userId) {
      _id
      name
      shortName
      sourceType
      contents
      jobType
      locations
      categories
      experienceLevels
      companyId
      companyName
      applyUrl
      salary
      remote
      benefits
      status
      createAt
    }
  }
`;

/**
 * __useGetFavoritesByUserIdQuery__
 *
 * To run a query within a React component, call `useGetFavoritesByUserIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetFavoritesByUserIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetFavoritesByUserIdQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useGetFavoritesByUserIdQuery(
  baseOptions: Apollo.QueryHookOptions<
    GetFavoritesByUserIdQuery,
    GetFavoritesByUserIdQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetFavoritesByUserIdQuery, GetFavoritesByUserIdQueryVariables>(
    GetFavoritesByUserIdDocument,
    options,
  );
}
export function useGetFavoritesByUserIdLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetFavoritesByUserIdQuery,
    GetFavoritesByUserIdQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<
    GetFavoritesByUserIdQuery,
    GetFavoritesByUserIdQueryVariables
  >(GetFavoritesByUserIdDocument, options);
}
export type GetFavoritesByUserIdQueryHookResult = ReturnType<
  typeof useGetFavoritesByUserIdQuery
>;
export type GetFavoritesByUserIdLazyQueryHookResult = ReturnType<
  typeof useGetFavoritesByUserIdLazyQuery
>;
export type GetFavoritesByUserIdQueryResult = Apollo.QueryResult<
  GetFavoritesByUserIdQuery,
  GetFavoritesByUserIdQueryVariables
>;
export const AddFavoriteDocument = gql`
  mutation AddFavorite($FavoriteJobInputDto: FavoriteJobInputDto!) {
    CreateFavorite(FavoriteJobInputDto: $FavoriteJobInputDto) {
      createAt
    }
  }
`;
export type AddFavoriteMutationFn = Apollo.MutationFunction<
  AddFavoriteMutation,
  AddFavoriteMutationVariables
>;

/**
 * __useAddFavoriteMutation__
 *
 * To run a mutation, you first call `useAddFavoriteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddFavoriteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addFavoriteMutation, { data, loading, error }] = useAddFavoriteMutation({
 *   variables: {
 *      FavoriteJobInputDto: // value for 'FavoriteJobInputDto'
 *   },
 * });
 */
export function useAddFavoriteMutation(
  baseOptions?: Apollo.MutationHookOptions<
    AddFavoriteMutation,
    AddFavoriteMutationVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<AddFavoriteMutation, AddFavoriteMutationVariables>(
    AddFavoriteDocument,
    options,
  );
}
export type AddFavoriteMutationHookResult = ReturnType<typeof useAddFavoriteMutation>;
export type AddFavoriteMutationResult = Apollo.MutationResult<AddFavoriteMutation>;
export type AddFavoriteMutationOptions = Apollo.BaseMutationOptions<
  AddFavoriteMutation,
  AddFavoriteMutationVariables
>;
export const RemoveFavoriteDocument = gql`
  mutation RemoveFavorite($FavoriteJobInputDto: FavoriteJobInputDto!) {
    RemoveFavorite(FavoriteJobInputDto: $FavoriteJobInputDto) {
      createAt
    }
  }
`;
export type RemoveFavoriteMutationFn = Apollo.MutationFunction<
  RemoveFavoriteMutation,
  RemoveFavoriteMutationVariables
>;

/**
 * __useRemoveFavoriteMutation__
 *
 * To run a mutation, you first call `useRemoveFavoriteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRemoveFavoriteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [removeFavoriteMutation, { data, loading, error }] = useRemoveFavoriteMutation({
 *   variables: {
 *      FavoriteJobInputDto: // value for 'FavoriteJobInputDto'
 *   },
 * });
 */
export function useRemoveFavoriteMutation(
  baseOptions?: Apollo.MutationHookOptions<
    RemoveFavoriteMutation,
    RemoveFavoriteMutationVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<RemoveFavoriteMutation, RemoveFavoriteMutationVariables>(
    RemoveFavoriteDocument,
    options,
  );
}
export type RemoveFavoriteMutationHookResult = ReturnType<
  typeof useRemoveFavoriteMutation
>;
export type RemoveFavoriteMutationResult = Apollo.MutationResult<RemoveFavoriteMutation>;
export type RemoveFavoriteMutationOptions = Apollo.BaseMutationOptions<
  RemoveFavoriteMutation,
  RemoveFavoriteMutationVariables
>;
export const GetRelatedJobsDocument = gql`
  query GetRelatedJobs(
    $limit: Int
    $skip: Int
    $sortBy: String
    $name: String
    $location: String
    $status: String
    $jobType: String
    $remote: Boolean
    $companyId: String
    $category: String
  ) {
    Jobs(
      limit: $limit
      skip: $skip
      sortBy: $sortBy
      name: $name
      location: $location
      status: $status
      jobType: $jobType
      remote: $remote
      companyId: $companyId
      category: $category
    ) {
      jobResult {
        _id
        name
        shortName
        sourceType
        contents
        jobType
        locations
        categories
        experienceLevels
        educationLevel
        benefits
        requirements
        description
        salary
        companyId
        companyName
        applyUrl
        salary
        remote
        benefits
        status
        createAt
      }
      totalCount
    }
  }
`;

/**
 * __useGetRelatedJobsQuery__
 *
 * To run a query within a React component, call `useGetRelatedJobsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetRelatedJobsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetRelatedJobsQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      skip: // value for 'skip'
 *      sortBy: // value for 'sortBy'
 *      name: // value for 'name'
 *      location: // value for 'location'
 *      status: // value for 'status'
 *      jobType: // value for 'jobType'
 *      remote: // value for 'remote'
 *      companyId: // value for 'companyId'
 *      category: // value for 'category'
 *   },
 * });
 */
export function useGetRelatedJobsQuery(
  baseOptions?: Apollo.QueryHookOptions<
    GetRelatedJobsQuery,
    GetRelatedJobsQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetRelatedJobsQuery, GetRelatedJobsQueryVariables>(
    GetRelatedJobsDocument,
    options,
  );
}
export function useGetRelatedJobsLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetRelatedJobsQuery,
    GetRelatedJobsQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetRelatedJobsQuery, GetRelatedJobsQueryVariables>(
    GetRelatedJobsDocument,
    options,
  );
}
export type GetRelatedJobsQueryHookResult = ReturnType<typeof useGetRelatedJobsQuery>;
export type GetRelatedJobsLazyQueryHookResult = ReturnType<
  typeof useGetRelatedJobsLazyQuery
>;
export type GetRelatedJobsQueryResult = Apollo.QueryResult<
  GetRelatedJobsQuery,
  GetRelatedJobsQueryVariables
>;
export const GetJobTrendsDocument = gql`
  query GetJobTrends($endYear: String!, $startYear: String!, $geo: String) {
    JobTrend(endYear: $endYear, startYear: $startYear, geo: $geo) {
      startDate
      averages {
        name
        score
      }
      keyword
      startDate
      timelineData {
        formattedTime
        formattedValue
        formattedAxisTime
        value
      }
    }
  }
`;

/**
 * __useGetJobTrendsQuery__
 *
 * To run a query within a React component, call `useGetJobTrendsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetJobTrendsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetJobTrendsQuery({
 *   variables: {
 *      endYear: // value for 'endYear'
 *      startYear: // value for 'startYear'
 *      geo: // value for 'geo'
 *   },
 * });
 */
export function useGetJobTrendsQuery(
  baseOptions: Apollo.QueryHookOptions<GetJobTrendsQuery, GetJobTrendsQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetJobTrendsQuery, GetJobTrendsQueryVariables>(
    GetJobTrendsDocument,
    options,
  );
}
export function useGetJobTrendsLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetJobTrendsQuery,
    GetJobTrendsQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetJobTrendsQuery, GetJobTrendsQueryVariables>(
    GetJobTrendsDocument,
    options,
  );
}
export type GetJobTrendsQueryHookResult = ReturnType<typeof useGetJobTrendsQuery>;
export type GetJobTrendsLazyQueryHookResult = ReturnType<typeof useGetJobTrendsLazyQuery>;
export type GetJobTrendsQueryResult = Apollo.QueryResult<
  GetJobTrendsQuery,
  GetJobTrendsQueryVariables
>;
export const CreateJobDocument = gql`
  mutation CreateJob($CreateJobInputDto: CreateJobInputDto!) {
    CreateJob(CreateJobInputDto: $CreateJobInputDto) {
      _id
      name
    }
  }
`;
export type CreateJobMutationFn = Apollo.MutationFunction<
  CreateJobMutation,
  CreateJobMutationVariables
>;

/**
 * __useCreateJobMutation__
 *
 * To run a mutation, you first call `useCreateJobMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateJobMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createJobMutation, { data, loading, error }] = useCreateJobMutation({
 *   variables: {
 *      CreateJobInputDto: // value for 'CreateJobInputDto'
 *   },
 * });
 */
export function useCreateJobMutation(
  baseOptions?: Apollo.MutationHookOptions<CreateJobMutation, CreateJobMutationVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<CreateJobMutation, CreateJobMutationVariables>(
    CreateJobDocument,
    options,
  );
}
export type CreateJobMutationHookResult = ReturnType<typeof useCreateJobMutation>;
export type CreateJobMutationResult = Apollo.MutationResult<CreateJobMutation>;
export type CreateJobMutationOptions = Apollo.BaseMutationOptions<
  CreateJobMutation,
  CreateJobMutationVariables
>;
export const UpdateJobDocument = gql`
  mutation UpdateJob($UpdateJobInputDto: UpdateJobInputDto!) {
    UpdateJob(UpdateJobInputDto: $UpdateJobInputDto) {
      _id
      name
    }
  }
`;
export type UpdateJobMutationFn = Apollo.MutationFunction<
  UpdateJobMutation,
  UpdateJobMutationVariables
>;

/**
 * __useUpdateJobMutation__
 *
 * To run a mutation, you first call `useUpdateJobMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateJobMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateJobMutation, { data, loading, error }] = useUpdateJobMutation({
 *   variables: {
 *      UpdateJobInputDto: // value for 'UpdateJobInputDto'
 *   },
 * });
 */
export function useUpdateJobMutation(
  baseOptions?: Apollo.MutationHookOptions<UpdateJobMutation, UpdateJobMutationVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<UpdateJobMutation, UpdateJobMutationVariables>(
    UpdateJobDocument,
    options,
  );
}
export type UpdateJobMutationHookResult = ReturnType<typeof useUpdateJobMutation>;
export type UpdateJobMutationResult = Apollo.MutationResult<UpdateJobMutation>;
export type UpdateJobMutationOptions = Apollo.BaseMutationOptions<
  UpdateJobMutation,
  UpdateJobMutationVariables
>;
export const RemoveJobDocument = gql`
  mutation RemoveJob($id: String!) {
    RemoveJob(id: $id)
  }
`;
export type RemoveJobMutationFn = Apollo.MutationFunction<
  RemoveJobMutation,
  RemoveJobMutationVariables
>;

/**
 * __useRemoveJobMutation__
 *
 * To run a mutation, you first call `useRemoveJobMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRemoveJobMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [removeJobMutation, { data, loading, error }] = useRemoveJobMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useRemoveJobMutation(
  baseOptions?: Apollo.MutationHookOptions<RemoveJobMutation, RemoveJobMutationVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<RemoveJobMutation, RemoveJobMutationVariables>(
    RemoveJobDocument,
    options,
  );
}
export type RemoveJobMutationHookResult = ReturnType<typeof useRemoveJobMutation>;
export type RemoveJobMutationResult = Apollo.MutationResult<RemoveJobMutation>;
export type RemoveJobMutationOptions = Apollo.BaseMutationOptions<
  RemoveJobMutation,
  RemoveJobMutationVariables
>;
export const GetSearchResultDocument = gql`
  query GetSearchResult(
    $limit: Int
    $skip: Int
    $sortBy: String
    $name: String
    $location: String
    $status: String
    $jobType: String
    $remote: Boolean
  ) {
    Jobs(
      limit: $limit
      skip: $skip
      sortBy: $sortBy
      name: $name
      location: $location
      status: $status
      jobType: $jobType
      remote: $remote
    ) {
      jobResult {
        _id
        name
        shortName
        sourceType
        contents
        jobType
        locations
        categories
        experienceLevels
        companyId
        companyName
        applyUrl
        salary
        remote
        benefits
        status
        createAt
      }
      totalCount
    }
  }
`;

/**
 * __useGetSearchResultQuery__
 *
 * To run a query within a React component, call `useGetSearchResultQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSearchResultQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetSearchResultQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      skip: // value for 'skip'
 *      sortBy: // value for 'sortBy'
 *      name: // value for 'name'
 *      location: // value for 'location'
 *      status: // value for 'status'
 *      jobType: // value for 'jobType'
 *      remote: // value for 'remote'
 *   },
 * });
 */
export function useGetSearchResultQuery(
  baseOptions?: Apollo.QueryHookOptions<
    GetSearchResultQuery,
    GetSearchResultQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetSearchResultQuery, GetSearchResultQueryVariables>(
    GetSearchResultDocument,
    options,
  );
}
export function useGetSearchResultLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetSearchResultQuery,
    GetSearchResultQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetSearchResultQuery, GetSearchResultQueryVariables>(
    GetSearchResultDocument,
    options,
  );
}
export type GetSearchResultQueryHookResult = ReturnType<typeof useGetSearchResultQuery>;
export type GetSearchResultLazyQueryHookResult = ReturnType<
  typeof useGetSearchResultLazyQuery
>;
export type GetSearchResultQueryResult = Apollo.QueryResult<
  GetSearchResultQuery,
  GetSearchResultQueryVariables
>;
export const GetUserFromSessionDocument = gql`
  query GetUserFromSession {
    GetUserBySession {
      _id
      email
      firstname
      lastname
      middlename
      dob
      phonenumber
      address
      role
      description
      education {
        school
        graduationYear
        educationLevel
      }
      experience {
        name
        year
        description
      }
      skill {
        name
      }
    }
  }
`;

/**
 * __useGetUserFromSessionQuery__
 *
 * To run a query within a React component, call `useGetUserFromSessionQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserFromSessionQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserFromSessionQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetUserFromSessionQuery(
  baseOptions?: Apollo.QueryHookOptions<
    GetUserFromSessionQuery,
    GetUserFromSessionQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetUserFromSessionQuery, GetUserFromSessionQueryVariables>(
    GetUserFromSessionDocument,
    options,
  );
}
export function useGetUserFromSessionLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetUserFromSessionQuery,
    GetUserFromSessionQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetUserFromSessionQuery, GetUserFromSessionQueryVariables>(
    GetUserFromSessionDocument,
    options,
  );
}
export type GetUserFromSessionQueryHookResult = ReturnType<
  typeof useGetUserFromSessionQuery
>;
export type GetUserFromSessionLazyQueryHookResult = ReturnType<
  typeof useGetUserFromSessionLazyQuery
>;
export type GetUserFromSessionQueryResult = Apollo.QueryResult<
  GetUserFromSessionQuery,
  GetUserFromSessionQueryVariables
>;
export const UpdateUserDocument = gql`
  mutation UpdateUser($UserUpdateInputDto: UserUpdateInputDto!) {
    UpdateUser(UserUpdateInputDto: $UserUpdateInputDto) {
      _id
      email
      firstname
      lastname
      middlename
      dob
      phonenumber
      address
      role
      description
      education {
        school
        graduationYear
        educationLevel
      }
      experience {
        name
        year
        description
      }
      skill {
        name
      }
    }
  }
`;
export type UpdateUserMutationFn = Apollo.MutationFunction<
  UpdateUserMutation,
  UpdateUserMutationVariables
>;

/**
 * __useUpdateUserMutation__
 *
 * To run a mutation, you first call `useUpdateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserMutation, { data, loading, error }] = useUpdateUserMutation({
 *   variables: {
 *      UserUpdateInputDto: // value for 'UserUpdateInputDto'
 *   },
 * });
 */
export function useUpdateUserMutation(
  baseOptions?: Apollo.MutationHookOptions<
    UpdateUserMutation,
    UpdateUserMutationVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<UpdateUserMutation, UpdateUserMutationVariables>(
    UpdateUserDocument,
    options,
  );
}
export type UpdateUserMutationHookResult = ReturnType<typeof useUpdateUserMutation>;
export type UpdateUserMutationResult = Apollo.MutationResult<UpdateUserMutation>;
export type UpdateUserMutationOptions = Apollo.BaseMutationOptions<
  UpdateUserMutation,
  UpdateUserMutationVariables
>;
