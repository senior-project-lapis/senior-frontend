import * as Types from 'common/generated/generated-types';

import * as Operations from 'common/generated/generated-types';
import { NextPage } from 'next';
import { NextRouter, useRouter } from 'next/router';
import { QueryHookOptions, useQuery } from '@apollo/client';
import * as Apollo from '@apollo/client';
import type React from 'react';
import { getApolloClient } from 'common/services/client';

export async function getServerPageGetWatchingListByUserId(
  options: Omit<
    Apollo.QueryOptions<Types.GetWatchingListByUserIdQueryVariables>,
    'query'
  >,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetWatchingListByUserIdQuery>({
    ...options,
    query: Operations.GetWatchingListByUserIdDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetWatchingListByUserId = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<
    Types.GetWatchingListByUserIdQuery,
    Types.GetWatchingListByUserIdQueryVariables
  >,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetWatchingListByUserIdDocument, options);
};
export type PageGetWatchingListByUserIdComp = React.FC<{
  data?: Types.GetWatchingListByUserIdQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetWatchingListByUserId =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<
      Types.GetWatchingListByUserIdQuery,
      Types.GetWatchingListByUserIdQueryVariables
    >,
  ) =>
  (WrappedComponent: PageGetWatchingListByUserIdComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetWatchingListByUserIdDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetWatchingListByUserId = {
  getServerPage: getServerPageGetWatchingListByUserId,
  withPage: withPageGetWatchingListByUserId,
  usePage: useGetWatchingListByUserId,
};
export async function getServerPageGetPendingPublisher(
  options: Omit<Apollo.QueryOptions<Types.GetPendingPublisherQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetPendingPublisherQuery>({
    ...options,
    query: Operations.GetPendingPublisherDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetPendingPublisher = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<
    Types.GetPendingPublisherQuery,
    Types.GetPendingPublisherQueryVariables
  >,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetPendingPublisherDocument, options);
};
export type PageGetPendingPublisherComp = React.FC<{
  data?: Types.GetPendingPublisherQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetPendingPublisher =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<
      Types.GetPendingPublisherQuery,
      Types.GetPendingPublisherQueryVariables
    >,
  ) =>
  (WrappedComponent: PageGetPendingPublisherComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetPendingPublisherDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetPendingPublisher = {
  getServerPage: getServerPageGetPendingPublisher,
  withPage: withPageGetPendingPublisher,
  usePage: useGetPendingPublisher,
};

export async function getServerPageDashboardStat(
  options: Omit<Apollo.QueryOptions<Types.DashboardStatQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.DashboardStatQuery>({
    ...options,
    query: Operations.DashboardStatDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useDashboardStat = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<Types.DashboardStatQuery, Types.DashboardStatQueryVariables>,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.DashboardStatDocument, options);
};
export type PageDashboardStatComp = React.FC<{
  data?: Types.DashboardStatQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageDashboardStat =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<Types.DashboardStatQuery, Types.DashboardStatQueryVariables>,
  ) =>
  (WrappedComponent: PageDashboardStatComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.DashboardStatDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrDashboardStat = {
  getServerPage: getServerPageDashboardStat,
  withPage: withPageDashboardStat,
  usePage: useDashboardStat,
};
export async function getServerPageLastestJobStat(
  options: Omit<Apollo.QueryOptions<Types.LastestJobStatQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.LastestJobStatQuery>({
    ...options,
    query: Operations.LastestJobStatDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useLastestJobStat = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<Types.LastestJobStatQuery, Types.LastestJobStatQueryVariables>,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.LastestJobStatDocument, options);
};
export type PageLastestJobStatComp = React.FC<{
  data?: Types.LastestJobStatQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageLastestJobStat =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<Types.LastestJobStatQuery, Types.LastestJobStatQueryVariables>,
  ) =>
  (WrappedComponent: PageLastestJobStatComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.LastestJobStatDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrLastestJobStat = {
  getServerPage: getServerPageLastestJobStat,
  withPage: withPageLastestJobStat,
  usePage: useLastestJobStat,
};

export async function getServerPageGetServerStatus(
  options: Omit<Apollo.QueryOptions<Types.GetServerStatusQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetServerStatusQuery>({
    ...options,
    query: Operations.GetServerStatusDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetServerStatus = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<Types.GetServerStatusQuery, Types.GetServerStatusQueryVariables>,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetServerStatusDocument, options);
};
export type PageGetServerStatusComp = React.FC<{
  data?: Types.GetServerStatusQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetServerStatus =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<
      Types.GetServerStatusQuery,
      Types.GetServerStatusQueryVariables
    >,
  ) =>
  (WrappedComponent: PageGetServerStatusComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetServerStatusDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetServerStatus = {
  getServerPage: getServerPageGetServerStatus,
  withPage: withPageGetServerStatus,
  usePage: useGetServerStatus,
};

export async function getServerPageGetCompanyForPublisherRegister(
  options: Omit<
    Apollo.QueryOptions<Types.GetCompanyForPublisherRegisterQueryVariables>,
    'query'
  >,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetCompanyForPublisherRegisterQuery>({
    ...options,
    query: Operations.GetCompanyForPublisherRegisterDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetCompanyForPublisherRegister = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<
    Types.GetCompanyForPublisherRegisterQuery,
    Types.GetCompanyForPublisherRegisterQueryVariables
  >,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetCompanyForPublisherRegisterDocument, options);
};
export type PageGetCompanyForPublisherRegisterComp = React.FC<{
  data?: Types.GetCompanyForPublisherRegisterQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetCompanyForPublisherRegister =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<
      Types.GetCompanyForPublisherRegisterQuery,
      Types.GetCompanyForPublisherRegisterQueryVariables
    >,
  ) =>
  (WrappedComponent: PageGetCompanyForPublisherRegisterComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(
      Operations.GetCompanyForPublisherRegisterDocument,
      options,
    );
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetCompanyForPublisherRegister = {
  getServerPage: getServerPageGetCompanyForPublisherRegister,
  withPage: withPageGetCompanyForPublisherRegister,
  usePage: useGetCompanyForPublisherRegister,
};

export async function getServerPageGetCompaniesAutocomplete(
  options: Omit<
    Apollo.QueryOptions<Types.GetCompaniesAutocompleteQueryVariables>,
    'query'
  >,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetCompaniesAutocompleteQuery>({
    ...options,
    query: Operations.GetCompaniesAutocompleteDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetCompaniesAutocomplete = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<
    Types.GetCompaniesAutocompleteQuery,
    Types.GetCompaniesAutocompleteQueryVariables
  >,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetCompaniesAutocompleteDocument, options);
};
export type PageGetCompaniesAutocompleteComp = React.FC<{
  data?: Types.GetCompaniesAutocompleteQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetCompaniesAutocomplete =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<
      Types.GetCompaniesAutocompleteQuery,
      Types.GetCompaniesAutocompleteQueryVariables
    >,
  ) =>
  (WrappedComponent: PageGetCompaniesAutocompleteComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(
      Operations.GetCompaniesAutocompleteDocument,
      options,
    );
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetCompaniesAutocomplete = {
  getServerPage: getServerPageGetCompaniesAutocomplete,
  withPage: withPageGetCompaniesAutocomplete,
  usePage: useGetCompaniesAutocomplete,
};
export async function getServerPageGetCompanyDetail(
  options: Omit<Apollo.QueryOptions<Types.GetCompanyDetailQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetCompanyDetailQuery>({
    ...options,
    query: Operations.GetCompanyDetailDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetCompanyDetail = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<
    Types.GetCompanyDetailQuery,
    Types.GetCompanyDetailQueryVariables
  >,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetCompanyDetailDocument, options);
};
export type PageGetCompanyDetailComp = React.FC<{
  data?: Types.GetCompanyDetailQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetCompanyDetail =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<
      Types.GetCompanyDetailQuery,
      Types.GetCompanyDetailQueryVariables
    >,
  ) =>
  (WrappedComponent: PageGetCompanyDetailComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetCompanyDetailDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetCompanyDetail = {
  getServerPage: getServerPageGetCompanyDetail,
  withPage: withPageGetCompanyDetail,
  usePage: useGetCompanyDetail,
};
export async function getServerPageGetCompanies(
  options: Omit<Apollo.QueryOptions<Types.GetCompaniesQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetCompaniesQuery>({
    ...options,
    query: Operations.GetCompaniesDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetCompanies = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<Types.GetCompaniesQuery, Types.GetCompaniesQueryVariables>,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetCompaniesDocument, options);
};
export type PageGetCompaniesComp = React.FC<{
  data?: Types.GetCompaniesQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetCompanies =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<Types.GetCompaniesQuery, Types.GetCompaniesQueryVariables>,
  ) =>
  (WrappedComponent: PageGetCompaniesComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetCompaniesDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetCompanies = {
  getServerPage: getServerPageGetCompanies,
  withPage: withPageGetCompanies,
  usePage: useGetCompanies,
};
export async function getServerPageGetJobByCompanyId(
  options: Omit<Apollo.QueryOptions<Types.GetJobByCompanyIdQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetJobByCompanyIdQuery>({
    ...options,
    query: Operations.GetJobByCompanyIdDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetJobByCompanyId = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<
    Types.GetJobByCompanyIdQuery,
    Types.GetJobByCompanyIdQueryVariables
  >,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetJobByCompanyIdDocument, options);
};
export type PageGetJobByCompanyIdComp = React.FC<{
  data?: Types.GetJobByCompanyIdQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetJobByCompanyId =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<
      Types.GetJobByCompanyIdQuery,
      Types.GetJobByCompanyIdQueryVariables
    >,
  ) =>
  (WrappedComponent: PageGetJobByCompanyIdComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetJobByCompanyIdDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetJobByCompanyId = {
  getServerPage: getServerPageGetJobByCompanyId,
  withPage: withPageGetJobByCompanyId,
  usePage: useGetJobByCompanyId,
};
export async function getServerPageGetReviewsByCompanyId(
  options: Omit<Apollo.QueryOptions<Types.GetReviewsByCompanyIdQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetReviewsByCompanyIdQuery>({
    ...options,
    query: Operations.GetReviewsByCompanyIdDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetReviewsByCompanyId = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<
    Types.GetReviewsByCompanyIdQuery,
    Types.GetReviewsByCompanyIdQueryVariables
  >,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetReviewsByCompanyIdDocument, options);
};
export type PageGetReviewsByCompanyIdComp = React.FC<{
  data?: Types.GetReviewsByCompanyIdQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetReviewsByCompanyId =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<
      Types.GetReviewsByCompanyIdQuery,
      Types.GetReviewsByCompanyIdQueryVariables
    >,
  ) =>
  (WrappedComponent: PageGetReviewsByCompanyIdComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetReviewsByCompanyIdDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetReviewsByCompanyId = {
  getServerPage: getServerPageGetReviewsByCompanyId,
  withPage: withPageGetReviewsByCompanyId,
  usePage: useGetReviewsByCompanyId,
};

export async function getServerPageGetAutoComplete(
  options: Omit<Apollo.QueryOptions<Types.GetAutoCompleteQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetAutoCompleteQuery>({
    ...options,
    query: Operations.GetAutoCompleteDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetAutoComplete = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<Types.GetAutoCompleteQuery, Types.GetAutoCompleteQueryVariables>,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetAutoCompleteDocument, options);
};
export type PageGetAutoCompleteComp = React.FC<{
  data?: Types.GetAutoCompleteQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetAutoComplete =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<
      Types.GetAutoCompleteQuery,
      Types.GetAutoCompleteQueryVariables
    >,
  ) =>
  (WrappedComponent: PageGetAutoCompleteComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetAutoCompleteDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetAutoComplete = {
  getServerPage: getServerPageGetAutoComplete,
  withPage: withPageGetAutoComplete,
  usePage: useGetAutoComplete,
};
export async function getServerPageGetLocations(
  options: Omit<Apollo.QueryOptions<Types.GetLocationsQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetLocationsQuery>({
    ...options,
    query: Operations.GetLocationsDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetLocations = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<Types.GetLocationsQuery, Types.GetLocationsQueryVariables>,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetLocationsDocument, options);
};
export type PageGetLocationsComp = React.FC<{
  data?: Types.GetLocationsQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetLocations =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<Types.GetLocationsQuery, Types.GetLocationsQueryVariables>,
  ) =>
  (WrappedComponent: PageGetLocationsComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetLocationsDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetLocations = {
  getServerPage: getServerPageGetLocations,
  withPage: withPageGetLocations,
  usePage: useGetLocations,
};
export async function getServerPageGetLatestCompanies(
  options: Omit<Apollo.QueryOptions<Types.GetLatestCompaniesQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetLatestCompaniesQuery>({
    ...options,
    query: Operations.GetLatestCompaniesDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetLatestCompanies = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<
    Types.GetLatestCompaniesQuery,
    Types.GetLatestCompaniesQueryVariables
  >,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetLatestCompaniesDocument, options);
};
export type PageGetLatestCompaniesComp = React.FC<{
  data?: Types.GetLatestCompaniesQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetLatestCompanies =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<
      Types.GetLatestCompaniesQuery,
      Types.GetLatestCompaniesQueryVariables
    >,
  ) =>
  (WrappedComponent: PageGetLatestCompaniesComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetLatestCompaniesDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetLatestCompanies = {
  getServerPage: getServerPageGetLatestCompanies,
  withPage: withPageGetLatestCompanies,
  usePage: useGetLatestCompanies,
};
export async function getServerPageGetLatestJobs(
  options: Omit<Apollo.QueryOptions<Types.GetLatestJobsQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetLatestJobsQuery>({
    ...options,
    query: Operations.GetLatestJobsDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetLatestJobs = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<Types.GetLatestJobsQuery, Types.GetLatestJobsQueryVariables>,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetLatestJobsDocument, options);
};
export type PageGetLatestJobsComp = React.FC<{
  data?: Types.GetLatestJobsQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetLatestJobs =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<Types.GetLatestJobsQuery, Types.GetLatestJobsQueryVariables>,
  ) =>
  (WrappedComponent: PageGetLatestJobsComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetLatestJobsDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetLatestJobs = {
  getServerPage: getServerPageGetLatestJobs,
  withPage: withPageGetLatestJobs,
  usePage: useGetLatestJobs,
};
export async function getServerPageGetJob(
  options: Omit<Apollo.QueryOptions<Types.GetJobQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetJobQuery>({
    ...options,
    query: Operations.GetJobDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetJob = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<Types.GetJobQuery, Types.GetJobQueryVariables>,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetJobDocument, options);
};
export type PageGetJobComp = React.FC<{
  data?: Types.GetJobQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetJob =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<Types.GetJobQuery, Types.GetJobQueryVariables>,
  ) =>
  (WrappedComponent: PageGetJobComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetJobDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetJob = {
  getServerPage: getServerPageGetJob,
  withPage: withPageGetJob,
  usePage: useGetJob,
};
export async function getServerPageGetJobAutoComplete(
  options: Omit<Apollo.QueryOptions<Types.GetJobAutoCompleteQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetJobAutoCompleteQuery>({
    ...options,
    query: Operations.GetJobAutoCompleteDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetJobAutoComplete = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<
    Types.GetJobAutoCompleteQuery,
    Types.GetJobAutoCompleteQueryVariables
  >,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetJobAutoCompleteDocument, options);
};
export type PageGetJobAutoCompleteComp = React.FC<{
  data?: Types.GetJobAutoCompleteQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetJobAutoComplete =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<
      Types.GetJobAutoCompleteQuery,
      Types.GetJobAutoCompleteQueryVariables
    >,
  ) =>
  (WrappedComponent: PageGetJobAutoCompleteComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetJobAutoCompleteDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetJobAutoComplete = {
  getServerPage: getServerPageGetJobAutoComplete,
  withPage: withPageGetJobAutoComplete,
  usePage: useGetJobAutoComplete,
};
export async function getServerPageGetJobDetail(
  options: Omit<Apollo.QueryOptions<Types.GetJobDetailQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetJobDetailQuery>({
    ...options,
    query: Operations.GetJobDetailDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetJobDetail = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<Types.GetJobDetailQuery, Types.GetJobDetailQueryVariables>,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetJobDetailDocument, options);
};
export type PageGetJobDetailComp = React.FC<{
  data?: Types.GetJobDetailQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetJobDetail =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<Types.GetJobDetailQuery, Types.GetJobDetailQueryVariables>,
  ) =>
  (WrappedComponent: PageGetJobDetailComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetJobDetailDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetJobDetail = {
  getServerPage: getServerPageGetJobDetail,
  withPage: withPageGetJobDetail,
  usePage: useGetJobDetail,
};
export async function getServerPageGetFavoritesByUserId(
  options: Omit<Apollo.QueryOptions<Types.GetFavoritesByUserIdQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetFavoritesByUserIdQuery>({
    ...options,
    query: Operations.GetFavoritesByUserIdDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetFavoritesByUserId = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<
    Types.GetFavoritesByUserIdQuery,
    Types.GetFavoritesByUserIdQueryVariables
  >,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetFavoritesByUserIdDocument, options);
};
export type PageGetFavoritesByUserIdComp = React.FC<{
  data?: Types.GetFavoritesByUserIdQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetFavoritesByUserId =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<
      Types.GetFavoritesByUserIdQuery,
      Types.GetFavoritesByUserIdQueryVariables
    >,
  ) =>
  (WrappedComponent: PageGetFavoritesByUserIdComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetFavoritesByUserIdDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetFavoritesByUserId = {
  getServerPage: getServerPageGetFavoritesByUserId,
  withPage: withPageGetFavoritesByUserId,
  usePage: useGetFavoritesByUserId,
};

export async function getServerPageGetRelatedJobs(
  options: Omit<Apollo.QueryOptions<Types.GetRelatedJobsQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetRelatedJobsQuery>({
    ...options,
    query: Operations.GetRelatedJobsDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetRelatedJobs = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<Types.GetRelatedJobsQuery, Types.GetRelatedJobsQueryVariables>,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetRelatedJobsDocument, options);
};
export type PageGetRelatedJobsComp = React.FC<{
  data?: Types.GetRelatedJobsQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetRelatedJobs =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<Types.GetRelatedJobsQuery, Types.GetRelatedJobsQueryVariables>,
  ) =>
  (WrappedComponent: PageGetRelatedJobsComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetRelatedJobsDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetRelatedJobs = {
  getServerPage: getServerPageGetRelatedJobs,
  withPage: withPageGetRelatedJobs,
  usePage: useGetRelatedJobs,
};
export async function getServerPageGetJobTrends(
  options: Omit<Apollo.QueryOptions<Types.GetJobTrendsQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetJobTrendsQuery>({
    ...options,
    query: Operations.GetJobTrendsDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetJobTrends = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<Types.GetJobTrendsQuery, Types.GetJobTrendsQueryVariables>,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetJobTrendsDocument, options);
};
export type PageGetJobTrendsComp = React.FC<{
  data?: Types.GetJobTrendsQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetJobTrends =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<Types.GetJobTrendsQuery, Types.GetJobTrendsQueryVariables>,
  ) =>
  (WrappedComponent: PageGetJobTrendsComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetJobTrendsDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetJobTrends = {
  getServerPage: getServerPageGetJobTrends,
  withPage: withPageGetJobTrends,
  usePage: useGetJobTrends,
};

export async function getServerPageGetSearchResult(
  options: Omit<Apollo.QueryOptions<Types.GetSearchResultQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetSearchResultQuery>({
    ...options,
    query: Operations.GetSearchResultDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetSearchResult = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<Types.GetSearchResultQuery, Types.GetSearchResultQueryVariables>,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetSearchResultDocument, options);
};
export type PageGetSearchResultComp = React.FC<{
  data?: Types.GetSearchResultQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetSearchResult =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<
      Types.GetSearchResultQuery,
      Types.GetSearchResultQueryVariables
    >,
  ) =>
  (WrappedComponent: PageGetSearchResultComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetSearchResultDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetSearchResult = {
  getServerPage: getServerPageGetSearchResult,
  withPage: withPageGetSearchResult,
  usePage: useGetSearchResult,
};
export async function getServerPageGetUserFromSession(
  options: Omit<Apollo.QueryOptions<Types.GetUserFromSessionQueryVariables>, 'query'>,
  ctx?: any,
) {
  const apolloClient = getApolloClient(ctx);

  const data = await apolloClient.query<Types.GetUserFromSessionQuery>({
    ...options,
    query: Operations.GetUserFromSessionDocument,
  });

  const apolloState = apolloClient.cache.extract();

  return {
    props: {
      apolloState: apolloState,
      data: data?.data,
      error: data?.error ?? data?.errors ?? null,
    },
  };
}
export const useGetUserFromSession = (
  optionsFunc?: (
    router: NextRouter,
  ) => QueryHookOptions<
    Types.GetUserFromSessionQuery,
    Types.GetUserFromSessionQueryVariables
  >,
) => {
  const router = useRouter();
  const options = optionsFunc ? optionsFunc(router) : {};
  return useQuery(Operations.GetUserFromSessionDocument, options);
};
export type PageGetUserFromSessionComp = React.FC<{
  data?: Types.GetUserFromSessionQuery;
  error?: Apollo.ApolloError;
}>;
export const withPageGetUserFromSession =
  (
    optionsFunc?: (
      router: NextRouter,
    ) => QueryHookOptions<
      Types.GetUserFromSessionQuery,
      Types.GetUserFromSessionQueryVariables
    >,
  ) =>
  (WrappedComponent: PageGetUserFromSessionComp): NextPage =>
  (props) => {
    const router = useRouter();
    const options = optionsFunc ? optionsFunc(router) : {};
    const { data, error } = useQuery(Operations.GetUserFromSessionDocument, options);
    return <WrappedComponent {...props} data={data} error={error} />;
  };
export const ssrGetUserFromSession = {
  getServerPage: getServerPageGetUserFromSession,
  withPage: withPageGetUserFromSession,
  usePage: useGetUserFromSession,
};
