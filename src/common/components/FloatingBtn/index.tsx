import React from 'react';

import { Flex } from '@chakra-ui/react';
import { MdCompare } from 'react-icons/md';
interface Props {
  onOpen: () => void;
}
const FloatingBtn: React.FC<Props> = (Props) => {
  const { onOpen } = Props;
  return (
    <Flex
      onClick={onOpen}
      cursor="pointer"
      position="fixed"
      overflow="hidden"
      w="60px"
      h="60px"
      bottom="20px"
      bgColor="blue.200"
      borderRadius="50px"
      fontSize="25px"
      color="white"
      boxShadow="rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 1px 3px 1px"
      right={['16px', '84px']}
      textAlign="center"
      alignItems="center"
      justifyContent="center"
      zIndex={10}>
      <MdCompare />
    </Flex>
  );
};

export default FloatingBtn;
