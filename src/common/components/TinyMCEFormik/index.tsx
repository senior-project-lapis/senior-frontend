import React from 'react';

import { Editor, IAllProps } from '@tinymce/tinymce-react';
import { useField, useFormikContext } from 'formik';
import { FormControl, BaseProps } from 'formik-chakra-ui';

interface EditorFieldProps extends IAllProps {
  label?: string;
  name: string;
}
export type TinyMCEFormikProps = BaseProps & EditorFieldProps;

export const TinyMCEFormik: React.FC<TinyMCEFormikProps> = (
  props: TinyMCEFormikProps,
) => {
  const { setFieldValue } = useFormikContext();
  const { name, label, ...rest } = props;
  const [field] = useField(name);
  return (
    <FormControl name={name} label={label} {...rest}>
      <Editor
        {...rest}
        id={name}
        apiKey="b3ccgp45l9ltq2oswqzilwz9n7tycjfrd4xrvknrq763fgrt"
        value={field.value}
        init={{
          height: 300,
          menubar: true,
          plugins: [
            'advlist',
            'autolink',
            'lists',
            'link',
            'image',
            'charmap',
            'anchor',
            'searchreplace',
            'visualblocks',
            'code',
            'fullscreen',
            'insertdatetime',
            'media',
            'table',
            'preview',
            'help',
            'wordcount',
          ],
          toolbar:
            'undo redo | blocks | ' +
            'bold italic forecolor | alignleft aligncenter ' +
            'alignright alignjustify | bullist numlist outdent indent | ' +
            'removeformat | help',
          content_style:
            'body { font-family:Montserrat Normal,Arial,sans-serif; font-size:14px }',
        }}
        onEditorChange={(stringifiedHtmlValue) => {
          setFieldValue(field.name, stringifiedHtmlValue);
        }}
      />
    </FormControl>
  );
};

export default TinyMCEFormik;
