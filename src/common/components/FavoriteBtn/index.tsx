/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useEffect } from 'react';

import { Button, useToast } from '@chakra-ui/react';
import { FaHeart } from 'react-icons/fa';
import { useSelector } from 'react-redux';

import {
  useGetFavoritesByUserIdQuery,
  useAddFavoriteMutation,
  useRemoveFavoriteMutation,
} from 'common/generated/generated-types';
import { StoresState } from 'common/stores';
interface Props {
  jobId: string;
  size?: string;
}

const FavoriteBtn: React.FC<Props> = (Props) => {
  const auth = useSelector((state: StoresState) => state.user.authenticated);
  const [isLogin, setIsLogin] = useState<boolean>(true);
  const [userId, setUserId] = useState<string>('');
  const { jobId, size = 'md' } = Props;
  const toast = useToast();
  const [favorite, setFavorite] = useState(false);
  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('user') || null);
    auth || user ? setIsLogin(true) : setIsLogin(false);
    setUserId(user ? user._id : '');
  }, []);

  const { data: existingFav, refetch } = useGetFavoritesByUserIdQuery({
    variables: {
      userId: userId,
    },
  });

  const [
    createFavoriteMutation,
    { data: addFavData, loading: addFavLoading, error: addFavError },
  ] = useAddFavoriteMutation({
    errorPolicy: 'all',
  });

  const [
    removeFavoriteMutation,
    { data: removeFavData, loading: removeFavLoading, error: removeFavError },
  ] = useRemoveFavoriteMutation({
    errorPolicy: 'all',
  });

  const checkFav = () => {
    if (existingFav) {
      return existingFav.Favorites.find((fav) => fav._id === jobId)
        ? setFavorite(true)
        : setFavorite(false);
    } else {
      return setFavorite(false);
    }
  };

  useEffect(() => {
    refetch();
    checkFav();
  }, [existingFav, jobId]);

  useEffect(() => {
    if (!addFavError && addFavData && favorite === true) {
      toast({
        title: 'Success',
        description: "We've add this job to your favorite lists.",
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
      // console.log(addFavData);
    } else if (addFavError) {
      toast({
        title: 'Error',
        description: addFavError.message,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }
    if (!removeFavError && removeFavData && favorite === false) {
      toast({
        title: 'Success',
        description: "We've remove this job from your favorite lists.",
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
      // console.log(removeFavData);
    } else if (removeFavError) {
      toast({
        title: 'Error',
        description: removeFavError.message,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }
  }, [
    addFavData,
    addFavLoading,
    addFavError,
    removeFavData,
    removeFavLoading,
    removeFavError,
  ]);

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const ToggleFavorite = (e: any) => {
    e.preventDefault();
    setFavorite((favorite) => {
      if (favorite === true) {
        sleep(500).then(() => {
          removeFavoriteMutation({
            variables: {
              FavoriteJobInputDto: {
                jobId: jobId,
                userId: userId,
              },
            },
          });
        });
      }
      if (favorite === false) {
        sleep(500).then(() => {
          createFavoriteMutation({
            variables: {
              FavoriteJobInputDto: {
                jobId: jobId,
                userId: userId,
              },
            },
          });
        });
      }
      return !favorite;
    });
  };

  return (
    <>
      {isLogin && (
        <Button
          size={size}
          color={favorite ? 'red' : 'gray.500'}
          fontSize="22px"
          _hover={{ bg: 'gray.200' }}
          onClick={(e) => {
            ToggleFavorite(e);
          }}>
          <FaHeart />
        </Button>
      )}
    </>
  );
};

export default FavoriteBtn;
