import React, { useEffect, useState } from 'react';

import { Menu, MenuButton, MenuList, MenuItem, Link, useToast } from '@chakra-ui/react';
import { useSelector } from 'react-redux';

import { useUpdateJobMutation } from 'common/generated/generated-types';
import { StoresState } from 'common/stores';

interface Props {
  jobId: string;
}

const ReportBtn: React.FC<Props> = (Props) => {
  const { jobId } = Props;
  const toast = useToast();
  const auth = useSelector((state: StoresState) => state.user.authenticated);
  const [isLogin, setIsLogin] = useState<boolean>(true);
  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('user') || null);
    auth || user ? setIsLogin(true) : setIsLogin(false);
  }, []);

  const [EditJobMutation, { data, loading, error }] = useUpdateJobMutation({
    errorPolicy: 'all',
  });

  useEffect(() => {
    if (!error && data) {
      toast({
        title: 'Report successfully.',
        description: "We've Reported this job for you.",
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
    } else {
      if (error) {
        toast({
          title: 'Error',
          description: error.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }
    }
  }, [data, loading, error]);

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const handleReport = () => {
    sleep(700).then(() => {
      EditJobMutation({
        variables: {
          UpdateJobInputDto: {
            id: jobId,
            status: 'reported',
          },
        },
      });
    });
  };

  return (
    <>
      {isLogin && (
        <Menu>
          <MenuButton as={Link} color="black">
            ...
          </MenuButton>
          <MenuList>
            <MenuItem onClick={() => handleReport()}>Report this job</MenuItem>
          </MenuList>
        </Menu>
      )}
    </>
  );
};

export default ReportBtn;
