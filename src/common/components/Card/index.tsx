/* eslint-disable @typescript-eslint/no-explicit-any */
import { Image, Flex, Divider, Text, Button } from '@chakra-ui/react';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import { BiBuildings } from 'react-icons/bi';
import { GiFactory } from 'react-icons/gi';
import { GrMapLocation } from 'react-icons/gr';
interface Props {
  isShowImage?: boolean | undefined;
  isCompanyDatail?: boolean | undefined;
  data?: any;
}
const CapitalizeFirstLetter = (string: string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

const Card: React.FC<Props> = (Props) => {
  const { isShowImage, data, isCompanyDatail } = Props;
  const router = useRouter();

  const handleShowDetail = (e: any) => {
    e.preventDefault();
    router.push(`/company/${data._id}`, undefined, { shallow: true });
  };

  return (
    <>
      {isShowImage ? (
        <Flex
          onClick={(e) => {
            handleShowDetail(e);
          }}
          cursor="pointer"
          bg="white"
          w="330px"
          h="330px"
          p="20px"
          flexDirection="column"
          border="none"
          _hover={{ boxShadow: '10px 10px  #1D70A2' }}
          boxShadow="rgba(149, 157, 165, 0.2) 0px 8px 24px"
          borderRadius="5px"
          overflow="hidden">
          <Image
            objectFit="contain"
            src={data?.logoImage || '/images/404.png'}
            alt={data?.shortName || 'not found'}
            h="230px"
            p="30px"
          />
          <Flex>
            <Divider
              border="1px solid rgba(0, 0, 0, 0.6)"
              width="100%"
              borderColor="gray.700"
            />
          </Flex>
          <Text fontSize="20px" fontFamily="medium" textAlign="center" mt="3">
            {data?.name}
          </Text>
        </Flex>
      ) : (
        <>
          {isCompanyDatail ? (
            <Flex
              bg="white"
              w="300px"
              minH="280px"
              p="20px"
              flexDirection="column"
              justifyContent="center"
              boxShadow="rgba(149, 157, 165, 0.2) 0px 8px 24px"
              borderRadius="5px"
              overflow="hidden">
              <Flex mb="5px" width="100%" flexDirection="column">
                <Text fontSize="18px" fontFamily="medium" noOfLines={1}>
                  {data?.name}
                </Text>
                <Divider
                  mt="10px"
                  border="1px solid rgba(0, 0, 0, 0.6)"
                  width="80%"
                  borderColor="gray.700"
                />
              </Flex>
              <Flex
                mt="5px"
                justifyContent="flex-start"
                flexDirection="column"
                alignItems="stretch">
                <Text fontSize="14px" noOfLines={3}>
                  {data?.description}
                </Text>
              </Flex>
              {data.industries.length > 0 ? (
                <Flex
                  mt="10px"
                  justifyContent="flex-start"
                  flexDirection="row"
                  alignItems="center">
                  <Text fontSize="16px" fontFamily="medium">
                    <GiFactory />
                  </Text>
                  <Text fontSize="16px" display="flex">
                    &nbsp;&nbsp;
                    {data?.industries.map((item: any) => CapitalizeFirstLetter(item))}
                  </Text>
                </Flex>
              ) : null}

              <Flex
                mt="10px"
                justifyContent="flex-start"
                flexDirection="row"
                alignItems="center">
                <Text fontSize="16px" fontFamily="medium">
                  <GrMapLocation />
                </Text>
                <Text fontSize="16px" display="flex">
                  &nbsp;&nbsp;
                  {data?.locations[0]}
                </Text>
              </Flex>
              <Flex mt="5px">
                <NextLink href={`/company/${data?._id}`} passHref>
                  <Button variant="orange" size="sm">
                    See details
                  </Button>
                </NextLink>
              </Flex>
            </Flex>
          ) : (
            <Flex
              bg="gray.50"
              w="300px"
              minH="280px"
              p="20px"
              flexDirection="column"
              justifyContent="center"
              boxShadow="rgba(149, 157, 165, 0.2) 0px 8px 24px"
              borderRadius="5px"
              overflow="hidden">
              <Flex mb="10px" width="100%" flexDirection="column">
                <Text fontSize="18px" fontFamily="medium" noOfLines={2}>
                  {data?.name}
                </Text>
                <Divider
                  mt="10px"
                  border="1px solid rgba(0, 0, 0, 0.6)"
                  width="80%"
                  borderColor="gray.700"
                />
              </Flex>
              <Flex
                mt="10px"
                justifyContent="flex-start"
                flexDirection="row"
                alignItems="center">
                <Text fontSize="16px" fontFamily="medium">
                  <BiBuildings />
                </Text>
                <Text fontSize="16px" display="flex">
                  &nbsp;&nbsp;
                  {CapitalizeFirstLetter(data?.companyName)}
                </Text>
              </Flex>
              <Flex
                mt="10px"
                justifyContent="flex-start"
                flexDirection="row"
                alignItems="center">
                <Text fontSize="16px" fontFamily="medium">
                  <GrMapLocation />
                </Text>
                <Text fontSize="16px" display="flex">
                  &nbsp;&nbsp;
                  {data?.locations[0]}
                </Text>
              </Flex>
              <Flex mt="10px">
                <NextLink href={`/job/${data?._id}`} passHref>
                  <Button variant="orange" size="sm">
                    See details
                  </Button>
                </NextLink>
              </Flex>
            </Flex>
          )}
        </>
      )}
    </>
  );
};
export default Card;
