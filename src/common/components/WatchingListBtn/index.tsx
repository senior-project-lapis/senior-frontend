/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useState } from 'react';

import { Button, useToast } from '@chakra-ui/react';
import { useSelector } from 'react-redux';

import {
  useGetWatchingListByUserIdQuery,
  useAddWatchingListMutation,
  useRemoveWatchingListMutation,
} from 'common/generated/generated-types';
import { StoresState } from 'common/stores';

interface Props {
  watchingType?: string;
  data: any;
  isCard?: boolean;
}

const WatchingListBtn: React.FC<Props> = (Props) => {
  const { watchingType, data, isCard = false } = Props;
  const toast = useToast();
  const auth = useSelector((state: StoresState) => state.user.authenticated);
  const [isLogin, setIsLogin] = useState<boolean>(true);
  const [watchingList, setWatchingList] = useState<boolean>(false);
  const [userId, setUserId] = useState<string>('');
  let companyId: string;
  if (watchingType === 'job') {
    companyId = data.companyId;
  } else if (watchingType === 'company') {
    if (isCard === true) {
      companyId = data.companyId;
    } else {
      companyId = data._id;
    }
  }
  const location = data.locations ? data.locations[0] : data.location || '';

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('user') || null);
    auth || user ? setIsLogin(true) : setIsLogin(false);
    setUserId(user ? user._id : '');
  }, []);

  const { data: existWatchingList, refetch } = useGetWatchingListByUserIdQuery({
    variables: {
      userId: userId,
    },
  });

  const [
    createWatchingListMutation,
    {
      data: addWatchingListData,
      loading: addWatchingListLoading,
      error: addWatchingListError,
    },
  ] = useAddWatchingListMutation({
    errorPolicy: 'all',
  });

  const [
    removeWatchingListMutation,
    {
      data: removeWatchingListData,
      loading: removeWatchingListLoading,
      error: removeWatchingListError,
    },
  ] = useRemoveWatchingListMutation({
    errorPolicy: 'all',
  });

  const checkWatchingList = () => {
    if (existWatchingList !== undefined) {
      return existWatchingList?.WatchingLists[0]?.list.find(
        (watchingList) => watchingList.name === data.name,
      )
        ? setWatchingList(true)
        : setWatchingList(false);
    } else {
      return setWatchingList(false);
    }
  };

  useEffect(() => {
    refetch();
    checkWatchingList();
  }, [existWatchingList, data]);

  const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
  const ToggleWatchingList = (e: any) => {
    e.preventDefault();
    setWatchingList((watchingList) => {
      if (watchingList === true) {
        sleep(500).then(() => {
          removeWatchingListMutation({
            variables: {
              WatchingListInputDto: {
                listType: watchingType,
                location: location,
                name: data.name,
                companyId: companyId,
                userId: userId,
              },
            },
          });
        });
      }
      if (watchingList === false) {
        sleep(500).then(() => {
          createWatchingListMutation({
            variables: {
              WatchingListInputDto: {
                listType: watchingType,
                location: location,
                name: data.name,
                companyId: companyId,
                userId: userId,
              },
            },
          });
        });
      }
      return !watchingList;
    });
  };

  useEffect(() => {
    if (!addWatchingListError && addWatchingListData && watchingList === true) {
      toast({
        title: 'Success',
        description: `We've add this ${watchingType} to your watching lists.`,
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
      // console.log(addWatchingListData);
    } else if (addWatchingListError) {
      toast({
        title: 'Error',
        description: addWatchingListError.message,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }
    if (!removeWatchingListError && removeWatchingListData && watchingList === false) {
      toast({
        title: 'Success',
        description: `We've remove this ${watchingType} from your watching lists.`,
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
      // console.log(removeWatchingListData);
    } else if (removeWatchingListError) {
      toast({
        title: 'Error',
        description: removeWatchingListError.message,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }
  }, [
    addWatchingListData,
    addWatchingListLoading,
    addWatchingListError,
    removeWatchingListData,
    removeWatchingListLoading,
    removeWatchingListError,
  ]);

  return (
    <>
      {isLogin && (
        <Button
          variant={watchingList ? 'red' : 'blue2'}
          onClick={(e) => {
            ToggleWatchingList(e);
          }}>
          {watchingList ? 'Remove from watching list' : 'Add to watching list'}
        </Button>
      )}
    </>
  );
};

export default WatchingListBtn;
