/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import { InputProps, Flex } from '@chakra-ui/react';
import {
  AutoComplete,
  AutoCompleteInput,
  AutoCompleteItem,
  AutoCompleteList,
  UseAutoCompleteProps,
  AutoCompleteCreatable,
} from '@choc-ui/chakra-autocomplete';
import { useFormikContext, useField } from 'formik';
import { FormControl } from 'formik-chakra-ui';
interface Option {
  name: string;
  value?: string;
}

export interface AutocompleteOptions {
  isloading?: string;
  name: string;
  label?: string;
  options: Option[];
}
export type AutoCompleteProps = UseAutoCompleteProps & InputProps & AutocompleteOptions;

export const AutoCompleteFormik: React.FC<AutoCompleteProps> = (
  props: AutoCompleteProps,
) => {
  const { name, label, options, isloading } = props;
  const [field] = useField(name);

  const { setFieldValue } = useFormikContext();

  return (
    <>
      <Flex>
        <FormControl name={name} label={label}>
          <AutoComplete
            onChange={(val) => {
              setFieldValue(field.name, val);
            }}
            creatable
            openOnFocus
            rollNavigation
            listAllValuesOnFocus
            suggestWhenEmpty
            freeSolo>
            <AutoCompleteInput {...props} {...field} id={name} autoComplete="off" />
            <AutoCompleteList bg="white">
              {isloading === 'true' ? (
                <AutoCompleteItem
                  disabled
                  _focus={{ bg: 'gray.100' }}
                  _hover={{ bg: 'gray.100' }}
                  value="null"
                  label="Loading...."
                  textTransform="capitalize">
                  Loading....
                </AutoCompleteItem>
              ) : (
                options.map((option, oid) => (
                  <AutoCompleteItem
                    _focus={{ bg: 'gray.100' }}
                    _hover={{ bg: 'gray.100' }}
                    key={`option-${oid}`}
                    value={option.value ? option.value : option.name}
                    label={option.name}
                    textTransform="capitalize">
                    {option.name}
                  </AutoCompleteItem>
                ))
              )}
              <AutoCompleteCreatable>
                {({ value }) => <span> {value} </span>}
              </AutoCompleteCreatable>
            </AutoCompleteList>
          </AutoComplete>
        </FormControl>
      </Flex>
    </>
  );
};
export default AutoCompleteFormik;
