import React, { useEffect, useRef, useState } from 'react';

import { Flex } from '@chakra-ui/react';
import type { LottiePlayer } from 'lottie-web';

const Loader: React.FC = () => {
  const ref = useRef<HTMLDivElement>(null);
  const [lottie, setLottie] = useState<LottiePlayer | null>(null);

  useEffect(() => {
    import('lottie-web').then((Lottie) => setLottie(Lottie.default));
  }, []);

  useEffect(() => {
    if (lottie && ref.current) {
      const animation = lottie.loadAnimation({
        container: ref.current,
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: '/assets/animation2.json',
      });

      return () => animation.destroy();
    }
  }, [lottie]);

  return (
    <Flex
      zIndex="20"
      height="100%"
      width="100%"
      top="0"
      left="0"
      alignItems="center"
      justifyContent="center"
      position="fixed"
      backgroundColor="rgba(255, 255, 255, 0.8)"
      background="linear-gradient(330deg, rgba(255, 255, 255, 0.3), rgba(255, 255, 255, 0.75))"
      backdropFilter="blur(10px) saturate(130%)"
      boxShadow="0px 4px 10px 0px rgba(0, 0, 0, 0.08)">
      <Flex w="3rem" h="3rem">
        <div ref={ref} />
      </Flex>
    </Flex>
  );
};

export default Loader;
