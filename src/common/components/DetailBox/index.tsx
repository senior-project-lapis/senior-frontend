import { Flex, Heading, Button, Text, SimpleGrid, Divider } from '@chakra-ui/react';
import NextLink from 'next/link';
import { BsBuilding } from 'react-icons/bs';
import { FaMapMarkedAlt } from 'react-icons/fa';
import { HiOutlineExternalLink } from 'react-icons/hi';

import CompareJobBtn from 'common/components/CompareJobBtn';
import FavoriteBtn from 'common/components/FavoriteBtn';
import { JobWithId } from 'common/generated/generated-types';
interface Props {
  job?: JobWithId;
}
const CapitalizeFirstLetter = (string: string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};
const DetailBox: React.FC<Props> = (Props) => {
  const { job } = Props;
  return (
    <>
      {job ? (
        <Flex
          overflowY="scroll"
          overflowX="hidden"
          flexDirection="column"
          p="15px"
          m="5px"
          w="100%">
          <Flex justifyContent="space-between">
            <Heading p="10px" fontSize="20px">
              {job.name}
            </Heading>
            <Flex p="10px">
              <a href={`https://${job.applyUrl}`} target={'_blank'} rel={'noreferrer'}>
                <Button variant="orange" mr="1">
                  Apply
                </Button>
              </a>
              <CompareJobBtn data={job} />
              <FavoriteBtn jobId={job._id} />
            </Flex>
          </Flex>
          <Flex p="10px" justifyContent="flex-start">
            <Text fontSize="16px" display="flex" marginRight="5px">
              <BsBuilding size="20px" />
              &nbsp;
              {job.companyName}&nbsp;&nbsp;
            </Text>

            <Text fontSize="16px" display="flex">
              {/* {job.locations.length > 2 ? (
                <FaMapMarkedAlt size="28px" />
              ) : (
                <FaMapMarkedAlt size="20px" />
              )}
              &nbsp;
              {job.locations.map((location) => location + ' / ')} */}
              <FaMapMarkedAlt size="20px" /> &nbsp;{job.locations[0]}
            </Text>
          </Flex>
          {job.categories ? (
            job.categories.length > 1 ? (
              <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                <Text fontSize="16px" fontFamily="medium">
                  Categories:
                </Text>
                <SimpleGrid
                  columns={2}
                  spacing={2}
                  mt="10px"
                  justifyItems="center"
                  alignItems="center">
                  {job.categories.map((item, index) => (
                    <Text fontSize="16px" display="flex" key={index}>
                      {CapitalizeFirstLetter(item)}
                    </Text>
                  ))}
                </SimpleGrid>
              </Flex>
            ) : (
              <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                <Text fontSize="16px" fontFamily="medium">
                  Categories:
                </Text>
                <Text fontSize="16px" display="flex">
                  &nbsp;&nbsp;{job.categories.map((item) => CapitalizeFirstLetter(item))}
                </Text>
              </Flex>
            )
          ) : null}
          {job.experienceLevels ? (
            job.experienceLevels.length > 1 ? (
              <Flex p="5px" justifyContent="flex-start" flexDirection="column">
                <Text fontSize="16px" fontFamily="medium">
                  Experience Levels:
                </Text>
                <SimpleGrid
                  columns={2}
                  spacing={2}
                  mt="10px"
                  justifyItems="center"
                  alignItems="center">
                  {job.experienceLevels.map((item, index) => (
                    <Text fontSize="16px" display="flex" key={index}>
                      {CapitalizeFirstLetter(item)}
                    </Text>
                  ))}
                </SimpleGrid>
              </Flex>
            ) : (
              <Flex p="5px" justifyContent="flex-start" flexDirection="row">
                <Text fontSize="16px" fontFamily="medium">
                  Experience Levels:
                </Text>
                <Text fontSize="16px" display="flex">
                  &nbsp;&nbsp;
                  {job.experienceLevels.map((item) => CapitalizeFirstLetter(item))}
                </Text>
              </Flex>
            )
          ) : null}
          {job.remote && job.remote === true ? (
            <Flex p="5px" justifyContent="flex-start" flexDirection="row">
              <Text fontSize="16px" fontFamily="medium">
                Remote:
              </Text>
              <Text fontSize="16px" display="flex">
                &nbsp;&nbsp; Yes
              </Text>
            </Flex>
          ) : (
            <Flex p="5px" justifyContent="flex-start" flexDirection="row">
              <Text fontSize="16px" fontFamily="medium">
                Remote:
              </Text>
              <Text fontSize="16px" display="flex">
                &nbsp;&nbsp; No
              </Text>
            </Flex>
          )}
          <Flex p="5px">
            <NextLink href={`/job/${job._id}`} passHref>
              <Button variant="orange2">
                <HiOutlineExternalLink fontSize="22px" />
                &nbsp; View full detail
              </Button>
            </NextLink>
          </Flex>

          <Divider
            mt="10px"
            border="1px solid rgba(0, 0, 0, 0.6)"
            width="100%"
            borderColor="gray.300"
          />
          {job.contents ? (
            <Flex
              p="5px"
              mt="10px"
              flexDirection="column"
              dangerouslySetInnerHTML={{ __html: job.contents }}
            />
          ) : null}
        </Flex>
      ) : null}
    </>
  );
};

export default DetailBox;
