import React, { useEffect, useState } from 'react';

import { useToast } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';

import { StoresState } from 'common/stores';

const PublisherGuard: React.FC = (props) => {
  const toast = useToast();
  const { children } = props;
  const router = useRouter();
  const [valid, setValid] = useState<boolean>(true);
  const auth = useSelector((state: StoresState) => state.publisher.authenticated);

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('user') || 'null');
    const adminRole = user && user.role === 'admin' ? true : false;
    const publisher = localStorage.getItem('publisher');
    auth || publisher || adminRole ? setValid(true) : setValid(false);
  }, []);

  if (!valid) {
    toast({
      title: 'Error',
      description: "You don't have permission to view this content",
      status: 'error',
      duration: 5000,
      isClosable: true,
    });

    router.push('/', undefined, { shallow: true });
  }

  return <>{children}</>;
};

export default PublisherGuard;
