import React, { useEffect, useState } from 'react';

import { useToast } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';

import { StoresState } from 'common/stores';

const UserGuard: React.FC = (props) => {
  const toast = useToast();
  const { children } = props;
  const router = useRouter();
  const [valid, setValid] = useState<boolean>(true);
  const auth = useSelector((state: StoresState) => state.user.authenticated);

  useEffect(() => {
    const user = localStorage.getItem('user');
    auth || user ? setValid(true) : setValid(false);
  }, []);

  if (!valid) {
    toast({
      title: 'Error',
      description: "You don't have permission to view this content",
      status: 'error',
      duration: 5000,
      isClosable: true,
    });
    router.push('/', undefined, { shallow: true });
  }

  return <>{children}</>;
};

export default UserGuard;
