import React from 'react';

import { Meta } from '@storybook/react';

import Container from '.';

const meta: Meta = {
  title: 'common/components/Container',
  component: Container,
};

export default meta;

export const Default: React.FC = () => <Container>Container</Container>;
