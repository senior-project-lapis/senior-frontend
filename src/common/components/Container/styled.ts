import styled from '@emotion/styled';

export const ContainerStyled = styled.div`
  display: flex;
  width: 100%;
  max-width: 1024px;
  margin: 0 auto;
  flex-wrap: wrap;
`;
