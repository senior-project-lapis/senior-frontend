export interface ContainerProps {
  children: React.ReactNode;
  variant: 'Default' | 'Primary';
}
