/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';

import { Text, Flex } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { BsBuilding } from 'react-icons/bs';
import { GrMapLocation } from 'react-icons/gr';

import ReportBtn from 'common/components/ReportBtn';
interface Props {
  job?: any;
  index?: any;
  setSelectedJob?: (value: any) => void;
  isUseSelected?: boolean;
}
const ResultBox: React.FC<Props> = (Props) => {
  const { index, job, setSelectedJob, isUseSelected = false } = Props;
  const router = useRouter();
  const handleSelect = (e: any) => {
    e.preventDefault();
    if (setSelectedJob) {
      setSelectedJob(job);
    }
  };

  const handleClick = (e: any) => {
    e.preventDefault();
    router.push(`/job/${job._id}`, undefined, { shallow: true });
  };

  useEffect(() => {
    if (index === 0 && isUseSelected) {
      if (setSelectedJob) {
        setSelectedJob(job);
      }
    }
  }, [job, index]);

  return (
    <>
      {isUseSelected ? (
        <Flex
          flexDirection="column"
          justifyContent="space-around"
          key={index}
          cursor="pointer"
          onClick={(e) => {
            handleSelect(e);
          }}
          px="10px"
          p="10px"
          borderRadius="5px"
          bg="white"
          minH="110px"
          overflow="hidden"
          boxShadow="rgba(149, 157, 165, 0.2) 0px 8px 24px">
          <Flex justifyContent="space-between">
            <Text fontFamily="medium" fontSize="18px">
              {job.name}
            </Text>
            <ReportBtn jobId={job._id} />
          </Flex>
          <Flex mt="2" justifyContent="flex-start">
            <Text fontSize="16px" display="flex">
              <BsBuilding /> &nbsp;
              {job.companyName}
            </Text>
            <Text fontSize="16px" display="flex">
              &nbsp;&nbsp;&nbsp;
              <GrMapLocation />
              &nbsp; {job.locations[0]}
            </Text>
          </Flex>
        </Flex>
      ) : (
        <Flex
          flexDirection="column"
          justifyContent="space-around"
          key={index}
          cursor="pointer"
          onClick={(e) => {
            handleClick(e);
          }}
          p="10px"
          w="400px"
          borderRadius="5px"
          bg="white"
          overflow="hidden"
          boxShadow="rgba(0, 0, 0, 0.02) 0px 1px 3px 0px, rgba(27, 31, 35, 0.15) 0px 0px 0px 1px">
          <Flex justifyContent="space-between">
            <Text fontFamily="medium" fontSize="18px">
              {job.name}
            </Text>
          </Flex>
          <Flex mt="2" justifyContent="flex-start">
            <Text fontSize="16px" display="flex">
              <GrMapLocation />
              &nbsp; {job.locations[0]}
            </Text>
          </Flex>
        </Flex>
      )}
    </>
  );
};

export default ResultBox;
