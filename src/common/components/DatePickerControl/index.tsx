/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { FC } from 'react';

import { ButtonProps } from '@chakra-ui/button';
import { BackgroundProps, InputProps } from '@chakra-ui/react';
import { SingleDatepicker } from 'chakra-dayzed-datepicker';
import { useField, useFormikContext } from 'formik';
import { FormControl, BaseProps } from 'formik-chakra-ui';
export interface DayOfMonthBtnStyleProps extends ButtonProps {
  selectedBg?: BackgroundProps['bg'];
  disabledBg?: BackgroundProps['bg'];
}

export interface propsConfigs {
  dateNavBtnProps?: ButtonProps;
  dayOfMonthBtnProps?: DayOfMonthBtnStyleProps;
  inputProps?: InputProps;
}
export interface DatePickProp {
  name: string;
  label?: string;
  propsConfigs?: propsConfigs;
}
export type DatePickerControlProps = BaseProps & DatePickProp;

export const DatePickerControl: FC<DatePickerControlProps> = (
  props: DatePickerControlProps,
) => {
  const { setFieldValue } = useFormikContext();
  const { name, label, ...rest } = props;
  const [field] = useField(name);
  return (
    <FormControl name={name} label={label} {...rest}>
      <SingleDatepicker
        {...field}
        {...props}
        id={name}
        date={(field.value && new Date(field.value)) || null}
        onDateChange={(val) => {
          setFieldValue(field.name, val);
        }}
      />
    </FormControl>
  );
};

export default DatePickerControl;
