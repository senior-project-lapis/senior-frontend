import { ReactNode } from 'react';

import { MenuType } from 'common/constants/menu/types';

export type SidebarLayoutProps = {
  children: ReactNode;
  title?: string;
  navColor?: string;
  sidebarColor?: string;
  btnColor?: string;
  constant: MenuType[];
};

export type SidebarNavProps = {
  navColor?: string;
  title?: string;
};

export type SidebarProps = {
  btnColor?: string;
  sidebarColor?: string;
  constant: MenuType[];
};
