import { Flex, Heading, VStack, Box, Button, Link, Center } from '@chakra-ui/react';
import NextLink from 'next/link';
import { useRouter } from 'next/router';

import Container from 'common/components/Container';
import pathJoin from 'common/utils/path-join';

import Navbar from 'modules/root/components/Navbar';

import { SidebarLayoutProps, SidebarNavProps, SidebarProps } from './types';

const SidebarNav: React.FC<SidebarNavProps> = (props) => {
  const { navColor = 'blue.100', title } = props;

  return (
    <>
      <Flex bg={navColor} py="60px" h="170px" width="full">
        <Container>
          <Heading color="white">{title}</Heading>
        </Container>
      </Flex>
    </>
  );
};

const Sidebar: React.FC<SidebarProps> = (props) => {
  const { sidebarColor = 'blue.100', btnColor = 'orange.100', constant } = props;
  const router = useRouter();
  return (
    <>
      <Flex
        bg={sidebarColor}
        overflow="hidden"
        maxWidth="290px"
        maxHeight="700px"
        h="80%"
        w="100%"
        borderRadius="5px"
        flexDirection="column">
        <VStack py="35%" w="100%" spacing={6} align="stretch" pl="30px">
          {constant.map(({ key, label, link }) => {
            return (
              <NextLink key={key} href={pathJoin(link)} passHref>
                <Button
                  pr="60px"
                  fontSize="20px"
                  as={Link}
                  fontFamily="medium"
                  color="white"
                  borderRightRadius="none"
                  maxH="50px"
                  size="lg"
                  bg={router.pathname === pathJoin(link) ? btnColor : 'none'}
                  _active={{ bg: 'blackAlpha.400', boxShadow: 'none', border: 'none' }}
                  _hover={{ bg: 'blackAlpha.400', boxShadow: 'none' }}
                  _focus={{
                    bg: 'blackAlpha.400',
                    outline: 'none',
                    boxShadow: 'none',
                    border: 'none',
                  }}>
                  {label}
                </Button>
              </NextLink>
            );
          })}
        </VStack>
        <Center mt="70px">
          <NextLink href="/logout" passHref>
            <Button
              as={Link}
              fontSize="18px"
              fontFamily="medium"
              bg="none"
              _active={{ bg: 'blackAlpha.400', boxShadow: 'none', border: 'none' }}
              _hover={{ bg: 'none', boxShadow: 'none' }}
              _focus={{
                bg: 'blackAlpha.400',
                outline: 'none',
                boxShadow: 'none',
                border: 'none',
              }}>
              Logout
            </Button>
          </NextLink>
        </Center>
      </Flex>
    </>
  );
};

const SidebarLayout: React.FC<SidebarLayoutProps> = (props) => {
  const { children, title, navColor, sidebarColor, btnColor, constant } = props;
  const router = useRouter();
  const isUserPage = router.pathname.includes('/user/');

  return (
    <>
      {isUserPage ? (
        <>
          <Navbar />
          <Box py="50px" mt="80px">
            <Container>
              <Flex w="30%" minH="100vh">
                <Sidebar
                  sidebarColor={sidebarColor}
                  btnColor={btnColor}
                  constant={constant}
                />
              </Flex>
              <Flex w="70%" py="20px">
                {children}
              </Flex>
            </Container>
          </Box>
        </>
      ) : (
        <>
          <SidebarNav navColor={navColor} title={title} />
          <Box py="50px">
            <Container>
              <Flex w="30%" minH="100vh">
                <Sidebar
                  sidebarColor={sidebarColor}
                  btnColor={btnColor}
                  constant={constant}
                />
              </Flex>
              <Flex w="70%" py="20px">
                {children}
              </Flex>
            </Container>
          </Box>
        </>
      )}
    </>
  );
};
export default SidebarLayout;
