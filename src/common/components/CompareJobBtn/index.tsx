/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useEffect, useMemo } from 'react';

import {
  Button,
  useDisclosure,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  Box,
  Flex,
} from '@chakra-ui/react';
import { BsTrash } from 'react-icons/bs';
import { useDispatch, useSelector } from 'react-redux';

import FloatingBtn from 'common/components/FloatingBtn';
import TableComponent from 'common/components/Table/TableComponent';
import { JobWithId } from 'common/generated/generated-types';
import { StoresState } from 'common/stores';

import { jobActions } from 'modules/job/reducers/jobReducer';
interface Props {
  data?: any;
  title?: string;
  titleWhenRm?: string;
  btnPadding?: string;
}
interface CompareProps {
  isOpen: boolean;
  onClose: () => void;
  jobs: JobWithId[];
}

const DisplayCompare: React.FC<CompareProps> = (CompareProps) => {
  const { isOpen, onClose, jobs } = CompareProps;
  const data = useMemo(() => jobs, [jobs]);
  const dispatch = useDispatch();
  const removeCompare = (cell: any) => {
    const job = cell?.row?.original;
    dispatch(
      jobActions.removeFromCompare({
        newItem: job,
      }),
    );
  };

  useEffect(() => {
    if (data.length === 0) {
      onClose();
    }
  }, [data]);

  const columns = useMemo(
    () => [
      {
        Header: 'Job Name',
        accessor: 'name',
      },
      {
        Header: 'Compay Name',
        accessor: 'companyName',
      },
      {
        Header: 'Locations',
        accessor: 'locations',
      },
      {
        Header: 'Job Type',
        accessor: 'jobType',
      },
      {
        Header: 'Experience Level',
        accessor: 'experienceLevels',
      },
      {
        Header: '',
        accessor: 'action',
        Cell: (props: any) => (
          <Flex justifyContent="center">
            <Button onClick={() => removeCompare(props)} variant="red">
              <BsTrash />
            </Button>
          </Flex>
        ),
      },
    ],
    [],
  );

  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered>
      <ModalOverlay />
      <ModalContent bgColor="white" color="black" p="5px" maxW="1700px">
        <ModalHeader>Compare</ModalHeader>
        <ModalCloseButton />
        <ModalBody p="20px">
          <Box
            borderRadius="5px"
            boxShadow=" rgba(0, 0, 0, 0.02) 0px 1px 3px 0px, rgba(27, 31, 35, 0.15) 0px 0px 0px 1px">
            <TableComponent columns={columns} data={data} />
          </Box>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

const CompareJobBtn: React.FC<Props> = (Props) => {
  const { data, title = 'Compare', titleWhenRm = 'Remove Compare', btnPadding } = Props;
  const [isCompare, setIsCompare] = useState(false);
  const jobCompare = useSelector((state: StoresState) => state.job.jobsInCompare);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const dispatch = useDispatch();

  const handleCompare = () => {
    setIsCompare((isCompare) => {
      if (isCompare === true) {
        dispatch(
          jobActions.removeFromCompare({
            newItem: data,
          }),
        );
        console.log(data);
      }
      if (isCompare === false) {
        dispatch(
          jobActions.addToCompare({
            newItem: data,
          }),
        );
      }
      return !isCompare;
    });
  };

  useEffect(() => {
    checkCompare();
    //NEED TO FIND NEW LOGIC!
    // if (jobCompare && jobCompare.length >= 2 && isCompare === true) {
    //   onOpen();
    // }
  }, [isCompare, data, isOpen]);

  const checkCompare = () => {
    if (jobCompare) {
      return jobCompare.find((compare) => compare._id === data._id)
        ? setIsCompare(true)
        : setIsCompare(false);
    } else {
      return setIsCompare(false);
    }
  };

  return (
    <>
      {isCompare ? (
        <Button variant="blue" mr="1" p={btnPadding} onClick={handleCompare}>
          {titleWhenRm}
        </Button>
      ) : (
        <Button variant="blue" mr="1" p={btnPadding} onClick={handleCompare}>
          {title}
        </Button>
      )}
      {jobCompare.length != 0 && <FloatingBtn onOpen={onOpen} />}
      <DisplayCompare jobs={jobCompare} isOpen={isOpen} onClose={onClose} />
    </>
  );
};

export default CompareJobBtn;
