/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import { TriangleDownIcon, TriangleUpIcon } from '@chakra-ui/icons';
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  chakra,
  Stack,
  Flex,
  Text,
  Select,
} from '@chakra-ui/react';
import {
  FiChevronLeft,
  FiChevronRight,
  FiChevronsLeft,
  FiChevronsRight,
} from 'react-icons/fi';
import { useTable, useSortBy, usePagination } from 'react-table';

import TableIconButton from 'common/components/Table/TableIconButton';

interface Props {
  columns: any;
  data: any;
  fetchData?: any;
  loading?: boolean;
  totalCount?: number;
  isPagination?: boolean;
  initialPageIndex?: number;
  initialPageSize?: number;
}

const TableComponent: React.FC<Props> = (Props) => {
  const {
    columns,
    data,
    fetchData,
    loading,
    totalCount,
    initialPageIndex = 0,
    initialPageSize = 10,
    isPagination = false,
  } = Props;
  const controlledPageCount = totalCount ? Math.ceil(totalCount / initialPageSize) : 1;
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    page,
    prepareRow,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: initialPageIndex, pageSize: initialPageSize },
      manualPagination: true,
      pageCount: controlledPageCount,
    },
    useSortBy,
    usePagination,
  );

  React.useEffect(() => {
    fetchData && fetchData({ pageIndex, pageSize });
  }, [fetchData, pageIndex, pageSize]);

  return (
    <>
      <Table {...getTableProps()}>
        <Thead>
          {headerGroups.map((headerGroup, i) => (
            <Tr {...headerGroup.getHeaderGroupProps()} key={i}>
              {headerGroup.headers.map((column, i) => (
                <Th
                  key={i}
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                  fontFamily="heading"
                  fontSize="14px"
                  textAlign="center">
                  {column.render('Header')}
                  <chakra.span pl="4">
                    {column.isSorted ? (
                      column.isSortedDesc ? (
                        <TriangleDownIcon aria-label="sorted descending" />
                      ) : (
                        <TriangleUpIcon aria-label="sorted ascending" />
                      )
                    ) : null}
                  </chakra.span>
                </Th>
              ))}
            </Tr>
          ))}
        </Thead>
        <Tbody {...getTableBodyProps()} textAlign="center">
          {(page.length > 0 &&
            rows.map((row, i) => {
              prepareRow(row);
              return (
                <Tr {...row.getRowProps()} key={i}>
                  {row.cells.map((cell, i) => (
                    <Td {...cell.getCellProps()} key={i} fontSize="14px">
                      {cell.render('Cell')}
                    </Td>
                  ))}
                </Tr>
              );
            })) || (
            <Flex>
              <Text fontSize="16px" fontFamily="medium" textAlign="center">
                There is currently no data ;)
              </Text>
            </Flex>
          )}
        </Tbody>
      </Table>
      {isPagination && (
        <Flex
          borderTopWidth="1px"
          overflowX="hidden"
          overflowY="hidden"
          px="4"
          py="2"
          roundedBottomLeft="4"
          roundedBottomRight="4"
          flexDir="row">
          <Stack isInline spacing={2}>
            <TableIconButton
              onClick={() => gotoPage(0)}
              isDisabled={!canPreviousPage}
              icon={<FiChevronsLeft size={20} />}
            />
            <TableIconButton
              isDisabled={!canPreviousPage}
              onClick={() => previousPage()}
              icon={<FiChevronLeft size={20} />}
            />
          </Stack>
          <Stack isInline flexWrap="nowrap" justify="center" align="center" px="4">
            <Text whiteSpace="nowrap" fontSize="xs">
              Page {pageIndex + 1} of {pageOptions.length}
            </Text>
            <Select
              size="sm"
              value={pageSize}
              onChange={(e) => {
                setPageSize(Number(e.target.value));
              }}
              isDisabled={loading}>
              {[5, 10, 20, 30, 40, 50].map((pageSize) => (
                <option key={pageSize} value={pageSize}>
                  {pageSize}
                </option>
              ))}
            </Select>
          </Stack>
          <Stack isInline spacing={2}>
            <TableIconButton
              isDisabled={!canNextPage}
              onClick={() => nextPage()}
              icon={<FiChevronRight size={20} />}
            />
            <TableIconButton
              onClick={() => gotoPage(pageCount ? pageCount - 1 : 1)}
              isDisabled={!canNextPage}
              icon={<FiChevronsRight size={20} />}
            />
          </Stack>
        </Flex>
      )}
    </>
  );
};

export default TableComponent;
