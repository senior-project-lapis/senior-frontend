/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from 'react';

import { IconButton } from '@chakra-ui/react';

type TableIconButtonProps = {
  icon: any;
  onClick: ((event: React.MouseEvent<HTMLElement, MouseEvent>) => void) | undefined;
  isDisabled: boolean;
  variantColor?: string;
};

const TableIconButton: React.FC<TableIconButtonProps> = ({
  icon,
  onClick,
  isDisabled,
  children,
  ...rest
}) => {
  return (
    <IconButton
      color="black"
      size="sm"
      {...rest}
      icon={icon}
      borderWidth={1}
      onClick={onClick}
      isDisabled={isDisabled}
      aria-label="Table Icon button">
      {children}
    </IconButton>
  );
};

export default TableIconButton;
