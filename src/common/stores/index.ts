import { combineReducers, configureStore } from '@reduxjs/toolkit';

import publisherReducer from 'modules/auth/reducers/publisherReducer';
import userReducer from 'modules/auth/reducers/userReducer';
import companyReducer from 'modules/company/reducers/companyReducer';
import jobReducer from 'modules/job/reducers/jobReducer';
import instantSearchReducer from 'modules/search/reducers/instantSearchReducer';
const rootReducer = combineReducers({
  user: userReducer,
  publisher: publisherReducer,
  instantSearch: instantSearchReducer,
  job: jobReducer,
  company: companyReducer,
});

export type StoresState = ReturnType<typeof rootReducer>;

const store = configureStore({
  reducer: rootReducer,
});

export { store, rootReducer };

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
