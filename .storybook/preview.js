const theme = require('../src/modules/root/utils/theme');

export const parameters = {
  chakra: {
    theme,
  },
};
