/** @type {import('next').NextConfig} */
module.exports = {
  distDir: 'build',
  outDir: 'out',
  generateBuildId: async () => {
    // Return custom build ID, like the latest git commit hash
    return 'my-build-id';
  },
  async redirects() {
    return [
      {
        source: '/user',
        destination: '/user/profile',
        permanent: true,
      },
      {
        source: '/admin',
        destination: '/admin/dashboard',
        permanent: true,
      },
      {
        source: '/publisher',
        destination: '/publisher/add-job',
        permanent: true,
      },
    ];
  },
};
